<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');
include($adp_root_path . 'ClassMemberPromoTukarPoint.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$Member			= new Member();

$body="";

switch ($mode){
	case 'blank':
		
		if($HTTP_GET_VARS['error']==1){
			$pesan	= "<font color='red'>KARTU TIDAK TERDAFTAR</font>";
		}
		else if($HTTP_GET_VARS['error']==2){
			$pesan	= "<font color='red'>STATUS MEMBER TIDAK AKTIF</font>";
		}		
		
		$body=
			"<table width='80%'>
				<tr>
					<td colspan=3 align='center'>$pesan</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>Silahkan gesekkan kartu member ke alat pembaca</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action='".append_sid('member_tukar_point.'.$phpEx)."&mode=input_nominal&submode=0"."' method='post'>
							<input type='password' id='id_kartu' name='id_kartu' />
						</form>
					</td>
				</tr>
				<tr>
					<td colspan='3' align='center'> ATAU <BR> Silahkan masukkan nomor seri kartu member lalu kemudian tekan enter pada kolom isian dibawah ini</td>
				</tr>
				<tr>
					<td colspan='3' align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action=\"".append_sid('member_tukar_point.'.$phpEx)."&mode=input_nominal&submode=1"."' method='post'>
							<input type='text' id='no_seri_kartu' name='no_seri_kartu' />
						</form>
					</td>
				</tr>
			</table>";
		
	break;
	
	//MEMINTA NOMINAL UANG =============================================================================================
	case 'input_nominal':	
		$sub_mode		= $HTTP_GET_VARS['submode'];		
		$id_kartu		= $HTTP_POST_VARS['id_kartu'];		
		$no_seri_kartu	= $HTTP_POST_VARS['no_seri_kartu'];		
		$id_kartu_md5	= md5(trim($id_kartu));
		
		//MENGAMBIL DATA MEMBER BY ID KARTU atau by NO SERI KARTU
		if($sub_mode==0){
			//jika pencarian data dilakukan berdasarkan ID KARTU
			$data_member	= $Member->ambilDataByIdKartu($id_kartu_md5);
		}
		else{
			//jika pencarian data dilakukan berdasarkan NO SERI KARTU
			$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
			$id_kartu=$data_member['id_kartu'];
			$id_kartu_md5	= trim($id_kartu);
		}
		
		//membuat combo tukar point
		$PromoTukarPoin	= new PromoPoin();
		
		$data_promo_point	= $PromoTukarPoin->ambilData('nama_penukaran_poin,kode_promo','asc');
		
		if ($data_promo_point){
			while ($row = $db->sql_fetchrow($data_promo_point)){
				if($row['status_aktif']==1){
					$option_tukar_poin	.="
						<option value='$row[id_promo_poin]'>$row[nama_penukaran_poin] [$row[poin] poin] ($row[kode_promo])</option>";
				}
				else{
					$option_tukar_poin	.="";
				}
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE - 01");
		}
		
		$combo_tukar_poin="
			<select id='opt_tukar_point' name='opt_tukar_point'>$option_tukar_poin</select>";
		
		if($data_member['status_member']){
			$pesan	=
				"<table width='100%'>
					<tr>
						<td colspan=3 align='center'>Silahkan pilih jenis promo</td>
					</tr>
					<tr>
						<td colspan=3 align='center'>
							<form id='frm_input_kartu' name='frm_input_kartu'>
								<input type='hidden' id='id_kartu' name='id_kartu' value='$id_kartu_md5'/> 
								$combo_tukar_poin
								<input type='button' onClick='showKonfirmasi();' value='&nbsp;&nbsp;OK&nbsp;&nbsp;' />
							</form>
						</td>
					</tr>
				</table>";
		}
		else{
			if($data_member['id_member']==''){
				//JIKA ID KARTU TIDAK TERDAFTAR
				$error	=1;
			}
			else{
				//JIKA STATUS MEMBER TIDAK AKTIF
				$error	=2;
			}
			redirect(append_sid('member_tukar_point.'.$phpEx)."&error=".$error,true); 
		}
						
		$body 	= 
			"<table width='80%'>
				<tr>
					<td width='20%'>ID MEMBER</td><td width='5%'>:</td><td width='75%'>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
				</tr>
				<tr>
					<td>Deposit</td><td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
				</tr>
				<tr>
					<td>Point</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				<tr>
					<td colspan=3 align='center'>
						<h2>$pesan</h2>
					</td>
				</tr>
			</table>";
		
	break;
	
	//PROSES TUKAR =============================================================================================
	case 'tukar':	
		$id_kartu				= $HTTP_GET_VARS['id_kartu'];		
		$id_promo_poin	= $HTTP_GET_VARS['id_promo_poin'];		
		
		//MENGAMBIL DATA MEMBER BY ID KARTU
		$data_member	= $Member->ambilDataByIdKartu($id_kartu);
		
		//melakukan top up
		$TransaksiMember = new TransaksiMember();
		$return = $TransaksiMember->mutasiTukarPoint($data_member['id_member'],$id_promo_poin);
		redirect(append_sid('member_tukar_point.'.$phpEx)."&mode=hasil&id_member=$data_member[id_member]&return=$return",true); 
	
	break;
	
	//HASIL==================================================================================================
	case 'hasil':		
		$id_member		= $HTTP_GET_VARS['id_member'];			
		$return				= $HTTP_GET_VARS['return'];			
		
		//MENGAMBIL DATA MEMBER BY ID KARTU
		$data_member	= $Member->ambilDataDetail($id_member);
		
		if($return=='false'){
			$keterangan="<font color='red'>TUKAR POIN GAGAL! $id_member</font>";
			
		}
		elseif($return=='poin kurang'){
			$keterangan="<font color='red'>POINT ANDA TIDAK MENCUKUPI!</font>";
		}
		elseif($return=='true'){
			$keterangan="<font color='green'>TUKAR POIN BERHASIL!</font>";
		}
		else{
			$keterangan="<font color='red'>Terjadi kesalahan dalam sistem</font>";
		}
		
		$body 	= 
			"<input type='hidden' id='id_topup' value='$id_topup'/>
			<table width='80%'>
				<tr>
					<td width='20%'>ID MEMBER</td><td width='5%'>:</td><td width='75%'>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
				</tr>
				<tr>
					<td>Deposit</td><td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
				</tr>
				<tr>
					<td>Point</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				<tr>
					<td colspan=3 align='center'>
						<h2>$keterangan</h2>
					</td>
				</tr>
			</table>";
	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_tukar_point.tpl')); 
$template->assign_vars (
	array(
		'USERNAME'  =>$userdata['username'],
	  'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_tukar_point.'.$phpEx).'">Tukar point</a>',
		'ACTION'		=>append_sid('member_tukar_point.'.$phpEx)."&mode=tukar",
		'BODY'			=> $body
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>