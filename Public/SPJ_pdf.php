<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$sopir_sekarang		= $HTTP_GET_VARS['sopir_sekarang'];
$mobil_sekarang		= $HTTP_GET_VARS['mobil_sekarang'];
$aksi							= $HTTP_GET_VARS['aksi'];

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	$Sopir = new Sopir();
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	$Mobil = new Mobil();
			
	$result=$Mobil->ambilDataForComboBox();
	$opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";
	
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}

if($aksi==0){
	$Jadwal						= new Jadwal();
	$BiayaOperasional	= new BiayaOperasional();
	
	//load data
	$str_option_sopir=setComboSopir($sopir_sekarang);
	$str_option_mobil=setComboMobil($mobil_sekarang);
	
	#mengambil data jadwal
	$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
	if($data_jadwal['FlagSubJadwal']!=1){
		$kode_jadwal_utama	= $kode_jadwal;
	}
	else{
		$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
	}
	
	$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
	
	echo("
		<table bgcolor='white' width='100%'>
		  <tr>
				<td colspan=2>Silahkan pilih mobil</td>
				<td  width='1'>:</td>
				<td>
					<select name='list_mobil' id='list_mobil'>$str_option_mobil</select>
					<span id='list_mobil_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
		  <tr>
				<td colspan=2>Silahkan pilih Sopir</td>
				<td>:</td>
				<td>
					<select name='list_sopir' id='list_sopir'>$str_option_sopir</select>
					<span id='list_sopir_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
			<tr><td colspan=3><br><h2>Biaya-biaya</h2></td></tr>
			<tr>
				<td colspan=2>Biaya Sopir</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaSopir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya Tol</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaTol'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya Parkir</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaParkir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td colspan=2>Biaya BBM</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaBBM'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr><td colspan=4 height=1 bgcolor='red'></td></tr>
			<tr>
				<td colspan=2>Total Biaya</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($data_biaya['BiayaSopir']+$data_biaya['BiayaTol']+$data_biaya['BiayaBBM']+$data_biaya['BiayaParkir'],0,",",".")."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
	  </table>");  
	exit;		
}
else 
if($aksi==1){
	//tombol OK di klik untuk mencetak SPJ
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Sopir			= new Sopir();
	$Mobil			= new Mobil();
	$BiayaOperasional	= new BiayaOperasional();
	
	$sopir_dipilih= $HTTP_GET_VARS['sopir_dipilih'];
	$mobil_dipilih= $HTTP_GET_VARS['mobil_dipilih'];
	$no_spj				= $HTTP_GET_VARS['no_spj'];
	
	$nourut = rand(1000,9999);
	$useraktif=$userdata['user_id'];
	
	if ($tgl_berangkat!='' && $kode_jadwal!=''){
		
		#mengambil data jadwal
		$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
		if($data_jadwal['FlagSubJadwal']!=1){
			$kode_jadwal_utama			= $kode_jadwal;
		}
		else{
			$kode_jadwal_utama			= $data_jadwal['KodeJadwalUtama'];
		}
		
		$jam_berangkat_show	= $data_jadwal['JamBerangkat'];
		$list_kode_jadwal		= "'$kode_jadwal'";
		
		//mengambil layout kursi
		
	  $layout_kursi = $Reservasi->ambilLayoutKursiByKodeJadwal($kode_jadwal_utama);
		
		$row	= $Sopir->ambilDataDetail($sopir_dipilih);
	  $nama_sopir = $row['Nama'];
	  
		$row				= $Mobil->ambilDataDetail($mobil_dipilih);
	  $no_polisi 	= $row['NoPolisi'];
	  
		
		//mendirect pencetakan spj sesuai dengan layout kursi
		
		//mengambil data tiket
		
		$row	= $Reservasi->ambilDataPosisiUntukSPJ($tgl_berangkat,$kode_jadwal_utama);
		
		$tgl_berangkat				= $row['TglBerangkat'];
		$jam_berangkat				= $row['JamBerangkat'];
		$no_polisi						= ($row['NoPolisi']=="")?$no_polisi:$row['NoPolisi'];
		
		$list_field_diupdate="";
		
		/*for ($idx=1;$idx<=$row['JumlahKursi'];$idx++){
			
			//mengisi array nama
			$field_bayar="bayar".$idx;
			
			if($row[$field_bayar]==1){
				$field_nama="Nama$idx";
				$nama[$idx]=substr($row[$field_nama],0,20);
			}
			else{
				$nama[$idx]="------------------------";
			}
			//MEMERIKSA TIKET-TIKET YANG BELUM DICETAK
			//$idx_bayar='bayar'.$idx;
				
			/*if($row[$idx_bayar]!=1){
				//menghapus tiket yang belum di konfirmasi
					
				$idx_no_tiket="T".$idx;
				$no_tiket=$row[$idx_no_tiket];
				$list_no_tiket_dihapus .="'$no_tiket',";
					
				//membuat nama menjadi blank
				$nama[$idx]="-";
					
				//update posisi yang belum dikonfirmasi
				$list_field_diupdate .="K$idx = 0,T$idx = '',Nama$idx='',sess$idx='',bayar$idx=0,";
					
			}*/
		#}
		
		//Mengambil jumlah penumpang dan omzet
		$data_total	=$Reservasi->hitungTotalOmzetdanJumlahPenumpangPerSPJ($tgl_berangkat,$list_kode_jadwal);
						
		$total_omzet			=($data_total['TotalOmzet']!='')?$data_total['TotalOmzet']:0;
		$jumlah_penumpang	=($data_total['JumlahPenumpang']!='')?$data_total['JumlahPenumpang']:0;	
		
		//Mengambil jumlah paket dan omzet
		$data_total_paket	=$Reservasi->hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$list_kode_jadwal);
		
		$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
		$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;	
		
		
		//Mengambil pembiayaan berdasarkan jurusan
		$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
		
		//mengupdate field utk SPJ di tbl posisi
		
		if($row['NoSPJ']==""){
			$no_spj= "MNF".substr($kode_jadwal_utama,0,3).dateYMD().$nourut;
			
			$Reservasi->tambahSPJ(
				$no_spj, $kode_jadwal_utama, $tgl_berangkat, 
				$jam_berangkat, $layout_kursi, $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket);
			
			//jika spj belum pernah dicetak, maka akan menambahkan biaya ke database
			
			//biaya sopir
			if($data_biaya['BiayaSopir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaSopir'],$FLAG_BIAYA_SOPIR,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaSopir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya tol
			if($data_biaya['BiayaTol']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaTol'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya parkir
			if($data_biaya['BiayaParkir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaParkir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya bbm
			if($data_biaya['BiayaBBM']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaBBM'],$FLAG_BIAYA_BBM,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaBBM'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			$duplikat	= ""; 
			
		}
		else{
			$Reservasi->ubahSPJ(
				$row['NoSPJ'], $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket);
				
			
			$no_spj	= $row['NoSPJ'];
			
			$duplikat	= " DUPLIKAT ";
		}
			
		//update data pada tbl posisi
		$Reservasi->ubahPosisiCetakSPJ(
			$kode_jadwal_utama, $tgl_berangkat,$list_field_diupdate, 
			$sopir_dipilih,$mobil_dipilih,$no_spj,$useraktif);
			
		//update tblReservasi
		$Reservasi->ubahDataReservasiCetakSPJ(
			$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
			$mobil_dipilih,$no_spj);
			
		//update tblspj untuk insentif sopir
		$Reservasi->updateInsentifSopir(
			$no_spj,$layout_kursi, $INSENTIF_SOPIR_LAYOUT_MAKSIMUM, 
			$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM, $data_biaya['KomisiPenumpangSopir']);
			
		//menghapus dari tabel reservasi utk pesanan yang belum bayar
		/*if($list_no_tiket_dihapus!=""){
			$list_no_tiket_dihapus=substr($list_no_tiket_dihapus,0,strlen($list_no_tiket_dihapus)-1);
			$sql .= "DELETE FROM TbReservasi WHERE NoUrut IN($list_no_tiket_dihapus);";
		}*/
			
		//menghapus waiting list
		/*$sql .= 
			"DELETE FROM tbl_waiting_list
			WHERE 
				(CONVERT(CHAR(20), tgl_berangkat, 105)) = '$tgl_berangkat_old' 
				AND kode_rute='$kode_jadwal';";*/
		
		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
		$line_space	=0.3;
		//EXPORT KE PDF
		class PDF extends FPDF {
			function Footer() {
				$this->SetY(-1.5);
		    $this->SetFont('Arial','I',8);
		    $this->Cell(0,1,'',0,0,'R');
			}
			
			var $javascript;
	    var $n_js;

	    function IncludeJS($script) {
	        $this->javascript=$script;
	    }

	    function _putjavascript() {
	        $this->_newobj();
	        $this->n_js=$this->n;
	        $this->_out('<<');
	        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
	        $this->_out('>>');
	        $this->_out('endobj');
	        $this->_newobj();
	        $this->_out('<<');
	        $this->_out('/S /JavaScript');
	        $this->_out('/JS '.$this->_textstring($this->javascript));
	        $this->_out('>>');
	        $this->_out('endobj');
	    }

	    function _putresources() {
	        parent::_putresources();
	        if (!empty($this->javascript)) {
	            $this->_putjavascript();
	        }
	    }

	    function _putcatalog() {
	        parent::_putcatalog();
	        if (isset($this->javascript)) {
	            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
	        }
	    }
			
			function AutoPrint($dialog=false)
			{
			    //Embed some JavaScript to show the print dialog or start printing immediately
			    $param=($dialog ? 'true' : 'false');
			    $script="print($param);";
			    $this->IncludeJS($script);
			}
		}
			
		//set kertas & file
		$pdf=new PDF('P','cm','spjkecil');
		$pdf->Open();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->Setmargins(1,0,0,0);
		$pdf->SetFont('courier','',6);
				
		// Header
		$pdf->Ln();
		$pdf->Ln();
		//$pdf->Cell(6.4,$line_space,$list_kode_jadwal,'',0,'C');$pdf->Ln(); //debug
		$pdf->Image('templates/images/signnaturespj.jpg',2.5,1,2);
		//HEADER 
		$pdf->SetFont('courier','',13);
		$pdf->Cell(6.4,$line_space,"-- [ MANIFEST] --",'',0,'C');$pdf->Ln();
		
		if($duplikat!=""){
			$pdf->SetFont('courier','',9);
			$pdf->Cell(6.4,$line_space,"->DUPLIKAT<-",'',0,'C');$pdf->Ln();
		}
		
		$pdf->SetFont('courier','',11);
		$pdf->Cell(6.4,$line_space,$data_perusahaan['NamaPerusahaan'],'',0,'');$pdf->Ln();
		$pdf->SetFont('courier','',10);
		$pdf->Cell(6.4,$line_space,$data_perusahaan['AlamatPerusahaan'],'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,$data_perusahaan['TelpPerusahaan'],'',0,'');$pdf->Ln();
		
		$pdf->SetFont('courier','',10);
		//content
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,$no_spj,'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,dateparse(FormatMySQLDateToTgl($tgl_berangkat)),'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,$kode_jadwal." ".$jam_berangkat_show,'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Tgl. Cetak:",'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,FormatMySQLDateToTglWithTime(dateNow(true)),'',0,'C');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Mobil:".$mobil_dipilih." (".$no_polisi.")",'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Sopir:".$nama_sopir." (".$sopir_dipilih.")",'',0,'');$pdf->Ln();
		
		//list paket
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,'DAFTAR PAKET',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		
		if($jumlah_paket>0){
			$result_paket = $Reservasi->ambilDataPaketUntukSPJ($tgl_berangkat,$list_kode_jadwal);
			$idx_no=0;
			while($row_paket=$db->sql_fetchrow($result_paket)){
				$idx_no++;
				$pdf->Cell(6.4,$line_space,"(".$idx_no.") Resi:".$row_paket['NoTiket'],'',0,'');$pdf->Ln();
				$pdf->Cell(6.4,$line_space,"Tujuan:".$row_paket['Tujuan'],'',0,'');$pdf->Ln();
				$pdf->Cell(6.4,$line_space,"Dr:".$row_paket['NamaPengirim'],'',0,'');$pdf->Ln();
				$pdf->Cell(6.4,$line_space,"   ".$row_paket['TelpPengirim'],'',0,'');$pdf->Ln();
				$pdf->Ln();
				$pdf->Cell(6.4,$line_space,"Ke:".$row_paket['NamaPenerima'],'',0,'');$pdf->Ln();
				$pdf->Cell(6.4,$line_space,"   ".$row_paket['TelpPenerima'],'',0,'');$pdf->Ln();
				$pdf->Cell(6.4,$line_space,'--------',0,0,'');$pdf->Ln();
			}
		}
		else{
			$pdf->Cell(6.4,$line_space,'TIDAK ADA PAKET',0,0,'');$pdf->Ln();
		}
		
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Jumlah Paket :".$jumlah_paket,'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Omzet paket Rp. ".number_format($total_omzet_paket,0,",","."),'',0,'');$pdf->Ln();$pdf->Ln();
		
		$pdf->Cell(6.4,$line_space,'DAFTAR PENUMPANG',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(0.5,$line_space,"K",'',0,'');$pdf->Cell(5.5,$line_space,"Nama Penumpang ",'',0,'');$pdf->Ln();		
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		
		//list penumpang
		
		$result_penumpang = $Reservasi->ambilDataPenumpangUntukSPJ($tgl_berangkat,$list_kode_jadwal);
		
		$total_jenis_penumpang	= array();
		
		if($jumlah_penumpang>0){
			while($row_penumpang=$db->sql_fetchrow($result_penumpang)){
				$pdf->Cell(0.5,$line_space,substr("0".$row_penumpang['NomorKursi'],-2),'',0,'');$pdf->Cell(6,$line_space,$row_penumpang['Nama']." (".$row_penumpang['JenisPenumpang'].")",'',0,'');$pdf->Ln();
				$pdf->Cell(0.5,$line_space,'','',0,'');$pdf->Cell(6,$line_space,$row_penumpang['NoTiket'],'',0,'');$pdf->Ln();
				$pdf->Cell(0.5,$line_space,'','',0,'');$pdf->Cell(6,$line_space,$row_penumpang['Telp'],'',0,'');$pdf->Ln();
				$pdf->Cell(0.5,$line_space,'','',0,'');$pdf->Cell(6,$line_space,$row_penumpang['Tujuan'],'',0,'');$pdf->Ln();
				$nama_index	= $row_penumpang['JenisPenumpang'];
				$total_jenis_penumpang[$nama_index]++;
			}
		}
		else{
			$pdf->Cell(6.4,$line_space,'TIDAK ADA PENUMPANG',0,0,'');$pdf->Ln();
		}
		
		//footer
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Jumlah Penumpang :".$jumlah_penumpang,'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Omzet penumpang Rp. ".number_format($total_omzet,0,",","."),'',0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"Total Omzet Rp. ".number_format($total_omzet+$total_omzet_paket,0,",","."),'',0,'');$pdf->Ln();$pdf->Ln();
		
		$list_total_by_jenis_penumpang = "";
		foreach($total_jenis_penumpang as $index=>$value){
			$list_total_by_jenis_penumpang .= $index."=".$value."|";
		}
		
		$list_total_by_jenis_penumpang	= substr($list_total_by_jenis_penumpang,0,-1);
		$pdf->Cell(6.4,$line_space,$list_total_by_jenis_penumpang,'',0,'');$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(6.4,$line_space,"CSO ".$userdata['nama'],'',0,'');$pdf->Ln();
		$pdf->Ln();
		
		//PESAN SPONSOR
		
		$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
		
		if(strlen($pesan_sponsor)>30){
			$arr_kata	= explode(" ",$pesan_sponsor);
			
			$temp_pesan_sponsor="";
			$jumlah_kata	= count($arr_kata);
			
			$idx	= 0;
			
			while($idx<$jumlah_kata){
				
				if(strlen($temp_pesan_sponsor." ".$arr_kata[$idx])<30){
					$temp_pesan_sponsor	= $temp_pesan_sponsor." ".$arr_kata[$idx];
					$idx++;
				}
				else{
					$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
					$temp_pesan_sponsor	= "";
					
				}
			}
			
			$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
		}
		else{
			$pdf->Cell(6.4,$line_space,$pesan_sponsor,0,0,'C');$pdf->Ln();
		}
		
		$pdf->Cell(6.4,$line_space,"-- Terima Kasih --",0,0,'C');$pdf->Ln();
		$pdf->SetFont('courier','',8);
		$pdf->Cell(6.4,$line_space,$data_perusahaan['EmailPerusahaan'],0,0,'C');$pdf->Ln();
		$pdf->SetFont('courier','',8);
		$pdf->Cell(6.4,$line_space,$data_perusahaan['WebSitePerusahaan'],0,0,'C');$pdf->Ln();
		
		$pdf->AutoPrint(true);
		$pdf->Output();
		
	}
}
?>