<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];


// LIST
$template->set_filenames(array('body' => 'daftar_tiket_mitra/daftar_tiket_mitra_body.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$Cabang	= new Cabang();

switch($mode){
	case 'getasal':
		
		echo "
			<select class='form-control' name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		echo "
			<select class='form-control' name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;

	case 'buatvoucher':
		$nama  			= isset($HTTP_GET_VARS['nama'])? $HTTP_GET_VARS['nama'] : $HTTP_POST_VARS['nama'];
		$telp  			= isset($HTTP_GET_VARS['telp'])? $HTTP_GET_VARS['telp'] : $HTTP_POST_VARS['telp'];
		$jam  			= isset($HTTP_GET_VARS['jam'])? $HTTP_GET_VARS['jam'] : $HTTP_POST_VARS['jam'];
		$mitra 			= isset($HTTP_GET_VARS['mitra'])? $HTTP_GET_VARS['mitra'] : $HTTP_POST_VARS['mitra'];
		$tiket 			= isset($HTTP_GET_VARS['tiket'])? $HTTP_GET_VARS['tiket'] : $HTTP_POST_VARS['tiket'];
		$komisi			= isset($HTTP_GET_VARS['komisi'])? $HTTP_GET_VARS['komisi'] : $HTTP_POST_VARS['komisi'];
		$tgl  			= isset($HTTP_GET_VARS['tglberangkat'])? $HTTP_GET_VARS['tglberangkat'] : $HTTP_POST_VARS['tglberangkat'];

		$tgl_berangkat 	= FormatTglToMySQLDate($tgl);

		$sql = "INSERT INTO tbl_tiket_mitra(NamaPenumpang,Telp,TglBerangkat,JamBerangkat,Mitra,TiketPublish,Komisi,WaktuInput,PetugasInput)
					VALUES ('$nama','$telp','$tgl_berangkat','$jam','$mitra','$tiket','$komisi',NOW(),'$userdata[user_id]')";

		if (!$db->sql_query($sql)){
			die_error("Err: ".__LINE__.$sql);
		}

		echo("alert('Tiket telah berhasil di input!');window.location.reload();");
		exit;

}

$kondisi =	$cari==""?"":
	" AND (NamaPenumpang LIKE '%$cari%'
		OR Mitra LIKE '%$cari%'
	  )";

/*$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'":"";*/

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,Jamberangkat":$sort_by;


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"id","tbl_tiket_mitra ts",
"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"daftar_tiket_mitra.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT 
		*
	FROM tbl_tiket_mitra ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'jadwal'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
				'nama'=>$row['NamaPenumpang'],
				'mitra'=>$row['Mitra'],
				'tiket'=>number_format($row['TiketPublish'],0,",","."),
				'petugas'=>$row['PetugasInput'],
				'komisi'=>number_format($row['Komisi'],0,",","."),
				'act'=>$act,
			)
		);
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&sort_by=$sort_by&order=$order&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir;
$script_cetak_excel="Start('daftar_tiket_mitra_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$Kota   = new Kota();

$template->assign_vars(array(
	'BCRUMP'    		=> '<ul id="breadcrumb"><li><a href="'.append_sid('main.'.$phpEx) .'">Home</a></li><li><a href="'.append_sid('daftar_tiket_mitra.'.$phpEx).'">Daftar Mitra</a></li></ul>',
	'ACTION_CARI'		=> append_sid('daftar_tiket_mitra.'.$phpEx),
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> $Kota->setComboKota($kota),
	'TXT_CARI'			=> $cari,
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'A_SORT_1'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Jadwal ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>