<input type='hidden' value='{NO_SPJ}' name='nospj' id='nospj' />

<div id="cargolayoutwrapper">
	<!-- BEGIN TOMBOL_MUTASIKAN -->
	<center><input type="button" value="Mutasikan Kesini" style="width:150px;height: 30px;" onclick="mutasiPaket();"></center>
	<!-- END TOMBOL_MUTASIKAN -->
	
	<table class="table">
		<tr><td>Tanggal</td><td>: {TANGGAL_BERANGKAT_DIPILIH}</td></tr>
		<tr><td>Poin Pick Up</td><td>: {POIN_PICKEDUP_DIPILIH}</td></tr>
	</table>
	
	<!-- BEGIN DATA_MANIFEST -->
	
	<table class="table">
		<tr><td>Manifest</td><td>: {DATA_MANIFEST.no_spj}</td></tr>
		<tr><td>Waktu Cetak</td><td>: {DATA_MANIFEST.waktu_cetak}</td></tr>
		<tr><td>Pencetak</td><td>: {DATA_MANIFEST.petugas_pencetak}</td></tr>
	</table>

	<!-- END DATA_MANIFEST -->
	
	<!-- BEGIN BELUM_PICKUP -->
	<center><h4 style="color: #FF552A;">Belum Di Picked Up</h4></center>
	<!-- END BELUM_PICKUP -->
	
	<!-- BEGIN DATA_PICKUP -->
	<span class="resvcargolayoutlabel">Waktu Picked Up</span><span class="resvcargolayoutfield">: {DATA_PICKUP.waktu_pickedup}</span><br>
	<span class="resvcargolayoutlabel">Pemberi</span><span class="resvcargolayoutfield">: {DATA_PICKUP.petugas_pemberi}</span><br>
	<span class="resvcargolayoutlabel">Penerima</span><span class="resvcargolayoutfield">: {DATA_PICKUP.petugas_penerima}</span><br>
	<!-- END DATA_PICKUP -->
	<br>
	<center>
		<!-- BEGIN TOMBOL_CETAK_MANIFEST -->
		<input type='button' value='{TOMBOL_CETAK_MANIFEST.value}' onclick='setDialogSPJ();' style="width: 150px; height: 30px;margin-right: 5px;" />
		<!-- END TOMBOL_CETAK_MANIFEST -->
		<input class="btn mybutton paper" type='button' value='Refresh' onclick='getUpdatePaket();getPengumuman();' />
		<hr>

		<table class="table table-hover">
			<tr><td align="right">Trx</td><td style="font-weight: 500;">{TOTAL_TRANSAKSI}</td></tr>
			<tr><td align="right">Koli</td><td style="font-weight: 500;">{TOTAL_PAX}</td></tr>
			<tr><td align="right">Omzet</td><td style="font-weight: 500;">Rp. {TOTAL_OMZET}</td></tr>
		</table>
		<hr>
	</center>
	<!-- BEGIN DAFTAR_CARGO -->
	<div class='{DAFTAR_CARGO.odd}' onclick="showPaket('{DAFTAR_CARGO.id}')" style="cursor: pointer;">
		<h3 style="float:left;font-size: 20px;padding-left: 5px;">{DAFTAR_CARGO.no}</h3>
		<div class="resvcargolistawb">AWB: {DAFTAR_CARGO.awb}</div>
		<div class="resvcargolisttitle">{DAFTAR_CARGO.jenis_barang} <span class="resvcargolistserviceurg">[{DAFTAR_CARGO.layanan}]</span></div>
		<div class="resvcargolistrute">{DAFTAR_CARGO.rute}</div>
		<div class="resvcargolistdata">Pengirim: {DAFTAR_CARGO.nama_pengirim}</div>
		<div class="resvcargolistdata">Penerima: {DAFTAR_CARGO.nama_penerima}</div>
		<div class="resvcargolistpax">Koli: {DAFTAR_CARGO.koli} Pax / Berat: {DAFTAR_CARGO.berat} Kg</div>
		<hr>
	</div>
	<!-- END DAFTAR_CARGO -->
	
	<!-- BEGIN NO_DATA -->
	<div style="text-align: center; color: #FF552A;"><h4>Daftar Barang Kosong</h4></div>
	<!-- END NO_DATA -->
</div>