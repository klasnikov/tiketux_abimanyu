<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript">

function UbahPengumuman(pilihan){
	if(pilihan==1){
		pengumuman=document.getElementById('txt_pengumuman_bandung').value;
		berlaku_hingga=document.getElementById('tgl_bandung').value;
		info="BANDUNG";
	}
	else if(pilihan==2){
		pengumuman=document.getElementById('txt_pengumuman_jakarta').value;
		berlaku_hingga=document.getElementById('tgl_jakarta').value;
		info="JAKARTA";
	}
	else{
		pengumuman=document.getElementById('txt_pengumuman_jateng').value;
		berlaku_hingga=document.getElementById('tgl_jateng').value;
		info="JAWA TENGAH";
	}
	
	new Ajax.Request("ubah_pengumuman.php?sid={SID}", 
		{
			asynchronous: true,
		  method: "get",
		  parameters: "pengumuman=" + pengumuman + "&berlaku_hingga="+berlaku_hingga+"&mode=ubah&pilihan="+pilihan,
			onLoading: function(request) 
		  {
		            
		  },
		  onComplete: function(request) 
		  {
				
				if(request.responseText=='sukses'){
					alert("pengumuman "+info+" telah BERHASIL DIUBAH!");
				}
				else{
					alert("error");
				}
				
		  },
		  onFailure: function(request) 
		  { 
				assignError(request.responseText); 
		  }
		});

}

</script>

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
	<tr>
		<th align='left'>Welcome {USERNAME},</th>
	</tr>
	<tr>
		<td align='left' class="indexer">{BCRUMP}</td>
	</tr>
	<tr>
		<td class="whiter" valign="middle" align="center">
			<table width='800'>
				<tr>
					<td>
						<h1 align='center'>Ubah pengumuman</h1>
					<td>
				</tr>
				<tr>
					<td  valign='top'>Masukkan pengumuman untuk BANDUNG</td>
				</tr>
				<tr>
					<td>
						<form name="frm_tgl_bandung" method="post">
							pengumuman berlaku hingga:<input readonly="yes" id="tgl_bandung" onclick="javascript:cal1.popup();" name="tgl_bandung" value="{TGL_BANDUNG}" type="Text">
						</form>
						<script language="JavaScript">
							<!-- // create calendar object(s) just after form tag closed
							// specify form element as the only parameter (document.forms['formname'].elements['inputname']);
							// note: you can have as many calendar objects as you need for your application
							var cal1 = new calendar1(document.forms['frm_tgl_bandung'].elements['tgl_bandung']);
							cal1.year_scroll = true;
							cal1.time_comp = false;
							//-->
						</script>
					</td>
				</tr>
				<tr>
					<td><textarea id='txt_pengumuman_bandung' cols="130"	rows='10'>{PENGUMUMAN_BANDUNG}</textarea><BR>diubah/dibuat oleh: {DIBUAT_OLEH_BANDUNG}<br>tgl diubah/dibuat:{TGL_DIBUAT_BANDUNG}</td>
				</tr>
				<tr>
					<td align='center'><input type='button' name='ubah' value='ubah' onclick="UbahPengumuman(1)"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td  valign='top'>Masukkan pengumuman untuk JAKARTA</td>
				</tr>
				<tr>
					<td>
						<form name="frm_tgl_jakarta" method="post">
							pengumuman berlaku hingga:<input readonly="yes" id="tgl_jakarta" onclick="javascript:cal2.popup();" name="tgl_jakarta" value="{TGL_JAKARTA}" type="Text">
						</form>
						<script language="JavaScript">
							<!-- // create calendar object(s) just after form tag closed
							// specify form element as the only parameter (document.forms['formname'].elements['inputname']);
							// note: you can have as many calendar objects as you need for your application
							var cal2 = new calendar1(document.forms['frm_tgl_jakarta'].elements['tgl_jakarta']);
							cal2.year_scroll = true;
							cal2.time_comp = false;
							//-->
						</script>
					</td>
				</tr>
				<tr>
					<td><textarea id='txt_pengumuman_jakarta' cols="130"	rows='10'>{PENGUMUMAN_JAKARTA}</textarea><BR>diubah/dibuat oleh: {DIBUAT_OLEH_JAKARTA}<br>tgl diubah/dibuat:{TGL_DIBUAT_JAKARTA}</td>
				</tr>
				<tr>
					<td align='center'><input type='button' name='ubah' value='ubah' onclick="UbahPengumuman(2)"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td  valign='top'>Masukkan pengumuman untuk JAWA TENGAH</td>
				</tr>
				<tr>
					<td>
						<form name="frm_tgl_jateng" method="post">
							pengumuman berlaku hingga:<input readonly="yes" id="tgl_jateng" onclick="javascript:cal3.popup();" name="tgl_jateng" value="{TGL_JATENG}" type="Text">
						</form>
						<script language="JavaScript">
							<!-- // create calendar object(s) just after form tag closed
							// specify form element as the only parameter (document.forms['formname'].elements['inputname']);
							// note: you can have as many calendar objects as you need for your application
							var cal3 = new calendar1(document.forms['frm_tgl_jateng'].elements['tgl_jateng']);
							cal3.year_scroll = true;
							cal3.time_comp = false;
							//-->
						</script>
					</td>
				</tr>
				<tr>
					<td><textarea id='txt_pengumuman_jateng' cols="130"	rows='10'>{PENGUMUMAN_JATENG}</textarea><BR>diubah/dibuat oleh: {DIBUAT_OLEH_JATENG}<br>tgl diubah/dibuat:{TGL_DIBUAT_JATENG}</td>
				</tr>
				<tr>
					<td align='center'><input type='button' name='ubah' value='ubah' onclick="UbahPengumuman(3)"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>