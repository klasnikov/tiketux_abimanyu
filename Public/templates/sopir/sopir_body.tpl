<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}


function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_sopir.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_sopir="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_sopir.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&kode_sopir="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">Master Sopir</td>
							<td colspan=2 align='right' class="bannernormal" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<div class="input-group">
								      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
								      <span class="input-group-btn">
								        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
								      </span>
								    </div><!-- /input-group -->
								</form>
							</td>
						</tr>
						<tr>
							<td class="mytd" align='left'>
								<a href="{U_SOPIR_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
								<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
							<td class="mytd" width='70%' align='right'>{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%' class="border table table-hover table-bordered">
				    <tr>
				       <th width="10"><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
				       <th>No</th>
							 <th >Nama sopir</th>
							 <th >Kode</th>
							 <th >Alamat</th>
							 <th >HP</th>
							 <th >No.SIM</th>
							 <th >Aktif</th>
							 <th >Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="center">{ROW.check}</div></td>
				       <td><div>{ROW.no}</div></td>
							 <td><div >{ROW.nama}</div></td>
							  <td><div >{ROW.kode_sopir}</div></td>
				       <td><div >{ROW.alamat}</div></td>
							 <td><div >{ROW.hp}</div></td>
							 <td><div >{ROW.no_sim}</div></td>
							 <td><div >{ROW.aktif}</div></td>
				       <td><div >{ROW.action}</div></td>
				     </tr>  
				     <!-- END ROW -->
						 {NO_DATA}
			    </table>
			    <table class="table" width="100%">
					<tr>
						<td class="mytd" align='left'>
							<a href="{U_SOPIR_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
							<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
						<td class="mytd" width='70%' align='right'>{PAGING}
						</td>
					</tr>
				</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>