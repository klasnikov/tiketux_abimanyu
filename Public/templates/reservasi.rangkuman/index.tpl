<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">

  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");

  function getUpdateTujuan(asal){
	  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]
	  if(document.getElementById('rewrite_tujuan'))
	  {
		  document.getElementById('rewrite_tujuan').innerHTML = "";
	  }
	  new Ajax.Updater("rewrite_tujuan","reservasi.rangkuman.php?sid={SID}",
			  {
				  asynchronous: true,
				  method: "get",
				  parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
				  onLoading: function(request)
				  {
					  //Element.show('loading_tujuan');
				  },
				  onComplete: function(request)
				  {
					  //Element.hide('loading_tujuan');

				  },
				  onFailure: function(request)
				  {
					  assignError(request.responseText);
				  }
			  });
  }
 
	function setDashboardCabang(id_jurusan,rewrite_id,is_arrival){

		//str 	   = document.getElementById("cabangasal").value;
		//cabangasal = str.substr(1, 3);;
		//alert(id_jurusan);
		
		new Ajax.Updater(rewrite_id,"reservasi.rangkuman_detail.php?sid={SID}", {
			asynchronous: true,
			method: "post",
		
			parameters: "idjurusan="+id_jurusan+"&tgl="+document.getElementById("tanggal_mulai").value+"&isarrival="+is_arrival,
			onLoading: function(request){
			},
			onSuccess: function(request){
				 //getUpdateTujuan(cabangasal);
				 //document.getElementById("idjurusan").value = idjurusan;
			},
			onComplete: function(request){
				
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});		
	}

  (function($){
	  $(function(){
		  $("#tanggal_mulai").datepicker({
			  dateFormat:"dd-mm-yy"
		  });
	  });

  })(jQuery);
</script>

<div class="container" style="width: 90%;">
	<div class="row">
		<div class="col-md-12 box">
			<div class="col-md-3" style="text-align: left;">
				<div class="bannerjudulw"><div class="bannerjudul">Rangkuman Reservasi</div></div>
			</div>
			<form action="{ACTION_CARI}" method="post">
				<div class="col-md-9">
					<div class="col-md-3">
						<input class="form-control input-sm" id="tanggal_mulai" name="tanggal_mulai" type="text" value="{TGL_AWAL}">
					</div>
					<div class="col-md-3">
						<select class="form-control input-sm" name='cabangasal' id='cabangasal' onChange="getUpdateTujuan(this.value)">{OPT_CABANG}</select>
					</div>
					<div class="col-md-3">
						<div id='rewrite_tujuan'></div>
						<span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span>

					</div>
					<div class="col-md-3">
						<input class="btn btn-sm pull-left" type="submit" value="Cari" />
					</div>
				</div>
			</form>
			<div class="col-md-12">
				<table>
					<tr>
						<td><strong>P</strong></td><td>: Paket</td>
						<td>&nbsp;|&nbsp; </td>
						<td><strong>B1</strong></td><td>: Penumpang Booking</td>
						<td>&nbsp;|&nbsp;</td>
						<td><strong>B2</strong></td><td>: Penumpang Booking Transit</td>
						<td>&nbsp;|&nbsp;</td>
						<td><strong>C</strong></td><td>: Penumpang Bayar / Confirm</td>
						<td>&nbsp;|&nbsp;</td>
						<td><strong>Av</strong></td><td>: Sisa Kursi</td>
					</tr>
				</table>
			</div>
			<table class="table" cellspacing="0" cellpadding="0" >
				<tr>
				 <td class="whiter" valign="middle" align="center">		
					<form action="{ACTION_CARI}" method="post">
						<!--<div id="loading" style="background: url('./templates/images/loading_bar.gif') no-repeat;height: 19px">&nbsp;</div>-->
						{LIST_DASHBOARD}
					</form>
				 </td>
				</tr>
			</table>
		</div>
	</div>
</div>
