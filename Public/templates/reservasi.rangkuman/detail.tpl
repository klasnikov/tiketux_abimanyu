<table class="table" cellpadding=0 cellspacing=0>
	<tr>
		<td valign="top">
			<table class="table table-hover table-striped table-bordered" cellpadding=0 >
				<tr><th colspan='9' style="font-size: 16px">{ASAL} Ke {TUJUAN}</th><th style="font-size: 16px;text-align: center">Tanggal : {TGL}</th></tr>
				<tr>
					<td class="dashboardkolomheader">Keterangan</td>
					<td class="dashboardkolomheader">P</td>
					<td class="dashboardkolomheader">B1</td>
					<td class="dashboardkolomheader">B2</td>
					<td class="dashboardkolomheader">C</td>
					<td class="dashboardkolomheader">Av.</td>
					<td class="dashboardkolomheader">#Body</td>
					<td class="dashboardkolomheader">CGS</td>
					<td class="dashboardkolomheader">Time</td>
					<td class="dashboardkolomheader">Action</td>
				</tr>
				<!-- BEGIN ROW1 -->
				<tr>
					<td ><div align="center">{ROW1.keterangan}</div></td>
					<td ><div align="right">{ROW1.p}</div></td>
					<td ><div align="right">{ROW1.b}</div></td>
					<td ><div align="right">{ROW1.t}</div></td>
					<td ><div align="right">{ROW1.c}</div></td>
					<td ><div align="right">{ROW1.av}</div></td>
					<td ><div align="left">{ROW1.body}</div></td>
					<td ><div align="left">{ROW1.cgs}</div></td>
					<td ><div align="center">{ROW1.time}</div></td>
					<td >
						<div align="center">
							<a href="{ROW1.act}">
								Reservasi
							</a>
						</div></td>
				</tr>
				<!-- END ROW1 -->
			</table>
		</td>
	</tr>
</table>

