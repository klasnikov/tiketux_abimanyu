<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='100%'>
    <tr>
    <td align='center'>
       <h2>{JUDUL}</h2>
    </td>
    <tr>
      <td>
        <table width="100%" border=1 cellpadding=0 cellspacing=0>
          <tr>
						<td align="center"  width="1%">No</td>
						<td align="center"  width="10%">Tgl.Brgkt</td>
            <td align="center"  width="8%">KD.Jdwl</td>
            <td align="center"  width="3%">Jam Brgkt</td>
            <td align="center"  width="3%">Jam Pesan</td>
            <td align="center"  width="20%">No.Tiket</td>
            <td align="center"  width="20%">Nama</td>
						<td align="center" >No.Kursi</td>
						<td align="center" >Harga Tiket</td>
            <td align="center" >J.Krs</td>
            <td align="center" >Subtotal</td>
            <td align="center" >Discount</td>
            <td align="center" >Total</td>
            <td align="center" >Jns Discount</td>
          </tr>
          <!-- BEGIN ROW -->
          <tr>
						<td align="right">{ROW.num}</td>
            <td align="center" >{ROW.tgl_berangkat}</td>
            <td align="left">{ROW.kode_jadwal}</td>
            <td align="center">{ROW.jam_berangkat}</td>
            <td align="center">{ROW.jam_pesan}</td>
            <td align="left">{ROW.no_tiket}</td>
            <td align="left">{ROW.nama}</td>
            <td align="left">{ROW.no_kursi}</td>
						<td align="right">{ROW.harga_tiket}</td>
						<td align="right">{ROW.pesan_kursi}</td>
            <td align="right">{ROW.sub_total}</td>
            <td align="right">{ROW.discount}</td>
            <td align="right">{ROW.total}</td>
            <td align="center">{ROW.jenis_discount}</td>
          </tr>
          <!-- END ROW -->
					<tr>
						<td align='center' colspan=9 bgcolor=>
							Total:
						</td>
						<td align='right'>
							{TOTAL_KURSI}
						</td>
						<td align='right'>
							{TOTAL_OMZET}
						</td>
						<td align='right'>
							{TOTAL_DISCOUNT}
						</td>
						<td align='right'>
							{TOTAL_PENDAPATAN}
						</td>
						<td>
							&nbsp;
						</td>
						
					</tr>
        </table>

      </td>
		</tr>
    </table>	
 </td>
</tr>
</table>