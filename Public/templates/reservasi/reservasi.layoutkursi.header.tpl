<style type="text/css">
  .headertitle{
    display: inline-block;
    font-weight: bold;
    width: 120px;
  }
  
  .headerfield{
    display: inline-block;
    width: 280px;
  }
</style>

<input type='hidden' value='{JAM_BERANGKAT}' id='jam_berangkat_aktif' />
<input type='hidden' readonly='yes' value='{NO_SPJ}' name='txt_spj' id='txt_spj' />
<input type='hidden' readonly='yes' value='{NO_POLISI}' name='plat' id='plat' />
<input type='hidden' value='{NO_UNIT}' name='hide_mobil_sekarang' id='hide_mobil_sekarang'></input>
<input type='hidden' value='{JUMLAH_KURSI}' name='hide_layout_kursi' id='hide_layout_kursi'></input>
<input type='hidden' name='hide_nama_sopir_sekarang' id='hide_nama_sopir_sekarang' value='{KODE_SOPIR}'>
  
<div class="layout_kursi_head">
  <table class="table table-hover table-condensed" style="width: 100%;">
    <tr><td>Manifest</td><td>: <span class="headerfield">{NO_SPJ}</span></td></tr>
    <tr><td>Kendaraan</td><td>: <span class="headerfield">{DATA_UNIT}</span></td></tr>
    <tr><td>Harga</td><td>: <span class="headerfield"><span class="{CLASS_HARGATIKET}">Rp.{HARGA_TIKET}</span>&nbsp;{HARGA_TIKET_PROMO}</span></td></tr>
  </table>
  <!--<span class="headertitle">Daftar Tunggu</span><span class="headerfield">:<a style="color: red;font-size: 12px;" href="" onClick="{ACTION_DAFTAR_TUNGGU};return false;">{DAFTAR_TUNGGU} Orang</a></span><br>-->
</div>
<div style="font-size: 13px; text-align: center; width:100%;">
  <div style="text-align: center;width:100%;vertical-align: top;background-color: #ffffff;height: 75px;">
    <!-- BEGIN TOMBOL_MANIFEST -->
    <div style="float: left; height: 64px; margin-top: 0; width: 64px; position: relative;">&nbsp;
    <span class="b_print_manifest" onclick='setDialogSPJ();' title="Cetak Manifest"><img class="manifest_img" src="{ROOT}templates/images/icon_print_manifest.png"></span></div>
    <!-- END TOMBOL_MANIFEST -->

    <div style="float: right; height: 64px; margin-top: 0; width: 64px; position: relative;">&nbsp;
    <span class="b_reload_layout" onclick='getUpdateMobil();' title="Reload Layout Kursi"><img class="manifest_img" src="{ROOT}templates/images/icon_reload.png"></span></div>
    <div style="max-width: 245px; width: 245px; margin: 0 auto;">
        <h5 class="kode_jadwal">{KODE_JADWAL}</h5>
        {TGL_BERANGKAT} 
    </div>
  </div>
  <hr />