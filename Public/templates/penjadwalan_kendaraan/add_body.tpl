<script language="JavaScript">

  function validateInput(){
    popuploading.show();
    return true;
  }

  dojo.require("dojo.widget.Dialog");

  function init(e) {
    // inisialisasi variabel
    popuploading = dojo.widget.byId("popuploading");
  }

  dojo.addOnLoad(init);

</script>

<form name="frm_data_mobil" action="{U_ADD}" method="post" onSubmit='return validateInput();'>

  <input type="hidden" id="idjurusan" name="idjurusan" value="{ID_JURUSAN}">
  <input type="hidden" id="tanggal" name="tanggal" value="{TGL_JADWAL}">
  <input type="hidden" id="kodejadwal" name="kodejadwal" value="{KODE_JADWAL}">
  <input type="hidden" id="cabangasal" name="cabangasal" value="{KODE_CABANG_ASAL}">

  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr class='banner' height=40>
      <td align='center' valign='middle' class="bannerjudul">&nbsp;Penjadwalan Non-Reguler Tanggal "{TANGGAL}"</td>
    </tr>
    <tr>
      <td class="whiter" valign="middle" align="center">
        <table width='1000'>
          <tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
          <tr>
            <td align='center' valign='top' width='500'>
              <table width='500'>
                <tr>
                  <td colspan=3><h2>{JUDUL}</h2></td>
                </tr>
                <tr>
                  <td width='200'>Kode Jadwal</td><td width='5'>:</td><td width='400'><input type="text" value="{KODE_JADWAL}" disabled="disabled" class="textdisabled"/></td>
                </tr>
                <tr>
                  <td>Asal</td><td>:</td><td>{CABANG_ASAL}</td>
                </tr>
                <tr>
                  <td>Tujuan</td><td>:</td><td>{CABANG_TUJUAN}</td>
                </tr>
                <tr>
                  <td>Waktu berangkat*</td><td>:</td>
                  <td>
                    <select id='jam' name='jam'>
                      <option value="00">00</option>
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                    </select>&nbsp;:&nbsp;
                    <select id='menit' name='menit'>
                      <option value="00">00</option>
                      <option value="15">15</option>
                      <option value="30">30</option>
                      <option value="45">45</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Layout Kursi*</td><td>:</td>
                  <td><select id='layoutkursi' name='layoutkursi'>{OPT_KURSI}</select></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan=3 align='center' valign='middle' height=40>
              <input type="hidden" name="mode" value="{MODE}">
              <input type="button" onClick="window.open('{U_BACK}','_self')" value="KEMBALI" style="width: 100px">&nbsp;&nbsp;&nbsp;
              <input type="submit" name="submit" value="SIMPAN" style="width: 100px">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>