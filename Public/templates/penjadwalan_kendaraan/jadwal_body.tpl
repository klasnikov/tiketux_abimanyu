<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
	
	function ubahStatus(id){
	
			new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=ubahstatus&id="+id,
			 onLoading: function(request) 
			 {
				Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
				
			 },
			 onSuccess: function(request) 
			 {			
				Element.hide('loading_proses');
				
				if(request.responseText==1){
					document.forms["formcari"].submit();
				}
				else{
					alert("Masih ada penumpang di jadwal tersebut, silahkan pindahkan terlebih dahulu penumpang dijadwal tersebut!");
				}
			},
			 onFailure: function(request) 
			 {
			 }
			});  
		
		return false;
			
	}
	
	function updateMobil(){
		
		tgl					= document.getElementById("tanggal").value;
		kode_jadwal	= document.getElementById("dlgdaftarmobilkodejadwal").innerHTML;
		kode_kendaraan = document.getElementById("dlgcbomobil").value;
		aktif				= 1;
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=0&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&kodekendaraaan="+kode_kendaraan+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}
					else {
						alert("Terjadi kegagalan");exit;
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function updateSopir(){
		
		tgl					= document.getElementById("tanggal").value;
		kode_jadwal	= document.getElementById("dlgdaftarsopirkodejadwal").innerHTML;
		kode_sopir	= document.getElementById("dlgcbosopir").value;
		aktif				= 1;
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=1&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&kode_sopir="+kode_sopir+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}
					else {
						alert("Terjadi kegagalan");exit;
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function simpan(tgl,kode_jadwal,aktif){
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=2&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}
					else {
						alert("Masih ada penumpang di jadwal tersebut, silahkan pindahkan terlebih dahulu penumpang dijadwal tersebut!");
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function getUpdateTujuan(asal){
			// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
			
			if(document.getElementById('rewrite_tujuan')){
				document.getElementById('rewrite_tujuan').innerHTML = "";
			}
			
			new Ajax.Updater("rewrite_tujuan","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "get",
					parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
					onLoading: function(request) 
					{
							Element.show('loading_tujuan');
					},
					onComplete: function(request) 
					{
							Element.hide('loading_tujuan');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			});   
	}
	
	function setDaftarMobil(mobil,kodejadwal,tanggal,aktif){
			
			new Ajax.Updater("dlgdaftarmobilcbomobil","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "post",
					parameters: "mode=setdaftarmobil" +
						"&mobil=" + mobil+
						"&kodejadwal=" + kodejadwal +
						"&tanggal=" + tanggal +
						"&aktif=" + aktif,
					onLoading: function(request) 
					{
							dlg_daftar_mobil.show();
							Element.show('dlgdaftarmobilloading');
					},
					onComplete: function(request) 
					{
							document.getElementById("dlgdaftarmobilkodejadwal").innerHTML	= kodejadwal;
							Element.hide('dlgdaftarmobilloading');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			});   
	}
	
	function setDaftarSopir(sopir,kodejadwal,tanggal,aktif){
			
			
			new Ajax.Updater("dlgdaftarsopircbosopir","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "post",
					parameters: "mode=setdaftarsopir" +
						"&sopir=" + sopir +
						"&kodejadwal=" + kodejadwal +
						"&tanggal=" + tanggal +
						"&aktif=" + aktif,
					onLoading: function(request) 
					{
							dlg_daftar_sopir.show();
							Element.show('dlgdaftarsopirloading');
					},
					onComplete: function(request) 
					{
							document.getElementById("dlgdaftarsopirkodejadwal").innerHTML	= kodejadwal;
							Element.hide('dlgdaftarsopirloading');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			}); 
	}

  function hapus(id){

    if(!confirm("Apakah anda yakin akan menghapus data ini?")){
      return;
    }

    new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
      asynchronous: true,
      method: "get",
      parameters: "mode=hapus&id="+id,
      onLoading: function(request)
      {
        popuploading.show();
      },
      onComplete: function(request)
      {

      },
      onSuccess: function(request)
      {
        Element.hide('loading_proses');

        var status="";
        eval(request.responseText);

        if(status=="OK"){
          location.reload();
        }
        else{
          popuploading.hide();
          alert("Masih ada penumpang di jadwal tersebut, silahkan pindahkan terlebih dahulu penumpang dijadwal tersebut!");
        }
      },
      onFailure: function(request)
      {
      }
    });

  }

	// komponen khusus dojo 
	dojo.require("dojo.widget.Dialog");
	
	function init(e) {
		// inisialisasi variabel
		
		//control dialog daftar mobil
		dlg_daftar_mobil				= dojo.widget.byId("dlgdaftarmobil");
		dlg_daftar_mobil_cancel	= document.getElementById("dlgdaftarmobilbuttoncancel");
		dlg_daftar_mobil.setCloseControl(dlg_daftar_mobil_cancel);
		
		//control dialog daftar sopir
		dlg_daftar_sopir				= dojo.widget.byId("dlgdaftarsopir");
		dlg_daftar_sopir_cancel	= document.getElementById("dlgdaftarsopirbuttoncancel");
		dlg_daftar_sopir.setCloseControl(dlg_daftar_sopir_cancel);
    popuploading = dojo.widget.byId("popuploading");
		
	}
	
	dojo.addOnLoad(init);

</script>

<!--BEGIN dialog daftar mobil-->
<div dojoType="dialog" id="dlgdaftarmobil" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="400" cellpadding="0" cellspacing="0"  style="background-color: white;">
	<tr><td>
		<input type="button" style="border-radius: 0;" class="button-close" id="dlgdaftarmobilbuttoncancel" value="&nbsp;X&nbsp;">
		<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Daftar Mobil</h4>
	</td></tr>
	<tr>
		<td style="padding: 20px;">
			<table class="table">
				<tr><td width="100">Kode Jadwal</td><td width="1">:</td><td><b></b><span id="dlgdaftarmobilkodejadwal"></span></b></td></tr>
				<tr><td>Mobil</td><td width="1">:</td><td><b></b><span id="dlgdaftarmobilcbomobil"></span><span id='dlgdaftarmobilloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
				<tr>
					<td colspan="3" align="right">
						<input class="btn mybutton paper" style="width: 40%;" type="button" id="dlgdaftarmobilbuttonsimpan" value="Simpan" onclick="updateMobil();">&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
</div>
<!--END dialog daftar mobil-->

<!--BEGIN dialog daftar sopir-->
<div dojoType="dialog" id="dlgdaftarsopir" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table style="background: white;" width="400" cellpadding="0" cellspacing="0" >
	<tr>
		<td>
			<input type="button" style="border-radius: 0;" class="button-close" id="dlgdaftarsopirbuttoncancel" value="&nbsp;X&nbsp;">
			<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Daftar Sopir</h4>
		</td>
	</tr>
	<tr>
		<td class="pad10">
			<table class="table" width="100%">
				<tr><td width="100">Kode Jadwal</td><td width="1">:</td><td><b></b><span id="dlgdaftarsopirkodejadwal"></span></b></td></tr>
				<tr><td>Sopir</td><td width="1">:</td><td><b></b><span id="dlgdaftarsopircbosopir"></span><span id='dlgdaftarsopirloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
				<tr><td colspan="3" align="right"><br><input class="btn mybutton paper" type="button" id="dlgdaftarsopirbuttonsimpan" value="Simpan" onclick="updateSopir();"></td></tr>
			</table>
		</td>
	</tr>
</table>
<br>
</div>
<!--END dialog daftar sopir-->
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<form action="{ACTION_CARI}" method="post" name="formcari">
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td>
								<div class="col-md-3">
									<div class="bannerjudulw">
										<div class="bannerjudul">Penjadwalan Kendaraan</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="col-md-3">
									Tanggal<br />
										<input class="form-control" readonly="yes"  id="tanggal" name="tanggal" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TANGGAL}">
									</div>
									<div class="col-md-3">
										Cabang Asal<br />
										<select class="form-control" name='cabangasal' id='cabangasal' onChange="getUpdateTujuan(this.value)">{CABANG_ASAL}</select>
									</div>
									<div class="col-md-3">
										Cabang Tujuan<br />
										<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span>
									</div>
									<div class="col-md-3">
										<input class="btn mybutton topwidth" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" />
									</div>
								</div>
							</td>
						</tr>
					</table>
					<script type="text/javascript">
						getUpdateTujuan(cabangasal.value);
					</script>
					</form>
					<table>
						<tr><td colspan=9 align='center' height=20><span id='loading_proses' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='909090' size=2>sedang memposes...</font></span></td></tr>
				    	<tr>
							<td width='100%' align='right' colspan=8 valign='bottom'>
					        	<div style="float: left;">
					          	<!-- BEGIN TOMBOLACTION -->
					          	<input type="button" value="Tambah Jadwal" onclick="window.open('{U_ADD}','_self');"/>
					          	<!-- END TOMBOLACTION -->
					        	</div>
				        		<div style="float: right;">{PAGING}</div>
				      		</td>
						</tr>
					</table>
					<table  class="table table-hover table-bordered table-responsive" style="margin-top: 20px;">
					
					<tr>
			       		<th width=20>No</th>
						<th width=50>Jam</th>
						<th width=30>Kode Jadwal</th>
						<th width=100>Kendaraan</th>
						<th width=100>Sopir</th>
						<th width=20>Kursi</th>
						<th width=50>{KOLOM_STATUS}</th>
						<th width=150>Remark</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       	<td align="right">{ROW.no}</td>
						<td align="center">{ROW.jam}</td>
				       	<td align="left">{ROW.kode}</td>
						<td align="left">{ROW.nopol}</td>
						<td align="left">{ROW.kode_sopir}</td>
						 <td align="right">{ROW.kursi}</td>
						 <td align="center">{ROW.status}</td>
						 <td align="left">{ROW.remark}</td>
				     </tr>  
				     <!-- END ROW -->
					 {NO_DATA}
			    </table>
			    <table >
						<tr>
			        <td width='100%' align='right' colspan=8 valign='bottom'>
			          <div style="float: left;">
			            <!-- BEGIN TOMBOLACTION -->
			            <input type="button" value="Tambah Jadwal" onclick="window.open('{U_ADD}','_self');" />
			            <!-- END TOMBOLACTION -->
			          </div>
			          <div style="float: right;">{PAGING}</div>
			        </td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>