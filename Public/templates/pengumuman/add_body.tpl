<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('judul_invalid');
	Element.hide('pengumuman_invalid');
	
	kode			= document.getElementById('kode');
	judul			= document.getElementById('judul');
	pengumuman= document.getElementById('pengumuman');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(judul.value==''){
		valid=false;
		Element.show('judul_invalid');
	}
	
	if(pengumuman.value==''){
		valid=false;
		Element.show('pengumuman_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<form name="frm_data_mobil" action="{U_PENGUMUMAN_ADD_ACT}" method="post" onSubmit='return validateInput();'>
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="center">
				<table width='100%' cellspacing="0">
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td align='center' valign='top' width='400'>
							<table width='600'>   
								<tr>
									<td colspan=3><h2>{JUDUL}</h2></td>
								</tr>
								<tr>
						      <input class="form-control"  type="hidden" name="id_pengumuman" value="{ID_PENGUMUMAN}">
						      <input class="form-control"  type="hidden" name="kode_old" value="{KODE_OLD}">
									<td width='200' class="pad10 td-title">Kode Pengumuman*</td><td width='5'></td>
									<td class="pad10">
										<input placeholder="Kode Pengumuman" class="form-control"  type="text" id="kode" name="kode" value="{KODE}" maxlength=50 onChange="Element.hide('kode_invalid');">
										<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      		<td class="pad10 td-title">Judul*</td><td class="pad10"></td>
									<td class="pad10">
										<input  placeholder="Judul" class="form-control"  type="text" id="judul" name="judul" value="{JUDUL_PENGUMUMAN}" maxlength=100 onChange="Element.hide('judul_invalid');">
										<span id='judul_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    	</tr>
								<tr>
						      	<td valign='top' class="pad10 td-title">Pengumuman*</td>
									<td  valign='top'></td>
									<td class="pad10">
										<textarea placeholder="Pengumuman" class="form-control" name="pengumuman" id="pengumuman" cols="30" rows="3"  onChange="Element.hide('pengumuman_invalid');">{PENGUMUMAN}</textarea>
										<span id='pengumuman_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
								</tr>
								<tr>
						      	<td class="pad10 td-title">Pembuat Pengumuman</td><td class="pad10"></td>
									<td class="pad10">
										<b>{PEMBUAT}<b>
									</td>
						   		</tr>
								<tr>
						      <td class="pad10 td-title">Waktu Pembuatan</td><td class="pad10"></td>
									<td class="pad10">
										<b>{WAKTU_BUAT}<b>
									</td>
						    </tr>
							</table>
						</td>
					</tr>
					         
				</table>
				</td>
			</tr>
			<tr>
				<td align='center' valign='middle' style="padding-bottom: 50px; padding-top: 20px;">
				<input type="hidden" name="mode" value="{MODE}">
			  	<input type="hidden" name="submode" value="{SUB}">
				<div class="col-md-6 col-md-offset-3">
					<div class="col-md-6">
						<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
					</div>
					<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
				</div>
			</td>
			</tr>
			</table>
			</form>
		</div>
	</div>
</div>