// global

var sid;
var DataRuteBtnSimpan,DataRuteBtnNo;

function ValidasiAngka(objek){
	temp_nilai=objek.value*0;
	jenis_rute_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(jenis_rute_objek+" harus angka!");
		exit;
	}
	
}

function setKode(){
	asal		= document.getElementById("asal").value;
	tujuan	= document.getElementById("tujuan").value;
	jam			= document.getElementById("jam").value;
	menit		= document.getElementById("menit").value;
	
	if(menit=="00") menit="";
	
	document.getElementById("txt_kode").value=asal+"-"+tujuan+jam+menit;
}

function SimpanRute(){
	
	txt_kode			=document.getElementById("txt_kode");
	status_aktif	=document.getElementById("status");
	asal					=document.getElementById("asal");
	kode_rute_old	=document.getElementById("kode_rute_old");
	tujuan				=document.getElementById("tujuan");
	jam						=document.getElementById("jam");
	menit					=document.getElementById("menit");
	nopol					=document.getElementById("no_pol");
	harga					=document.getElementById("harga_tiket");
	layout_kursi	=document.getElementById("layout_kursi");
	
	//memeriksa apakah field harga tiket valid
	if(harga_tiket.value==''){
		alert("Harga tiket tidak boleh kosong!");
		document.forms.f_data_rute.harga_tiket.focus();
		return false;
	}
	
	ValidasiAngka(harga_tiket);
	
	kode	=txt_kode.value;

	new Ajax.Request("rute_detail.php?sid="+sid, 
  {
    asynchronous: true,
    method: "get",
    parameters:
			"kode_rute="+kode+
			"&kode_rute_old="+kode_rute_old.value+
			"&asal="+asal.value+
			"&tujuan="+tujuan.value+
			"&jam="+jam.value+
			"&menit="+menit.value+
			"&nopol="+nopol.value+
			"&harga="+harga.value+
			"&layout_kursi="+layout_kursi.value+
			"&status_aktif="+status_aktif.value+
			"&mode=simpan_rute"+
			"&submode="+aksi.value,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
		},
    onSuccess: function(request) 
    {
			if(request.responseText=="duplikasi"){
				alert("Data gagal disimpan,Kode Jadwal yang anda masukkan sudah ada dalam sistem!");
			}
			else if(request.responseText=="sukses"){
				alert("Data rute berhasil disimpan!");
				
			}

		},
    onFailure: function(request) 
    {
       
    }
  })      
}

function init(e) {
  // inisialisasi variabel
	
	//dialog data rute_________________________
	DataRuteBtnSimpan = document.getElementById("DataRuteBtnSimpan");
	DataRuteBtnNo = document.getElementById("DataRuteBtnNo");
	
  aksi					=document.getElementById("aksi");
	txt_kode			=document.getElementById("txt_kode");
	status_aktif	=document.getElementById("status");
	asal					=document.getElementById("asal");
	kode_rute_old	=document.getElementById("kode_rute_old");
	tujuan				=document.getElementById("tujuan");
	jam						=document.getElementById("jam");
	menit					=document.getElementById("menit");
	nopol					=document.getElementById("no_pol");
	harga					=document.getElementById("harga_tiket");
	layout_kursi	=document.getElementById("layout_kursi");
	sid						=document.getElementById("sid").value;
}

window.onload=init;