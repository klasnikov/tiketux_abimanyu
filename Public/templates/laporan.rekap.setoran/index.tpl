<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<!-- HEADER -->
			<div class="">
				<div class="col-md-3"><div class="bannerjudulw"><div class="bannerjudul">Laporan Rekap Uang</div></div></div>
				<div class="col-md-9">
					<div class="col-md-4" style="text-align: left;">
						Periode:<br /><input  class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
					</div>
					<div class="col-md-4" style="text-align: left;">
						s/d<br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
					</div>
					<div class="col-md-4">
						<div class="input-group" style="width: 100%; margin-top: 13px;">
					      <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
					      <span class="input-group-btn">
					        <input class="tombol form-control" type="submit" value="cari" />
					      </span>
					    </div><!-- /input-group -->
					</div>
				</div>
			</div>
			<br>
			<!--<center><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></center>-->
			<br>
			<br>
			<br>
			<!-- HEADER END -->
			<div class="table-responsive">
			<table class="table table-bordered table-hover table-responsive" cellspacing="1" cellpadding="1">
				<tr>
					<th width="50" rowspan="2">No.</th>
					<th width="150" rowspan="2">Waktu</th>
					<th width="200" rowspan="2">#Resi</th>
					<th class="thintop" colspan="3">Penumpang</th>
					<th class="thintop" colspan="2">Paket</th>
					<th width="150" rowspan="2">Biaya Op.</th>
					<th width="150" rowspan="2">Total Setor</th>
					<th width="200" rowspan="2">Act</th>
				</tr>
				<tr>
					<th class="thintop" width="100">Qty</th>
					<th class="thintop" width="100">Diskon</th>
					<th class="thintop" width="100">Setoran</th>
					<th class="thintop" width="100">Qty</th>
					<th class="thintop" width="100">Setoran</th>
			  </tr>
			  <!-- BEGIN ROW -->
			  <tr class="{ROW.odd}">
					<td align="center">{ROW.no}</td>
					<td align="center">{ROW.waktu_setor}</td>
					<td align="center">{ROW.no_resi}</td>
					<td align="right">{ROW.jum_pnp}</div></td>
					<td align="right">{ROW.disc_pnp}</div></td>
					<td align="right">{ROW.omz_pnp}</div></td>
					<td align="right">{ROW.jum_pkt}</div></td>
					<td align="right">{ROW.omz_pkt}</div></td>
					<td align="right">{ROW.biaya}</div></td>
					<td align="right">{ROW.total_setor}</div></td>
					<td align="center">{ROW.act}</div></td>
			  </tr>
			  <!-- END ROW -->
			</table>
			</div>
			<!-- BEGIN NO_DATA -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA -->
		</div>
	</div>
</div>
