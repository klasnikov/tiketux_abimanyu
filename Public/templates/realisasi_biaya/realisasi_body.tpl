<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">

  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");

  function getUpdateTujuan(asal){
  	  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

	  if(document.getElementById('rewrite_tujuan'))
	  {
		  document.getElementById('rewrite_tujuan').innerHTML = "";
	  }
	  new Ajax.Updater("rewrite_tujuan","pengaturan_realisasi_biaya.php?sid={SID}",
			  {
				  asynchronous: true,
				  method: "get",
				  parameters: "asal=" + asal + "&mode=get_tujuan",
				  onLoading: function(request)
				  {
					  //Element.show('loading_tujuan');
				  },
				  onComplete: function(request)
				  {
					  //Element.hide('loading_tujuan');

				  },
				  onFailure: function(request)
				  {
					  assignError(request.responseText);
				  }
			  });
  }

  (function($){
	  $(function(){
		  $("#tanggal_mulai").datepicker({
			  dateFormat:"dd-mm-yy"
		  });
	  });

  })(jQuery);
</script>

<!-- dialog Realisasi-->
<div dojoType="dialog" id="realisasi_dialog" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table class="table">
			<tr>
				<td bgcolor='ffffff'>
					<table>
						<tr><td><h2>Realisasi Biaya</h2></td></tr>
						<tr><td><div id="rewritedialogrealisasi"></div></td></tr>
					</table>
					<span id='progress_dialog_realisasi' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
					<br>
				</td>
			</tr>
		</table>
	</form>
</div>
<!--END dialog Realisasi-->
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 	<td class="whiter" valign="middle" align="center">		
					<div class="col-md-3" style="text-align: left;">
						<div class="bannerjudul">Realisasi BIaya</div>
					</div>
					<form action="{ACTION_CARI}" method="post">
						<div class="col-md-9">
							<div class="col-md-3">
								<input class="form-control input-sm" id="tanggal_mulai" name="tanggal_mulai" type="text" value="{TGL_AWAL}">
							</div>
							<div class="col-md-3">
								<select class="form-control input-sm" name='cabangasal' id='cabangasal' onChange="getUpdateTujuan(this.value)">{OPT_CABANG}</select>
							</div>
							<div class="col-md-3">
								<div id='rewrite_tujuan'></div>
								<span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span>

							</div>
							<div class="col-md-3">
								<input class="btn btn-sm pull-left" type="submit" value="Cari" />
							</div>
						</div>
					</form>
				</td>
				<table width='100%' class="border table table-hover table-bordered">
				    <tr>
				       	<th rowspan="2" style="text-align:center;">No</th>
						<th rowspan="2" style="text-align:center;">No SPJ</th>
						<th colspan="4" style="text-align:center;">Release</th>
						<th rowspan="2" style="text-align:center;">Jenis Release</th>
						<th colspan="4" style="text-align:center;">Realisasi</th>
						<th rowspan="2" style="text-align:center;">WaktuRealisasi</th>
						<th rowspan="2" style="text-align:center;">Action</th>
					</tr>
					<tr>
						<th>BBM</th>
						<th>Parkir</th>
						<th>Tol</th>
						<th>Sopir</th>
						<th>BBM</th>
						<th>Parkir</th>
						<th>Tol</th>
						<th>Sopir</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <form action="{EDIT_ACTION}" method="post">
				     <tr class="{ROW.odd}">
				       	<td>
				       		<div align="right">{ROW.NO}</div>
				       		<input type="hidden" name="page" value="{idx_page}">
				       	</td>
						<td>
							{ROW.no_spj}
						</td>
				       	<td>
				       		<div align="right">{ROW.bbm_release}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.parkir_release}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.tol_release}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.sopir_release}</div>
				       	</td>
				       	<td>
				       		{ROW.jenis_release}
				       	</td>
				       	<td>
				       		<div align="right">{ROW.bbm_realisasi}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.parkir_realisasi}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.tol_realisasi}</div>
				       	</td>
				       	<td>
				       		<div align="right">{ROW.sopir_realisasi}</div>
				       	</td>
				       	<td>
				       		{ROW.waktu_realisasi}
				       	</td>
				       	<td align="middle">
				       		{ROW.realisasi_action}
				       	</td>

				     </tr>  
				     </form>
				     <!-- END ROW -->
			    </table>
			    <table class="table" width="100%">
					<tr>
						<td class="mytd" width='70%' align='right'>{PAGING}</td>
					</tr>
				</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
<script>
var realisasi_dialog;

function init(e) {
	
	//control dialog box SPJ
	realisasi_dialog = dojo.widget.byId("realisasi_dialog");
	
}
dojo.addOnLoad(init);

function setDialogRealisasi(NoSPJ)
{
	new Ajax.Updater("rewritedialogrealisasi","pengaturan_realisasi_biaya.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters:
          "mode=realisasi_dialog"+"&NoSPJ="+NoSPJ,
        onLoading: function(request) 
        {
          Element.show('progress_dialog_realisasi');
        },
        onComplete: function(request) 
        {
          Element.hide('progress_dialog_realisasi');
        },
        onFailure: function(request) 
        { 
        }
    });

	realisasi_dialog.show();
}

function simpanRealisasi()
{
	NoSPJ  = document.getElementById('NoSPJ').value;  
	bbm    = document.getElementById('bbm').value;  
	tol    = document.getElementById('tol').value;  
	sopir  = document.getElementById('sopir').value;  
	parkir = document.getElementById('parkir').value;  

	new Ajax.Request("pengaturan_realisasi_biaya.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters:
          "mode=simpan_realisasi"+"&NoSPJ="+NoSPJ+
          "&bbm="+bbm+"&tol="+tol+"&parkir="+parkir+"&sopir="+sopir,
        onLoading: function(request) 
        {
          Element.show('progress_dialog_realisasi');
        },
        onComplete: function(request) 
        {
          Element.hide('progress_dialog_realisasi');
        },
        onFailure: function(request) 
        { 
        }
    });
location.reload();
}
</script>