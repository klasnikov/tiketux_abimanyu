<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
	//redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################
class Area{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	var $WAKTU_BACKUP;

	//CONSTRUCTOR
	function AREA(){
		$this->ID_FILE="C-ARE";
	}

	function generateIdArea()
	{

		global $db;

		$sql = "select * from maps_area order by id_area desc limit 1";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$latest_id = $row['id_area'];
		$counter= substr($latest_id,1,4)+1;

		return "A" . sprintf('%04d', $counter);

	}

	
  	function getOptKota($KodeKota="")
  	{
  		global $db;

  		$sql="select * from tbl_md_kota";
	    
	    if(!$result = $db->sql_query($sql))
	    {
	      	echo("Err:".__LINE__);exit;
	    }
	    else
	    {
			while($row = $db->sql_fetchrow($result))
			{
				if($row['KodeKota'] == $KodeKota)
				{
					$selected = "selected";
				}
				else
				{
					$selected = "";
				}

				$opt .= "<option value='".$row['KodeKota']."' $selected >".$row['NamaKota']."</option>";
			}

			return $opt;
		}
  	}

  	function setCOmboArea($jenis_biaya="")
  	{
  		global $db;

  		$sql="select * from maps_area";
	    
	    if(!$result = $db->sql_query($sql))
	    {
	      	echo("Err:".__LINE__);exit;
	    }
	    else
	    {
			while($row = $db->sql_fetchrow($result))
			{
				switch($jenis_biaya)
				{
					case "jemput":
						$opt .= "<option value='".$row['id_area']."|".$row['biaya_jemput']."' $selected >".$row['nama_area']."</option>";
					break;
					case "antar":
						$opt .= "<option value='".$row['id_area']."|".$row['biaya_antar']."' $selected >".$row['nama_area']."</option>";
					break;
				}
					
				
			}

			return $opt;
		}
  	}

}
?> 
