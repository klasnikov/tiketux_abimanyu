<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
	// komponen khusus dojo 
	dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">			
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
							<div class="bannerjudul">
								Laporan Omzet Sopir
							</div>
							</td>
							<td align='left' valign='middle'>
								<div class="col-md-3">
									Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								</div>
								<div class="col-md-3">
									s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								</div>
								<div class="col-md-3">
									Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
								</div>
								<div class="col-md-3">
									<input class="btn mybutton topwidth" style="margin-top: 16px;" type="submit" value="cari" />
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
							<td>
								<table width='100%'>
									<tr>
										<td class="pad10" align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-hover table-bordered table-responsive" width='100%' >
					<tr>
						 <th width=30>No</th>
						 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Nama Sopir</a></th>
						 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>NRP</a></th>
						 <th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Total Hari Kerja</a></th>
						 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Total Ritase</a></th>
						 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Total Penumpang</a></th>
						 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Total Omzet</a></th>
						 <th width=200><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Total Insentif</a></th>
						 <th width=100>Action</th>
					 </tr>
					 <!-- BEGIN ROW -->
					 <tr class="{ROW.odd}">
						 <td ><div align="right">{ROW.no}</div></td>
						 <td ><div align="left">{ROW.nama}</div></td>
						 <td ><div align="left">{ROW.nrp}</div></td>
						 <td ><div align="right">{ROW.total_hari_kerja}</div></td>
						 <td ><div align="right">{ROW.total_jalan}</div></td>
						 <td ><div align="right">{ROW.total_penumpang}</div></td>
						 <td ><div align="right">{ROW.total_omzet}</div></td>
						 <td ><div align="right">{ROW.total_insentif}</div></td>
						 <td ><div align="center">{ROW.act}</div></td>
					 </tr>
					 <!-- END ROW -->
					</table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>