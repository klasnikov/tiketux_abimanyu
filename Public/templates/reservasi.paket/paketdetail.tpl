<div style="margin-left: 5px;">
	<input type="button" value="INPUT DATA BARU" style="width:150px;height: 30px;" onclick="document.getElementById('showdatadetail').innerHTML='';"><br>
	<input type="hidden" id="noresi" value="{RESI}"/>
	
	<h2>#Resi: {RESI}</h2>
	<input type="button" value="C E T A K   R E S I" style="width:250px;height: 50px;" onclick="CetakResi(0);"><br><br>
	<h2>Tujuan: {TUJUAN}</h2>
	<span class="resvcargobooklabel">#Jadwal</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{KODE_JADWAL}</span></br>	
	<h2>Data Pengirim</h2>
	<span class="resvcargobooklabel">Telp Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{TELP_PENGIRIM}</span></br>	
	<span class="resvcargobooklabel">Nama Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{NAMA_PENGIRIM}</span></br>
	<span class="resvcargobooklabel">Alamat Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{ALAMAT_PENGIRIM}</span></br>
	
	<h2>Data Penerima</h2>
	<span class="resvcargobooklabel">Telp Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{TELP_PENERIMA}</span></br>	
	<span class="resvcargobooklabel">Nama Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{NAMA_PENERIMA}</span></br>
	<span class="resvcargobooklabel">Alamat Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{ALAMAT_PENERIMA}</span></br>	
	
	<h2>Data Barang</h2>
	<span class="resvcargobooklabel">Asal (Origin)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{ASAL}</span></br>
	<span class="resvcargobooklabel">Tujuan (Destination)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{TUJUAN}</span></br>
	<span class="resvcargobooklabel">Jenis</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{JENIS_BARANG}</span></br>
	<span class="resvcargobooklabel">Service</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{LAYANAN}</span></br>
	<span class="resvcargobooklabel">Koli</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{KOLI} Pax</span></br>
  <span class="resvcargobooklabel">Perhitungan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{IS_VOLUMETRIK}</span></br>
  <!-- BEGIN VOLUMETRIK -->
  <span class="resvcargobooklabel">Dimensi (P x L x T)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{VOLUMETRIK.dimensi}</span></br>
   <!-- END VOLUMETRIK -->
  <span class="resvcargobooklabel">{LABEL_BERAT}</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{BERAT}</span><br>
	<span class="resvcargobooklabel">Biaya Kirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp.{BIAYA_KIRIM}</span></br>						
	<span class="resvcargobooklabel">Isi Barang (Description)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{DESKRIPSI_BARANG}</span></br>
	<hr>
	<span class="resvcargobooklabel">CSO</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{CSO}</span></br>
	<span class="resvcargobooklabel">Waktu Terima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{WAKTU_TERIMA}</span></br>
	
	<!-- BEGIN INFO_MUTASI -->
	<hr>
	<h2>Mutasi</h2>
	<span class="resvcargobooklabel">CSO</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.cso}</span></br>
	<span class="resvcargobooklabel">Waktu Mutasi</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.waktu}</span></br>
	<span class="resvcargobooklabel">Tgl Lama</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.tgl}</span></br>
	<span class="resvcargobooklabel">Jadwal Lama</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.jadwal}</span></br>
	<!-- END INFO_MUTASI -->
	
	<!-- BEGIN INFO_PENGAMBILAN -->
	<hr>
	<h2>Pengamblan</h2>
	<span class="resvcargobooklabel">Waktu Pengambilan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_PENGAMBILAN.waktu}</span></br>
	<span class="resvcargobooklabel">Nama Pengambil</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_PENGAMBILAN.nama}</span></br>
	<span class="resvcargobooklabel">Telp</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_PENGAMBILAN.telp}</span></br>
	<span class="resvcargobooklabel">Petugas Pemberi</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_PENGAMBILAN.cso}</span></br>
	<!-- END INFO_PENGAMBILAN -->
	<br><br>
	<center>
		<input type="button" value="B A T A L" style="width:250px;height: 30px;" onclick="{ACTION_BATAL}"><br><br>
		<!-- BEGIN TOMBOL_MUTASI -->
		<input type="button" value="M U T A S I" id="tombolmutasipaket" style="width:250px;height: 30px;" onclick="{TOMBOL_MUTASI.action}"><br>
		<!-- END TOMBOL_MUTASI -->
	</center>
	<br><br>
</div>