<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}

function validateInput(){
	
	valid	=	true;
	
	jumlah		= document.getElementById('jumlah').value;
	
	if(jumlah=='' || jumlah=="0"){
		valid	= false;
		alert("Anda belum memasukkan JUMLAH TOP UP DEPOSIT!");
	}
	
	if(valid){
		jumlah_formated	= FormatUang(jumlah,".");
		
		if(confirm("Anda akan menambahkan deposit sebesar Rp." + jumlah_formated + ". Klik [OK] untuk melanjutkan, atau [CANCEL] untuk membatalkan")){
			return true;	
		}
		else{
			return false;
		}
		
	}
	else{
		return false;
	}
}

</script>

<div class="container" style="width: 30%;">
	<div class="row">
		<div class="col-md-12 box">
			<form name="forminput" action="{U_ACT_FORM}" method="post" onSubmit='return validateInput();'>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr class='' height=40>
				<td align='center' valign='middle' class="bannerjudulw" style="color: #787878; font-size: 24px;">Tambah Deposit</td>
			</tr>
			<tr>
				<td class="whiter" valign="middle" align="center">
				</br>
				<table>
					<tr>
						<td align='left' valign='top'>
							<input type="hidden" name="mode" value="addprocess">
							Jumlah Top Up Deposit:<br><br>
							<input class="form-control" placeholder="Jumlah" type="text" id="jumlah" name="jumlah" onkeypress='validasiAngka(event);' style="text-align: right;">&nbsp;
							<input class="topwidth mybutton paper" style="margin-top: 0px;" type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
			</form>
		</div>
	</div>
</div>
