<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function setSortId(){
        listHrefSort = [{ARRAY_SORT}];

        for (i=0;i<listHrefSort.length;i++){
            document.getElementById("sort"+(i+1)).href=listHrefSort[i];
        }

    }

    function validasi(evt){
        var theEvent = evt || window.event;

        var key = theEvent.keyCode || theEvent.which;

        key = String.fromCharCode(key);

        var regex = /[0-9]/;

        if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
                [evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            theEvent.preventDefault();
        }
    }

    function init(e){
        setSortId();
        //control dialog paket
        dlg	= dojo.widget.byId("dialog");
        dlg_btn_cancel = document.getElementById("dialogcancel");
    }

    dojo.addOnLoad(init);

    function TOPUP(kodecabang){
        document.getElementById("cabang").value = kodecabang;
        dlg.show();
    }

    function create() {
        topup = document.getElementById("topup").value;
        kode = document.getElementById("cabang").value;
        if(topup == ""){
            alert("Jumlah Tidak Boleh Kosong");
        }else{
            new Ajax.Request("biayaharian.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters:
                "mode=topup"+"&saldo="+topup+"&kode="+kode,
                onLoading: function(request){
                    popup_loading.show();
                    console.log("onloading");
                },
                onComplete: function(request){
                    popup_loading.hide();
                    console.log("oncomplete");
                },
                onSuccess: function(request) {
                    if(request.responseText > 0){
                        alert('Saldo Berhasil Di Top Up!');
                        dlg.hide();
                        document.getElementById("myForm").reset();
                        window.location.reload();
                    }else{
                        alert("Terjadi Kegagalan");
                    }
                },
                onFailure: function(request)
                {
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                    console.log("onfailed");
                }
            })
        }
    }

</script>
<!--dialog -->
<div dojoType="dialog" id="dialog" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;" id="myForm">
        <table width="500" style="background: white;">
            <tr>
                <td>
                    <input type="button" style="border-radius: 0;" class="button-close" id="dialogcancel" onClick="dlg.hide();" value="&nbsp;X&nbsp;">
                    <h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Top Up Saldo Cabang</h4>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <table class="table">
                        <tr>
                            <td align='right'>
                                Jumlah Top Up :
                            </td>
                            <td>
                                <input type="hidden" id="cabang">
                                <input type="text" class="form-control" id="topup" onkeypress="validasi(event);">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <input class="btn mybutton paper" type="button" onclick="create();" id="dialogproses" value="&nbsp;Proses&nbsp;">
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog-->

<div class="container">
    <div class="row">
        <div class="col-md-12 box">
            <table class="table table-hover" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="whiter" valign="middle" align="center">
                        <table width='100%' cellspacing="0">
                            <tr class='' height="">
                                <td align='center' valign='middle' class="bannerjudul">Biaya Harian </td>
                                <td colspan=2 align='right' class="bannernormal" valign='middle'>
                                    <form action="{ACTION_CARI}" method="post">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
									      <span class="input-group-btn">
									        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
									      </span>
                                        </div><!-- /input-group -->
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td class="mytd" align='left'>
                                </td>
                                <td class="mytd" width='70%' align='right'>
                                    {PAGING}
                                </td>
                            </tr>
                        </table>
                        <table width='100%' class="border table table-hover table-bordered">
                            <tr>
                                <th >No</th>
                                <th ><a class="th" id="sort1" href='#'>Kode</a></th>
                                <th ><a class="th" id="sort2" href='#'>Nama cabang</a></th>
                                <th ><a class="th" id="sort3" href='#'>Alamat</a></th>
                                <th ><a class="th" id="sort4" href='#'>Kota</a></th>
                                <th ><a class="th" id="sort5" href='#'>Saldo</a></th>
                                <th class="center">Action</th>
                            </tr>
                            <!-- BEGIN ROW -->
                            <tr class="{ROW.odd}">
                                <td><div align="center">{ROW.no}</div></td>
                                <td><div align="left">{ROW.kode}</div></td>
                                <td><div align="left">{ROW.nama}</div></td>
                                <td><div align="left">{ROW.alamat}</div></td>
                                <td><div align="left">{ROW.kota}</div></td>
                                <td><div align="left">{ROW.saldo}</div></td>
                                <td><div align="center"><a onclick="{ROW.topup}">TOP UP</a> &nbsp;|&nbsp;<a onclick="{ROW.detail}">Detail</a></div></td>
                            </tr>
                            <!-- END ROW -->
                            {NO_DATA}
                        </table>
                        <table class="table" width="100%">
                            <tr>
                                <td class="mytd" align='left'>
                                </td>
                                <td class="mytd" width='70%' align='right'>
                                    {PAGING}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>