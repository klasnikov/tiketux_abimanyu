<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }

    function cetak(id) {
        window.open('reservasi.biayalain.php?sid={SID}&IdBiayaHarian='+id+'&mode=cetak','_blank',"width=300,height=350");
    }

    function validasi(evt){
        var theEvent = evt || window.event;

        var key = theEvent.keyCode || theEvent.which;

        key = String.fromCharCode(key);

        var regex = /[0-9]/;

        if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
                [evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            theEvent.preventDefault();
        }
    }

    function showDialog() {
        saldo = document.getElementById("saldo").value;
        if(saldo > 0){
            dlg.show();
        }else{
            alert("Maaf Saldo Rp. 0 Tidak Bisa Membuat Biaya Harian");
        }

    }

    function create(){
        jenis = document.getElementById("jenis").value;
        penerima = document.getElementById("penerima").value;
        jumlah = document.getElementById("jumlah").value;
        keterangan = document.getElementById("keterangan").value;
        saldo = document.getElementById("saldo").value;

        if(jenis == ""){
            alert("Jenis Biaya Tidak Boleh Kosong");
        }else if(penerima == ""){
            alert("Penerima Tidak Boleh Kosong");
        }else if(jumlah == ""){
            alert("Jumlah Tidak Boleh Kosong");
        }else if(parseFloat(jumlah) > parseFloat(saldo)) {
            alert("Saldo"+saldo+" "+jumlah);
        }else if(keterangan == ""){
            alert("Keterangan Tidak Boleh Kosong");
        }else{
            sisa = saldo-jumlah;
            new Ajax.Request("reservasi.biayalain.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters:
                "mode=create"+"&jenis="+jenis+"&penerima="+penerima+"&jumlah="+jumlah+"&keterangan="+keterangan+"&saldo="+sisa,
                onLoading: function(request){
                    popup_loading.show();
                    console.log("onloading");
                },
                onComplete: function(request){
                    popup_loading.hide();
                    console.log("oncomplete");
                },
                onSuccess: function(request) {
                    if(request.responseText > 0){
                        alert('Biaya Harian Berhasil dibuat!');
                        dlg.hide();
                        document.getElementById("myForm").reset();
                        window.location.reload();
                    }else{
                        alert("Terjadi Kegagalan");
                    }
                },
                onFailure: function(request)
                {
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                    console.log("onfailed");
                }
            })
        }
    }

    function init(e){
        //control dialog paket
        dlg						= dojo.widget.byId("dialog");
        dlg_btn_cancel = document.getElementById("dialogcancel");
    }

    dojo.addOnLoad(init);
</script>
<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialog" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;" id="myForm">
        <table width="500" style="background: white;">
            <tr>
                <td>
                    <input type="button" style="border-radius: 0;" class="button-close" id="dialogcancel" onClick="dlg.hide();" value="&nbsp;X&nbsp;">
                    <h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Biaya Harian</h4>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <table class="table">
                        <tr>
                            <td align='right'>
                                Jenis Biaya :
                            </td>
                            <td>
                                <select class="form-control" id='jenis' name='jenis' >
                                    <option value="">-- PILIH JENIS BIAYA --</option>
                                    <option value="ATK">ATK</option>
                                    <option value="RTK">RTK</option>
                                    <option value="Perawatan/Peralatan">Perawatan/Peralatan</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Penerima :</td>
                            <td>
                                <input type="text" class="form-control" id="penerima" name="penerima">
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Jumlah :</td>
                            <td>
                                <input type="text" id="jumlah" name="jumlah" class="form-control" onkeypress="validasi(event);">
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Keterangan</td>
                            <td>
                                <textarea rows="3" class="form-control" id="keterangan" name="keterangan"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <input class="btn mybutton paper" type="button" onclick="create();" id="dialogproses" value="&nbsp;Proses&nbsp;">
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog create voucher-->
<div class="container">
    <div class="row">
        <div class="col-md-12 box">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="whiter" valign="middle" align="left">
                        <form action="{ACTION_CARI}" method="post">
                            <!--HEADER-->
                            <table width='100%' cellspacing="0">
                                <tr class='' height=40>
                                    <td align='center' valign='top' class="bannerjudulw"><div class="bannerjudul">Biaya Harian</div></td>
                                    <td align='left' valign='middle'>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-1">
                                                <br />Tgl:<br /><input class='form-control' readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
                                            </div>
                                            <div class="col-md-4">
                                                <br />s/d<br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
                                            </div>
                                            <div class="col-md-4 topwidth">
                                                <br /><div class="input-group" style="width: 100%;">
                                                    <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									      <span class="input-group-btn">
									        <input class="tombol form-control" name="btn_cari" type="submit" value="cari" />
									      </span>
                                                </div><!-- /input-group -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table style="margin-top: 20px; margin-bottom: 20px; width: 100%">
                                <tr>
                                    <td colspan="3" align="left">
                                        <b><h4>SALDO CABANG {CABANG} = Rp. {SALDOCABANG}</h4></b>
                                        <input type="hidden" id="saldo" value="{SALDO}">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom">
                                        <div style="float: left"><a href="#" onclick="showDialog();return false;"> + Tambah Biaya</a></div>
                                    </td>
                                    <td align="center">
                                        <img src="{TPL}/images/icon_msexcel.png"><a href='#' onClick="{EXPORT}">Cetak ke MS EXCEL</a>&nbsp;</td>
                                    </td>
                                    <td align="right">
                                        <div style="float: right;">{PAGING}</div>
                                    </td>
                                </tr>
                            </table>
                            <!-- END HEADER-->
                            <table width='100%' class="table table-bordered table-hover">
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Petugas</th>
                                    <th>Penerima</th>
                                    <th>Biaya</th>
                                    <th>Jumlah</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                                <!-- BEGIN ROW -->
                                <tr class="{ROW.odd}">
                                    <td align="center">{ROW.no}</td>
                                    <td align="center">{ROW.TglTransaksi}</td>
                                    <td align="left">{ROW.Petugas}</td>
                                    <td align="left">{ROW.Penerima}</td>
                                    <td align="left">{ROW.JenisBiaya}</td>
                                    <td align="left">{ROW.Jumlah}</td>
                                    <td align="left">{ROW.Keterangan}</td>
                                    <td align="center"><a onclick="cetak({ROW.id})"><img src="{TPL}/images/b_print.png"></a></td>
                                </tr>
                                <!-- END ROW -->
                            </table>
                            {NO_DATA}
                            <table width='100%'>
                                <tr>
                                    <td align='right' width='100%'>
                                        {PAGING}
                                    </td>
                                </tr>
                                <tr>
                                    <td align='left' valign='bottom' colspan=3>
                                        {SUMMARY}
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
