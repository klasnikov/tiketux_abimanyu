<html>
<head>
    <title>Cetak Struk</title>
    <style type="text/css">
        body {margin: 0; padding: 0; font-family: 'Calibri'; font-size: 16px;}
        html {margin: 0; padding: 0;}
        table.container {min-width: 230px; max-width: 230px; border-collapse: collapse;}
            /*table.container tr td {padding-left: 10px;}*/
        hr {border:1px dashed #ccc; margin: 2px 0}
        .text-lowercase {text-transform: lowercase;}
    </style>
</head>
<body onload="window.print(); window.close();">
<table class="container" cellpadding="0" >
<tr>
<td colspan="2"><hr></td>
</tr>
<tr>
<td colspan="2" align="center">PASTEUR TRANS</td>
</tr>
<tr>
<td colspan="2" align="center">Biaya Harian</td>
</tr>
<!-- ./header -->
<tr>
<td colspan="2"><hr></td>
</tr>
<tr>
<td valign="top">Petugas</td>
<td align="right">{PETUGAS}</td>
</tr>
<tr>
<td valign="top">Tanggal Buat</td>
<td align="right">{TGL}</td>
</tr>
<tr>
<td valign="top">Penerima</td>
<td align="right">{PENERIMA}</td>
</tr>
<tr>
<td>Cabang</td>
<td align="right">{CABANG}</td>
</tr>
<tr>
<td colspan="2"><hr></td>
</tr>
<tr>
<td>Jenis Biaya </td>
<td align="right">{JENIS}</td>
</tr>
<tr>
<td>Jumlah</td>
<td align="right">Rp. {JUMLAH}</td>
</tr>
<tr>
<td>Keterangan</td>
<td align="right">{KETERANGAN}</td>
</tr>
<tr>
<td colspan="2"><hr></td>
</tr>
<tr>
<td>Tanggal Cetak</td>
<td align="right">{CETAK}</td>
</tr>
<tr>
<td colspan="2"><hr></td>
</tr>
<tr>
<td colspan="2" align="center">Tanda Tangan Bukti</td>
</tr>
<tr>
<td height="75" valign="top">Petugas</td>
<td height="75" valign="top" align="right">Penerima</td>
</tr>
<tr>
<td>{PETUGAS}</td>
<td align="right">{PENERIMA}</td>
</tr>
</table>
</body>
</html>