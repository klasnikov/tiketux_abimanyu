<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
	// komponen khusus dojo
	dojo.require("dojo.widget.Dialog");

	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

	function setData(bulan){
		tahun	=document.getElementById('tahun').value;

		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}

	function getUpdateAsal(kota){

		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});
	}

	function getUpdateTujuan(asal){
		// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}",
				{
					asynchronous: true,
					method: "get",
					parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
					onLoading: function(request)
					{
					},
					onComplete: function(request)
					{
					},
					onFailure: function(request)
					{
						assignError(request.responseText);
					}
				});
	}

	function buatVoucher(){

		is_valid=true;

		if(nama.value==""){
			nama.style.background = 'red';
			is_valid = false;
		}

		if(telp.value==""){
			telp.style.background = 'red';
			is_valid = false;
		}

		if(komisi.value==""){
			komisi.style.background = 'red';
			is_valid = false;
		}

		if(tiket.value==""){
			tiket.style.background = 'red';
			is_valid = false;
		}

		if(mitra.value==""){
			mitra.style.background = 'red';
			is_valid = false;
		}

		if(!is_valid){
			exit;
		}

		new Ajax.Request("daftar_tiket_mitra.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=buatvoucher"+
			"&nama="+nama.value+
			"&telp="+telp.value+
			"&komisi="+komisi.value+
			"&tglberangkat="+tgl_berangkat.value+
			"&jam="+jam.value+
			"&mitra="+mitra.value+
			"&tiket="+tiket.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_input_tiket.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				alert('Error !!! Cannot Save');
				assignError(request.responseText);
			}
		});

	}

	function showDialogBuatVoucher(){
		nama.value="";
		telp.value="";
		jam.value="";
		tiket.value="";
		tgl_berangkat.value="";
		mitra.value="";
		tiket.value="";
		dlg_input_tiket.show();
	}

	function init(e){
		//control dialog paket
		dlg_input_tiket						= dojo.widget.byId("dialoginputtiket");
		dlg_input_tiket_btn_cancel = document.getElementById("dialoginputtiketcancel");

		//getUpdateTujuan(null);
	}

	dojo.addOnLoad(init);

	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
</script>
<!--dialog Create Voucher-->

<div dojoType="dialog" id="dialoginputtiket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width="500" style="background: white;">
			<tr>
				<td>
					<input type="button" style="border-radius: 0;" class="button-close" id="dialoginputtiketcancel" onClick="dlg_input_tiket.hide();" value="&nbsp;X&nbsp;">
					<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Buat Voucher</h4>
				</td>
			</tr>
			<tr>
				<td align='center'>
					<table class="table">
						<tr><td align='left'>Nama Penumpang</td><td><input type="text" class="form-control" id='nama' name='nama'></td></tr>
						<tr><td align='left'>Telepon</td><td><input type="text" class="form-control" id='telp' name='telp'></td></tr>
						<tr><td align='left'>Tgl.Berangkat</td><td><input class="form-control" id="tgl_berangkat" name="tgl_berangkat" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERANGKAT}"></td></tr>
						<tr><td align='left'>Jam Berangkat</td><td><input type="text" class="form-control" id='jam' name='jam' placeholder="00:00"></td></tr>
						<tr>
							<td align='left'>Mitra</td>
							<td>
								<select class="form-control" id='mitra'>
									<option value=''>-- Pilih --</option>
									<option value='1'>Mitra 1</option>
								</select>
							</td>
						</tr>
						<tr><td align='left'>Tiket Publish</td><td><input type="text" class="form-control" id='tiket' name='tiket'></td></tr>
						<tr><td align='left'>Komisi</td><td><input type="text" class="form-control" id='komisi' name='komisi'></td></tr>
						<tr>
							<td></td><td align="center">
								<input class="btn mybutton paper" type="button" onclick="buatVoucher();" id="dialogbuatvoucherproses" value="&nbsp;Proses&nbsp;">
							</td>
						</tr>
					</table>
					<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
					<br>
				</td>
			</tr>
		</table>
	</form>
</div>
<!--END dialog create voucher-->
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="whiter" valign="middle" align="left">
						<form action="{ACTION_CARI}" method="post">
							<!--HEADER-->
							<table width='100%' cellspacing="0">
								<tr><td colspan="2" class="bannerjudulw"><div class="bannerjudul" style="padding-left: 12px;">Daftar Tiket Mitra</div></td></tr>
								<tr class='' height=40>
									<td colspan="2" align='left' valign='middle'>
										<!--<div class="row">
									<div class="col-md-4">
										Kota:<br /><select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
									</div>
									<div class="col-md-4">
										Asal:&nbsp; <div id='rewrite_asal'></div>
									</div>
									<div class="col-md-4">
										Tujuan:&nbsp; <div id='rewrite_tujuan'></div>
									</div>
								</div>-->
										<div class="row">
											<div class="col-md-4">
												Tgl:&nbsp; <input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
											</div>
											<div class="col-md-4">
												s/d &nbsp; <input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
											</div>
											<div class="col-md-4">
												<div class="input-group" style="width: 100%; margin: 0; margin-top: 12px;">
													<input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									      <span class="input-group-btn"  style="padding: 0; margin: 0;">
									        <input class="form-control tombol" name="btn_cari" type="submit" value="cari" />
									      </span>
												</div><!-- /input-group -->
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="pad10" align='center' valign='middle'>
										<table>
											<tr>
												<td class="pad10">

												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										&nbsp;<a href='#' onClick="showDialogBuatVoucher();"><i class="fa fa-plus"></i> Input Data</a>&nbsp;
										<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
									</td>
									<td>
										<table width='100%'>
											<tr>
												<td align='right' valign='bottom'>
													{PAGING}
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END HEADER-->
							<table  class="table table-hover table-bordered table-responsive" style="margin-top: 20px;" width='100%' >
								<tr>
									<th width=30>No</th>
									<th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
									<th width=150><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Nama Penumpang</a></th>
									<th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Mitra</a></th>
									<th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Tiket Publish</a></th>
									<th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Petugas</a></th>
									<th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Komisi</a></th>
									<th width=70><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Act</a></th>
								</tr>
								<!-- BEGIN ROW -->
								<tr class="{ROW.odd}">
									<td align="right">{ROW.no}</td>
									<td align="center">{ROW.jadwal}</td>
									<td align="center">{ROW.nama}</td>
									<td align="center">{ROW.mitra}</td>
									<td align="center">{ROW.tiket}</td>
									<td align="center">{ROW.petugas}</td>
									<td align="right">{ROW.komisi}</td>
									<td align="center">{ROW.act}</td>
								</tr>
								<!-- END ROW -->
							</table>
							<table width='100%'>
								<tr>
									<td align='right' width='100%'>
										{PAGING}
									</td>
								</tr>
								<tr>
									<td align='left' valign='bottom' colspan=3>
										{SUMMARY}
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
