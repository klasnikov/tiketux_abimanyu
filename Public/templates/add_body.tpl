<center>
	<form action="{U_PROMO_ADD_ACT}" method="post">
	<br/><br/><font style="font-weight:bold">Promo Baru</font><br/><br/>
	<table width="500px">
		<tbody>
			<tr style="height:20px;">
				<td width="40%">Kode Promo:</td>
				<td width="60%"><input type="text" name="kode_promo" id="kode_promo"/></td>
			</tr>
			<tr style="height:20px;">
				<td width="30%">Nama Promo:</td>
				<td width="70%"><input type="text" name="nama_promo" id="nama_promo"/></td>
			</tr>
			<tr style="height:20px;">
				<td>Priority</td>
				<td><select name="priority" id="priority"><option value="1">high</option><option value="2">medium</option><option value="3">low</option></select></td>
			</tr>
			<tr style="height:20px;">
				<td>Asal</td>
				<td>
				<select name="asal" id="asal">
				<option value="all">All</option>
				{ASAL}
				</select>
				</td>
			</tr>
			<tr style="height:20px;">
				<td>Tujuan</td>
				<td>
				<select name="tujuan" id="tujuan">
				<option value="all">All</option>
				{TUJUAN}
				</select>
				</td>
			</tr>
			<tr style="height:20px;">
				<td>Mulai Jam</td>
				<td><input type="text" name="mulai_jam" id="mulai_jam" size="15"/>&nbsp;s/d&nbsp;<input type="text" name="akhir_jam" id="akhir_jam" size="15"/></td>
			</tr>
			<tr style="height:20px;">
				<td>Tanggal Mulai Promo</td>
				<td><input type="text" autocomplete="off" name="tanggal_mulai_promo" id="tanggal_mulai_promo" onClick="setYears(2010, 2020);showCalender(this, 'tanggal_mulai_promo');"/>
					<div id="calender_mulai_promo">
					<table id="calenderTable">
				        <tbody id="calenderTableHead">
				          <tr>
				            <td colspan="4" align="center">
					          <select onChange="showCalenderBody(createCalender(document.getElementById('selectYear').value,
					           this.selectedIndex, false));" id="selectMonth">
					              <option value="0">Jan</option>
					              <option value="1">Feb</option>
					              <option value="2">Mar</option>
					              <option value="3">Apr</option>
					              <option value="4">May</option>
					              <option value="5">Jun</option>
					              <option value="6">Jul</option>
					              <option value="7">Aug</option>
					              <option value="8">Sep</option>
					              <option value="9">Oct</option>
					              <option value="10">Nov</option>
					              <option value="11">Dec</option>
					          </select>
				            </td>
				            <td colspan="2" align="center">
							    <select onChange="showCalenderBody(createCalender(this.value, 
								document.getElementById('selectMonth').selectedIndex, false));" id="selectYear">
								</select>
							</td>
				            <td align="center">
							    <a href="#" onClick="closeCalender();"><font color="#003333" size="+1">X</font></a>
							</td>
						  </tr>
				       </tbody>
				       <tbody id="calenderTableDays">
				         <tr style="">
				           <td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td>
				         </tr>
				       </tbody>
				       <tbody id="calender"></tbody>
				    </table>
					</div>
				</td>
			</tr>
			<tr style="height:20px;">
				<td>Tanggal Akhir Promo</td>
				<td><input type="text" autocomplete="off" name="tanggal_akhir_promo" id="tanggal_akhir_promo" onClick="setYears(2010, 2020);showCalender(this, 'tanggal_akhir_promo');"/>
					<div id="akhir_promo">
					<table id="calenderTable">
				        <tbody id="calenderTableHead">
				          <tr>
				            <td colspan="4" align="center">
					          <select onChange="showCalenderBody(createCalender(document.getElementById('selectYear').value,
					           this.selectedIndex, false));" id="selectMonth">
					              <option value="0">Jan</option>
					              <option value="1">Feb</option>
					              <option value="2">Mar</option>
					              <option value="3">Apr</option>
					              <option value="4">May</option>
					              <option value="5">Jun</option>
					              <option value="6">Jul</option>
					              <option value="7">Aug</option>
					              <option value="8">Sep</option>
					              <option value="9">Oct</option>
					              <option value="10">Nov</option>
					              <option value="11">Dec</option>
					          </select>
				            </td>
				            <td colspan="2" align="center">
							    <select onChange="showCalenderBody(createCalender(this.value, 
								document.getElementById('selectMonth').selectedIndex, false));" id="selectYear">
								</select>
							</td>
				            <td align="center">
							    <a href="#" onClick="closeCalender();"><font color="#003333" size="+1">X</font></a>
							</td>
						  </tr>
				       </tbody>
				       <tbody id="calenderTableDays">
				         <tr style="">
				           <td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td>
				         </tr>
				       </tbody>
				       <tbody id="calender"></tbody>
				    </table>
					</div>
				</td>
			</tr>
			<tr style="height:20px;">
				<td>Discount</td>
				<td><input type="text" name="discount" id="discount"/></td>
			</tr>
			<tr style="height:20px;">
				<td>Target Discount</td>
				<td><select name="target_discount" id="target_discount"/><option value=0>All</option><option value=1>Member</option><option value=2>Non Member</option></select></td>
			</tr>
			<tr style="height:20px;">
				<td>Jumlah Minimum</td>
				<td><input type="text" name="jumlah_minimum" id="jumlah_minimum"/></td>
			</tr>
			<tr style="height:20px;">
				<td><br/></td>
				<td><br/><input type="submit" value="Simpan"/>&nbsp;&nbsp;&nbsp;<input type="button" value="Batal"/></td>
			</tr>
		</tbody>
	</table>
	</form>
</center>	