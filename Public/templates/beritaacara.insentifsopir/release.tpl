<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","reservasi.releaseinsentifsopir.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","reservasi.releaseinsentifsopir.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	
	function bayarInsentif(idba){
		
		isBayar = confirm("Anda akan melakukan pembayaran insentif sopir! Silahkan tekan OK untuk melanjutkan proses pembayaran insentif sopir");
		
		if(!isBayar) exit;
		
		new Ajax.Request("reservasi.releaseinsentifsopir.php?sid={SID}",{
    asynchronous: true,
    method: "post",
    parameters:
			"mode=bayar"+
			"&idba="+idba,
    onLoading: function(request){
			popup_loading.show();
    },
    onComplete: function(request){popup_loading.hide();},
    onSuccess: function(request) {
			if(request.responseText==0){
				Start("./reservasi.releaseinsentifsopir.cetak.php?sid={SID}&idba="+idba);
				window.location.reload();
			}
			else{
				alert("Terjadi kegagalan!");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
	}
	
	function init(e){	
		popup_loading	= dojo.widget.byId("popuploading");
		
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
		setSortId();
	}
	
	dojo.addOnLoad(init);
</script>

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<div class="row">
						<div class="col-md-4">
							<div class="bannerjudulw">
								<div class="bannerjudul">Pembayaran Insentif Sopir</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="col-md-4">
								Kota<br /><select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
							</div>
							<div class="col-md-4">
								Asal:<br /><div id='rewrite_asal'></div>
							</div>
							<div class="col-md-4">
								Tujuan:<br /><div id='rewrite_tujuan'></div>
							</div>
							<div class="col-md-4">
								Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
							</div>
							<div class="col-md-4">
								s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
							</div>
							<div class="col-md-4">
								<div class="input-group" style="width: 100%; margin-top: 12px;">
							      <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
							      <span class="input-group-btn">
							        <input class="tombol form-control" name="btn_cari" type="submit" value="cari" />
							      </span>
							    </div><!-- /input-group -->
							</div>
						</div>
					</div>
					<table style="margin: 10px 0px;" width='100%' cellspacing="0">
						<tr>
							<td align="right">
								{PAGING}
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table width='100%' class="table table-hover table-bordered">
						<!-- BEGIN ROW -->
						<tr style="display:{ROW.showheader};">
							<th class="thin" colspan="5">1:Jadwal</th>
							<th class="thin" colspan="7">2:Berita Acara</th>
							<th class="thin" colspan="3">3:Release</th>
						</tr>
						<tr style="display:{ROW.showheader};">
							<th width=30>No</th>
							<th width=70><a class="th" id="sort1" href='#'>Tgl.Berangkat</a></th>
							<th width=100><a class="th" id="sort2" href='#'>#Jadwal</a></th>
							<th width=70><a class="th" id="sort3" href='#'>Jam</a></th>
							<th width=100><a class="th" id="sort4" href='#'>Remark</a></th>
							<th width=100><a class="th" id="sort5" href='#'>BA Oleh</a></th>
							<th width=100><a class="th" id="sort6" href='#'>Dibuat</a></th>
							<th width=100><a class="th" id="sort7" href='#'>Sopir</a></th>
							<th width=70><a class="th" id="sort8" href='#'>Jumlah</a></th>
							<th width=100><a class="th" id="sort9" href='#'>Approver</a></th>
							<th width=100><a class="th" id="sort10" href='#'>Approved</a></th>
							<th width=100><a class="th" id="sort11" href='#'>Keterangan</a></th>
							<th width=100><a class="th" id="sort12" href='#'>Releaser</a></th>
							<th width=100><a class="th" id="sort13" href='#'>Released</a></th>
							<th width=100><a class="th" href='#'>Act.</a></th>
						</tr>
						<tr class="{ROW.odd}">
							<td align="center">{ROW.no}</td>
							<td align="center">{ROW.tglberangkat}</td>
							<td align="center">{ROW.kodejadwal}</td>
							<td align="center">{ROW.jamberangkat}</td>
							<td align="center">{ROW.remark}</td>
							<td align="center">{ROW.dibuatoleh}</td>
							<td align="center">{ROW.waktubuat}</td>
							<td align="center">{ROW.sopir}</td>
							<td align="right">{ROW.jumlah}</td>
							<td align="center">{ROW.approver}</td>
							<td align="center">{ROW.waktuapproved}</td>
							<td align="left">{ROW.keterangan}</td>
							<td align="center">{ROW.releaser}</td>
							<td align="center">{ROW.waktureleased}</td>
							<td align="center">{ROW.act}</td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					{NO_DATA}
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
