<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('mac_address_invalid');
	Element.hide('nama_invalid');
	Element.hide('cabang_invalid');
	
	mac_address	= document.getElementById('mac_address');
	nama		= document.getElementById('nama_komputer');
	cabang		= document.getElementById('cabang');
		
	if(mac_address.value==''){
		valid=false;
		Element.show('mac_address_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(cabang.value==''){
		valid=false;
		Element.show('cabang_invalid');
	}
	
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Pendaftaran Komputer</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top'>
				<table width='500'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="id" value="{ID}">
			      <input type="hidden" name="mac_address_old" value="{MAC_ADDRESS_OLD}">
						<td width='200'><u>Mac Address</u></td><td width='5'>:</td>
						<td>
							<input type="text" id="mac_address" name="mac_address" value="{MAC_ADDRESS}" maxlength=100 onFocus="Element.hide('mac_address_invalid');">
							<span id='mac_address_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Nama Komputer</u></td><td>:</td>
						<td>
							<input type="text" id="nama_komputer" name="nama_komputer" value="{NAMA_KOMPUTER}" maxlength=100 onFocus="Element.hide('nama_invalid');">
							<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Cabang</td><td>:</td>
						<td><select id='cabang' name='cabang' onFocus="Element.hide('cabang_invalid');">{OPT_CABANG}</select><span id='cabang_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
			      <td>Status</td><td>:</td>
						<td>
							<select id='status' name='status'>
								<option value="1" {STATUS_1_SELECTED}>AKTIF</option>
								<option value="0" {STATUS_0_SELECTED}>NONAKTIF</option>
							</select>
						</td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
			  <input type="hidden" name="page" value="{PAGE}">
			  <input type="hidden" name="cari" value="{CARI}">
				<input type="button" onClick="window.location='{U_BACK}';" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>