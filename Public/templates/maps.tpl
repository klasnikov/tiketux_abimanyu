<!DOCTYPE html>
<html lang="en">
<head>
    <title>Test Google Maps</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- jquery ui-->
    <link rel="stylesheet" href="jquery-ui/jquery-ui.css" type="text/css" />
</head>
<body>
<div class="container" style="padding: 50px 50px 50px 50px">
  <div class="row">
    <div class="col-sm-12">
            <div class="box">
                <div class="box-body">
                    <input type="text" class="form-control" id="tdxt_alamat" placeholder="Type to search address" style="margin-bottom: 5px;"/>
                    <div class="row">
                        <div class="col-lg-6">
                            <div id="map" style="height: 500px;"></div>
                        </div>
                        <div class="col-lg-6">
                            <form role="form" method="post" action="">
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Latitude</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="latitude" id="txt_lat" placeholder="Latitude" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Longitude</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="longitude" id="txt_long" placeholder="Longitude" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="2" name="alamat" id="txt_alamat" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 form-control-label">Status</label>
                                    <div class="col-sm-10">
                                        <p id="status"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
<!-- jquery cdn google -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- jquery ui -->
<script src="jquery-ui/jquery-ui.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript">
    var map;
    var marker;
    var geocoder;
    var latLng;

    window.onload = function(){
        // check for Geolocation support
        if (navigator.geolocation) {
          console.log('Geolocation is supported!');
        }
        else {
          console.log('Geolocation is not supported for this Browser/OS.');
        }
    }

    function initMap() {
        geocoder = new google.maps.Geocoder();
        latLng = new google.maps.LatLng(-6.878085, 107.560814);
        // initialize map
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -6.878085, lng: 107.560814},
            zoom: 13,
            fullscreenControl: true
        });
        // initialize marker
        marker = new google.maps.Marker({
            position: {lat: -6.878085, lng: 107.560814},
            title: 'Point A',
            map: map,
            draggable: true
        });

        // Define the LatLng coordinates for the polygon's path.
        garis = [];

        // <?php

        //     $servername = "localhost";
        //     $username = "root";
        //     $password = "";
        //     $dbname = "testing";

        //     // Create connection
        //     $conn = mysqli_connect($servername, $username, $password, $dbname);

        //     // Check connection
        //     if (!$conn) {
        //         die("Connection failed: " . mysqli_connect_error());
        //     }

        //     $sql = "SELECT nama, GROUP_CONCAT(latitude) AS latitude, GROUP_CONCAT(longitude) AS longitude FROM lokasi GROUP BY nama";

        //     $result = mysqli_query($conn, $sql);
        //         if (mysqli_num_rows($result) > 0) {
        //             // output data of each row
        //             $z = 0;
        //             while($row = mysqli_fetch_assoc($result)) {
                        
        //                 $latitude = explode(",", $row["latitude"]);
        //                 $longitude = explode(",", $row["longitude"]);

        //                 for ($i=0; $i < count($latitude) ; $i++) {

        //                     $posisi[$z][$i] = array("lat" => $latitude[$i], "lng" => $longitude[$i]);   
                        
        //                 }

        //                 $z++;
        //             }
        //         }

        //     mysqli_close($conn);
        // ?>

        // $.ajax({
        //     url: "http://localhost/maps/index.php",
        //     method: "GET", 
        //     success: function(result){
        //         console.log(result);
        //     }
        // });


        garis[0] = [
          {lat: -6.888721, lng: 107.555907},
          {lat: -6.877897, lng: 107.549412},
          {lat: -6.858903, lng: 107.564279},
          {lat: -6.858903, lng: 107.564279}
        ];

        garis[1] =[
          {lat: -6.865701, lng: 107.574598},
          {lat: -6.873722, lng: 107.571250}, 
          {lat: -6.876476, lng: 107.579244},
          {lat: -6.868345, lng: 107.581908}
        ];
        


        polygon = [];

        for (var i = 0; i < garis.length; i++) {
            // Construct the polygon.
            polygon[i] = new google.maps.Polygon({
              paths: garis[i],
              strokeColor: '#FF0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#FF0000',
              fillOpacity: 0.35
            });

            polygon[i].setMap(map);
        }

    
        // auto complete address
        $('#tdxt_alamat').autocomplete({
            source: function(request, response) {
                geocoder.geocode({'address': request.term}, function(results, status) {
                    response($.map(results, function(item) {
                        return {
                            value: item.formatted_address,
                            location: item.geometry.location
                        }
                    }));
                })
            },
            select: function(event, ui) {
                marker.setPosition(ui.item.location);
                map.setCenter(ui.item.location);
                updateMarkerPosition(ui.item.location);
                geocodePosition(ui.item.location);
                map.setZoom(15);
            }
        });

        // Update current position info.
        updateMarkerPosition(latLng);
        geocodePosition(latLng);

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'dragstart', function() {
            updateMarkerAddress('Dragging...');
        });
        // event when dragging active
        google.maps.event.addListener(marker, 'drag', function() {
            updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
        });

        // event when drag ended
        google.maps.event.addListener(marker, 'dragend', function() {
            updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());

            x = [];
            
            for (var i = 0; i < polygon.length; i++) {

                x[i] = google.maps.geometry.poly.containsLocation(marker.getPosition(), polygon[i])? 1 : 0;
            }

            for (var i = 0; i < x.length; i++) {
                    if(x[i] == 1){
                        $('#status').text("Success");
                        break;
                    }else{
                        $('#status').text("Error Posisi Diluar Area Pickup");
                    }
            }    
            
        });
    }

    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                updateMarkerAddress(responses[0].formatted_address);
            } else {
                updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }

    function updateMarkerPosition(latLng) {
        $('#txt_long').val(latLng.lng());
        $('#txt_lat').val(latLng.lat());

    }

    function updateMarkerAddress(str) {
        $('#txt_alamat').val(str);
    }

    function updateMarkerStatus(str) {
        /*
         document.getElementById('markerStatus').innerHTML = str;
         */
    }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7NhVgmBLqymxffvQMhiZ4pQGex4NGE5s&callback=initMap"></script>
</body>
</html>