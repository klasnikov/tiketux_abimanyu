<script language="JavaScript">

function UbahPassword(){
	passlama=document.getElementById('passlama').value;
	passbaru=document.getElementById('passbaru').value;
	konfpassbaru=document.getElementById('konfpassbaru').value;
	
	if(passlama!='' && passbaru!='' && konfpassbaru!='')
	{
	
		new Ajax.Request("ubah_password.php?sid={SID}", 
		{
			asynchronous: true,
		  method: "get",
		  parameters: "passlama=" + passlama + "&passbaru="+ passbaru + "&konfpassbaru="+ konfpassbaru +"&mode=ubah",
			onLoading: function(request) 
		  {
		            
		  },
		  onComplete: function(request) 
		  {
				
				if(request.responseText=='sukses'){
					alert("Password anda telah BERHASIL DIUBAH!");
				}
				else{
					alert("Password lama yang anda masukkan tidak benar, atau password baru anda tidak sesuai dengan konfirmasi password baru.");
				}
				
				document.getElementById('passlama').value = "";
				document.getElementById('passbaru').value = "";
				document.getElementById('konfpassbaru').value = "";
				
		  },
		  onFailure: function(request) 
		  { 
				assignError(request.responseText); 
		  }
		});
	}
	else{
		alert("Input anda belum lengkap!");
	}
}

</script>

<div class="container" style="width: 23%;">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr class='' height=40>
					<td align='center' valign='middle' class="bannerjudulw" style="text-align: center;">
						<div class="bannerjudul pad10" >
							Ubah Password
						</div>
					</td>
				</tr>		
				<tr>
					<td class="pad10">
					Masukkan password lama anda<br /><input placeholder="Password Lama" class="form-control" type='password' size='20' id='passlama' name='passlama' value='' /></td>
				</tr>
				<tr>
					<td class="pad10">
					Masukkan password baru anda<br /><input placeholder="Password Baru" class="form-control" type='password' size='20' id='passbaru' name='passbaru' value='' /></td>
				</tr>
				<tr>
					<td class="pad10">
					Ulangi password baru anda<br /><input placeholder="Ulangi Password Baru" class="form-control" type='password' size='20' id='konfpassbaru' name='konfpassbaru' value='' /></td>
				</tr>
				<tr>
					<td class="pad10">
					<br /><input class="btn mybutton paper topwidth" style="margin: 0 auto; display: block;" type='button' name='ubah' value='ubah' onclick="UbahPassword()"></td>
				</tr>
			</table>
		</div>
	</div>
</div>