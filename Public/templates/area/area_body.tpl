<script>
function hapusData(kode){
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		new Ajax.Request("pengaturan_area.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=hapus_area&id_area="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
	   },
	   onSuccess: function(request) 
	   {			
			//window.location.reload();
			//deselectAll();
	   },
	   onFailure: function(request) 
	   {
	   }
	  })  
	}	
}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Pengaturan Area</td>
							<td colspan=2 align='right' class="bannernormal" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<div class="input-group">
								      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
								      <span class="input-group-btn">
								        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
								      </span>
								    </div><!-- /input-group -->
								</form>
							</td>
						</tr>
						<tr>
							<td class="mytd" width='70%' align='right'>{PAGING}</td>
						</tr>
					</table>
				<table width='100%' class="border table table-hover table-bordered">
				    <tr>
				       	<th>No</th>
						<th width="30px"><a class="th" href='{A_SORT_1}' >ID Area</a></th>
						<th width="150px"><a class="th" href='{A_SORT_2}' >Nama Area</a></th>
						<th width="80px">Kota</th>
						<th width="150px">Jurusan</th>
						<th width="50px">Bandara</th>
						<th width="80px"><a class="th" href='{A_SORT_3}'>Biaya Jemput</a></th>
						<th width="80px"><a class="th" href='{A_SORT_4}'>Biaya Antar</a></th>
						<th>Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <form action="{EDIT_ACTION}" method="post">
				     <tr class="{ROW.odd}">
				       	<td>
				       		<div align="right">{ROW.NO}</div>
				       		<input type="hidden" name="page" value="{idx_page}">
				       	</td>
						<td style="padding:0px;margin:0px;">
							<input style='border:0px;' class="form-control" type="text" name="id_area" value="{ROW.id_area}" readonly>
						</td>
				       	<td style="padding:0px;margin:0px;">
				       		<input style='border:0px;' class="form-control" type="text" name="nama_area" value="{ROW.nama_area}">
				       	</td>
				       	<td style="padding:0px;margin:0px;">
				     		<select class="form-control" name="kota_area">
				     			<option value=''>None</option>
				     			{ROW.opt_kota}
				     		</select>
				     	</td>
				       	<td style="padding:0px;margin:0px;">
				     		<select class="form-control" name="jurusan_area">
				     			<option value=''>None</option>
				     			{ROW.opt_jurusan}
				     		</select>
				     	</td>
				     	<td style="padding:0px;margin:0px;">
				     		<center>
				     			<input type="checkbox" style="margin-top:10px;" value="1" {ROW.is_bandara} name="is_bandara">
				     		</center>
				     	</td>
				       	<td style="padding:0px;margin:0px;">
				       		<input style='border:0px;text-align:right;' class="form-control" type="text" name="biaya_jemput" value="{ROW.biaya_jemput}">
				       	</td>
				       	<td style="padding:0px;margin:0px;">
				       		<input style='border:0px;text-align:right;' class="form-control" type="text" name="biaya_antar" value="{ROW.biaya_antar}">
				       	</td>
				       	<td><center>{ROW.action}</center></td>
				     </tr>  
				     </form>
				     <!-- END ROW -->
				     <form action="{ADD_ACTION}" method="post">
				     	<tr>
				     		<td style="background:grey;"><input type="hidden" name="page" value="{idx_page}"></td>
				     		<td style="padding:0px;margin:0px;width:150px;">
				     			<input style='border:0px;' class="form-control" type="text" name="id_area" value='{ID_AREA}' readonly>
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     			<input style='border:0px;' class="form-control" type="text" name="nama_area">
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     			<select class="form-control"  name="kota_area">
				     				<option value=''>-- Pilih Kota --</option>
				     				{OPT_KOTA}
				     			</select>
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     			<select class="form-control"  name="jurusan_area">
				     				<option value=''>-- Pilih Jurusan --</option>
				     				{OPT_JURUSAN}
				     			</select>
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     		<center>
				     			<input type="checkbox" style="margin-top:10px;" value="1" name="is_bandara">
				     		</center>
				     		</td>				     		
				     		<td style="padding:0px;margin:0px;">
				     			<input style='border:0px;text-align:right;' class="form-control" type="text" name="biaya_jemput">
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     			<input style='border:0px;text-align:right;' class="form-control" type="text" name="biaya_antar">
				     		</td>
				     		<td style="padding:0px;margin:0px;">
				     			<input type="submit" value="Tambah" class="btn btn-success form-control">
				     		</td>
				     	</tr>
				     </form>
			    </table>
			    <table class="table" width="100%">
					<tr>
						<td class="mytd" width='70%' align='right'>{PAGING}</td>
					</tr>
				</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>