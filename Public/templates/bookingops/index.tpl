<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
	// komponen khusus dojo
	dojo.require("dojo.widget.Dialog");
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

	function FormatUang(uang,separator){
		len_uang = String(uang).length;
		return_val='';
		for (i=len_uang;i>=0;i--){
			if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

			return_val =String(uang).substring(i,i-1)+return_val;
		}

		return return_val;
	}

	var Combobox=[];

	Combobox.Local=Class.create(Autocompleter.Local,{
		initialize:function($super,element,update,array,options){
			$super(element,update,array,options);
			this.options.minChars=this.options.minChars||0;
			Event.observe(this.element,'click',this.onClick.bindAsEventListener(this));
		},
		onClick:function($super,event){
			if(Event.element(event).id){
				if(this.active){
					this.hasFocus=false;
					this.active=false;
					this.hide();
				}else{
					this.activate();
					this.render();
				}
				return;
			}
			$super(event);
		}
	});

	var dialog_SPJ;
	function init(e){
		dialog_SPJ = dojo.widget.byId("dialog_SPJ");
	}

	dojo.addOnLoad(init);

	function setData(id_jurusan,rewrite_id,kolom){

		new Ajax.Updater(rewrite_id,"bookingops.detail.php?sid={SID}", {
			asynchronous: true,
			method: "post",

			parameters: "idjurusan="+id_jurusan+"&tgl="+document.getElementById("tanggal_mulai").value+"&col="+kolom,
			onLoading: function(request){
			},
			onSuccess: function(request){

			},
			onComplete: function(request){

			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});
	}

	function setDialogSPJ(tgl_berangkat,kode_jadwal,no_spj){
		//menampilkan dialog spj

		//tgl_berangkat  	= document.getElementById('p_tgl_user').value;
		//sopir_sekarang  = document.getElementById('hide_nama_sopir_sekarang').value;
		//mobil_sekarang  = document.getElementById('hide_mobil_sekarang').value;
		//kode_jadwal  		= document.getElementById('p_jadwal').value;
		//no_spj					= document.getElementById('txt_spj').value;

		new Ajax.Updater("rewritedialogspj","SPJ.php?sid={SID}",
				{
					asynchronous: true,
					method: "get",
					parameters:
					"mode=0"+
					"&tglberangkat="+tgl_berangkat+
					"&kodejadwal="+kode_jadwal+
					//"&sopirdipilih="+sopir_sekarang+
					//"&mobildipilih="+mobil_sekarang+
					"&nospj="+no_spj,
					onLoading: function(request)
					{
						//document.getElementById('hide_nama_sopir_sekarang').innerHTML="";
						//document.getElementById('hide_mobil_sekarang').innerHTML="";
						Element.show('progress_dialog_spj');
					},
					onComplete: function(request)
					{
						Element.hide('progress_dialog_spj');
						document.getElementById('no_spj').value = no_spj;
						document.getElementById('tgl_berangkat').value = tgl_berangkat;
						document.getElementById('kode_jadwal').value = kode_jadwal;
						getListDataMobilSopir(kode_jadwal);
					},
					onFailure: function(request)
					{
						assignError(request.responseText);
					}
				});

		dialog_SPJ.show();
	}

	var mobils  = [];
	var mobilsid= [];
	var sopirs  = [];
	var sopirsid= [];

	function getListDataMobilSopir(kode_jadwal){

		//kodejadwal  = document.getElementById('p_jadwal').value;

		new Ajax.Request("SPJ.controldata.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters:
			"mobildipilih="+mobildipilih.value+
			"&sopirdipilih="+sopirdipilih.value+
			"&kodejadwal="+kode_jadwal,

			onLoading: function(request){
			},
			onComplete: function(request){
			},
			onSuccess: function(request){

				//alert(request.responseText);

				eval(request.responseText);

				new Combobox.Local('manflistmobil','manflistmobilchoices',mobils,{ignoreCase:true,choices:15,partialSearch:true,afterUpdateElement:setMobilManifest});
				new Combobox.Local('manflistsopir','manflistsopirchoices',sopirs,{ignoreCase:true,choices:15,partialSearch:true,afterUpdateElement:setSopirManifest});

			},
			onFailure: function(request){
				alert('Error !!! Cannot Save');
				assignError(request.responseText);
			}
		});


	}

	function setMobilManifest(text,li){
		ditemukan=false;

		i=0;
		while(i<mobils.length && !ditemukan){
			str1=mobils[i];
			str2=text.value;
			if(str1.toUpperCase()==str2.toUpperCase()){
				mobildipilih.value=mobilsid[i];
				ditemukan=true;
			}
			i++;
		}

		if(!ditemukan){
			manflistmobil.style.background="red";
			mobildipilih.value="";
		}
	}

	function setSopirManifest(text,li){
		ditemukan=false;

		i=0;
		while(i<sopirs.length && !ditemukan){
			str1=sopirs[i];
			str2=text.value;
			if(str1.toUpperCase()==str2.toUpperCase()){
				sopirdipilih.value=sopirsid[i];
				ditemukan=true;
			}
			i++;
		}

		if(!ditemukan){
			manflistsopir.style.background="red";
			sopirdipilih.value="";
		}
	}

	function setBiayaBBM(jurusan){

		if(mobildipilih.value==""){
			return false;
		}

		no_spj			= document.getElementById('no_spj').value;
		//id_jurusan	= document.getElementById('tujuan').value;

		new Ajax.Request("SPJ.php?sid="+SID,
				{
					asynchronous: true,
					method: "get",
					parameters: "mode=2" +
					"&nospj="+no_spj+
					"&mobildipilih="+mobildipilih.value+
					"&idjurusan="+jurusan,
					onLoading: function(request)
					{
						Element.show('loadingbiayabbm');
					},
					onComplete: function(request)
					{
						Element.hide('loadingbiayabbm');
					},
					onSuccess: function(request)
					{
						eval(request.responseText);
					},
					onFailure: function(request)
					{
						alert('Terjadi Kegagalan');
						assignError(request.responseText);
					}
				});

	}

	function hitungTotalBiayaOperasional(){

		biaya_sopir	= document.getElementById('biayasopir').value*1;
		biaya_tol		= document.getElementById('biayatol').value*1;
		biaya_parkir= document.getElementById('biayaparkir').value*1;
		biaya_bbm		= document.getElementById('biayabbm').value*1;

		total_biaya	= biaya_sopir + biaya_tol + biaya_parkir + biaya_bbm;

		document.getElementById('biayatotal').innerHTML	= FormatUang(total_biaya,".");
	}

	function CetakSPJ(){

		//mencetak SPJ
		tgl_berangkat  	= document.getElementById('tgl_berangkat').value;
		kode_jadwal 		= document.getElementById('kode_jadwal').value;
		sopir_dipilih		= document.getElementById('sopirdipilih').value;
		mobil_dipilih		= document.getElementById('mobildipilih').value;
		no_spj					= document.getElementById('no_spj').value;
		parameter_biaya_bbm	= document.getElementById('biaya_bbm')?"&biaya_bbm="+document.getElementById('biaya_bbm').value:"";
		//username				= document.getElementById('username_bbm').value;
		//alasan					= document.getElementById('alasan_bbm').value;
		//signature				= document.getElementById('signature_bbm').value;
		//iscetakulangbbm	= document.getElementById('chkcetakulangvoucherbbm')?(document.getElementById('chkcetakulangvoucherbbm').value?1:0):0;

		if(sopir_dipilih!='' && mobil_dipilih!=''){
			Start("SPJ.php?sid="+SID+
					"&mode=1&tglberangkat="+tgl_berangkat+"&kodejadwal="+kode_jadwal+
					"&mobildipilih="+mobil_dipilih+"&sopirdipilih="+sopir_dipilih+"&no_spj="+no_spj+
					parameter_biaya_bbm);
			//getUpdateMobil();
			dialog_SPJ.hide();
		}
		else{
			alert("Anda belum memilih kendaraan atau nama sopir!");
		}
	}
</script>
<div class="container" style="width: 90%;">
	<!--dialog SPJ-->
	<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none; width: 80%;">
		<form onsubmit="return false;">
			<table class="table">
				<tr>
					<td bgcolor='ffffff'>
						<input type="hidden" id="tgl_berangkat">
						<input type="hidden" id="kode_jadwal">
						<input type="hidden" id="no_spj">
						<table>
							<tr><td><h2>Cetak Manifest</h2></td></tr>
							<tr><td><div id="rewritedialogspj"></div></td></tr>
						</table>
						<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
						<br>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<!--END dialog SPJ-->
	<div class="row">
		<div class="col-md-12 box">
			<div class="col-md-3" style="text-align: left;">
				<div class="bannerjudulw"><div class="bannerjudul">Dispatcher</div></div>
			</div>
			<form action="{ACTION_CARI}" method="post">
				<div class="col-md-9" style="text-align: left;">
					<div class="col-md-4">
						Cabang:<br /><select class="form-control" id='cabang' name='cabang'>{OPT_CABANG}</select>
					</div>
					<div class="col-md-4">
						Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
					</div>
					<div class="col-md-4">
						<input class="mybutton btn topwidth" style="margin-top: 17px;" name="btn_cari" type="submit" value="cari" />
					</div>
				</div>
			</form>
			<div class="col-md-12" style="text-align: center;">
				<div style="font-size: 36px;text-align: center; padding-left: 5px;">{CABANG_BERANGKAT}</div>
				<!-- BEGIN LISTDATA -->
				<div class="col-md-4">
					<span class="bookopsdata">
						<span id="rewrite_{LISTDATA.idjurusan}"><img src='./templates/images/loading_bar.gif' /></span>
						<script type='text/javascript'>setData('{LISTDATA.idjurusan}','rewrite_{LISTDATA.idjurusan}','{LISTDATA.col}');</script>
					</span>
				</div>
				<!-- END LISTDATA -->
			</div>
			<div class="col-md-12" style="text-align: center">
				<h3>Book: Jumlah kursi dipesan | Conf: Jumlah kursi yang sudah dibayar | Av.: Kursi Tersisa</h3>
			</div>
		</div>
		<div>
		</div>

