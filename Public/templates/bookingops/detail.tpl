<table class="table" cellpadding=0 cellspacing=0>
	<tr>
		<td width="50%" valign="top">
			<table class="table table-hover table-striped table-bordered" cellpadding=0 >
				<tr><th colspan='9' style="font-size: 16px">Ke {TUJUAN}</th></tr>
					<tr>
						<td class="dashboardkolomheader" >Time</td>
						<td class="dashboardkolomheader" colspan="2">Book</td>
						<td class="dashboardkolomheader" colspan="2">Conf</td>
						<td class="dashboardkolomheader">Av.</td>
						<td class="dashboardkolomheader" >Desc</td>
						<td class="dashboardkolomheader" >Act</td>
					</tr>
					<!-- BEGIN ROW -->
					<tr class="{ROW.odd}">
						<td align="center" style="font-size: {ROW.alert}"><b>{ROW.time}</b></td>
						<td align="right" style="color: blue;">{ROW.b}</td>
						<td align="right" style="color: green;">{ROW.b_transit}</td>
						<td align="right" style="color: red;">{ROW.c}</td>
						<td align="right" style="color: orange;">{ROW.c_transit}</td>
						<td align="right" >{ROW.available}</td>
						<td align="center">{ROW.keterangan}</td>
						<td align="center">{ROW.act}</td>
					</tr>
					<!-- END ROW -->
			</table>
		</td>
	</tr>
</table>
	