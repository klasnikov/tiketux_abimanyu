<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr colspan="3" height=40>
							<td align='left' valign='middle' class="bannerjudul">&nbsp;Pelanggan Berpotensi Jadi Member</td>
						</tr>
						<tr>
							<td colspan="3" align='left' class="" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<div class="col-md-3">
										Periode:<br /><input class="form-control" id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
									</div>
									<div class="col-md-3">
										s/d <br /><input class="form-control" id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
									</div>
									<div class="col-md-3">
										Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									</div>
									<div class="col-md-3">
										<input class="btn mybutton topwidth" id='btn_submit' name='btn_submit' type="submit" value="cari" />
									</div>
								</form>
							</td>
						</tr>
						<tr>
							<td align='center' colspan=3>
								<table>
									<tr>
										<td style="padding: 20px;">
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=3 width='100%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%' class="table table-bordered table-hover">
				    <tr>
				       <th width=30>No</th>
							 <th width=150><a class="th" href='{A_SORT_BY_NAMA}'>Nama Pelanggan</a></th>
							 <th width=70><a 	class="th" href='{A_SORT_BY_TELP}'>Telepon</a></th>
							 <th width=60><a class="th" href='{A_SORT_BY_FREKWENSI}'>Frekwensi</a></th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="right">{ROW.no}</div></td>
							 <td><div align="left">{ROW.nama}</div></td>
							 <td><div align="left">{ROW.hp}</div></td>
							 <td><div align="right">{ROW.frekwensi}</div></td>
				     </tr>  
				     <!-- END ROW -->
						 {NO_DATA}
				    </table>
				    <table width='100%'>
						<tr>
							<td width='100%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
