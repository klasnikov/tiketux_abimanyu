<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}


function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_member="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&id_member="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}


</script>

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Member</td>
							<td colspan=2 align='right' class="bannernormal" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<table>
										<tr>
											<td class=''>
												<div class="col-md-4">&nbsp;Tgl.Registrasi:&nbsp;<input class="form-control" id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10></div>
												<div class="col-md-4">&nbsp; s/d &nbsp;<input class="form-control" id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10></div>
												<div class="col-md-4" style="padding: 0; padding-top: 12px;">
													<div class="input-group">
												      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
												      <span class="input-group-btn" style="width: 30%;">
												        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
												      </span>
												    </div><!-- /input-group -->
												</div>
											</td>
										</tr>
									</table>
								</form>
							</td>
						</tr>
						<tr>
							<td align='center' colspan=2>
								<table>
									<tr>
										<td style="padding: 20px;">
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="mytd" align='left'>
								<a href="{U_MEMBER_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
								<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
							<td class="mytd" width='70%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%' class="border table table-hover table-bordered">
				    <tr>
				       <th style="padding-left: 20px;"><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
				       <th>No</th>
							 <th><a class="th" href='{A_SORT_BY_NAMA}'>Nama Member</a></th>
							 <th><a class="th" href='{A_SORT_BY_KODE}'>Kode</a></th>
							 <th><a class="th" href='{A_SORT_BY_ALAMAT}'>Alamat</a></th>
							 <th><a 	class="th" href='{A_SORT_BY_HP}'>HP</a></th>
							 <th><a 	class="th" href='{A_SORT_BY_EMAIL}'>Email</a></th>
							 <th><a class="th" href='{A_SORT_BY_PEKERJAAN}'>Pekerjaan</a></th>
							 <th><a class="th" href='{A_SORT_TGL_DAFTAR}'>Tgl.Daftar</a></th>
							 <th><a class="th" href='{A_SORT_BY_FREKWENSI}'>Freq.</a></th>
							 <th><a class="th" href='{A_SORT_BY_STATUS}'>Aktif</th>
							 <th>Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="center">{ROW.check}</div></td>
				       <td><div>{ROW.no}</div></td>
						 <td><div >{ROW.nama}</div></td>
						 <td><div >{ROW.id_member}</div></td>
			      		 <td><div >{ROW.alamat}</div></td>
						 <td><div >{ROW.hp}</div></td>
						 <td><div >{ROW.email}</div></td>
						 <td><div >{ROW.pekerjaan}</div></td>
						 <td><div >{ROW.tgl_daftar}</div></td>
						 <td><div >{ROW.frekwensi}</div></td>
						 <td><div >{ROW.aktif}</div></td>
				       <td><div >{ROW.action}</div></td>
				     </tr>  
				     <!-- END ROW -->
				    </table>
					{NO_DATA}
			    <table width='100%'>
						<tr>
							<td width='30%' align='left'>
								<a href="{U_MEMBER_ADD}"><i class='fa fa-plus'></i> Tambah</a>&nbsp;|&nbsp;
								<a href="" onClick="return hapusData('');"><i class='fa fa-trash-o'></i> Hapus</a></td>
							<td width='70%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>