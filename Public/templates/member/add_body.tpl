<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	Element.hide('id_member_invalid');
	Element.hide('nama_invalid');
	Element.hide('tempat_lahir_invalid');
	Element.hide('no_ktp_invalid');
	Element.hide('alamat_invalid');
	Element.hide('kota_invalid');
	Element.hide('telp_invalid');
	Element.hide('hp_invalid');
	Element.hide('cabang_invalid');
	
	id_member			= document.getElementById('id_member');
	nama					= document.getElementById('nama');
	tempat_lahir	= document.getElementById('tempat_lahir');
	noktp					= document.getElementById('noktp');
	alamat				= document.getElementById('alamat');
	kota					= document.getElementById('kota');
	telp					= document.getElementById('telp');
	hp						= document.getElementById('hp');
	cabang_daftar	= document.getElementById('cabang_daftar');
	
	valid=true;
	
	if(id_member.value==''){
		valid=false;
		Element.show('id_member_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(tempat_lahir.value==''){
		valid=false;
		Element.show('tempat_lahir_invalid');
	}
	
	if(noktp.value==''){
		valid=false;
		Element.show('no_ktp_invalid');
	}
	
	if(alamat.value==''){
		valid=false;
		Element.show('alamat_invalid');
	}
	
	if(kota.value==''){
		valid=false;
		Element.show('kota_invalid');
	}
	
	if(!cekValue(hp.value) || hp.value==''){	
		valid=false;
		Element.show('hp_invalid');
	}	
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(cabang_daftar.value==""){	
		valid=false;
		Element.show('cabang_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 box">
			<form name="frm_data" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="whiter" valign="middle" align="center">
				<table>
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td align='center' valign='top' class="pad10">
							<table>   
								<tr>
									<td colspan=3><h2>{JUDUL}</h2></td>
								</tr>
								<tr>
						      <input class="form-control" type="hidden" name="id_member" value="{ID_MEMBER}">
									<td class="pad10">Kode Member*</td><td width='5'></td>
									<td class="pad10">
										<input class="form-control" type="text" id="id_member" name="id_member" value="{ID_MEMBER}" maxlength=25 onChange="Element.hide('id_member_invalid');" {READONLY}>
										<span id='id_member_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Nama*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="nama" name="nama" value="{NAMA}" maxlength=50 onChange="Element.hide('nama_invalid');">
										<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
									<td class="pad10">Jenis Kelamin</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
											<option value=0 {JK_0}>Laki-laki</option>
											<option value=1 {JK_1}>Perempuan</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="pad10">Kategori Member</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" id="kategori_member" name="kategori_member">
											<option value=0 {KATEGORI_0}>Member tidur</option>
											<option value=1 {KATEGORI_1}>Member aktif</option>
										</select>
									</td>
								</tr>
								<tr>
						      <td class="pad10">Tempat Lahir*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" value="{TEMPAT_LAHIR}" maxlength=30 onChange="Element.hide('tempat_lahir_invalid');">
										<span id='tempat_lahir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Tanggal Lahir*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" id="tgl_lahir" name="tgl_lahir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_LAHIR}">
									</td>
						    </tr>
								<tr>
						      <td class="pad10">No. KTP*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="noktp" name="noktp" value="{NO_KTP}" maxlength=30 onChange="Element.hide('no_ktp_invalid');">
										<span id='no_ktp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Alamat*</td><td  valign='top'></td>
									<td class="pad10">
										<textarea class="form-control" name="alamat" id="alamat" cols="30" rows="3"  maxlength=150 onChange="Element.hide('alamat_invalid');">{ALAMAT}</textarea>
										<span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
								</tr>
								<tr>
						      <td class="pad10">Kota*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="kota" name="kota" value="{KOTA}" maxlength=30 onChange="Element.hide('kota_invalid');">
										<span id='kota_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Kodepos</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="kode_pos" name="kode_pos" value="{KODE_POS}" maxlength=6>
									</td>
						    </tr>
							</table>
						</td>
						<td height=1 bgcolor='EFEFEF'></td>
						<td align='center' valign='top' style="padding-top: 60px;" class="pad10">
							<table>
							<tr>
						      <td class="pad10">Tanggal Registrasi*</td><td width='5'></td>
									<td class="pad10">
										<input class="form-control" id="tgl_registrasi" name="tgl_registrasi" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_REGISTRASI}">
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Handphone*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="hp" name="hp" value="{HP}" maxlength=20 onChange="Element.hide('hp_invalid');">
										<span id='hp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Telp</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="telp" name="telp" value="{TELP}" maxlength=20 onChange="Element.hide('telp_invalid');">
										<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Email</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="email" name="email" value="{EMAIL}" maxlength=30 />
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Pekerjaan</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" type="text" id="pekerjaan" name="pekerjaan" value="{PEKERJAAN}" maxlength=50 />
									</td>
						    </tr>
								<tr>
						      <td class="pad10">Berlaku Hingga*</td><td class="pad10"></td>
									<td class="pad10">
										<input class="form-control" id="tgl_berlaku" name="tgl_berlaku" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERLAKU}">
									</td>
						    </tr>
								<tr>
									<td class="pad10">Cabang Daftar</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" id="cabang_daftar" name="cabang_daftar" onChange="Element.hide('cabang_invalid');">
											{OPT_CABANG_DAFTAR}
										</select>
										<span id='cabang_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
								</tr>
								<tr>
									<td class="pad10">Status Aktif</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" id="aktif" name="aktif">
											<option value=1 {AKTIF_1}>AKTIF</option>
											<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					            
				</table>
				</td>
			</tr>
			<tr>
						<td align='center' valign='middle' style="padding-bottom: 50px;">
						<input type="hidden" name="mode" value="{MODE}">
					  	<input type="hidden" name="submode" value="{SUB}">
						<div class="col-md-6 col-md-offset-3">
							<div class="col-md-6">
								<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
							</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>
					</tr>
			</table>
			</form>
		</div>
	</div>
</div>