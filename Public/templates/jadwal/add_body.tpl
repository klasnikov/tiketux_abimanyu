<script language="JavaScript">

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_jadwal_invalid');
	
	kode_jadwal		= document.getElementById('kode_jadwal');
	
	if(!document.getElementById('opt_tujuan')){
		alert("Anda belum memilih tujuan");
		valid=false;
	}
	
	if(kode_jadwal.value==''){
		valid=false;
		Element.show('kode_jadwal_invalid');
	}
	
	if(valid){
    popuploading.show();
		return true;
	}
	else{
		return false;
	}
}

function getUpdateTujuan(asal,sub){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(sub==0){
			rewrite	='rewrite_tujuan';
			loading	='loading_tujuan';
			mode		='get_tujuan';
			jurusan	='{ID_JURUSAN}';
		}
		else{
			rewrite	='rewrite_sub_tujuan';
			loading	='loading_sub_tujuan';
			mode		='get_tujuan_utama';
			jurusan	='{SUB_ID_JURUSAN}';
		}
		
		if(document.getElementById(rewrite)){
			document.getElementById(rewrite).innerHTML = "";
    }
		
		new Ajax.Updater(rewrite,"pengaturan_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&jurusan="+jurusan+"&mode=" + mode,
        onLoading: function(request) 
        {
            Element.show(loading);
        },
        onComplete: function(request) 
        {
          if(sub==1){
            getUpdateJadwal(document.getElementById("opt_tujuan_asal").value);
          }
          Element.hide(loading);
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_sub_jadwal')){
			document.getElementById('rewrite_sub_jadwal').innerHTML = "";
    }
		
		new Ajax.Updater('rewrite_sub_jadwal',"pengaturan_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "jurusan=" + id_jurusan + "&kode_jadwal={KODE_SUB_JADWAL}&mode=get_jadwal",
        onLoading: function(request) 
        {
            Element.show('loading_sub_jadwal');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_sub_jadwal');
            setDetailRute();
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function showSubJadwal(flag){
	
	if(flag==0){
		Element.hide('show_sub_jadwal');
    document.getElementById("showdetailrute").style.display="none";
	}
	else{
		Element.show('show_sub_jadwal');
    document.getElementById("showdetailrute").style.display="block";
	}
}

function setDetailRute(){

  if(flag_sub_jadwal.value==0){
    kodejadwal      =kode_jadwal_old.value;
    kodejadwalutama =kode_jadwal_old.value;
  }
  else{
    kodejadwal      =kode_jadwal_old.value;
    kodejadwalutama =kode_jadwal_utama.value;
  }

  if(kodejadwalutama==""){
    return false;
  }

  new Ajax.Updater(showdetailrute,"pengaturan_jadwal.php?sid={SID}",{
    asynchronous: true,
    method: "post",
    parameters: "mode=detailrute&kodejadwal="+kodejadwal+"&kodejadwalutama="+kodejadwalutama,
    onLoading: function(request){
      popuploading.show();
    },
    onComplete: function(request){
      popuploading.hide();
    },
    onSuccess: function(request){
    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });
}

dojo.require("dojo.widget.Dialog");

function init(e) {
  // inisialisasi variabel
  popuploading				= dojo.widget.byId("popuploading");

  getUpdateTujuan("{ASAL}",0);
  getUpdateTujuan("{SUB_ASAL}",1);
  getUpdateJadwal({SUB_ID_JURUSAN});
  setDetailRute();
}

dojo.addOnLoad(init);

</script>
<div class="container">
<div class="row">
	<div class="col-md-12 box">
		<form name="frm_data_mobil" action="{U_JADWAL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="whiter" valign="middle" align="center">
			<table width='1000'>
				<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
				<tr>
					<td align='center' valign='top' width='500'>
						<table width='500'>  
							<tr>
								<td colspan=3><h2>{JUDUL}</h2></td>
							</tr>
							<tr>
					      <input type="hidden" id="kode_jadwal_old" name="kode_jadwal_old" value="{KODE_JADWAL_OLD}">
								<td class="td-title" width='200'>Kode Jadwal*</td><td width='5'>:</td><td class="pad10" width='400'><input type="text" required placeholder="Kode Jadwal" class="form-control"  id="kode_jadwal" name="kode_jadwal" value="{KODE_JADWAL}" maxlength=50 onChange="Element.hide('kode_jadwal_invalid');" style="text-transform: uppercase;" onkeypress="valIn(event);"><span id='kode_jadwal_invalid' style='display:none;'><font color=red>&nbsp;<b>Wajib Diisi</b></font></span></td>
					    </tr>
							<tr>
					      <td class="td-title">Asal</td><td>:</td>
								<td class="pad10">
									<select class="form-control" id='opt_asal' name='opt_asal' onChange="getUpdateTujuan(this.value,0)">
										{OPT_ASAL}
									</select>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Tujuan</td><td>:</td>
								<td class="pad10">
									<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Waktu berangkat*</td><td>:</td>
								<td class="pad10">
									<select id='opt_jam' name='opt_jam' style="padding: 5px; width: 30%;">
										{OPT_JAM}
									</select>&nbsp;:&nbsp;
									<select id='opt_menit' name='opt_menit' style="padding: 5px; width: 30%;">
										{OPT_MENIT}
									</select>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Jum. Kursi</td><td>:</td>
								<td class="pad10">
									<select class="form-control" id='opt_kursi' name='opt_kursi'>
										{OPT_KURSI}
									</select>
								</td>
					    </tr>
							<tr>
								<td class="td-title">Status Aktif</td><td>:</td>
								<td class="pad10">
									<select class="form-control" id="aktif" name="aktif">
										<option value=1 {AKTIF_1}>AKTIF</option>
										<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
									</select>
								</td>
							</tr>
							<tr>
					      <td class="td-title" valign='top'>Jenis Jadwal</td><td valign='top'>:</td>
								<td class="pad10" valign='top'>
									<select class="form-control" id="flag_sub_jadwal" name="flag_sub_jadwal" onChange="showSubJadwal(this.value);" >
										<option value=0 {SUB_JADWAL_0}>Jadwal Utama</option>
										<option value=1 {SUB_JADWAL_1}>Jadwal Transit</option>
									</select>
								</td>
					    </tr>
							<tr>
								<td colspan=3>
									<span id='show_sub_jadwal' style='display:{SUB_JADWAL}; margin-bottom: 16px;'>
										<table width=600>
											<tr>
												<td colspan=3><b>Jadwal Utama</b></td>
											</tr>
											<tr>
												<td width=190>Asal Utama</td><td width=5>:</td><td><select class="form-control" id='opt_sub_asal' name='opt_sub_asal' onChange="getUpdateTujuan(this.value,1)">{OPT_SUB_ASAL}</select></td>
											</tr>
											<tr>
												<td>Tujuan Utama</td><td>:</td><td><div id='rewrite_sub_tujuan'></div><span id='loading_sub_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
											</tr>
											<tr>
												<td>Jadwal</td><td>:</td><td><div id='rewrite_sub_jadwal'></div><span id='loading_sub_jadwal' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
											</tr>
										</table>
									</span>
								</td>
							</tr>
						</table>
		        <div id="showdetailrute"></div>
					</td>
					<!--<td width=1 bgcolor='D0D0D0'></td>
					<td align='left' valign='top' width='500'>
						<table width='500'>  
							<tr><td colspan=3><h3>Biaya-biaya:</h3></td></tr>
							<tr>
					      <td>Uang Makan Senin</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayasopir1" name="biayasopir1" value="{BIAYA_SOPIR1}" maxlength=8 onkeypress='validasiAngka(event);' >
									<input type='checkbox' id='isbiayasopirkumulatif' name='isbiayasopirkumulatif' {IS_BIAYA_SOPIR_KUMULATIF} /><span class='noticeMessage'>Dibayar kumulatif</span>
								</td>
					    </tr>
							<tr>
					      <td>Uang Makan Selasa-Kamis</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayasopir2" name="biayasopir2" value="{BIAYA_SOPIR2}" maxlength=8 onkeypress='validasiAngka(event);'>
								</td>
					    </tr>
							<tr>
					      <td>Uang Makan Jumat-Minggu</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayasopir3" name="biayasopir3" value="{BIAYA_SOPIR3}" maxlength=8 onkeypress='validasiAngka(event);'>
								</td>
					    </tr>
							<!--<tr>
					      <td>Biaya Tol</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayatol" name="biayatol" value="{BIAYA_TOL}" maxlength=8 onkeypress='validasiAngka(event);'>
								</td>
					    </tr>
							<tr>
					      <td>Biaya Parkir</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayaparkir" name="biayaparkir" value="{BIAYA_PARKIR}" maxlength=8 onkeypress='validasiAngka(event);'>
								</td>
					    </tr>
							<tr>
					      <td>Biaya BBM</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="biayabbm" name="biayabbm" value="{BIAYA_BBM}" maxlength=8 onkeypress='validasiAngka(event);'>
									<input type='checkbox' id='isbbmvoucher' name='isbbmvoucher' {IS_VOUCHER_BBM} /><span class='noticeMessage'>Voucher BBM</span>
								</td>
					    </tr>
							<tr><td colspan='3'><hr></td></tr>
							<tr><td colspan=3><h3>Komisi-komisi:</h3></td></tr>
							<tr>
					      <td>Komisi Sopir/penumpang</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="komisi_penumpang_sopir" name="komisi_penumpang_sopir" value="{KOMISI_PENUMPANG_SOPIR}" maxlength=8 onChange="Element.hide('komisi_penumpang_sopir_invalid');">
									<span id='komisi_penumpang_sopir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
								</td>
					    </tr>
							<tr>
					      <td>Komisi CSO/penumpang</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="komisi_penumpang_cso" name="komisi_penumpang_cso" value="{KOMISI_PENUMPANG_CSO}" maxlength=8 onChange="Element.hide('komisi_penumpang_cso_invalid');">
									<span id='komisi_penumpang_cso_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
								</td>
					    </tr>
							<tr>
					      <td>Komisi Sopir/Paket</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="komisi_paket_sopir" name="komisi_paket_sopir" value="{KOMISI_PAKET_SOPIR}" maxlength=8 onChange="Element.hide('komisi_paket_sopir_invalid');">
									<span id='komisi_paket_sopir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
								</td>
					    </tr>
							<tr>
					      <td>Komisi CSO/Paket</td><td>:</td>
								<td>
									<input type="text" class="form-control"  id="komisi_paket_cso" name="komisi_paket_cso" value="{KOMISI_PAKET_CSO}" maxlength=8 onChange="Element.hide('komisi_paket_cso_invalid');">
									<span id='komisi_paket_cso_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
								</td>
					    </tr>
						</table>
					</td>-->
				</tr>

				
				<tr>
					<td colspan=3 align='center' valign='middle' style="padding-bottom: 50px; margin-top: 30px">
						<input type="hidden" name="mode" value="{MODE}">
					  	<input type="hidden" name="submode" value="{SUB}">
						<input type="hidden" name="txt_cari" value="{CARI}">
						<input type="hidden" name="page" value="{PAGE}">
						<br><br><br>
						<div class="col-md-4 col-md-offset-4">
							<div class="col-md-6">
								<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
							</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>
				</tr> 

			</table>
			</td>
		</tr>
		</table>
		</form>
	</div>
</div>
</div>
