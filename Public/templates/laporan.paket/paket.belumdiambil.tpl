<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
					Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("{ASAL}");

</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<form action="{ACTION_CARI}" method="post" name="my_form">
					<input type='hidden' id='is_cari' name='is_cari' value='1'>
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
								<div class="bannerjudul">Laporan Paket Belum Diambil</div>
							</td>
							<td colspan=2 align='left' valign='middle'>
								
								<div class="col-md-3">
									Diterima di cabang:<br /><select class="form-control" name='tujuan' id='tujuan'>{OPT_TUJUAN}</select>
								</div>
								<div class="col-md-2">
									Tgl:<br /><input class="form-control" readonly="yes"  id="tgl_awal" name="tgl_awal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
								</div>
								<div class="col-md-2">
									s/d<br /><input class="form-control" readonly="yes"  id="tgl_akhir" name="tgl_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
								</div>
								<div class="col-md-3">
									Cari<br /><input type="text" class="form-control" id="cari" name="cari" value="{CARI}" />
								</div>
								<div class="col-md-2"><input type="submit" class="btn mybutton topwidth" value="cari" /></div>
							</td>
						</tr>
						<tr><td align='center' colspan='3'><br><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></td></tr>
						<tr>
							<td width='100%' align='right' colspan="3" valign='bottom'>{PAGING}</td>
						</tr>
					</table>
					</form>
					<table class="table table-hover table-bordered">
					    <tr>
					       <th rowspan="2" width="30">No</th>
					       <th rowspan="2" width="120">#resi</th>
					       <th rowspan="2" width="100">#Jadwal</th>
								 <th rowspan="2" width="100">Waktu Berangkat</th>
								 <th colspan='3' class="thintop">Pengirim</th>
								 <th colspan='3' class="thintop">Penerima</th>
								 <th rowspan="2" width="50">Berat<br>(Kg)</th>
								 <th rowspan="2" width="70">Harga</th>
								 <th rowspan="2" width="70">Diskon</th>
								 <th rowspan="2" width="70">Bayar</th>
								 <th rowspan="2" width="70">Layanan</th>
								 <th rowspan="2" width="100">Jenis Bayar</th>
					     </tr>
					     <tr>
					 			 <th width="100" class="thinbottom">Nama</th>
								 <th width="100" class="thinbottom">Alamat</th>
								 <th width="70"  class="thinbottom">Telp</th>
								 <th width="100" class="thinbottom">Nama</th>
								 <th width="100" class="thinbottom">Alamat</th>
								 <th width="70"  class="thinbottom">Telp</th>
					     </tr>
					     <!-- BEGIN ROW -->
					     <tr class="{ROW.odd}">
					       <td><div>{ROW.no}</div></td>
								 <td><div>{ROW.no_tiket}</div></td>
								 <td><div>{ROW.kode_jadwal}</div></td>
								 <td><div>{ROW.waktu_berangkat}</div></td>
								 <td><div>{ROW.nama_pengirim}</div></td>
								 <td><div>{ROW.alamat_pengirim}</div></td>
								 <td><div>{ROW.telp_pengirim}</div></td>
								 <td><div>{ROW.nama_penerima}</div></td>
								 <td><div>{ROW.alamat_penerima}</div></td>
								 <td><div>{ROW.telp_penerima}</div></td>
								 <td><div>{ROW.berat}</div></td>
								 <td><div>{ROW.harga}</div></td>
								 <td><div>{ROW.diskon}</div></td>
								 <td><div>{ROW.bayar}</div></td>
								 <td><div>{ROW.layanan}</div></td>
								 <td><div>{ROW.jenis_bayar}</div></td>
					     </tr>  
					     <!-- END ROW -->
			    </table>
			    <table width='100%'>
						<tr>
							<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
						</tr>
						<tr>
							<td colspan=13 align='center'>
								<br>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
						<tr>
							<td align="left">
								<table>
									<tr><td colspan="3">LAYANAN:<td></tr>
									<tr><td>P2</td><td>:</td><td>POINT TO POINT</td><tr>
									<tr><td>D2</td><td>:</td><td>DOOR TO DOOR</td><tr>
								</table>	
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>