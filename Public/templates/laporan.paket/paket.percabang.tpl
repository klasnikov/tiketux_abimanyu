<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
						<td align='center' valign='middle' class="bannerjudulw">
							<div class="bannerjudul">
								Laporan Omzet Paket per Cabang
							</div>
						</td></tr>
						<tr class='' height=40>
							<td align='left' valign='middle'>
								<div class="col-md-3">
									Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
								</div>
								<div class="col-md-3">
									s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
								</div>
								<div class="col-md-3">
									Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
								</div>
								<div class="col-md-3">
									<input class="btn mybutton topwidth" style="height: 34px;" name="btn_cari" type="submit" value="cari" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="pad10" align='center' valign='middle'>
								<table>
									<tr>
										<!--<td>
											 &nbsp;
										</td><td bgcolor='D0D0D0'></td>-->
										<td class="pad10">
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table width='100%'>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-bordered table-hover" style="margin-top: 10px;" width='100%' >
						<tr>
							<th width='30'>No</th>
							<th width='200'><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Cabang</a></th>
							<th width='100'><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}' >Pax Paket (Qty)</a></th>
							<th width='100'><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}' >Omz.Paket (Rp.)</a></th>
							<th width=100>Action</th>
					 </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td align='right'>{ROW.no}</td>
			       <td align='left'>{ROW.cabang}</td>
						 <td align='right'>{ROW.total_paket}</td>
						 <td align='right'>{ROW.omz_paket}</td>
			       <td align='center'>{ROW.act}</td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>