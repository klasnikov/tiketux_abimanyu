<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="{ROOT}css/login.css" />
	<style type="text/css">
	/*body{
		background: url("{TPL}images/login/{IMAGE_BACKGROUND}");
	}*/
	</style>
</style>
<body class="loginnya" id="loginnya">

	<div class="wrapper">
	  <form class="login" name="main" action="{U_LOGIN}" method="POST">
	    <p class="title">
		    <img  style="max-width: 150px;" src="{ROOT}img/logo.png">
		    <br> 
		    <img src="{ROOT}img/ic_logo_login.png" style="max-width: 100px;">
	    </p>
	    
	    <input type="text" size="30" id="username" name="username" value=""  placeholder='Username' autofocus="autofocus" />
	    <i class="fa fa-user"></i>
	    <input type="password" size="30" id="password" name="password" placeholder='Password' value=""  />
	    <i class="fa fa-key"></i>
	    <select id='opt_cabang' name='opt_cabang' class="form-control">
			{OPT_CABANG}
		</select>
	    <input id="submit" name="submit" value ="Log In" type="submit" />
	  </form>
	  <footer>
	  </footer>
	  </p>
	</div>

	<script type="text/javascript">
		document.getElementById("loginnya").style.height = 0;
	</script>
</body>p