<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript">

function init(e) {
	Element.show('progress');
	document.forms.frm_input_kartu.no_seri_kartu.focus();
}

dojo.addOnLoad(init);

var waktu_refresh=4;//dedtik
var flag=1;
var temp_waktu=waktu_refresh;

function beginrefresh(){
	if (temp_waktu==1) {
		temp_waktu=waktu_refresh;
		
		if(flag){
			flag=0;
			beginrefresh();
			Element.hide('progress');
			document.forms.frm_input_kartu.no_seri_kartu.focus();
		}
	}
	else {
		//get the minutes, seconds remaining till next refersh
		temp_waktu-=1;
		setTimeout("beginrefresh()",1000);
	}
	
}
//call the function beginrefresh
window.onload=beginrefresh();

</script>

<input type='hidden' id='hdn_asc' value='0' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='100%'>
    <tr>
    <td valign="middle" align="center">
		<h1>Registrasi Kartu Baru</h1>
		<span id='progress' style='display:none;'>{PESAN}</span><br>
    <form id='frm_input_kartu' name='frm_input_kartu' action='{U_ACTION}' method='post'>
			<table>
				<tr>
					<td>Silahkan masukan nomor seri kartu yang tertera pada kartu *</td>
					<td>
						<input type='text' id='no_seri_kartu' name='no_seri_kartu' value='{NO_SERI_KARTU}'/>
					</td>
				</tr>
				<tr>
					<td>Silahkan gesekkan kartu member yang akan diregristasi ke alat pembaca *</td>
					<td>
						<input type='password' id='id_kartu' name='id_kartu' />
					</td>
				</tr>
				<tr>
					<td colspan=2 align='right'><input type='submit' value="Daftarkan"/></td>
				</tr>
	    </table>
		</form>
		</td>
	</tr>
</table>