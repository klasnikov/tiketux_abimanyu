<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function batal(no_sj){
	
	if(confirm("Apakah anda yakin akan membatalkan transaksi ini?")){
		
		new Ajax.Request("surat_jalan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=batal&no_sj="+no_sj,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Surat Jalan</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'><select onchange='getUpdateAsal(this.value);' id='pool' name='pool'><option value=''>-semua pool-</option><option value="0" {POOL_0}>RAWA BOKOR</option><option value="1" {POOL_1}>CIHAMPELAS</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='left' width='400'>
					<a href="{U_ADD}">[+]Tambah Surat Jalan</a>&nbsp;|&nbsp;<a>{KETERANGAN_SORT}</a>
				</td>
				<td align='right'>
						{PAGING}
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
      <th width=30>No</th>
			<th width=100><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>#SJ</a></th>
			<th width=90><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Berangkat</a></th>
			<th width=80><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Kendaraan</a></th>
			<th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Sopir</a></th>
			<th width=50><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Trip</a></th>
			<th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Pool</a></th>
			<th width=170><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Rute</a></th>
			<th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Total</a></th>
			<th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>WaktuCetak</a></th>
			<th width=120><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Petugas</a></th>
			<th width=100>Remark</th>
			<th width=120>Action</th>
    </tr>
    <!-- BEGIN ROW -->
    <tr class="{ROW.odd}" title="{ROW.title}">
      <td align="right" valign="top">{ROW.no}</td>
			<td align="left" valign="top">{ROW.nosj}</td>
      <td align="center" valign="top">{ROW.berangkat}</td>
			<td align="left" valign="top">{ROW.kendaraan}</td>
      <td align="left" valign="top">{ROW.sopir}</td>
      <td align="right" valign="top">{ROW.trip}</td>
			<td align="left" valign="top">{ROW.pool}</td>
			<td align="left" valign="top">{ROW.rute}</td>
			<td align="right" valign="top">{ROW.total}</td>
			<td align="center" valign="top">{ROW.waktucetak}</td>
			<td align="left" valign="top">{ROW.petugas}</td>
			<td align="left" valign="top">{ROW.reamark}</td>
			<td align="center" valign="top">{ROW.act}</td>
    </tr>
    <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>