<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
		// komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function selectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=true;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}
	
	function deselectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=false;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}

	function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}

	
	function hapusData(kode){
		
		if(confirm("Apakah anda yakin akan menghapus data ini?")){
			
			if(kode!=''){
				list_dipilih="'"+kode+"'";
			}
			else{
				i=1;
				loop=true;
				list_dipilih="";
				do{
					str_var='checked_'+i;
					if(chk=document.getElementById(str_var)){
						if(chk.checked){
							if(list_dipilih==""){
								list_dipilih +=chk.value;
							}
							else{
								list_dipilih +=","+chk.value;
							}
						}
					}
					else{
						loop=false;
					}
					i++;
				}while(loop);
			}
				
			new Ajax.Request("pengaturan_kota.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=delete&list="+list_dipilih,
			 onLoading: function(request) 
			 {
			 },
			 onComplete: function(request) 
			 {
				
			 },
			 onSuccess: function(request) 
			 {			
				window.location.reload();
				deselectAll();
			},
			 onFailure: function(request) 
			 {
			 }
			})  
		}
		
		return false;
			
	}
		
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
		
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table class="table table-hover" cellspacing="0" cellpadding="0" border="0">
				<tr>
				 <td class="whiter" valign="middle" align="center">		
						<table width='100%' cellspacing="0">
							<tr class='' height="">
								<td align='center' valign='middle' class="bannerjudul">Master Kota </td>
								<td colspan=2 align='right' class="bannernormal" valign='middle'>
									<form action="{ACTION_CARI}" method="post">
										<div class="input-group">
									      <input type="text" class="form-control" id="cari" name="cari" value="{CARI}" size=50 />
									      <span class="input-group-btn">
									        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</form>
								</td>
							</tr>
							<tr>
								<td class="mytd" align='left'>
									<a onclick="window.open('{U_ADD}','_self');"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
									<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
								<td class="mytd" width='70%' align='right'>
									{PAGING}
								</td>
							</tr>
						</table>
						<table width='100%' class="border table table-hover table-bordered">
						    <!-- <tr>
						       <th ></th>
						       <th >No</th>
									 <th ><a class="th" id="sort1" href='#'>Kode</a></th>
									 <th ><a class="th" id="sort2" href='#'>Nama cabang</a></th>
									 <th ><a class="th" id="sort3" href='#'>Alamat</a></th>
									 <th ><a class="th" id="sort4" href='#'>Kota</a></th>
									 <th ><a class="th" id="sort5" href='#'>Telp</a></th>
									 <th ><a class="th" id="sort6" href='#'>Fax</a></th>
									 <th ><a class="th" id="sort7" href='#'>Tipe</a></th>
									 <th class="center">Action</th>
						     </tr> -->

						     <tr>
							    <th width="30"><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
							    <th >No</th>
									<th ><a class="th" id="sort1" href='#'>Kode Kota</a></th>
									<th ><a class="th" id="sort2" href='#'>Nama Kota</a></th>
									<th >Action</th>
							  </tr>
						     <!-- BEGIN ROW -->
							  <tr class="{ROW.odd}">
							    <td align="">{ROW.check}</td>
							    <td align="">{ROW.no}</td>
							    <td align="">{ROW.kodekota}</td>
									<td align="">{ROW.namakota}</td>
							    <td align="">{ROW.action}</td>
							  </tr>
							  <!-- END ROW -->
							
							  <!-- BEGIN NO_DATA -->
							  	<div style="font-size: 18px;background-color: yellow;text-align: center;width: 800px;">Tidak ada data ditemukan</div>
							  <!-- END NO_DATA -->
						    </table>
					    	<table class="table" width="100%">
								<tr>
									<td class="mytd" align='left'>
										<a onclick="window.open('{U_ADD}','_self');"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
										<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>	
									</td>
									<td class="mytd" width='70%' align='right'>
										{PAGING}
									</td>
								</tr>
							</table>
				 </td>
				</tr>
				</table>
		</div>
	</div>
</div>