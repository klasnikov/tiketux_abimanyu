<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<div class="row">
							<div class="col-md-4 bannerjudulw">
								<div class="bannerjudul" style="padding-left: 15px;">
									Laporan Rekap Uang Cabang
								</div>
							</div>
							<div class="col-md-8 searchwrapper">
								<div class="col-md-3 line-height left">
									Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								</div>
								<div class="col-md-3 line-height left">
									s/d <br />
									<input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								</div>
								<div class="col-md-4 line-height left">
									Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
								</div>
								<div class="col-md-2 " style="padding: 0;">
									<input class="btn mybutton mysearch" type="submit" value="cari" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 center">
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="pull-right kanan">
									<!-- {PAGING} -->
								</div>
							</div>
						</div>
					</table>
					<!-- END HEADER-->
					<table class="table table-hover table-bordered table-responsive" width='100%' style="margin-top: 10px;">
				    <tr>
				       <th width=30>No</th>
							 <th><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Cabang</a></th>
							 <th><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Cabang</a></th>
							 <th><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Alamat</a></th>
							 <th><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Tot.Pnp</a></th>
							 <th><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Tot.Omz.Pnp (Rp.)</a></th>
							 <th><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Tot.Pkt</a></th>
							 <th><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Tot.Omz.Pkt</a></th>
							 <th><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Tot.Disc</a></th>
							 <th><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Tot.Biaya Langsung</a></th>
							 <th><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Tot.Laba Kotor</a></th>
							 <th>Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td ><div>{ROW.no}</div></td>
				       <td ><div>{ROW.cabang}</div></td>
							 <td ><div>{ROW.kode_cabang}</div></td>
							 <td ><div>{ROW.alamat}</div></td>
							 <td ><div>{ROW.total_penumpang}</div></td>
							 <td ><div>{ROW.total_omzet}</div></td>
							 <td ><div>{ROW.total_paket}</div></td>
							 <td ><div>{ROW.total_omzet_paket}</div></td>
							 <td ><div>{ROW.total_discount}</div></td>
							 <td ><div>{ROW.total_biaya}</div></td>
							 <td ><div>{ROW.total_profit}</div></td>
				       <td ><div align="center">{ROW.act}</div></td>
				     </tr>
				     <!-- END ROW -->
				    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>