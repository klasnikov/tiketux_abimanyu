<script language="JavaScript">

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;

	if(kode.value==''){
		valid=false;
		isValidCss(kode,false);
	}

	if(asal.value==''){
		valid=false;
		isValidCss(asal,false);
	}

	if(tujuan.value==''){
		valid=false;
		isValidCss(tujuan,false);
	}

	switch(flag_op_jurusan.value){
    case "0":
    case "2":
      if(harga_tiket.value=='' || harga_tiket<=0){
        valid=false;
        isValidCss(harga_tiket,false);
      }

      if(harga_tiket_tuslah.value=='' || harga_tiket_tuslah<=0){
        valid=false;
        isValidCss(harga_tiket_tuslah,false);
      }
    break;

    case "3":
      if(hargatiketnonreg1.value=='' || hargatiketnonreg1.value<=0){
        valid=false;
        isValidCss(hargatiketnonreg1,false);
      }

      if(hargatiketnonreg2.value=='' || hargatiketnonreg2.value<=0){
        valid=false;
        isValidCss(hargatiketnonreg2,false);
      }

      if(hargatiketnonreg3.value=='' || hargatiketnonreg3.value<=0){
        valid=false;
        isValidCss(hargatiketnonreg3,false);
      }

      if(hargatiketnonreg4.value=='' || hargatiketnonreg4.value<=0){
        valid=false;
        isValidCss(hargatiketnonreg4,false);
      }
    break;
  }
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function lockUnlockHarga(nilai){

  showhargareguler.style.display    = "none";
  showharganonreguler.style.display = "none";
  showbiayapaket.style.display      = "none";

  switch(nilai){
    case "0":
      showhargareguler.style.display="inline-block";
    break;

    case "1":
      showbiayapaket.style.display="inline-block";
    break;

    case "2":
      showhargareguler.style.display="inline-block";
      showbiayapaket.style.display="inline-block";
    break;


    case "3":
      showharganonreguler.style.display="inline-block";
    break;
  }

}

</script>
<div class="container" style="width: 95%;">
	<div class="row">
		<div class="col-md-12 box">
		<form name="frm_data_jurusan" action="{U_ADD}" method="post" onSubmit='return validateInput();'>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="whiter" valign="middle" align="center">
			<table width='1000'>
				<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
				<tr>
					<td align='center' valign='top' width='500' class="pad10">
						<table width='500'>   
							<tr>
								<td colspan=3><h3>{JUDUL}</h3></td>
							</tr>
							<tr>
					      <input type="hidden" name="id_jurusan" value="{ID_JURUSAN}">
					      <input type="hidden" name="kode_jurusan_old" value="{KODE_JURUSAN_OLD}">
								<td class="td-title" width='200'>Kode Jurusan*</td><td width='5'></td>
								<td width='300'>
									<input type="text" placeholder="Kode Jurusan" class="form-control" id="kode" name="kode" value="{KODE_JURUSAN}" maxlength=30 onFocus="isValidCss(this,true);" required>
								</td>
					    </tr>
							<tr>
								<td class="td-title">Asal</td> <td></td>
								<td>
									<select required class="form-control martop10" id='asal' name='asal' onFocus="isValidCss(this,true);">
										{OPT_ASAL}
									</select>
								</td>
							</tr>
							<tr>
								<td class="td-title">Tujuan</td> <td></td>
								<td>
									<select required class="form-control martop10" id='tujuan' name='tujuan' onFocus="isValidCss(this,true);">
										{OPT_TUJUAN}
									</select>
								</td>
							</tr>
							<tr>
					      <td class="td-title">Status Aktif</td> <td></td>
								<td>
									<select class="form-control martop10" id="flag_aktif" name="flag_aktif">
										<option value=1}>AKTIF</option>
										<option value=0}>TIDAK AKTIF</option>
									</select>
								</td>
					    </tr> 
							<tr>
					      <td class="td-title">Jenis Jurusan</td><td></td>
								<td>
									<select  class="form-control martop10" id="flag_jenis" name="flag_jenis">
										<option value=1>Luar Kota</option>
										<option value=0>Dalam Kota</option>
									</select>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Operasional untuk</td> <td></td>
								<td>
									<select  class="form-control martop10" id="flag_op_jurusan" name="flag_op_jurusan" onChange="lockUnlockHarga(this.value);">
										<option value=0>Reguler</option>
										<option value=1>Paket</option>
										<option value=2>Reguler & Paket</option>
										<option value=3>Non-Reguler</option>
										<option value=4>Charter</option>
									</select>
								</td>
					    </tr>
		          <tr>
		            <td colspan="3"  >
		              <div id="showhargareguler" style="width: 100%;">
		                <table class="table table-hover">
		                  <thead><td colspan=3><h3>Harga Tiket</h3></td></thead>
		                  <tr><th width="100">Jenis Unit</th><th width="150">Normal</th><th width="150">Tuslah</th></tr>
		                  <!-- BEGIN LISTHARGATIKET -->
		                  <tr class="{LISTHARGATIKET.odd}">
		                    <td align="center">{LISTHARGATIKET.idlayout}</td>
		                    <td align="">
		                      <div class="input-group">
										<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
										<input type="text" id="hargatiket{LISTHARGATIKET.idlayout}" name="hargatiket{LISTHARGATIKET.idlayout}" value="{LISTHARGATIKET.hargatiket}" maxlength="6" size="10" onkeypress='validasiAngka(event);'style="text-align: right;">
							  </div>
		                    </td>
		                    <td align="">
		                      <div class="input-group">
								<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
									<input type="text" id="hargatikettuslah{LISTHARGATIKET.idlayout}" name="hargatikettuslah{LISTHARGATIKET.idlayout}" value="{LISTHARGATIKET.hargatikettuslah}" maxlength="6" size="10" onkeypress='validasiAngka(event);'style="text-align: right;">
								</div>
		                    </td>
		                  </tr>
		                  <!-- END LISTHARGATIKET -->
		                </table>
		              </div>
		              <div id="showharganonreguler" style="width: 100%;">
		                <span style="padding-bottom: 4px;display: inline-block;"><span style="width:195px;display: inline-block">Harga Tiket 1 /org* (Rp.)</span><span style="width: 8px;display: inline-block"">:</span><span><input type="text" id="hargatiketnonreg1" name="hargatiketnonreg1" value="{HARGA_TIKET1}" maxlength=8 onFocus="isValidCss(this,true);" onkeypress='validasiAngka(event);' style="text-align: right;"></span></span>
		                <span style="padding-bottom: 4px;display: inline-block;"><span style="width:195px;display: inline-block">Harga Tiket 2 /org* (Rp.)</span><span style="width: 8px;display: inline-block"">:</span><span><input type="text" id="hargatiketnonreg2" name="hargatiketnonreg2" value="{HARGA_TIKET2}" maxlength=8 onFocus="isValidCss(this,true);" onkeypress='validasiAngka(event);' style="text-align: right;"></span></span>
		                <span style="padding-bottom: 4px;display: inline-block;"><span style="width:195px;display: inline-block">Harga Tiket 3 /org* (Rp.)</span><span style="width: 8px;display: inline-block"">:</span><span><input type="text" id="hargatiketnonreg3" name="hargatiketnonreg3" value="{HARGA_TIKET3}" maxlength=8 onFocus="isValidCss(this,true);" onkeypress='validasiAngka(event);' style="text-align: right;"></span></span>
		                <span style="padding-bottom: 4px;display: inline-block;"><span style="width:195px;display: inline-block">Harga Tiket >=4 /org* (Rp.)</span><span style="width: 8px;display: inline-block"">:</span><span><input type="text" id="hargatiketnonreg4" name="hargatiketnonreg4" value="{HARGA_TIKET4}" maxlength=8 onFocus="isValidCss(this,true);" onkeypress='validasiAngka(event);' style="text-align: right;"></span></span>
		              </div>
		            </td>
		          </tr>
		          <tr>
		            <td colspan="3"  class="pad10">
		              <div id="showbiayapaket" style="width: 100%;">
		                <hr>
		                <h3>Harga Kirim Paket</h3>
		                <span style="padding-bottom: 4px;display: inline-block;"><span style="font-size: 14px; width:195px;display: inline-block">Dokumen</span>
							<div class="input-group">
								<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
								<input type="text" class="form-control" id="harga_paket_1_kilo_pertama" name="harga_paket_1_kilo_pertama" value="{HARGA_PAKET_1_KILO_PERTAMA}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
								<span class="input-group-addon" style="background: none; border: none;">/Kg pertama</span>
							</div>
							<div class="input-group">
								<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
								<input class="form-control" type="text" id="harga_paket_1_kilo_berikut" name="harga_paket_1_kilo_berikut" value="{HARGA_PAKET_1_KILO_BERIKUT}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
								<span class="input-group-addon" style="background: none; border: none;">/Kg berikutnya</span>
							</div>
		                </span>
		                <br><br>
		                


		                <span style="padding-bottom: 4px;display: inline-block;"><span style="font-size: 14px; width:195px;display: inline-block">Barang</span>

		                <div class="input-group">
							<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
							<input class="form-control" type="text" id="harga_paket_2_kilo_pertama" name="harga_paket_2_kilo_pertama" value="{HARGA_PAKET_2_KILO_PERTAMA}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
							<span class="input-group-addon" style="background: none; border: none;">/Kg pertama</span>
						</div>
						<div class="input-group">
							<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
							<input class="form-control" type="text" id="harga_paket_2_kilo_berikut" name="harga_paket_2_kilo_berikut" value="{HARGA_PAKET_2_KILO_BERIKUT}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
							<span class="input-group-addon" style="background: none; border: none;">/Kg berikutnya</span>
						</div>
						</span>


		                <br><br>
		                <span style="padding-bottom: 4px;display: inline-block;">

		                <span style="font-size: 14px; width:195px;display: inline-block">Charge Bagasi</span>
						
						<div class="input-group">
							<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
							<input class="form-control" type="text" id="harga_paket_3_kilo_pertama" name="harga_paket_3_kilo_pertama" value="{HARGA_PAKET_3_KILO_PERTAMA}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
							<span class="input-group-addon" style="background: none; border: none;">/Kg pertama</span>
						</div>


						<div class="input-group">
							<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
							<input class="form-control" type="text" id="harga_paket_3_kilo_berikut" name="harga_paket_3_kilo_berikut" value="{HARGA_PAKET_3_KILO_BERIKUT}" maxlength=8 size=12 onkeypress='validasiAngka(event);' style="text-align: right;">
							<span class="input-group-addon" style="background: none; border: none;">/Kg berikutnya</span>
						</div>

						</span>
		                





		              </div>
		            </td>
		          </tr>
						</table>
					</td>
					<td width=1 bgcolor='D0D0D0'></td>
					<td align='center' valign='top' width='500'  class="pad10" style="padding-left: 40px;">
						<table width='500'>
							<tr><td colspan=3><h3>Biaya-biaya</h3></td></tr>
							<tr>
					      <td class="td-title">Biaya Tol</td> <td></td>
								<td>
									<div class="input-group">
									  <span class="input-group-addon" style="background: none; border: none;">Rp.</span>
									  <input placeholder="Biaya Tol" class="form-control" type="text" id="biayatol" name="biayatol" value="{BIAYA_TOL}" maxlength=8 onkeypress='validasiAngka(event);'>
									</div>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Biaya Sopir</td> <td></td>
								<td>
									<div class="input-group">
									  <span class="input-group-addon" style="background: none; border: none;">Rp.</span>
									  <input placeholder="Biaya Sopir" class="form-control" type="text" id="biayasopir" name="biayasopir" value="{BIAYA_SOPIR}" maxlength=8 onkeypress='validasiAngka(event);'>
									 </div>
								</td>
					    </tr>
							<tr>
					      <td class="td-title">Biaya Parkir</td> <td></td>
								<td>
									<div class="input-group">
										<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
										<input placeholder="Biaya Parkir" class="form-control" type="text" id="biayaparkir" name="biayaparkir" value="{BIAYA_PARKIR}" maxlength=8 onkeypress='validasiAngka(event);'>
									</div>
								</td>
					    </tr>
							<tr><td colspan='3'><hr></td></tr>
							<tr><td colspan=3><h3>Liter BBM</h3></td></tr>
							<!-- BEGIN LISTBBM -->
							<tr>
					      	<td class="td-title">Layout {LISTBBM.idlayout}</td> <td></td>
								<td>
									<div class="input-group">
										<input class="form-control" type="text" id="biayabbm{LISTBBM.idlayout}" name="biayabbm{LISTBBM.idlayout}" value="{LISTBBM.literbbm}" maxlength="3" size="5" onkeypress='validasiAngka(event);'style="text-align: right;"> 
										<span class="input-group-addon" style="background: none; border: none;">Liter</span>
										
									</div>
								</td>
					    </tr>
							<!-- END LISTBBM -->
							<!--<tr>
					      <td valign='top'>Paket Small</td><td  valign='top'>:</td>
								<td valign='top'>
									Rp.&nbsp;<input type="text" id="harga_paket_3_kilo_pertama" name="harga_paket_3_kilo_pertama" value="{HARGA_PAKET_3_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
									Rp.&nbsp;<input type="text" id="harga_paket_3_kilo_berikut" name="harga_paket_3_kilo_berikut" value="{HARGA_PAKET_3_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
								</td>
					    </tr>
							<tr>
					      <td valign='top'>Paket Medium</td><td  valign='top'>:</td>
								<td valign='top'>
									Rp.&nbsp;<input type="text" id="harga_paket_4_kilo_pertama" name="harga_paket_4_kilo_pertama" value="{HARGA_PAKET_4_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
									Rp.&nbsp;<input type="text" id="harga_paket_4_kilo_berikut" name="harga_paket_4_kilo_berikut" value="{HARGA_PAKET_4_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
								</td>
					    </tr>
							<tr>
					      <td valign='top'>Paket Large</td><td  valign='top'>:</td>
								<td valign='top'>
									Rp.&nbsp;<input type="text" id="harga_paket_5_kilo_pertama" name="harga_paket_5_kilo_pertama" value="{HARGA_PAKET_5_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
									Rp.&nbsp;<input type="text" id="harga_paket_5_kilo_berikut" name="harga_paket_5_kilo_berikut" value="{HARGA_PAKET_5_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
								</td>
					    </tr>
							<tr>
					      <td valign='top'>Paket Xtra Large</td><td  valign='top'>:</td>
								<td valign='top'>
									Rp.&nbsp;<input type="text" id="harga_paket_6_kilo_pertama" name="harga_paket_6_kilo_pertama" value="{HARGA_PAKET_6_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
									Rp.&nbsp;<input type="text" id="harga_paket_6_kilo_berikut" name="harga_paket_6_kilo_berikut" value="{HARGA_PAKET_6_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya
								</td>
					    </tr>
							<tr><td colspan=3><br><h3>Kode-kode Akun:</h3></td></tr>
							<tr>
								<td width='200'>Kode Akun Pendapatan Penumpang</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_pendapatan_penumpang" name="kode_akun_pendapatan_penumpang" value="{KODE_AKUN_PENDAPATAN_PENUMPANG}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Pendapatan Paket</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_pendapatan_paket" name="kode_akun_pendapatan_paket" value="{KODE_AKUN_PENDAPATAN_PAKET}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Biaya Sopir</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_biaya_sopir" name="kode_akun_biaya_sopir" value="{KODE_AKUN_BIAYA_SOPIR}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Biaya Tol</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_biaya_tol" name="kode_akun_biaya_tol" value="{KODE_AKUN_BIAYA_TOL}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='300'>Kode Akun Biaya Parkir</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_biaya_parkir" name="kode_akun_biaya_parkir" value="{KODE_AKUN_BIAYA_PARKIR}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='300'>Kode Akun Biaya BBM</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_biaya_bbm" name="kode_akun_biaya_bbm" value="{KODE_AKUN_BIAYA_BBM}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Komisi Sopir/Penumpang</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_komisi_penumpang_sopir" name="kode_akun_komisi_penumpang_sopir" value="{KODE_AKUN_KOMISI_PENUMPANG_SOPIR}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Komisi CSO/Penumpang</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_komisi_penumpang_cso" name="kode_akun_komisi_penumpang_cso" value="{KODE_AKUN_KOMISI_PENUMPANG_CSO}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Komisi Sopir/Paket</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_komisi_paket_sopir" name="kode_akun_komisi_paket_sopir" value="{KODE_AKUN_KOMISI_PAKET_SOPIR}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Komisi CSO/Paket</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_komisi_paket_cso" name="kode_akun_komisi_paket_cso" value="{KODE_AKUN_KOMISI_PAKET_CSO}" maxlength=20>
								</td>
							</tr>
							<tr>
								<td width='200'>Kode Akun Charge</td><td width='5'>:</td>
								<td>
									<input type="text" id="kode_akun_charge" name="kode_akun_charge" value="{KODE_AKUN_CHARGE}" maxlength=20>
								</td>
							</tr>-->
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=3 align='center' valign='middle'>
						<input type="hidden" name="mode" value="{MODE}">
						<input type="hidden" name="submode" value="{SUB}">
						<div class="col-md-4 col-md-offset-4">
							<div class="col-md-6"><input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI">&nbsp;&nbsp;&nbsp;</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>



				</tr>           
			</table>
			</td>
		</tr>
		</table>
		</form>
		</div>
	</div>
</div>


<script language="javascript">
  lockUnlockHarga('{OP_JUR_SELECT}');
  flag_op_jurusan.value = {OP_JUR_SELECT};
  flag_jenis.value      = {FLAG_JENIS_SELECT};
  flag_aktif.value      = {FLAG_AKTIF_SELECT};
</script>