<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}


function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}

		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_jurusan="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatusAktif(id){
	
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatusaktif&id="+id,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function ubahStatusTuslah(id,tuslah){
		
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatustuslah&id="+id+"&tuslah="+tuslah,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}


</script>

<div class="container" style="width: 95%;">
	<div class="row">
		<div class="col-md-12 box">
			<table class="table table-hover" cellspacing="0" cellpadding="0" border="0">
				<tr>
				 <td class="whiter" valign="middle" align="center">		
						<table width='100%' cellspacing="0">
							<tr class='' height="">
								<td align='center' valign='middle' class="bannerjudul">Master Jurusan </td>
								<td colspan=2 align='right' class="bannernormal" valign='middle'>
									<form action="{ACTION_CARI}" method="post">
										<div class="input-group">
									      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
									      <span class="input-group-btn">
									        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</form>
								</td>
							</tr>
							<tr>
								<td class="mytd" align='left'>
									<a href="{U_JURUSAN_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
									<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
								<td class="mytd" width='70%' align='right'>
									{PAGING}
								</td>
							</tr>
						</table>
						<table width='100%' class="border table table-hover table-bordered">
						    <tr>
						       <th   ><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
						       <th   >No</th>
							    <th   >#Jurusan</th>
							    <th   >Asal</th>
							    <th   >Tujuan</th>
							    <th   >Jenis</th>
							    <th width="100">Harga Tiket</th>
							    <th   >Harga Tiket Tuslah</th>
							    <th >Harga Kirim Paket (Dok)</th>
							    <th>Harga Kirim Barang</th>
							    <th   >Biaya</th>
							    <th   >Aktif</th>
							    <th   >Action</th>
						     </tr>
						     <!-- BEGIN ROW -->
							  <tr class="{ROW.odd}">
							    <td >{ROW.check}</td>
							    <td >{ROW.no}</td>
							    <td >{ROW.kode}</td>
							    <td >{ROW.asal}</td>
							    <td >{ROW.tujuan}</td>
							    <td  class="{ROW.class_op_jurusan}" title="{ROW.titlejenis}">{ROW.jenis}</td>
							    <td >{ROW.harga_tiket}</td>
							    <td >{ROW.harga_tiket_tuslah}</td>
							    <td >{ROW.harga_paket_1}</td>
							    <td >{ROW.harga_paket_2}</td>
							    <td >Tol: Rp.{ROW.biayatol}<br>Parkir: Rp.{ROW.biayaparkir}<br>Sopir: Rp.{ROW.biayasopir}</td>
							    <!--<td><div align="right">{ROW.harga_paket_3}</div></td>
							    <td><div align="right">{ROW.harga_paket_4}</div></td>
							    <td><div align="right">{ROW.harga_paket_5}</div></td>
							    <td><div align="right">{ROW.harga_paket_6}</div></td>-->
							    <td >{ROW.status_aktif}</td>
							    <td >{ROW.action}</td>
							  </tr>
							  <!-- END ROW -->
							  {NO_DATA}
						    </table>
					    	<table class="table" width="100%">
								<tr>
									<td class="mytd" align='left'>
										<a href="{U_JURUSAN_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
										<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
									<td class="mytd" width='70%' align='right'>
										{PAGING}
									</td>
								</tr>
							</table>
				 </td>
				</tr>
				</table>
		</div>
	</div>
</div>