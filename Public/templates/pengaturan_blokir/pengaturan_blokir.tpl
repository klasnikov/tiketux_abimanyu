<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>
<script type="text/javascript">

function ubah(){
	
	status_blokir	= document.getElementById("status_blokir").value;
	
	if(confirm("Apakah anda yakin akan memblokir aplikasi ini?")){
		
			new Ajax.Request("pengaturan_blokir.php?sid={SID}",
			{
				asynchronous: true,
				method: "get",
				parameters: "mode=ubah&status_blokir="+status_blokir,
				onLoading: function(request){
				},
				onComplete: function(request){
				},
				onSuccess: function(request) {			
					if(request.responseText==1){
						alert('Proses berhasil dilakukan');
					}
					else{
						alert("Terjadi kegagalan!");
					}
					
				},
				onFailure: function(request){
				}
			})  
	}
	
	return false;
		
}

function init(e) {
	
	//control dialog data
	dialog_data = dojo.widget.byId("dialog_data");
}

dojo.addOnLoad(init);

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Pengaturan Blokir</td>
			</tr>
		</table>
 </td>
</tr>
<tr>
	<td>
		<table width='100%'>
			<tr>
				<td align='center' valign='middle'>
					<br>
					<br>
					<br>
						STATUS BLOKIR:&nbsp;
						<select id='status_blokir'>
							<option value='0' {STATUS_BLOKIR0}>Open</option>
							<option value='1' {STATUS_BLOKIR1}>Blokir</option>
						</select>&nbsp;<input type="button" value="Simpan" onClick="ubah();" />&nbsp;
					<br>
					<br>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>