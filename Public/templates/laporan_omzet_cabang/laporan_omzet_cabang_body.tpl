<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
		
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>
<div class="container" style="width: 95%;">
	<div class="row">
		<div class="col-md-12 box">

			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
								<div class="bannerjudul">
									Laporan Omzet Cabang
								</div>
							</td>
							<td align='left' valign='middle'>
								<div class="col-md-3">
									Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								</div>
								<div class="col-md-3">
									s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								</div>
								<div class="col-md-3">
									Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
								</div>
								<div class="col-md-3">
									<input type="submit" class="btn mybutton topwidth" style="height: 34px;" value="cari" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center' valign='middle'>
								<table>
									<tr>
										<td  class="pad10">
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table width='100%'>
									<tr>
										<td align='center' valign='bottom' colspan=3>
											<table class="border">
												<tr><td colspan='7' class="pad10" align="center"><b>TOTAL</b></td></tr>
												<tr>
													<td>
														<span class="action"><font style="color: #2196FD;"><b>Book</b> = {TOTAL_B}</font></span>
														<span class="action"><b>Umum</b> = {TOTAL_U}</span>
														<span class="action"><b>Anak/Lansia</b> = {TOTAL_K}</span>
														<span class="action"><b>Member</b> = {TOTAL_M}</span>
														<span class="action"><b>Staff</b> = {TOTAL_KK}</span>
														<span class="action"><b>Gratis</b> = {TOTAL_G}</span>
														<span class="action"><b>Return</b> = {TOTAL_R}</span>
														<span class="action"><b>Voucher</b> = {TOTAL_V}</span>
														<span class="action"><b>Online</b> = {TOTAL_O}</span>
														<span class="action"><font style="color: green;"><b>TOTAL</b> = {TOTAL_T}</font></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-bordered table-hover" width='100%' >
			    <tr>
			       <th width=30 rowspan=2 >No</th>
						 <th width=400 rowspan=2><a class="th" id="sort1" href='#'>Cabang</a></th>
						 <th width=100 rowspan=2><a class="th" id="sort2" href='#'>Kode Cabang</a></th>
						 <th width=100 rowspan=2><a class="th" id="sort3" href='#'>Alamat</a></th>
						 <th width=100 rowspan=2><a class="th" id="sort4" href='#'>Open Trip</a></th>
						 <th width=100 rowspan=2><a class="th" id="sort5" href='#'>Trip</a></th>
						 <th colspan=10 class='thintop'>Total Penumpang</th>
						 <th width=100 rowspan=2><a class="th" id="sort6" href='#'>Pnp<br>/Trip</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort7" href='#'>Tot.Omz.Pnp (Rp.)</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort8" href='#'>Tot.Pkt</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort9" href='#'>Tot.Omz.Pkt</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort10" href='#'>Tot.Disc</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort11" href='#'>Tot.Biaya Langsung</a></th>
						 <th width=200 rowspan=2><a class="th" id="sort12" href='#'>Tot.Laba Kotor</a></th>
						 <th width=100 rowspan=2>Action</a></th>
			     </tr>
					 <tr>
						<th class='thinbottom'><a class="th" id="sort13" href='#'>B</a></th>
						<th class='thinbottom'><a class="th" id="sort14" href='#'>U</a></th>
						<th class='thinbottom'><a class="th" id="sort15" href='#'>M</a></th>
						<th class='thinbottom'><a class="th" id="sort16" href='#'>M</a></th>
						<th class='thinbottom'><a class="th" id="sort17" href='#'>S</a></th>
						<th class='thinbottom'><a class="th" id="sort18" href='#'>G</a></th>
						<th class='thinbottom'><a class="th" id="sort19" href='#'>R</a></th>
						<th class='thinbottom'><a class="th" id="sort20" href='#'>V</a></th>
						<th class='thinbottom'><a class="th" id="sort21" href='#'>O</a></th>
						<th class='thinbottom'><a class="th" id="sort22" href='#'>T</a></th>
					 </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td ><div align="right">{ROW.no}</div></td>
			       <td ><div align="left">{ROW.cabang}</div></td>
						 <td ><div align="left">{ROW.kode_cabang}</div></td>
						 <td ><div align="left">{ROW.alamat}</div></td>
						 <td ><div align="right">{ROW.open_trip}</div></td>
						 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
						 <td width=60 style="background: #2196FD;color: white;"><div align="right" >{ROW.total_penumpang_b}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_r}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_v}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_o}</div></td>
						 <td width=60 style="background: yellow"><div align="right" >{ROW.total_penumpang}</div></td>
						 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
						 <td ><div align="right">{ROW.total_omzet}</div></td>
						 <td ><div align="right">{ROW.total_paket}</div></td>
						 <td ><div align="right">{ROW.total_omzet_paket}</div></td>
						 <td ><div align="right">{ROW.total_discount}</div></td>
						 <td ><div align="right">{ROW.total_biaya}</div></td>
						 <td ><div align="right">{ROW.total_profit}</div></td>
			       <td ><div align="center">{ROW.act}</div></td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align="center">
								<table class="border">
									<tr><td colspan='7' align="center" style="padding: 10px;"><b>TOTAL</b></td></tr>
									<tr>
										<td>
											<span class="action"><font style="color: #2196FD;"><b>Book</b> = {TOTAL_B}</font></span>
											<span class="action"><b>Umum</b> = {TOTAL_U}</span>
											<span class="action"><b>Anak/Lansia</b> = {TOTAL_K}</span>
											<span class="action"><b>Member</b> = {TOTAL_M}</span>
											<span class="action"><b>Staff</b> = {TOTAL_KK}</span>
											<span class="action"><b>Gratis</b> = {TOTAL_G}</span>
											<span class="action"><b>Return</b> = {TOTAL_R}</span>
											<span class="action"><b>Voucher</b> = {TOTAL_V}</span>
											<span class="action"><b>Online</b> = {TOTAL_O}</span>
											<span class="action"><font style="color: green;"><b>TOTAL</b> = {TOTAL_T}</font></span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
								<table>
									<tr><td>B</td><td width="15">=</td><td>Penumpang Booking</td></tr>
									<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
									<tr><td>MH</td><td>=</td><td>Mahasiswa</td></tr>
									<tr><td>M</td><td>=</td><td>Member</td></tr>
									<tr><td>S</td><td>=</td><td>Special Ticket Staff</td></tr>
									<tr><td>G</td><td>=</td><td>Tiket Gratis</td></tr>
									<tr><td>O</td><td>=</td><td>online</td></tr>
									<tr><td>T</td><td>=</td><td>Total</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
</div>
	</div>
</div>