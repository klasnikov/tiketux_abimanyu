<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
$('#tanggal_mulai').datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate",new Date());
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<div class="container" style="width: 90%;">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='left' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan {NAMA}</td>
							<td align='right'valign='middle' colspan="2">
								<form action="{ACTION_CARI}" method="post">
										<table>
											<tr>
												<td>
													Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="date" value="{TGL_AWAL}" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)">
												</td>
												<td>
													s/d
													<input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text"  value="{TGL_AKHIR}" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)">
												</td>
												<td>
													<input class="mybutton btn topwidth" style="margin-top: 16px;" type="submit" value="cari" />
												</td>
											</tr>
										</table>
								</form>
							</td>
						</tr>
					</table>
					<table width="100%">	
						<tr>
							<td>{SUMMARY}</td>
							<td style="padding: 20px; padding-right: 0;" colspan='2' align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
					<table class="table table-bordered table-hover">
				    <tr>
				       <th width=30>No</th>
							 <th width=200>Waktu Pesan</th>
							 <th width=200>No.Tiket</th>
							 <th width=200>Waktu Berangkat</th>
							 <th width=100>Kode Jadwal</th>
							 <th width=200>Nama</th>
							 <th width=50>Kursi</th>
							 <th width=100>Harga Tiket</th>
							 <th width=100>Discount</th>
							 <th width=70>Total</th>
							 <th width=100>Tipe Disc.</th>
							 <th width=200>CSO</th>
							 <th width=100>Status</th>
							 <th width=100>Ket.</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="right">{ROW.no}</div></td>
				       <td><div align="left">{ROW.waktu_pesan}</div></td>
							 <td><div align="left">{ROW.no_tiket}</div></td>
				       <td><div align="left">{ROW.waktu_berangkat}</div></td>
				       <td><div align="left">{ROW.kode_jadwal}</div></td>
							 <td><div align="left">{ROW.nama}</div></td>
							 <td><div align="center">{ROW.no_kursi}</div></td>
							 <td><div align="right">{ROW.harga_tiket}</div></td>
							 <td><div align="right">{ROW.discount}</div></td>
							 <td><div align="right">{ROW.total}</div></td>
							 <td><div align="left">{ROW.tipe_discount}</div></td>
							 <td><div align="left">{ROW.cso}</div></td>
				       <td><div align="center">{ROW.status}</div></td>
				       <td><div align="left">{ROW.ket}</div></td>
				     </tr>  
				     <!-- END ROW -->
				    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
