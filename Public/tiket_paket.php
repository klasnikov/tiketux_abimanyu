<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSms.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];
$cso							= $userdata['nama'];

$Reservasi	= new Reservasi();
$Paket			= new Paket();

//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
$row	= $Paket->ambilDataDetail($no_tiket);

$no_tiket						= $row['NoTiket'];
$nama_pengirim			= $row['NamaPengirim'];
$telp_pengirim			= $row['TelpPengirim'];
$alamat_pengirim		= $row['AlamatPengirim'];
$nama_penerima			= $row['NamaPenerima'];
$telp_penerima			= $row['TelpPenerima'];
$alamat_penerima		= $row['AlamatPenerima'];
$tanggal						= FormatMySQLDateToTgl($row['TglBerangkat']);
$jam								= $row['JamBerangkat'];
$asal								= explode("(",$row['NamaAsal']);
$tujuan							= explode("(",$row['NamaTujuan']);
$harga_paket				= number_format($row['HargaPaket'],0,",",".");
$operator						= $row['NamaCSO'];
$cetak_tiket				= $row['CetakTiket'];
$jenis_pembayaran		= ($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
$kode_jadwal				= $row['KodeJadwal'];
$jenis_barang				= $LIST_JENIS_LAYANAN_PAKET[$row['Dimensi']];
$jenis_layanan			= ($row['JenisLayanan']=='P2P'?"Port to Port":"Port to Door ");
$keterangan					= $row['KeteranganPaket'];
$waktu_cetak_tiket	= $row['CetakTiket']==0?date("d-m-Y H:i:s"):FormatMySQLDateToTglWithTime($row['WaktuCetakTiket']);

switch($row['CaraPembayaran']){
  case 0:
    $cara_bayar	= "TUNAI";
    break;
  case 1:
    $cara_bayar	= "LANGGANAN";
    break;
  case 2:
    $cara_bayar	= "<b>BAYAR DI TUJUAN</b>";
    break;
}

$id_jurusan	= $row['IdJurusan'];

$output_tiket	= "<table width='300' cellspacing='0' cellpadding='0'>";

//TIKET==========

if(!$row["IsVolumetrik"]){
  $perhitungan  = "BERAT";
  $dimensi      = "";
}
else{
  $perhitungan  = "VOLUMETRIK";
  $dimensi      = $row["Panjang"]." cm X ".$row["Lebar"]." cm X ".$row["Tinggi"]." cm<br>";
}


//content
$output_tiket	.="
	<tr><td class='tiket_body' align='center'>
		<b>RESI PENGIRIMAN PAKET</b><br>
		<div style='font-size:24px;text-transform:uppercase;font-weight:bold;'>$tujuan[0]</div>
		<div style='font-size:24px;text-transform:uppercase;font-weight:bold;'>$no_tiket</div>
		Tgl.Kirim: $tanggal<br>
		Carier:".substr($asal[0],0,20)."-". substr($tujuan[0],0,20)."<br>
		Jam: $jam<br>
		---------------------------<br>
		<div style='font-size:24px;text-transform:uppercase;'>$jenis_barang</div>
		<div style='font-size:18px;text-transform:uppercase;'>$jenis_layanan</div>
		---------------------------<br>
		Pengirim:<br>
		$nama_pengirim<br>
		$telp_pengirim<br>
		---------------------------<br>
		Penerima:<br>
		$nama_penerima<br>
		$telp_penerima<br>
		---------------------------<br>
		Keterangan:<br>
		$keterangan<br>
		---------------------------<br>
		<div style='text-align:center;'>Pax:$row[JumlahKoli] Pax</div>
		Perhitungan: $perhitungan<br>
		$dimensi
		$cara_bayar<br>
		Biaya: Rp. $harga_paket<br>
		CSO:$operator<br>
		cetak:$waktu_cetak_tiket<br>
		=========<font style='font-size:14px;'>potong disini</font>=========<br><br>";

//ditempel di paket
for($idx=1;$idx<=$row['JumlahKoli'];$idx++){
  $output_tiket	.="
		<b>DITEMPEL DI PAKET</b><br>
		<div style='font-size:36px;text-transform:uppercase;font-weight:bold;'>$tujuan[0]</div>
		<div style='font-size:24px;text-transform:uppercase;font-weight:bold;'>$row[NoTiket]</div>
		<div style='font-size:20px'>Pax $idx dari $row[JumlahKoli] Pax</div>
		Tgl.Kirim: ".FormatMySQLDateToTgl($row['TglBerangkat'])."<br>".
    "Carier:".substr($asal[0],0,20)."-". substr($tujuan[0],0,20)."<br>
		Jam: $row[JamBerangkat]<br>
		---------------------------<br>
		<div style='font-size:18px;text-transform:uppercase;'>$jenis_barang</div>
		<div style='font-size:18px;text-transform:uppercase;'>$jenis_layanan</div>
		---------------------------<br>
		Untuk:<br>
		<div style='font-size:24px;text-transform:uppercase;font-weight:bold;'>$row[NamaPenerima]</div>
		$telp_penerima<br>
		$alamat_penerima<br>
		---------------------------<br>
		Perhitungan: $perhitungan<br>
		$dimensi
		Keterangan:<br>
		$keterangan<br>
		---------------------------<br>
		CSO:$row[NamaCSO]<br>
		cetak:$waktu_cetak_tiket<br>";

  if($row['JumlahKoli']>1 && $idx!=$row['JumlahKoli']){
    $output_tiket .="=========<font style='font-size:14px;'>potong disini</font>=========<br><br>";
  }
}

$output_tiket .="</td></tr>";

//mengupate flag cetak tiket
if($cetak_tiket!=1){
  $cabang_transaksi	=($cara_bayar<$PAKET_CARA_BAYAR_DI_TUJUAN)?$userdata['KodeCabang']:"";
  $Paket->updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi);


  $LogSMS	= new LogSMS();

  //SMS REMINDER
  //mengambil asal dan tujuan
  $asal		= substr($asal[0],0,10);
  $tujuan	= substr($tujuan[0],0,10);

  $keterangan_bayar	= $row['CaraPembayaran']!=2?"":"PAKET DIBAYAR OLEH PENERIMA,";

  //Mengirim kepada penerima
  if(in_array(substr($telp_penerima,0,2),$HEADER_NO_TELP)){

    $isi_pesan	= "SDR/I ".strtoupper(substr($nama_pengirim,0,10))." MENGIRIMKAN PAKET UNTUK ANDA PADA ".dateparse($tanggal)." $jam RESI $no_tiket.PAKET DAPAT DIAMBIL DI DAYTRANS $tujuan.". $keterangan_bayar ." INFO 021123555";

    $telepon	= "62".substr($telp_penerima,1);
    $parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";
    //$response	= sendHttpPost($sms_config['url'],$parameter);

    if($response=="00"){
      $LogSMS->tambah(
        $telp_penerima, $nama_penerima, 2,
        $no_tiket, $isi_pesan);
    }
  }

  //Mengirim kepada pengirim
  if(in_array(substr($telp_pengirim,0,2),$HEADER_NO_TELP)){

    $isi_pesan	= "TERIMA KASIH SDR/I ".strtoupper(substr($nama_pengirim,0,10))." SUDAH MENGIRIMKAN PAKET PADA ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam RESI:$no_tiket.PAKET DIKIRIM KE DAYTRANS $tujuan. $keterangan_bayar,INFO 021123456";

    $telepon	= "62".substr($telp_pengirim,1);
    $parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";
    //$response	= sendHttpPost($sms_config['url'],$parameter);

    if($response=="00"){
      $LogSMS->tambah(
        $telp_pengirim, $nama_pengirim, 2,
        $no_tiket, $isi_pesan);
    }

  }

}

$template->set_filenames(array('body' => 'tiket_paket.tpl'));
$template->assign_vars(array(
    'OUTPUT_TIKET'    				=>$output_tiket
  )
);

$template->pparse('body');

?>
