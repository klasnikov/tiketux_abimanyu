-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5169
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for tiketux_abimanyu
CREATE DATABASE IF NOT EXISTS `tiketux_abimanyu` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;
USE `tiketux_abimanyu`;

-- Dumping structure for function tiketux_abimanyu.f_cabang_get_kota_by_kode_cabang
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_kota_by_kode_cabang`(`p_kode` VARCHAR(30)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kota VARCHAR(50);

  SELECT
    Kota INTO p_kota
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_kota;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_cabang_get_name_by_kode
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_name_by_kode`(`p_kode` VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT
    Nama INTO p_nama
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_nama;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_cabang_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_periksa_duplikasi`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeCabang) INTO p_jum_data
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_discount_get_jumlah_by_id
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_discount_get_jumlah_by_id`(`p_id` INTEGER) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT JumlahDiscount INTO p_jumlah
  FROM tbl_jenis_discount
  WHERE IdDiscount=p_id;

  RETURN p_jumlah;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jadwal_ambil_id_jurusan_by_kode_jadwal
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_id_jurusan_by_kode_jadwal`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_id_jurusan INTEGER;

  SELECT  IdJurusan INTO p_id_jurusan
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_id_jurusan;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jadwal_ambil_jumlah_kursi_by_kode_jadwal
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_jumlah_kursi_by_kode_jadwal`(`p_kode` VARCHAR(50)) RETURNS int(2)
BEGIN
  DECLARE p_jumlah_kursi INT(2);

  SELECT  JumlahKursi INTO p_jumlah_kursi
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jumlah_kursi;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jadwal_ambil_kodeutama_by_kodejadwal
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_kodeutama_by_kodejadwal`(`p_kode` VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_jadwal_utama VARCHAR(50);

  SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama) INTO p_kode_jadwal_utama
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_kode_jadwal_utama;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jadwal_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_periksa_duplikasi`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJadwal) INTO p_jum_data
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jurusan_get_harga_tiket_by_id_jurusan
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_id_jurusan`(`p_id_jurusan` INT(11), `p_tgl_berangkat` DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah1 DATE;
  DECLARE p_tgl_akhir_tuslah1 DATE;
  DECLARE p_tgl_awal_tuslah2 DATE;
  DECLARE p_tgl_akhir_tuslah2 DATE;
  
  SELECT TglMulaiTuslah,TglAkhirTuslah INTO p_tgl_awal_tuslah1,p_tgl_akhir_tuslah1
  FROM tbl_pengaturan_umum LIMIT 0,1;
  
  SELECT 
    IF(FlagLuarKota=1,IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket),IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket)) INTO p_harga_tiket
  FROM tbl_md_jurusan
  WHERE IdJurusan=p_id_jurusan;
  
  RETURN p_harga_tiket;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jurusan_get_harga_tiket_by_kode_jadwal
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_kode_jadwal`(`p_kode_jadwal` VARCHAR(30), `p_tgl_berangkat` DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah1 DATE;
  DECLARE p_tgl_akhir_tuslah1 DATE;

  SELECT TglMulaiTuslah,TglAkhirTuslah INTO p_tgl_awal_tuslah1,p_tgl_akhir_tuslah1
  FROM tbl_pengaturan_umum LIMIT 0,1;

  SELECT 
    IF(FlagLuarKota=1,IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket),IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket)) INTO p_harga_tiket
  FROM tbl_md_jurusan AS tmjr INNER JOIN tbl_md_jadwal tmj ON tmjr.IdJurusan=tmj.IdJurusan
  WHERE tmj.KodeJadwal=p_kode_jadwal;
  
  RETURN p_harga_tiket;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jurusan_get_kode_cabang_asal_by_jurusan
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_asal_by_jurusan`(`p_id_jurusan` INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_asal VARCHAR(20);

    SELECT KodeCabangAsal INTO p_kode_cabang_asal FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;
  
    RETURN p_kode_cabang_asal;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jurusan_get_kode_cabang_tujuan_by_jurusan
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_tujuan_by_jurusan`(`p_id_jurusan` INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_tujuan VARCHAR(20);

    SELECT KodeCabangTujuan INTO p_kode_cabang_tujuan FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;
  
    RETURN p_kode_cabang_tujuan;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_jurusan_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_periksa_duplikasi`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJurusan) INTO p_jum_data
  FROM tbl_md_jurusan
  WHERE KodeJurusan=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_kendaraan_ambil_nopol_by_kode
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_ambil_nopol_by_kode`(`p_kode` VARCHAR(50)) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_nopol VARCHAR(20);

  SELECT NoPolisi INTO p_nopol
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_nopol;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_kendaraan_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_kendaraan_periksa_duplikasi_by_nopol
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi_by_nopol`(`p_nopol` VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE NoPolisi=p_nopol;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_laporan_user_get_summary
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_summary`(`p_userid` INTEGER, `p_tgl_awal` DATE, `p_tgl_akhir` DATE, `p_jenis_pembayaran` INT(1)) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT IF(SUM(Total) IS NOT NULL,SUM(Total),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 )
  AND Jenispembayaran=p_jenis_pembayaran
  AND FlagBatal!=1;

  RETURN p_jumlah;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_laporan_user_get_total_discount
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_total_discount`(`p_userid` INTEGER, `p_tgl_awal` DATE, `p_tgl_akhir` DATE) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT IF(SUM(Discount) IS NOT NULL,SUM(Discount),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 OR TglBerangkat>=DATE(NOW()))
  AND FlagBatal!=1;

  RETURN p_jumlah;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_log_user_get_last_login_by_user_id
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_log_user_get_last_login_by_user_id`(`p_userid` INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_userid
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  RETURN p_id_log;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_member_hitung_frekwensi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi`(`p_id_member` VARCHAR(25)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member ;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_member_hitung_frekwensi_by_tanggal
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi_by_tanggal`(`p_id_member` VARCHAR(25), `p_tgl_awal` DATE, `p_tgl_akhir` DATE) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member
    AND  (LEFT(TglBerangkat,7) BETWEEN LEFT(p_tgl_awal,7) AND LEFT(p_tgl_akhir,7));

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_member_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_periksa_duplikasi`(`p_id_member` VARCHAR(25), `p_email` VARCHAR(30), `p_handphone` VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdMember) INTO p_jum_data
  FROM tbl_md_member
  WHERE IdMember=p_id_member OR Email=p_email OR handphone=p_handphone; 

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_paket_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_paket_periksa_duplikasi`(`p_no_tiket` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(NoTiket) INTO p_jum_data
  FROM tbl_paket
  WHERE NoTiket=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_pelanggan_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_pelanggan_periksa_duplikasi`(`p_no_telp` VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPelanggan) INTO p_jum_data
  FROM tbl_pelanggan
  WHERE NoTelp=p_no_telp;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_pengumuman_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_pengumuman_periksa_duplikasi`(`p_kode` VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPengumuman) INTO p_jum_data
  FROM tbl_pengumuman
  WHERE KodePengumuman=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_promo_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_promo_periksa_duplikasi`(`p_kode` VARCHAR(50), `p_asal` VARCHAR(20), `p_tujuan` VARCHAR(20), `p_berlaku_mula` DATETIME, `p_berlaku_akhir` DATETIME, `p_target_promo` INT, `p_level_promo` INT) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodePromo) INTO p_jum_data
  FROM tbl_md_promo
  WHERE KodePromo=p_kode
    OR (
      ((p_berlaku_mula BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir))
        OR (p_berlaku_akhir BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir)))
    AND ((KodeCabangAsal LIKE CONCAT(p_asal,'%') AND KodeCabangTujuan LIKE CONCAT(p_tujuan,'%'))
         OR (KodeCabangAsal LIKE CONCAT(p_tujuan,'%') AND KodeCabangTujuan LIKE CONCAT(p_asal,'%')))
    AND LevelPromo=p_level_promo
    AND (FlagTargetPromo=0 OR FlagTargetPromo=p_target_promo)
    );

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_reservasi_cabang_get_name_by_kode
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_cabang_get_name_by_kode`(`p_kode` VARCHAR(50)) RETURNS varchar(100) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(100);
  
    SELECT nama INTO p_nama FROM tbl_md_cabang WHERE KodeCabang=p_kode;
  
    RETURN p_nama;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_reservasi_get_kode_booking_by_no_tiket
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_get_kode_booking_by_no_tiket`(`p_no_tiket` VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_booking VARCHAR(50);

  SELECT KodeBooking INTO p_kode_booking
  FROM tbl_reservasi
  WHERE NoTiket=p_no_tiket;

  RETURN p_kode_booking;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_reservasi_session_time_selisih
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_session_time_selisih`(`p_session_time` DATETIME) RETURNS int(11)
BEGIN
  DECLARE p_selisih INTEGER;

  SELECT
   HOUR(TIMEDIFF(p_session_time,NOW()))*3600 + MINUTE(TIMEDIFF(p_session_time,NOW()))*60 + SECOND(TIMEDIFF(p_session_time,NOW())) INTO p_selisih;

  RETURN p_selisih;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_sopir_get_insentif
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_insentif`(`p_kode` VARCHAR(20), `p_tgl_awal` DATE, `p_tgl_akhir` DATE) RETURNS double
BEGIN
  DECLARE p_return DOUBLE;

  SELECT SUM(InsentifSopir) INTO p_return
  FROM tbl_spj
  WHERE KodeDriver=p_kode
    AND (TglBerangkat BETWEEN p_tgl_awal AND p_tgl_akhir);

  RETURN p_return;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_sopir_get_nama_by_id
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_nama_by_id`(`p_kode` VARCHAR(20)) RETURNS varchar(150) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(150);

  SELECT Nama INTO p_nama
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_nama;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_sopir_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_periksa_duplikasi`(`p_kode` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeSopir) INTO p_jum_data
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_user_get_jumlah_pengumuman_baru
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_jumlah_pengumuman_baru`(`p_userid` INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_jumlah INT;

  SELECT jumlah_pengumuman_baru INTO p_jumlah
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_jumlah;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_user_get_nama_by_userid
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_nama_by_userid`(`p_userid` INTEGER) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT nama INTO p_nama
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_nama;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.f_user_periksa_duplikasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_periksa_duplikasi`(`p_username` VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(user_id) INTO p_jum_data
  FROM tbl_user
  WHERE username=p_username;

  RETURN p_jum_data;
END//
DELIMITER ;

-- Dumping structure for function tiketux_abimanyu.IS_NULL
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `IS_NULL`(`p_input` DOUBLE, `p_pengganti` DOUBLE) RETURNS double
BEGIN
  DECLARE p_output DOUBLE;

  SELECT IF(!ISNULL(p_input),p_input,p_pengganti) INTO p_output;
  
  RETURN p_output;
END//
DELIMITER ;

-- Dumping structure for table tiketux_abimanyu.maps_area
CREATE TABLE IF NOT EXISTS `maps_area` (
  `id_area` char(10) COLLATE latin1_general_ci NOT NULL,
  `nama_area` char(200) COLLATE latin1_general_ci DEFAULT NULL,
  `kota_area` char(10) COLLATE latin1_general_ci DEFAULT NULL,
  `jurusan_area` char(11) COLLATE latin1_general_ci DEFAULT NULL,
  `biaya_jemput` int(11) DEFAULT '0',
  `biaya_antar` int(11) DEFAULT '0',
  `is_bandara` int(1) DEFAULT '0',
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.maps_area: ~215 rows (approximately)
/*!40000 ALTER TABLE `maps_area` DISABLE KEYS */;
INSERT INTO `maps_area` (`id_area`, `nama_area`, `kota_area`, `jurusan_area`, `biaya_jemput`, `biaya_antar`, `is_bandara`) VALUES
	('A0001', 'Tunggul Wulung', 'MLG', '', 10000, 10000, 0),
	('A0002', 'Lawang', 'MLG', '', 50000, 50000, 0),
	('A0003', 'Singosari', 'MLG', NULL, 75000, 75000, 0),
	('A0004', 'Karanglo', 'MLG', NULL, 20000, 20000, 0),
	('A0005', 'Karang Ploso', 'MLG', NULL, 20000, 20000, 0),
	('A0006', 'Puncak Buring', 'MLG', NULL, 15000, 15000, 0),
	('A0007', 'Mangliawan', 'MLG', NULL, 10000, 10000, 0),
	('A0008', 'Ampel Dento', 'MLG', NULL, 15000, 15000, 0),
	('A0009', 'Pakis', 'MLG', NULL, 20000, 20000, 0),
	('A0010', 'Wendit', 'MLG', NULL, 25000, 25000, 0),
	('A0011', 'Tumpang', 'MLG', NULL, 50000, 50000, 0),
	('A0012', 'Turen', 'MLG', NULL, 50000, 50000, 0),
	('A0013', 'Bulu Lawang', 'MLG', NULL, 30000, 30000, 0),
	('A0014', 'Kebon Agung', 'MLG', NULL, 10000, 10000, 0),
	('A0015', 'Pakis Soji', 'MLG', NULL, 20000, 20000, 0),
	('A0016', 'Tretes', 'MLG', NULL, 100000, 100000, 0),
	('A0017', 'Tulung Agung', 'MLG', NULL, 100000, 100000, 0),
	('A0018', 'Songgoriti', 'MLG', NULL, 25000, 25000, 0),
	('A0019', 'Villa Puncak Tidar', 'MLG', NULL, 10000, 10000, 0),
	('A0020', 'Joyobran', 'MLG', NULL, 10000, 10000, 0),
	('A0021', 'Kedung Kandang', 'MLG', NULL, 10000, 10000, 0),
	('A0022', 'Lowok Waru', 'MLG', NULL, 25000, 25000, 0),
	('A0023', 'Tumpang', 'MLG', NULL, 75000, 75000, 0),
	('A0024', 'Bale Arjosari', 'MLG', NULL, 15000, 15000, 0),
	('A0025', 'Pandaan', 'MLG', NULL, 125000, 125000, 0),
	('A0026', 'Cieulenyi', 'BDG', NULL, 20000, 20000, 0),
	('A0027', 'Cimahi', 'BDG', NULL, 30000, 30000, 0),
	('A0028', 'Cibaduyut', 'BDG', NULL, 15000, 15000, 0),
	('A0029', 'Kopo Indah', 'BDG', NULL, 20000, 20000, 0),
	('A0030', 'Taman Holis', 'BDG', NULL, 10000, 10000, 0),
	('A0031', 'Perum Cipaganti', 'BDG', NULL, 10000, 10000, 0),
	('A0032', 'Suka Birus', 'BDG', NULL, 20000, 20000, 0),
	('A0033', 'Sadang', 'BDG', NULL, 10000, 10000, 0),
	('A0034', 'Cipagalan', 'BDG', NULL, 10000, 10000, 0),
	('A0035', 'Buah Batu', 'BDG', NULL, 10000, 10000, 0),
	('A0036', 'Cibolerang', 'BDG', NULL, 10000, 10000, 0),
	('A0037', 'Pasir wangi', 'BDG', NULL, 15000, 15000, 0),
	('A0038', 'Daeyekolot', 'BDG', NULL, 20000, 20000, 0),
	('A0039', 'Taman Rahayu', 'BDG', NULL, 20000, 20000, 0),
	('A0040', 'Cinunuk', 'BDG', NULL, 20000, 20000, 0),
	('A0041', 'Pasir Impun', 'BDG', NULL, 15000, 15000, 0),
	('A0042', 'Kopo', 'BDG', NULL, 15000, 15000, 0),
	('A0043', 'Melayang Regency', 'BDG', NULL, 15000, 15000, 0),
	('A0044', 'Sarijadi', 'BDG', NULL, 15000, 15000, 0),
	('A0045', 'Dago Pakar', 'BDG', NULL, 25000, 25000, 0),
	('A0046', 'Terusan Sari Asih', 'BDG', NULL, 15000, 15000, 0),
	('A0047', 'Jatinangor', 'BDG', NULL, 20000, 20000, 0),
	('A0048', 'Padalarang', 'BDG', NULL, 75000, 75000, 0),
	('A0049', 'Cimareme', 'BDG', NULL, 30000, 30000, 0),
	('A0050', 'Rancaengkek', 'BDG', NULL, 40000, 40000, 0),
	('A0051', 'Adipura', 'BDG', NULL, 15000, 15000, 0),
	('A0052', 'Padasuka', '-- Pilih K', NULL, 10000, 10000, 0),
	('A0053', 'Bojong Soang', 'BDG', NULL, 15000, 15000, 0),
	('A0054', 'Gempol Asri', 'BDG', NULL, 15000, 15000, 0),
	('A0055', 'Sumedang', 'BDG', NULL, 20000, 20000, 0),
	('A0056', 'Ciganitri', 'BDG', NULL, 25000, 25000, 0),
	('A0057', 'Puspa Kencana', 'BDG', NULL, 20000, 20000, 0),
	('A0058', 'Cijambe', 'BDG', NULL, 30000, 30000, 0),
	('A0059', 'Soreang', 'BDG', NULL, 150000, 150000, 0),
	('A0060', 'STT Telkom', 'BDG', NULL, 20000, 20000, 0),
	('A0061', 'Pakuwon', 'SBY', NULL, 20000, 20000, 0),
	('A0062', 'Waru', 'SBY', NULL, 20000, 20000, 0),
	('A0063', 'Juanda', 'SBY', NULL, 30000, 30000, 0),
	('A0064', 'Sidoarjo Kota', 'SBY', NULL, 30000, 30000, 0),
	('A0065', 'Perak', 'SBY', NULL, 20000, 20000, 0),
	('A0066', 'Candi', 'SBY', NULL, 40000, 40000, 0),
	('A0067', 'Benowo', 'SBY', NULL, 50000, 50000, 0),
	('A0068', 'Sedati', 'SBY', NULL, 30000, 30000, 0),
	('A0069', 'Graha Family', 'SBY', NULL, 15000, 15000, 0),
	('A0070', 'Gunungsari', 'SBY', NULL, 20000, 20000, 0),
	('A0071', 'Dian Istana', 'SBY', NULL, 20000, 20000, 0),
	('A0072', 'Lontar', 'SBY', NULL, 20000, 20000, 0),
	('A0073', 'Karang Pilang', 'SBY', NULL, 25000, 25000, 0),
	('A0074', 'Pondok Candra', 'SBY', NULL, 30000, 30000, 0),
	('A0075', 'Tropodo', 'SBY', NULL, 30000, 30000, 0),
	('A0076', 'Sepanjang', 'SBY', NULL, 30000, 30000, 0),
	('A0077', 'Kletek', 'SBY', NULL, 30000, 30000, 0),
	('A0078', 'Kebraon', 'SBY', NULL, 25000, 25000, 0),
	('A0079', 'Wiyung', 'SBY', NULL, 25000, 25000, 0),
	('A0080', 'Lidah Kulon', 'SBY', NULL, 20000, 20000, 0),
	('A0081', 'Lakan Santri', 'SBY', NULL, 70000, 70000, 0),
	('A0082', 'Citraland', 'SBY', NULL, 20000, 20000, 0),
	('A0083', 'Moro Seneng', 'SBY', NULL, 20000, 20000, 0),
	('A0084', 'Citra Raya', 'SBY', NULL, 30000, 30000, 0),
	('A0085', 'Delta Sari', 'SBY', NULL, 25000, 25000, 0),
	('A0086', 'Berbek Industri', 'SBY', NULL, 20000, 20000, 0),
	('A0087', 'Gresik', 'SBY', NULL, 50000, 50000, 0),
	('A0088', 'Tandes', 'SBY', NULL, 10000, 10000, 0),
	('A0089', 'Sedayu', 'SBY', NULL, 30000, 30000, 0),
	('A0090', 'Bungah', 'SBY', NULL, 30000, 30000, 0),
	('A0091', 'Kedurus', 'SBY', NULL, 15000, 15000, 0),
	('A0092', 'Krian', 'SBY', NULL, 50000, 50000, 0),
	('A0093', 'Bukit Palma', 'SBY', NULL, 30000, 30000, 0),
	('A0094', 'Gedangan', 'SBY', NULL, 40000, 40000, 0),
	('A0095', 'Sambi Kerep', 'SBY', NULL, 20000, 20000, 0),
	('A0096', 'Tirto', 'PKL', NULL, 15000, 15000, 0),
	('A0097', 'Slamaran', 'PKL', NULL, 15000, 15000, 0),
	('A0098', 'Buaran', 'PKL', NULL, 10000, 10000, 0),
	('A0099', 'Kedungwuni', 'PKL', NULL, 25000, 25000, 0),
	('A0100', 'Wiradesa', 'PKL', NULL, 20000, 20000, 0),
	('A0101', 'Kalisalak', 'PKL', NULL, 20000, 20000, 0),
	('A0102', 'Batang', 'PKL', NULL, 20000, 20000, 0),
	('A0103', 'Pekajangan', 'PKL', NULL, 15000, 15000, 0),
	('A0104', 'Dracik', 'PKL', NULL, 15000, 15000, 0),
	('A0105', 'Perum Pasekaran', 'PKL', NULL, 15000, 15000, 0),
	('A0106', 'Kuripan Kidul', 'PKL', NULL, 15000, 15000, 0),
	('A0107', 'Ambokembang', 'PKL', NULL, 15000, 15000, 0),
	('A0108', 'Warung Asem', 'PKL', NULL, 15000, 15000, 0),
	('A0109', 'Watusalam', 'PKL', NULL, 15000, 15000, 0),
	('A0110', 'Sapu Garut', 'PKL', NULL, 15000, 15000, 0),
	('A0111', 'Simbang Wetan', 'PKL', NULL, 15000, 15000, 0),
	('A0112', 'Jenggot', 'PKL', NULL, 15000, 15000, 0),
	('A0113', 'Kradenan', 'PKL', NULL, 15000, 15000, 0),
	('A0114', 'Talang', 'TGL', NULL, 10000, 10000, 0),
	('A0115', 'Slawi', 'TGL', NULL, 30000, 30000, 0),
	('A0116', 'Brebes', 'TGL', NULL, 35000, 35000, 0),
	('A0117', 'Karanganyar', 'TGL', NULL, 10000, 10000, 0),
	('A0118', 'Pagongan', 'TGL', NULL, 10000, 10000, 0),
	('A0119', 'Lemah Duwur', 'TGL', NULL, 15000, 15000, 0),
	('A0120', 'Adiwerna', 'TGL', NULL, 20000, 20000, 0),
	('A0121', 'Tembok', 'TGL', NULL, 15000, 15000, 0),
	('A0122', 'Debog', 'TGL', NULL, 15000, 15000, 0),
	('A0123', 'Margadana', 'TGL', NULL, 20000, 20000, 0),
	('A0124', 'Dampyak', 'TGL', NULL, 15000, 15000, 0),
	('A0125', 'Pacul', 'TGL', NULL, 10000, 10000, 0),
	('A0126', 'Maribaya', 'TGL', NULL, 30000, 30000, 0),
	('A0127', 'Pangkah', 'TGL', NULL, 30000, 30000, 0),
	('A0128', 'Surodadi', 'TGL', NULL, 20000, 20000, 0),
	('A0129', 'Dukuhturi', 'TGL', NULL, 30000, 30000, 0),
	('A0130', ' Ungaran', 'SMG', NULL, 40000, 40000, 0),
	('A0131', 'Gedawang', '-- Pilih K', NULL, 15000, 15000, 0),
	('A0132', ' Watugong', 'SMG', NULL, 20000, 20000, 0),
	('A0133', 'Pudak Payung', 'SMG', NULL, 25000, 25000, 0),
	('A0134', 'Kedung Pane', 'SMG', NULL, 25000, 25000, 0),
	('A0135', 'BSB', 'SMG', NULL, 30000, 30000, 0),
	('A0136', 'Mijen', 'SMG', NULL, 30000, 30000, 0),
	('A0137', 'Bukit Permata Puri', 'SMG', NULL, 15000, 15000, 0),
	('A0138', 'Pandana Merdeka', 'SMG', NULL, 15000, 15000, 0),
	('A0139', 'Beringin Lestari Iindah', 'SMG', NULL, 15000, 15000, 0),
	('A0140', 'Mega Permai', 'SMG', NULL, 15000, 15000, 0),
	('A0141', 'Pondok Bringin', 'SMG', NULL, 15000, 15000, 0),
	('A0142', 'Pucang Gading Atas', 'SMG', NULL, 15000, 15000, 0),
	('A0143', 'Pucang Gading', 'SMG', NULL, 10000, 10000, 0),
	('A0144', 'Klipang', 'SMG', NULL, 15000, 15000, 0),
	('A0145', 'Sendang Mulyo', 'SMG', NULL, 15000, 15000, 0),
	('A0146', 'Bukit Dinar Mas', 'SMG', NULL, 15000, 15000, 0),
	('A0147', 'Bukit kencana jaya', 'SMG', NULL, 15000, 15000, 0),
	('A0148', 'Gunung Pati', 'SMG', NULL, 50000, 50000, 0),
	('A0149', '(Pekalongan -Tegal) - Pudak Payung', 'SMG', NULL, 30000, 30000, 0),
	('A0150', 'Banyumanik', 'SMG', NULL, 20000, 20000, 0),
	('A0151', 'Tembalang', 'SMG', NULL, 20000, 20000, 0),
	('A0152', 'Jatingaleh', 'SMG', NULL, 15000, 15000, 0),
	('A0153', 'Pawiyatan Luhur', 'SMG', NULL, 15000, 15000, 0),
	('A0154', 'Mranggen', 'SMG', NULL, 30000, 30000, 0),
	('A0155', 'Plamongan', 'SMG', NULL, 25000, 25000, 0),
	('A0156', 'Pucang Gading', 'SMG', NULL, 25000, 25000, 0),
	('A0157', 'Pedurungan', 'SMG', NULL, 20000, 20000, 0),
	('A0158', 'Kedungmundu', 'SMG', NULL, 15000, 15000, 0),
	('A0159', 'Ketileng', 'SMG', NULL, 20000, 20000, 0),
	('A0160', 'Tlogosari', 'SMG', NULL, 10000, 10000, 0),
	('A0161', 'Supriyadi', 'SMG', NULL, 10000, 10000, 0),
	('A0162', 'Gayam Sari', 'SMG', NULL, 10000, 10000, 0),
	('A0163', 'Wolter Monginsidi', 'SMG', NULL, 25000, 25000, 0),
	('A0164', 'Genuk', 'SMG', NULL, 15000, 15000, 0),
	('A0165', 'Terboyo', 'SMG', NULL, 15000, 15000, 0),
	('A0166', 'Sampangan', 'SMG', NULL, 10000, 10000, 0),
	('A0167', 'Unnes', 'SMG', NULL, 25000, 25000, 0),
	('A0168', 'Ngaliyan ( batas pasar )', 'SMG', NULL, 15000, 15000, 0),
	('A0169', 'Kedung pane', 'SMG', NULL, 25000, 25000, 0),
	('A0170', 'BSB', 'SMG', NULL, 35000, 35000, 0),
	('A0171', 'Mijen', 'SMG', NULL, 50000, 50000, 0),
	('A0172', 'Green Wood', 'SMG', NULL, 15000, 15000, 0),
	('A0173', 'Dinar Mas', 'SMG', NULL, 25000, 25000, 0),
	('A0174', 'Sendang Mulyo', 'SMG', NULL, 25000, 25000, 0),
	('A0175', 'Gedawang', 'SMG', NULL, 20000, 20000, 0),
	('A0176', 'Pudak Payung', 'SMG', NULL, 30000, 30000, 0),
	('A0177', 'Mranggen - Bandung', 'SMG', NULL, 25000, 25000, 0),
	('A0178', 'Pucang Gading Atas', 'SMG', NULL, 15000, 15000, 0),
	('A0179', 'Pucang Gading', 'SMG', NULL, 10000, 10000, 0),
	('A0180', 'Klipang', 'SMG', NULL, 10000, 10000, 0),
	('A0181', 'Sendang Mulyo', 'SMG', NULL, 10000, 10000, 0),
	('A0182', 'Dinar Mas', 'SMG', NULL, 15000, 15000, 0),
	('A0183', 'Bukit Kencana Jaya', 'SMG', NULL, 15000, 15000, 0),
	('A0184', 'Ungaran', 'SMG', NULL, 30000, 30000, 0),
	('A0185', 'Green Wood / Manyaran Permai', 'SMG', NULL, 10000, 10000, 0),
	('A0186', 'Bukit Permata Puri', 'SMG', NULL, 10000, 10000, 0),
	('A0187', 'Gunung Pati Unnes', 'SMG', NULL, 30000, 30000, 0),
	('A0188', 'BSB', 'SMG', NULL, 30000, 30000, 0),
	('A0189', 'Mijen', 'SMG', NULL, 50000, 50000, 0),
	('A0190', 'Bukit Permata Puri - Malang', 'SMG', NULL, 15000, 15000, 0),
	('A0191', 'Pandana Merdeka', 'SMG', NULL, 15000, 15000, 0),
	('A0192', 'Bringin Indah', 'SMG', NULL, 15000, 15000, 0),
	('A0193', 'Mega Permai', 'SMG', NULL, 15000, 15000, 0),
	('A0194', 'Pondok Beringin', 'SMG', NULL, 15000, 15000, 0),
	('A0195', 'Kedung pane', 'SMG', NULL, 25000, 25000, 0),
	('A0196', 'BSB', 'SMG', NULL, 30000, 30000, 0),
	('A0197', 'Mijen', 'SMG', NULL, 30000, 30000, 0),
	('A0198', 'Genuk', 'SMG', NULL, 10000, 10000, 0),
	('A0199', 'Terminal terboyo', 'SMG', NULL, 10000, 10000, 0),
	('A0200', 'Pasar Genuk', 'SMG', NULL, 15000, 15000, 0),
	('A0201', 'Banget Ayu', 'SMG', NULL, 20000, 20000, 0),
	('A0202', 'Wolter Monginsidi', 'SMG', NULL, 10000, 10000, 0),
	('A0203', 'Graha Mukti Utama', 'SMG', NULL, 10000, 10000, 0),
	('A0204', 'Pucang gading', 'SMG', NULL, 10000, 10000, 0),
	('A0205', 'Mranggen', 'SMG', NULL, 30000, 30000, 0),
	('A0206', 'Plamongan Indah', 'SMG', NULL, 10000, 10000, 0),
	('A0207', 'Pucang Gading Atas', 'SMG', NULL, 15000, 15000, 0),
	('A0208', 'Klipang', 'SMG', NULL, 15000, 15000, 0),
	('A0209', 'Sendang Mulyo', 'SMG', NULL, 15000, 15000, 0),
	('A0210', 'Puri Dinar Mas', 'SMG', NULL, 15000, 15000, 0),
	('A0211', 'Bukit Kencana Jaya', 'SMG', NULL, 15000, 15000, 0),
	('A0212', 'Unnes Gunung Pati', 'SMG', NULL, 25000, 25000, 0),
	('A0213', 'Bandara Semarang', 'SMG', '25', 10000, 20000, 1),
	('A0214', 'Bandara Juanda', 'SMG', '25', 20000, 30000, 1),
	('A0215', 'Uye', 'BDG', '', 12, 13, 0);
/*!40000 ALTER TABLE `maps_area` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.maps_koordinat
CREATE TABLE IF NOT EXISTS `maps_koordinat` (
  `id_koordinat` char(13) COLLATE latin1_general_ci NOT NULL,
  `id_area` char(10) COLLATE latin1_general_ci DEFAULT NULL,
  `lat` char(20) COLLATE latin1_general_ci DEFAULT NULL,
  `lng` char(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_koordinat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.maps_koordinat: ~0 rows (approximately)
/*!40000 ALTER TABLE `maps_koordinat` DISABLE KEYS */;
/*!40000 ALTER TABLE `maps_koordinat` ENABLE KEYS */;

-- Dumping structure for procedure tiketux_abimanyu.sp_biaya_op_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_biaya_op_tambah`(`p_no_spj` VARCHAR(20), `p_kode_akun` VARCHAR(20), `p_flag_jenis_biaya` INT(1), `p_kode_kendaraan` VARCHAR(20), `p_kode_sopir` VARCHAR(50), `p_jumlah` DOUBLE, `p_id_petugas` INTEGER, `p_kode_jadwal` VARCHAR(30), `p_kode_cabang` VARCHAR(50))
BEGIN

   INSERT INTO tbl_biaya_op (
     NoSPJ, KodeAkun, FlagJenisBiaya,
     TglTransaksi, NoPolisi, KodeSopir,
     Jumlah, IdPetugas, IdJurusan,
     KodeCabang)
   VALUES (
     p_no_spj, p_kode_akun, p_flag_jenis_biaya,
     NOW(),p_kode_kendaraan,p_kode_sopir,
     p_jumlah, p_id_petugas, f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
     p_kode_cabang);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_cabang_hapus
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_hapus`(`p_list_kode` VARCHAR(255))
BEGIN

  DELETE FROM tbl_md_cabang
  WHERE KodeCabang IN(p_list_kode);

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_cabang_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_tambah`(`p_kode` VARCHAR(20), `p_nama` VARCHAR(70), `p_alamat` VARCHAR(255), `p_kota` VARCHAR(20), `p_telp` VARCHAR(50), `p_fax` VARCHAR(50), `p_flag_agen` INT(1))
BEGIN

   INSERT INTO tbl_md_cabang (KodeCabang,Nama, Alamat,Kota,Telp,Fax,FlagAgen)
   VALUES (p_kode,p_nama,p_alamat,p_kota,p_telp,p_fax,p_flag_agen);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_cabang_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_ubah`(`p_kode_old` VARCHAR(20), `p_kode` VARCHAR(20), `p_nama` VARCHAR(70), `p_alamat` VARCHAR(255), `p_kota` VARCHAR(20), `p_telp` VARCHAR(50), `p_fax` VARCHAR(50), `p_flag_agen` INT(1))
BEGIN

   UPDATE tbl_md_cabang
   SET
     KodeCabang=p_kode, Nama=p_nama, Alamat=p_alamat, Kota=p_kota, Telp=p_telp,
     Fax=p_fax,FlagAgen=p_flag_agen
   WHERE KodeCabang=p_kode_old;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_deposit_debit
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_debit`(IN `p_kode_booking` VARCHAR(30), IN `p_jumlah` DOUBLE, IN `p_keterangan` TEXT)
BEGIN

    DECLARE saldo_deposit DOUBLE;
  DECLARE p_komisi DOUBLE;
  DECLARE p_jumlah_debet DOUBLE;

  START TRANSACTION;

    
    SELECT NilaiParameter INTO saldo_deposit FROM tbl_pengaturan_parameter WHERE NamaParameter ='DEPOSIT_SALDO';

    
   

   SELECT SUM(HargaTiketux-Komisi) INTO p_jumlah_debet
    FROM tbl_reservasi
    WHERE KodeBooking=p_kode_booking;

    
    IF saldo_deposit>=p_jumlah_debet THEN
      

      
      
      UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter-p_jumlah_debet WHERE NamaParameter='DEPOSIT_SALDO';

     
      INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
        VALUES(NOW(),1,p_keterangan,p_jumlah_debet,saldo_deposit-p_jumlah_debet);

      
      UPDATE tbl_reservasi SET IsSettlement=0 WHERE KodeBooking=p_kode_booking;

    END IF;
    
		
  COMMIT;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_deposit_refund
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_refund`(`p_NoTiket` VARCHAR(50))
BEGIN


  DECLARE nominal_refund DOUBLE;
  DECLARE tgl_berangkat DATE;
  DECLARE kode_jadwal VARCHAR(25);
  DECLARE nama_penumpang VARCHAR(30);
  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  SELECT (HargaTiket-Komisi),TglBerangkat,KodeJadwal,Nama INTO nominal_refund,tgl_berangkat,kode_jadwal,nama_penumpang FROM tbl_reservasi WHERE NoTiket=p_NoTiket;

  UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter+nominal_refund WHERE NamaParameter='DEPOSIT_SALDO';

  SELECT NilaiParameter INTO saldo_terakhir FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO';

  INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
    VALUES (NOW(),0,CONCAT('REFUND TIKET ',p_NoTiket,' ',nama_penumpang,' ',tgl_berangkat,' ',kode_jadwal),nominal_refund,saldo_terakhir);


  COMMIT;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_deposit_topup
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_topup`(`p_kode_referensi` VARCHAR(25))
BEGIN


  DECLARE jumlah_topup DOUBLE;
  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  SELECT Jumlah INTO jumlah_topup FROM tbl_deposit_log_topup WHERE KodeReferensi=p_kode_referensi;

  UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter+jumlah_topup WHERE NamaParameter='DEPOSIT_SALDO';

  SELECT NilaiParameter INTO saldo_terakhir FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO';

  INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
    VALUES (NOW(),0,CONCAT('TOP UP DEPOSIT ',p_kode_referensi),jumlah_topup,saldo_terakhir);


  COMMIT;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jadwal_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_tambah`(`p_kode_jadwal` VARCHAR(30), `p_id_jurusan` INTEGER, `p_jam` VARCHAR(5), `p_jumlah_kursi` INT(2), `p_flag_sub_jadwal` INT(1), `p_kode_jadwal_utama` VARCHAR(30), `p_flag_aktif` INT(1))
BEGIN

  INSERT INTO tbl_md_jadwal (
    KodeJadwal,IdJurusan,JamBerangkat,
    JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
    FlagAktif)
  VALUES(
    p_kode_jadwal,p_id_jurusan,p_jam,
    p_jumlah_kursi,p_flag_sub_jadwal,p_kode_jadwal_utama,
    p_flag_aktif);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jadwal_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah`(`p_kode_jadwal_old` VARCHAR(30), `p_kode_jadwal` VARCHAR(30), `p_id_jurusan` INTEGER, `p_jam` VARCHAR(5), `p_jumlah_kursi` INT(2), `p_flag_sub_jadwal` INT(1), `p_kode_jadwal_utama` VARCHAR(30), `p_flag_aktif` INT(1))
BEGIN

  UPDATE tbl_md_jadwal SET
    KodeJadwal=p_kode_jadwal,IdJurusan=p_id_jurusan,
    JamBerangkat=p_jam,JumlahKursi=p_jumlah_kursi,
    FlagSubJadwal=p_flag_sub_jadwal,KodeJadwalUtama=p_kode_jadwal_utama,
    FlagAktif=p_flag_aktif
  WHERE KodeJadwal = p_kode_jadwal_old;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jadwal_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah_status_aktif`(`p_kode_jadwal` VARCHAR(30))
BEGIN

   UPDATE tbl_md_jadwal SET
     FlagAktif=1-FlagAktif
   WHERE KodeJadwal=p_kode_jadwal;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jenis_discount_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jenis_discount_ubah_status_aktif`(`p_id_discount` INTEGER)
BEGIN

   UPDATE tbl_jenis_discount SET
     FlagAktif=1-FlagAktif
   WHERE IdDiscount=p_id_discount;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jurusan_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_tambah`(`p_kode_jurusan` VARCHAR(10), `p_kode_cabang_asal` VARCHAR(20), `p_kode_cabang_tujuan` VARCHAR(20), `p_harga_tiket` DOUBLE, `p_harga_tiket_tuslah` DOUBLE, `p_flag_tiket_tuslah` INT(1), `p_kode_akun_pendapatan_penumpang` VARCHAR(20), `p_kode_akun_pendapatan_paket` VARCHAR(20), `p_kode_akun_charge` VARCHAR(20), `p_kode_akun_biaya_sopir` VARCHAR(20), `p_biaya_sopir` DOUBLE, `p_kode_akun_biaya_tol` VARCHAR(20), `p_biaya_tol` DOUBLE, `p_kode_akun_biaya_parkir` VARCHAR(20), `p_biaya_parkir` DOUBLE, `p_kode_akun_biaya_bbm` VARCHAR(20), `p_biaya_bbm` DOUBLE, `p_kode_akun_komisi_penumpang_sopir` VARCHAR(20), `p_komisi_penumpang_sopir` DOUBLE, `p_kode_akun_komisi_penumpang_cso` VARCHAR(20), `p_komisi_penumpang_cso` DOUBLE, `p_kode_akun_komisi_paket_sopir` VARCHAR(20), `p_komisi_paket_sopir` DOUBLE, `p_kode_akun_komisi_paket_cso` VARCHAR(20), `p_komisi_paket_cso` DOUBLE, `p_flag_aktif` INT(1), `p_flag_luar_kota` INT(1), `p_harga_paket_1_kilo_pertama` DOUBLE, `p_harga_paket_1_kilo_berikut` DOUBLE, `p_harga_paket_2_kilo_pertama` DOUBLE, `p_harga_paket_2_kilo_berikut` DOUBLE, `p_harga_paket_3_kilo_pertama` DOUBLE, `p_harga_paket_3_kilo_berikut` DOUBLE, `p_harga_paket_4_kilo_pertama` DOUBLE, `p_harga_paket_4_kilo_berikut` DOUBLE, `p_harga_paket_5_kilo_pertama` DOUBLE, `p_harga_paket_5_kilo_berikut` DOUBLE, `p_harga_paket_6_kilo_pertama` DOUBLE, `p_harga_paket_6_kilo_berikut` DOUBLE, `p_flag_op_jurusan` INT(1), `p_is_biaya_sopir_kumulatif` INT(1), `p_is_voucher_bbm` INT(1))
BEGIN

   INSERT INTO tbl_md_jurusan (
     KodeJurusan, KodeCabangAsal, KodeCabangTujuan, HargaTiket,
     HargaTiketTuslah, FlagTiketTuslah,KodeAkunPendapatanPenumpang, KodeAkunPendapatanPaket,
     KodeAkunCharge, KodeAkunBiayaSopir, BiayaSopir, KodeAkunKomisiPenumpangSopir,
     KomisiPenumpangSopir, KodeAkunKomisiPenumpangCSO, KomisiPenumpangCSO, KodeAkunKomisiPaketSopir,
     KomisiPaketSopir, KodeAkunKomisiPaketCSO, KomisiPaketCSO,FlagAktif,
     KodeAkunBiayaTol,BiayaTol,
     KodeAkunBiayaParkir,BiayaParkir,
     KodeAkunBiayaBBM,BiayaBBM,FlagLuarKota,
     HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
     HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
     HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
     HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
     HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
     HargaPaket6KiloPertama,HargaPaket6KiloBerikut,
     FlagOperasionalJurusan,IsBiayaSopirKumulatif,
     IsVoucherBBM)
   VALUES (
     p_kode_jurusan, p_kode_cabang_asal, p_kode_cabang_tujuan, p_harga_tiket,
     p_harga_tiket_tuslah, p_flag_tiket_tuslah, p_kode_akun_pendapatan_penumpang, p_kode_akun_pendapatan_paket,
     p_kode_akun_charge, p_kode_akun_biaya_sopir, p_biaya_sopir, p_kode_akun_komisi_penumpang_sopir,
     p_komisi_penumpang_sopir, p_kode_akun_komisi_penumpang_cso, p_komisi_penumpang_cso, p_kode_akun_komisi_paket_sopir,
     p_komisi_paket_sopir, p_kode_akun_komisi_paket_cso, p_komisi_paket_cso,p_flag_aktif,
     p_kode_akun_biaya_tol,p_biaya_tol,
     p_kode_akun_biaya_parkir,p_biaya_parkir,
     p_kode_akun_biaya_bbm,p_biaya_bbm,p_flag_luar_kota,
     p_harga_paket_1_kilo_pertama,p_harga_paket_1_kilo_berikut,
     p_harga_paket_2_kilo_pertama,p_harga_paket_2_kilo_berikut,
     p_harga_paket_3_kilo_pertama,p_harga_paket_3_kilo_berikut,
     p_harga_paket_4_kilo_pertama,p_harga_paket_4_kilo_berikut,
     p_harga_paket_5_kilo_pertama,p_harga_paket_5_kilo_berikut,
     p_harga_paket_6_kilo_pertama,p_harga_paket_6_kilo_berikut,
     p_flag_op_jurusan,p_is_biaya_sopir_kumulatif,
     p_is_voucher_bbm);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jurusan_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah`(`p_id_jurusan` INTEGER, `p_kode_jurusan` VARCHAR(10), `p_kode_cabang_asal` VARCHAR(20), `p_kode_cabang_tujuan` VARCHAR(20), `p_harga_tiket` DOUBLE, `p_harga_tiket_tuslah` DOUBLE, `p_flag_tiket_tuslah` INT(1), `p_kode_akun_pendapatan_penumpang` VARCHAR(20), `p_kode_akun_pendapatan_paket` VARCHAR(20), `p_kode_akun_charge` VARCHAR(20), `p_kode_akun_biaya_sopir` VARCHAR(20), `p_biaya_sopir` DOUBLE, `p_kode_akun_biaya_tol` VARCHAR(20), `p_biaya_tol` DOUBLE, `p_kode_akun_biaya_parkir` VARCHAR(20), `p_biaya_parkir` DOUBLE, `p_kode_akun_biaya_bbm` VARCHAR(20), `p_biaya_bbm` DOUBLE, `p_kode_akun_komisi_penumpang_sopir` VARCHAR(20), `p_komisi_penumpang_sopir` DOUBLE, `p_kode_akun_komisi_penumpang_cso` VARCHAR(20), `p_komisi_penumpang_cso` DOUBLE, `p_kode_akun_komisi_paket_sopir` VARCHAR(20), `p_komisi_paket_sopir` DOUBLE, `p_kode_akun_komisi_paket_cso` VARCHAR(20), `p_komisi_paket_cso` DOUBLE, `p_flag_aktif` INT(1), `p_flag_luar_kota` INT(1), `p_harga_paket_1_kilo_pertama` DOUBLE, `p_harga_paket_1_kilo_berikut` DOUBLE, `p_harga_paket_2_kilo_pertama` DOUBLE, `p_harga_paket_2_kilo_berikut` DOUBLE, `p_harga_paket_3_kilo_pertama` DOUBLE, `p_harga_paket_3_kilo_berikut` DOUBLE, `p_harga_paket_4_kilo_pertama` DOUBLE, `p_harga_paket_4_kilo_berikut` DOUBLE, `p_harga_paket_5_kilo_pertama` DOUBLE, `p_harga_paket_5_kilo_berikut` DOUBLE, `p_harga_paket_6_kilo_pertama` DOUBLE, `p_harga_paket_6_kilo_berikut` DOUBLE, `p_flag_op_jurusan` INT(1), `p_is_biaya_sopir_kumulatif` INT(1), `p_is_voucher_bbm` INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     KodeJurusan=p_kode_jurusan,
     KodeCabangAsal=p_kode_cabang_asal,KodeCabangTujuan=p_kode_cabang_tujuan,
     HargaTiket=p_harga_tiket,HargaTiketTuslah=p_harga_tiket_tuslah,
     FlagTiketTuslah=p_flag_tiket_tuslah,KodeAkunPendapatanPenumpang=p_kode_akun_pendapatan_penumpang,
     KodeAkunPendapatanPaket=p_kode_akun_pendapatan_paket,KodeAkunCharge=p_kode_akun_charge,
     KodeAkunBiayaSopir=p_kode_akun_biaya_sopir, BiayaSopir=p_biaya_sopir,
     KodeAkunKomisiPenumpangSopir=p_kode_akun_komisi_penumpang_sopir, KomisiPenumpangSopir=p_komisi_penumpang_sopir,
     KodeAkunKomisiPenumpangCSO=p_kode_akun_komisi_penumpang_cso, KomisiPenumpangCSO=p_komisi_penumpang_cso,
     KodeAkunKomisiPaketSopir=p_kode_akun_komisi_paket_sopir, KomisiPaketSopir=p_komisi_paket_sopir,
     KodeAkunKomisiPaketCSO=p_kode_akun_komisi_paket_cso, KomisiPaketCSO=p_komisi_paket_cso,
     FlagAktif=p_flag_aktif,KodeAkunBiayaTol=p_kode_akun_biaya_tol,
     BiayaTol=p_biaya_tol,KodeAkunBiayaParkir=p_kode_akun_biaya_parkir,
     BiayaParkir=p_biaya_parkir,KodeAkunBiayaBBM=p_kode_akun_biaya_bbm,
     BiayaBBM=p_biaya_bbm,FlagLuarKota=p_flag_luar_kota,
     HargaPaket1KiloPertama=p_harga_paket_1_kilo_pertama,HargaPaket1KiloBerikut=p_harga_paket_1_kilo_berikut,
     HargaPaket2KiloPertama=p_harga_paket_2_kilo_pertama,HargaPaket2KiloBerikut=p_harga_paket_2_kilo_berikut,
     HargaPaket3KiloPertama=p_harga_paket_3_kilo_pertama,HargaPaket3KiloBerikut=p_harga_paket_3_kilo_berikut,
     HargaPaket4KiloPertama=p_harga_paket_4_kilo_pertama,HargaPaket4KiloBerikut=p_harga_paket_4_kilo_berikut,
     HargaPaket5KiloPertama=p_harga_paket_5_kilo_pertama,HargaPaket5KiloBerikut=p_harga_paket_5_kilo_berikut,
     HargaPaket6KiloPertama=p_harga_paket_6_kilo_pertama,HargaPaket6KiloBerikut=p_harga_paket_6_kilo_berikut,
     FlagOperasionalJurusan=p_flag_op_jurusan,IsBiayaSopirKumulatif=p_is_biaya_sopir_kumulatif,
     IsVoucherBBM=p_is_voucher_bbm
   WHERE IdJurusan=p_id_jurusan;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jurusan_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_status_aktif`(`p_id_jurusan` INTEGER)
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagAktif=1-FlagAktif
   WHERE IdJurusan=p_id_jurusan;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_jurusan_ubah_tuslah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_tuslah`(`p_id_jurusan` INTEGER, `p_tuslah` INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagTiketTuslah=p_tuslah
   WHERE IdJurusan=p_id_jurusan;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_kendaraan_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_tambah`(`p_kode_kendaraan` VARCHAR(20), `p_kode_cabang` VARCHAR(20), `p_no_polisi` VARCHAR(20), `p_jenis` VARCHAR(50), `p_merek` VARCHAR(50), `p_tahun` VARCHAR(50), `p_warna` VARCHAR(50), `p_jumlah_kursi` INT(2), `p_kode_sopir1` VARCHAR(50), `p_kode_sopir2` VARCHAR(50), `p_no_STNK` VARCHAR(50), `p_no_BPKB` VARCHAR(50), `p_no_rangka` VARCHAR(50), `p_no_mesin` VARCHAR(50), `p_kilometer_akhir` DOUBLE, `p_flag_aktif` INT(1))
BEGIN

  INSERT INTO tbl_md_kendaraan (
    KodeKendaraan, KodeCabang, NoPolisi,
    Jenis,Merek, Tahun, Warna,
    JumlahKursi, KodeSopir1, KodeSopir2,
    NoSTNK, NoBPKB, NoRangka,
    NoMesin, KilometerAkhir, FlagAktif)
  VALUES(
   p_kode_kendaraan, p_kode_cabang, p_no_polisi,
    p_jenis,p_merek, p_tahun, p_warna,
    p_jumlah_kursi, p_kode_sopir1, p_kode_sopir2,
    p_no_STNK, p_no_BPKB, p_no_rangka,
    p_no_mesin, p_kilometer_akhir, p_flag_aktif);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_kendaraan_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah`(`p_kode_kendaraan_old` VARCHAR(20), `p_kode_kendaraan` VARCHAR(20), `p_kode_cabang` VARCHAR(20), `p_no_polisi` VARCHAR(20), `p_jenis` VARCHAR(50), `p_merek` VARCHAR(50), `p_tahun` VARCHAR(50), `p_warna` VARCHAR(50), `p_jumlah_kursi` INT(2), `p_kode_sopir1` VARCHAR(50), `p_kode_sopir2` VARCHAR(50), `p_no_STNK` VARCHAR(50), `p_no_BPKB` VARCHAR(50), `p_no_rangka` VARCHAR(50), `p_no_mesin` VARCHAR(50), `p_kilometer_akhir` DOUBLE, `p_flag_aktif` INT(1))
BEGIN

  UPDATE tbl_md_kendaraan SET
    KodeKendaraan=p_kode_kendaraan, KodeCabang=p_kode_cabang, NoPolisi=p_no_polisi,
    Jenis=p_jenis,Merek=p_merek, Tahun=p_tahun, Warna=p_warna,
    JumlahKursi=p_jumlah_kursi, KodeSopir1=p_kode_sopir1, KodeSopir2=p_kode_sopir2,
    NoSTNK=p_no_STNK, NoBPKB=p_no_BPKB, NoRangka=p_no_rangka,
    NoMesin=p_no_mesin, KilometerAkhir=p_kilometer_akhir, FlagAktif=p_flag_aktif
  WHERE KodeKendaraan = p_kode_kendaraan_old;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_kendaraan_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah_status_aktif`(`p_kode_kendaraan` VARCHAR(50))
BEGIN

   UPDATE tbl_md_kendaraan SET
     FlagAktif=1-FlagAktif
   WHERE KodeKendaraan=p_kode_kendaraan;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_log_user_login
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_login`(`p_user_id` INTEGER, `p_user_ip` VARCHAR(255))
BEGIN

   INSERT INTO  tbl_log_user
     (user_id, waktu_login, user_ip)
   VALUES (p_user_id, NOW(), p_user_ip);

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_log_user_logout
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_logout`(`p_user_id` INTEGER)
BEGIN

  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_user_id
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  UPDATE tbl_log_user
  SET waktu_logout=NOW()
  WHERE id_log=p_id_log;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_member_deposit_topup
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_topup`(`p_IdMember` VARCHAR(30), `p_KodeReferensi` VARCHAR(30), `p_JumlahRupiah` DOUBLE, `p_JumlahPoin` DOUBLE, `p_PetugasTopUp` INT(10), `p_NamaPetugasTopUp` VARCHAR(30), `p_KodeToken` VARCHAR(10))
BEGIN

DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;

  
  UPDATE tbl_md_member SET
    SaldoDeposit=SaldoDeposit+p_JumlahPoin,
    WaktuTransaksiTerakhir=NOW(),
    Signature=p_KodeToken
  WHERE IdMember=p_IdMember;

  
  SELECT SaldoDeposit INTO saldo_terakhir FROM tbl_md_member WHERE IdMember=p_IdMember;

  
  INSERT INTO tbl_member_deposit_trx_log (IdMember,KodeReferensi,WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo,Signature)
    VALUES (p_IdMember,p_KodeReferensi,NOW(),0,CONCAT('TOP UP DEPOSIT Rp.',p_JumlahRupiah,' Poin: ',p_JumlahPoin),p_JumlahPoin,saldo_terakhir,
    p_KodeToken);

  COMMIT;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_member_deposit_trx
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_trx`(`p_IdMember` VARCHAR(30), `p_KodeReferensi` VARCHAR(30), `p_KodeBooking` VARCHAR(50), `p_KodeJadwal` VARCHAR(30), `p_TglBerangkat` DATE, `p_Keterangan` TEXT, `p_CSO` INTEGER, `p_CabangTransaksi` VARCHAR(30), `p_HargaPoin` DOUBLE, `p_Discount` DOUBLE, `p_KodePromo` VARCHAR(50), `p_Signature` VARCHAR(100))
BEGIN

  DECLARE saldo_terakhir DOUBLE;
  DECLARE jumlah_poin DOUBLE;

  START TRANSACTION;

  
  SELECT SUM(Total-IF(Discount<=0,p_Discount,0))/p_HargaPoin INTO jumlah_poin FROM tbl_reservasi WHERE KodeBooking=p_KodeBooking AND FlagBatal!=1 AND CetakTiket!=1;

  
  SELECT Saldo-jumlah_poin INTO saldo_terakhir FROM tbl_member_deposit_trx_log WHERE IdMember=p_IdMember ORDER BY WaktuTransaksi DESC LIMIT 0,1;

  
  INSERT INTO tbl_member_deposit_trx_log (IdMember,KodeReferensi,WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo,Signature)
    VALUES (p_IdMember,p_KodeReferensi,NOW(),1,p_Keterangan,jumlah_poin,saldo_terakhir,
    p_Signature);

  
  UPDATE tbl_reservasi
    SET
	CetakTiket=1,PetugasCetakTiket=p_CSO,
	WaktuCetakTiket=NOW(),
	JenisPembayaran=4,
	KodeCabang=p_CabangTransaksi,
        IdMember=p_IdMember,
        
	Total=Total-IF(Discount<=0,p_Discount,0),
	Discount=IF(Discount<=0,p_Discount,Discount),
	JenisDiscount=p_KodePromo
  WHERE KodeBooking=p_KodeBooking AND FlagBatal!=1 AND CetakTiket!=1;

  
  UPDATE tbl_posisi_detail SET StatusBayar=1
  WHERE
    KodeJadwal=p_KodeJadwal
    AND TGLBerangkat=p_TglBerangkat
    AND KodeBooking=p_KodeBooking;

    
	UPDATE tbl_md_member SET
    SaldoDeposit=saldo_terakhir,
    WaktuTransaksiTerakhir=NOW(),
    Signature=p_Signature
  WHERE IdMember=p_IdMember;


  COMMIT;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_member_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_tambah`(`p_id_member` VARCHAR(25), `p_nama` VARCHAR(150), `p_jenis_kelamin` INT(1), `p_kategori_member` INT(1), `p_tempat_lahir` VARCHAR(30), `p_tgl_lahir` DATE, `p_no_ktp` VARCHAR(30), `p_tgl_registrasi` DATE, `p_alamat` TEXT, `p_kota` VARCHAR(30), `p_kode_pos` VARCHAR(6), `p_telp` VARCHAR(20), `p_handphone` VARCHAR(20), `p_email` VARCHAR(30), `p_pekerjaan` VARCHAR(50), `p_point` DOUBLE, `p_expired_date` DATE, `p_id_kartu` VARCHAR(255), `p_no_seri_kartu` VARCHAR(30), `p_kata_sandi` TEXT, `p_flag_aktif` INT(1), `p_cabang_daftar` VARCHAR(20))
BEGIN

  INSERT INTO tbl_md_member (
    IdMember, Nama, JenisKelamin,
    KategoriMember, TempatLahir, TglLahir,
    NoKTP, TglRegistrasi, Alamat,
    Kota, KodePos, Telp,
    Handphone, Email, Pekerjaan,
    Point, ExpiredDate, IdKartu,
    NoSeriKartu, KataSandi, FlagAktif,
    CabangDaftar)
  VALUES(
    p_id_member, p_nama, p_jenis_kelamin,
    p_kategori_member,p_tempat_lahir,p_tgl_lahir,
    p_no_ktp, p_tgl_registrasi, p_alamat,
    p_kota, p_kode_pos, p_telp,
    p_handphone, p_email, p_pekerjaan,
    p_point, p_expired_date, p_id_kartu,
    p_no_seri_kartu, p_kata_sandi, p_flag_aktif,
    p_cabang_daftar);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_member_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah`(`p_id_member` VARCHAR(25), `p_nama` VARCHAR(150), `p_jenis_kelamin` INT(1), `p_kategori_member` INT(1), `p_tempat_lahir` VARCHAR(30), `p_tgl_lahir` DATE, `p_no_ktp` VARCHAR(30), `p_tgl_registrasi` DATE, `p_alamat` TEXT, `p_kota` VARCHAR(30), `p_kode_pos` VARCHAR(6), `p_telp` VARCHAR(20), `p_handphone` VARCHAR(20), `p_email` VARCHAR(30), `p_pekerjaan` VARCHAR(50), `p_point` DOUBLE, `p_expired_date` DATE, `p_id_kartu` VARCHAR(255), `p_no_seri_kartu` VARCHAR(30), `p_kata_sandi` TEXT, `p_flag_aktif` INT(1), `p_cabang_daftar` VARCHAR(20))
BEGIN

  UPDATE tbl_md_member SET
    Nama=p_nama, JenisKelamin=p_jenis_kelamin,
    KategoriMember=p_kategori_member, TempatLahir=p_tempat_lahir, TglLahir=p_tgl_lahir,
    NoKTP=p_no_ktp, TglRegistrasi=p_tgl_registrasi, Alamat=p_alamat,
    Kota=p_kota, KodePos=p_kode_pos, Telp=p_telp,
    Handphone=p_handphone, Email=p_email, Pekerjaan=p_pekerjaan,
    Point=p_point, ExpiredDate=p_expired_date, IdKartu=p_id_kartu,
    NoSeriKartu=p_no_seri_kartu, KataSandi=p_kata_sandi, FlagAktif=p_flag_aktif,
    CabangDaftar=p_cabang_daftar
  WHERE IdMember=p_id_member;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_member_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah_status_aktif`(`p_id_member` VARCHAR(25))
BEGIN

   UPDATE tbl_md_member SET
     FlagAktif=1-FlagAktif
   WHERE IdMember=p_id_member;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_paket_ambil_paket
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ambil_paket`(`p_no_tiket` VARCHAR(50), `p_nama_pengambil` VARCHAR(20), `p_no_ktp_pengambil` VARCHAR(20), `p_petugas_pemberi` INTEGER)
BEGIN

  UPDATE tbl_paket
  SET
    NamaPengambil=p_nama_pengambil,
    NoKTPPengambil=p_no_ktp_pengambil,
    PetugasPemberi=p_petugas_pemberi,
    WaktuPengambilan=NOW(),
    StatusDiambil=1
  WHERE
    NoTiket = p_no_tiket;


END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_paket_batal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_batal`(`p_NoTiket` VARCHAR(50), `p_PetugasPembatalan` INTEGER, `p_WaktuPembatalan` DATETIME)
BEGIN

  UPDATE tbl_paket SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;


END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_paket_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_tambah`(`p_no_tiket` VARCHAR(50), `p_kode_cabang` VARCHAR(20), `p_kode_jadwal` VARCHAR(30), `p_id_jurusan` INTEGER, `p_kode_kendaraan` VARCHAR(20), `p_kode_sopir` VARCHAR(50), `p_tgl_berangkat` DATE, `p_jam_berangkat` VARCHAR(10), `p_nama_pengirim` VARCHAR(100), `p_alamat_pengirim` VARCHAR(100), `p_telp_pengirim` VARCHAR(15), `p_waktu_pesan` DATETIME, `p_nama_penerima` VARCHAR(100), `p_alamat_penerima` VARCHAR(100), `p_telp_penerima` VARCHAR(15), `p_harga_paket` DOUBLE, `p_keterangan_paket` TEXT, `p_petugas_penjual` INTEGER, `p_no_SPJ` VARCHAR(35), `p_tgl_cetak_SPJ` DATE, `p_cetak_SPJ` INT(1), `p_komisi_paket_CSO` DOUBLE, `p_komisi_paket_sopir` DOUBLE, `p_kode_akun_pendapatan` VARCHAR(20), `p_kode_akun_komisi_paket_CSO` VARCHAR(20), `p_kode_akun_komisi_paket_sopir` VARCHAR(20), `p_jumlah_koli` DOUBLE, `p_berat` DOUBLE, `p_jenis_barang` VARCHAR(10), `p_layanan` CHAR(2), `p_cara_bayar` INT(1), `p_is_ekspedisi` INT(1))
BEGIN

  INSERT INTO tbl_paket (
    NoTiket, KodeCabang, KodeJadwal,
    IdJurusan, KodeKendaraan, KodeSopir,
    TglBerangkat, JamBerangkat, NamaPengirim,
    AlamatPengirim, TelpPengirim, WaktuPesan,
    NamaPenerima, AlamatPenerima, TelpPenerima,
    HargaPaket,KeteranganPaket, PetugasPenjual,
    NoSPJ, TglCetakSPJ,
    CetakSPJ, KomisiPaketCSO, KomisiPaketSopir,
    KodeAkunPendapatan,KodeAkunKomisiPaketCSO,KodeAkunKomisiPaketSopir,
    JumlahKoli,Berat,JenisBarang,
    Layanan,CaraPembayaran,
    FlagBatal,IsEkspedisi)
  VALUES(
    p_no_tiket, p_kode_cabang, p_kode_jadwal,
    p_id_jurusan, p_kode_kendaraan , p_kode_sopir ,
    p_tgl_berangkat, p_jam_berangkat , p_nama_pengirim ,
    p_alamat_pengirim, p_telp_pengirim, p_waktu_pesan,
    p_nama_penerima, p_alamat_penerima, p_telp_penerima,
    p_harga_paket,p_keterangan_paket, p_petugas_penjual,
    p_no_SPJ, p_tgl_cetak_SPJ,
    p_cetak_SPJ, p_komisi_paket_CSO, p_komisi_paket_sopir,
    p_kode_akun_pendapatan, p_kode_akun_komisi_paket_CSO, p_kode_akun_komisi_paket_sopir,
    p_jumlah_koli,p_berat,p_jenis_barang,
    p_layanan,p_cara_bayar,
    0,p_is_ekspedisi);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_paket_ubah_data_after_spj
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ubah_data_after_spj`(`p_kode_jadwal` VARCHAR(50), `p_tgl_berangkat` DATE, `p_sopir_dipilih` VARCHAR(50), `p_mobil_dipilih` VARCHAR(50), `p_no_spj` VARCHAR(50))
BEGIN

  UPDATE tbl_paket
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    TglCetakSPJ=NOW(),
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_paket_update_cetak_tiket
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_update_cetak_tiket`(`p_no_tiket` VARCHAR(50), `p_jenis_pembayaran` INT(1), `p_cabang_transaksi` VARCHAR(30))
BEGIN

  UPDATE tbl_paket
  SET
    CetakTiket=1,
    JenisPembayaran=p_jenis_pembayaran,
    KodeCabang=p_cabang_transaksi
  WHERE
    NoTiket = p_no_tiket;


END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_pelanggan_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_tambah`(`p_no_hp` VARCHAR(20), `p_no_telp` VARCHAR(20), `p_nama` VARCHAR(150), `p_alamat` VARCHAR(50), `p_tgl_pertama_transaksi` DATE, `p_tgl_terakhir_transaksi` DATE, `p_cso_terakhir` INTEGER, `p_kode_jurusan_terakhir` VARCHAR(10), `p_frekwensi_pergi` DOUBLE, `p_flag_member` INT(1))
BEGIN

  INSERT INTO tbl_pelanggan (
    NoHP, NoTelp, Nama,
    Alamat, TglPertamaTransaksi, TglTerakhirTransaksi,
    CSOTerakhir, KodeJurusanTerakhir, FrekwensiPergi,
    FlagMember)
  VALUES(
    p_no_hp, p_no_telp,p_nama,
    p_alamat, p_tgl_pertama_transaksi, p_tgl_terakhir_transaksi,
    p_cso_terakhir, p_kode_jurusan_terakhir,p_frekwensi_pergi,
    p_flag_member);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_pelanggan_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_ubah`(`p_no_telp_old` VARCHAR(20), `p_no_hp_old` VARCHAR(20), `p_no_hp` VARCHAR(20), `p_no_telp` VARCHAR(20), `p_nama` VARCHAR(150), `p_alamat` VARCHAR(50), `p_tgl_terakhir_transaksi` DATE, `p_cso_terakhir` INTEGER, `p_kode_jurusan_terakhir` VARCHAR(10))
BEGIN

  UPDATE tbl_pelanggan SET
    NoHP=p_no_hp, NoTelp=p_no_telp, Nama=p_nama,
    TglTerakhirTransaksi=p_tgl_terakhir_transaksi,
    CSOTerakhir=p_cso_terakhir, KodeJurusanTerakhir=p_kode_jurusan_terakhir, FrekwensiPergi=FrekwensiPergi+1
  WHERE NoTelp=p_no_telp_old;

  IF p_alamat!='' THEN
    UPDATE tbl_pelanggan SET
      Alamat=p_alamat
    WHERE NoTelp=p_no_telp_old;
  END IF;

  END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_pengumuman_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_tambah`(`p_kode_pengumuman` VARCHAR(20), `p_judul_pengumuman` VARCHAR(255), `p_pengumuman` TEXT, `p_kota` VARCHAR(20), `p_pembuat_pengumuman` INTEGER, `p_waktu_pembuatan` DATE)
BEGIN

  INSERT INTO tbl_pengumuman (
    KodePengumuman,JudulPengumuman, Pengumuman,
    Kota,PembuatPengumuman, WaktuPembuatanPengumuman)
  VALUES(
    p_kode_pengumuman,p_judul_pengumuman, p_pengumuman,
    p_kota, p_pembuat_pengumuman, p_waktu_pembuatan);

  CALL sp_user_update_pengumuman_baru();
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_pengumuman_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_ubah`(`p_id_pengumuman` INTEGER, `p_kode_pengumuman_old` VARCHAR(20), `p_kode_pengumuman` VARCHAR(20), `p_judul_Pengumuman` VARCHAR(255), `p_pengumuman` TEXT, `p_kota` VARCHAR(20), `p_pembuat_pengumuman` INTEGER, `p_waktu_pembuatan` DATE)
BEGIN

  UPDATE tbl_pengumuman SET
    KodePengumuman=p_kode_pengumuman, JudulPengumuman=p_judul_pengumuman, Pengumuman=p_pengumuman,
    Kota=p_kota, PembuatPengumuman=p_pembuat_pengumuman, WaktuPembuatanPengumuman=p_waktu_pembuatan
  WHERE IdPengumuman=p_id_pengumuman;

  CALL sp_user_update_pengumuman_baru();
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_penjadwalan_kendaraan_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_penjadwalan_kendaraan_ubah_status_aktif`(`p_id_jadwal` INTEGER)
BEGIN

   UPDATE tbl_penjadwalan_kendaraan SET
     StatusAktif=1-StatusAktif
   WHERE IdPenjadwalan=p_id_jadwal;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_posisi_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_posisi_tambah`(`p_kode_jadwal` VARCHAR(50), `p_tgl_berangkat` DATE, `p_jam_berangkat` TIME, `p_jumlah_kursi` INT(2), `p_kode_kendaraan` VARCHAR(20), `p_kode_sopir` VARCHAR(50))
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_tgl_berangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    INSERT INTO tbl_posisi
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  ELSE
    INSERT INTO tbl_posisi_backup
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  END IF;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_promo_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_ubah_status_aktif`(`p_kode` VARCHAR(50))
BEGIN

   UPDATE tbl_md_promo SET
     FlagAktif=1-FlagAktif
   WHERE KodePromo=p_kode;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_batal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_batal`(`p_NoTiket` VARCHAR(50), `p_PetugasPembatalan` INTEGER, `p_WaktuPembatalan` DATETIME)
BEGIN

  UPDATE tbl_reservasi SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
      WaktuPembatalan=p_WaktuPembatalan
    WHERE NoTiket=p_NoTiket;
  END IF;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_insert_status_kursi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_insert_status_kursi`(`p_NomorKursi` INT(2), `p_Session` INTEGER, `p_KodeJadwal` VARCHAR(50), `p_TglBerangkat` DATE)
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_posisi_detail (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  ELSE
    INSERT INTO tbl_posisi_detail_backup (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  END IF;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_mutasi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_mutasi`(`p_NoTiket` VARCHAR(50), `p_KodeJadwal` VARCHAR(50), `p_IdJurusan` INTEGER, `p_KodeKendaraan` VARCHAR(20), `p_KodeSopir` VARCHAR(50), `p_TglBerangkat` DATE, `p_JamBerangkat` VARCHAR(10), `p_NomorKursi` INT(2), `p_HargaTiket` DOUBLE, `p_Charge` DOUBLE, `p_SubTotal` DOUBLE, `p_Discount` DOUBLE, `p_PPN` DOUBLE, `p_Total` DOUBLE, `p_NoSPJ` VARCHAR(35), `p_TglCetakSPJ` DATETIME, `p_CetakSPJ` INT(1), `p_KomisiPenumpangCSO` DOUBLE, `p_PetugasCetakSPJ` INTEGER, `p_Keterangan` VARCHAR(100), `p_JenisDiscount` VARCHAR(50), `p_KodeAkunPendapatan` VARCHAR(20), `p_KodeAkunKomisiPenumpangCSO` VARCHAR(20), `p_PaymentCode` VARCHAR(20), `p_Nama` VARCHAR(50), `p_StatusBayar` INT(1), `p_KodeJadwalLama` VARCHAR(50), `p_TglBerangkatLama` DATE, `p_NomorKursiLama` INT(2), `p_Pemutasi` INTEGER)
BEGIN

  

  DECLARE p_KodeBooking VARCHAR(50);
  DECLARE p_KodeJadwalOld VARCHAR(50);
  DECLARE p_TglBerangkatOld DATE;

  SELECT
    KodeBooking,KodeJadwal,TglBerangkat
    INTO p_KodeBooking,p_KodeJadwalOld,p_TglBerangkatOld
  FROM tbl_reservasi WHERE NoTiket=p_NoTiket;

  UPDATE tbl_reservasi SET
    KodeJadwal=p_KodeJadwal, IdJurusan=p_IdJurusan,
    KodeKendaraan=p_KodeKendaraan, KodeSopir=p_KodeSopir, TglBerangkat=p_TglBerangkat,
    JamBerangkat=p_JamBerangkat, NomorKursi=p_NomorKursi, NoSPJ=p_NoSPJ,
    TglCetakSPJ=p_TglCetakSPJ, CetakSPJ=p_CetakSPJ, KomisiPenumpangCSO=p_KomisiPenumpangCSO,
    PetugasCetakSPJ=p_PetugasCetakSPJ, Keterangan=p_Keterangan, JenisDiscount=p_JenisDiscount,
    KodeAkunPendapatan=p_KodeAkunPendapatan, KodeAkunKomisiPenumpangCSO=p_KodeAkunKomisiPenumpangCSO,PaymentCode=p_PaymentCode,
    WaktuMutasi=NOW(),Pemutasi=p_Pemutasi,
    KodeBooking=if(p_KodeJadwalOld=p_KodeJadwal AND p_TglBerangkatOld=p_TglBerangkat,KodeBooking,CONCAT(KodeBooking,"M"))
  WHERE NoTiket=p_NoTiket;

  
  CALL sp_reservasi_set_status_kursi(
    p_NomorKursi,null, f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal),
    p_TglBerangkat,0,0);

  
  UPDATE tbl_posisi_detail SET
    StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
    KodeBooking=if(f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalOld)=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal) AND p_TglBerangkatOld=p_TglBerangkat,p_KodeBooking,CONCAT(p_KodeBooking,"M")),
    Session=NULL,StatusBayar=p_StatusBayar
  WHERE
    NomorKursi=p_NomorKursi
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
    AND TglBerangkat=p_TglBerangkat;


  
  UPDATE tbl_posisi_detail SET
    StatusKursi=0,Nama=NULL,NoTiket=NULL,
    KodeBooking=NULL,Session=NULL,StatusBayar=0
  WHERE
    NomorKursi=p_NomorKursiLama
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalLama)
    AND TglBerangkat=p_TglBerangkatLama;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_set_status_kursi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_set_status_kursi`(`p_NomorKursi` INT(2), `p_Session` INTEGER, `p_KodeJadwal` VARCHAR(50), `p_TglBerangkat` DATE, `p_StatusKursiAdmin` INT(1), `p_session_time_expired` INT)
BEGIN

  DECLARE p_ditemukan INT;
  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  ELSE
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail_backup WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  END IF;

  IF(p_ditemukan>0) THEN
    CALL sp_reservasi_update_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat,p_StatusKursiAdmin,p_session_time_expired);
  ELSE
    CALL sp_reservasi_insert_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat);
  END IF;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah`(
IN `p_NoTiket` VARCHAR(50), 
IN `p_KodeCabang` VARCHAR(20), 
IN `p_KodeJadwal` VARCHAR(50), 
IN `p_IdJurusan` INT, 
IN `p_KodeKendaraan` VARCHAR(20), 
IN `p_KodeSopir` VARCHAR(50), 
IN `p_TglBerangkat` DATE, 
IN `p_JamBerangkat` VARCHAR(10), 
IN `p_KodeBooking` VARCHAR(50), 
IN `p_IdMember` VARCHAR(25), 
IN `p_PointMember` INT(3), 
IN `p_Nama` VARCHAR(100), 
IN `p_AlamatRumah` TEXT, 
IN `p_Telp` VARCHAR(15), 
IN `p_HP` VARCHAR(15), 
IN `p_WaktuPesan` DATETIME, 
IN `p_NomorKursi` INT(2), 
IN `p_HargaTiket` DOUBLE, 
IN `p_Charge` DOUBLE, 
IN `p_SubTotal` DOUBLE, 
IN `p_Discount` DOUBLE, 
IN `p_PPN` DOUBLE, 
IN `p_Total` DOUBLE, 
IN `p_PetugasPenjual` INT, 
IN `p_FlagPesanan` INT(1), 
IN `p_NoSPJ` VARCHAR(35), 
IN `p_TglCetakSPJ` DATETIME, 
IN `p_CetakSPJ` INT(1), 
IN `p_KomisiPenumpangCSO` DOUBLE, 
IN `p_FlagSetor` INT(1), 
IN `p_PetugasCetakSPJ` INT, 
IN `p_Keterangan` VARCHAR(100), 
IN `p_JenisDiscount` VARCHAR(50), 
IN `p_KodeAkunPendapatan` VARCHAR(20), 
IN `p_JenisPenumpang` CHAR(6), 
IN `p_KodeAkunKomisiPenumpangCSO` VARCHAR(20), 
IN `p_PaymentCode` VARCHAR(20), 
IN `p_AlamatTujuan` TEXT, 
IN `p_AlamatJemput` TEXT, 
IN `p_AreaJemput` CHAR(20), 
IN `p_AreaAntar` CHAR(20),
IN `p_BiayaJemput` INT(11), 
IN `p_BiayaAntar` INT(11))
BEGIN
    INSERT INTO tbl_reservasi (
NoTiket, 
KodeCabang, 
KodeJadwal,
IdJurusan, 
KodeKendaraan, 
KodeSopir,
TglBerangkat, 
JamBerangkat, 
KodeBooking,
IdMember, 
PointMember, 
Nama,
Alamat,
Telp, 
HP,
WaktuPesan, 
NomorKursi, 
HargaTiket,
Charge, 
SubTotal, 
Discount,
PPN, 
Total, 
PetugasPenjual,
FlagPesanan, 
NoSPJ, 
TglCetakSPJ,
CetakSPJ, 
KomisiPenumpangCSO, 
FlagSetor,
PetugasCetakSPJ, 
Keterangan, 
JenisDiscount,
KodeAkunPendapatan, 
JenisPenumpang, 
KodeAkunKomisiPenumpangCSO,
PaymentCode,
AlamatTujuan,
AlamatJemput,
area_jemput,
area_antar,
BiayaJemput,
BiayaAntar
)
    VALUES(
p_NoTiket, 
p_KodeCabang, 
p_KodeJadwal,
p_IdJurusan, 
p_KodeKendaraan, 
p_KodeSopir,
p_TglBerangkat, 
p_JamBerangkat, 
p_KodeBooking,
p_IdMember, 
p_PointMember, 
p_Nama,
p_AlamatRumah,
p_Telp, 
p_HP,
p_WaktuPesan, 
p_NomorKursi, 
p_HargaTiket,
p_Charge, 
p_SubTotal, 
p_Discount,
p_PPN, 
p_Total, 
p_PetugasPenjual,
p_FlagPesanan, 
p_NoSPJ, 
p_TglCetakSPJ,
p_CetakSPJ, 
p_KomisiPenumpangCSO, 
p_FlagSetor,
p_PetugasCetakSPJ, 
p_Keterangan, 
p_JenisDiscount,
p_KodeAkunPendapatan, 
p_JenisPenumpang, 
p_KodeAkunKomisiPenumpangCSO,
p_PaymentCode,
p_AlamatTujuan,
p_AlamatJemput,
p_AreaJemput,
p_AreaAntar,
p_BiayaJemput,
p_BiayaAntar
);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwal
      AND TglBerangkat=p_TglBerangkat;
  
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_tambah_with_komisi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah_with_komisi`(IN `p_NoTiket` VARCHAR(50), IN `p_KodeCabang` VARCHAR(20), IN `p_KodeJadwal` VARCHAR(50), IN `p_IdJurusan` INT, IN `p_KodeKendaraan` VARCHAR(20), IN `p_KodeSopir` VARCHAR(50), IN `p_TglBerangkat` DATE, IN `p_JamBerangkat` VARCHAR(10), IN `p_KodeBooking` VARCHAR(50), IN `p_IdMember` VARCHAR(25), IN `p_PointMember` INT(3), IN `p_Nama` VARCHAR(100), IN `p_Alamat` VARCHAR(100), IN `p_Telp` VARCHAR(15), IN `p_HP` VARCHAR(15), IN `p_WaktuPesan` DATETIME, IN `p_NomorKursi` INT(2), IN `p_HargaTiket` DOUBLE, IN `p_Charge` DOUBLE, IN `p_SubTotal` DOUBLE, IN `p_Discount` DOUBLE, IN `p_PPN` DOUBLE, IN `p_Total` DOUBLE, IN `p_PetugasPenjual` INT, IN `p_FlagPesanan` INT(1), IN `p_NoSPJ` VARCHAR(35), IN `p_TglCetakSPJ` DATETIME, IN `p_CetakSPJ` INT(1), IN `p_KomisiPenumpangCSO` DOUBLE, IN `p_FlagSetor` INT(1), IN `p_PetugasCetakSPJ` INT, IN `p_Keterangan` VARCHAR(100), IN `p_JenisDiscount` VARCHAR(50), IN `p_KodeAkunPendapatan` VARCHAR(20), IN `p_JenisPenumpang` CHAR(2), IN `p_KodeAkunKomisiPenumpangCSO` VARCHAR(20), IN `p_PaymentCode` VARCHAR(20), IN `p_harga_tiketux` DOUBLE)
BEGIN

  DECLARE p_selisih_hari INTEGER;
  DECLARE p_komisi DOUBLE;

  
  SELECT
    IF((SELECT FlagLuarKota FROM tbl_md_jurusan WHERE IdJurusan=p_IdJurusan)=1
      AND p_JenisPenumpang!='R',NilaiParameter,0) INTO p_komisi
  FROM tbl_pengaturan_parameter
  WHERE NamaParameter='KOMISITTX1';

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi,HargaTiketux)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi,p_harga_tiketux);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  ELSE
    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi,HargaTiketux)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi,p_harga_tiketux);

    UPDATE tbl_posisi_detail_backup SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  END IF;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_ubah_data_after_spj
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_after_spj`(`p_kode_jadwal` VARCHAR(50), `p_tgl_berangkat` DATE, `p_sopir_dipilih` VARCHAR(50), `p_mobil_dipilih` VARCHAR(50), `p_no_spj` VARCHAR(50))
BEGIN

  UPDATE tbl_reservasi
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    TglCetakSPJ=NOW(),
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap
    SET
      KodeSopir=p_sopir_dipilih,
      KodeKendaraan=p_mobil_dipilih,
      NoSPJ=p_no_spj,
      TglCetakSPJ=NOW(),
      CetakSPJ=1
    WHERE
      TglBerangkat = p_tgl_berangkat
      AND KodeJadwal=p_kode_jadwal;
  END IF;

  UPDATE tbl_paket
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    TglCetakSPJ=NOW(),
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_ubah_data_penumpang
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_penumpang`(`p_no_tiket` VARCHAR(50), `p_nama` VARCHAR(100), `p_alamat` VARCHAR(100), `p_telp` VARCHAR(15), `p_id_member` VARCHAR(25))
BEGIN

  UPDATE tbl_reservasi SET
    Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
  WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
    WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;
  END IF;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_ubah_discount
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_discount`(IN `p_NoTiket` VARCHAR(50), IN `p_kode_discount` CHAR(6), IN `p_NamaDiscount` VARCHAR(30), IN `p_JumlahDiscount` DOUBLE)
BEGIN

  DECLARE p_kode_booking VARCHAR(50);

  

  

  

  UPDATE tbl_reservasi SET
    JenisPenumpang=p_kode_discount,
    Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
    JenisDiscount=p_NamaDiscount,
    Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      JenisPenumpang=p_kode_discount,
      Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
      JenisDiscount=p_NamaDiscount,
      Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);
  END IF;



END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_reservasi_update_status_kursi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_update_status_kursi`(`p_NomorKursi` INT(2), `p_Session` INTEGER, `p_KodeJadwalDipilih` VARCHAR(50), `p_JamBerangkat` TIME, `p_KodeCabangAsal` VARCHAR(50), `p_KodeCabangTujuan` VARCHAR(50), `p_IsSubJadwal` INT(1), `p_KodeJadwalUtama` VARCHAR(50), `p_TglBerangkat` DATE, `p_StatusKursiAdmin` INT(1), `p_session_time_expired` INT)
BEGIN DECLARE p_selisih_hari INTEGER;
  DECLARE p_ditemukan INTEGER;
  
    SELECT COUNT(1) INTO p_ditemukan FROM tbl_posisi_detail
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwalDipilih
			AND KodeJadwalUtama=p_KodeJadwalUtama
			AND IsSubJadwal=p_IsSubJadwal
      AND TglBerangkat=p_TglBerangkat
      AND (NoTiket='' OR NoTiket IS NULL)
      AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    IF p_ditemukan<=0 THEN
     INSERT INTO tbl_posisi_detail (
        NomorKursi, KodeJadwal, JamBerangkat,
				IsSubJadwal, KodeCabangAsal,KodeCabangTujuan, 
				KodeJadwalUtama,TglBerangkat,StatusKursi, 
				Session,SessionTime)
      VALUES(
        p_NomorKursi, p_KodeJadwalDipilih , p_JamBerangkat,
				p_IsSubJadwal, p_KodeCabangAsal ,p_KodeCabangTujuan ,
				p_KodeJadwalUtama, p_TglBerangkat,1, 
				p_Session,NOW());
    ELSE
      UPDATE tbl_posisi_detail SET
        StatusKursi=IF(f_reservasi_session_time_selisih(SessionTime)<=p_session_time_expired OR StatusKursi=0,1-StatusKursi,StatusKursi), Session=p_Session , SessionTime=NOW()
      WHERE
        NomorKursi=p_NomorKursi
        AND KodeJadwal=p_KodeJadwalDipilih
				AND KodeJadwalUtama=p_KodeJadwalUtama
				AND IsSubJadwal=p_IsSubJadwal
        AND TglBerangkat=p_TglBerangkat
        AND (NoTiket='' OR NoTiket IS NULL)
        AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    END IF;

   
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_sessions_create
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_create`(`p_user_id` INTEGER, `p_session_id` VARCHAR(255), `p_user_ip` VARCHAR(255), `p_current_time` FLOAT(15), `p_page_id` FLOAT(15), `p_login` FLOAT(15), `p_admin` FLOAT(15))
BEGIN

   DELETE FROM tbl_sessions
   WHERE session_user_id=-1 OR session_user_id=p_user_id;

   INSERT INTO  tbl_sessions
     (session_id, session_user_id, session_start, session_time, session_ip, session_page, session_logged_in, session_admin)
   VALUES (p_session_id, p_user_id, p_current_time, p_current_time,p_user_ip, p_page_id, p_login, p_admin);

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_sessions_end
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_end`(`p_user_id` INTEGER, `p_session_id` VARCHAR(255))
BEGIN

   DELETE FROM  tbl_sessions
   WHERE session_id = p_session_id
   AND session_user_id = p_user_id;

   UPDATE tbl_user
   SET status_online=0,waktu_logout=NOW()
   WHERE user_id = p_user_id;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_sopir_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_tambah`(`p_kode_sopir` VARCHAR(50), `p_nama` VARCHAR(150), `p_HP` VARCHAR(50), `p_alamat` VARCHAR(50), `p_no_SIM` VARCHAR(50), `p_flag_aktif` INT(1))
BEGIN

  INSERT INTO tbl_md_sopir (
    KodeSopir, Nama, HP, Alamat, NoSIM, FlagAktif)
  VALUES(
    p_kode_sopir, p_nama, p_HP, p_alamat, p_no_SIM, p_flag_aktif);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_sopir_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah`(`p_kode_sopir_old` VARCHAR(50), `p_kode_sopir` VARCHAR(50), `p_nama` VARCHAR(150), `p_HP` VARCHAR(50), `p_alamat` VARCHAR(50), `p_no_SIM` VARCHAR(50), `p_flag_aktif` INT(1))
BEGIN

  UPDATE tbl_md_sopir SET
    KodeSopir=p_kode_sopir, Nama=p_nama, HP=p_HP,
    Alamat=p_alamat, NoSIM=p_no_SIM, FlagAktif=p_flag_aktif
  WHERE KodeSopir=p_kode_sopir_old;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_sopir_ubah_status_aktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah_status_aktif`(`p_kode_sopir` VARCHAR(50))
BEGIN

   UPDATE tbl_md_sopir SET
     FlagAktif=1-FlagAktif
   WHERE KodeSopir=p_kode_sopir;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_spj_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_tambah`(`p_no_spj` VARCHAR(50), `p_kode_jadwal` VARCHAR(50), `p_tgl_berangkat` DATE, `p_jam_berangkat` VARCHAR(10), `p_layout_kursi` INT(2), `p_jumlah_penumpang` INT(2), `p_mobil_dipilih` VARCHAR(50), `p_cso` INTEGER, `p_sopir_dipilih` VARCHAR(50), `p_nama_sopir` VARCHAR(50), `p_total_omzet` DOUBLE, `p_jumlah_paket` INT(3), `p_total_omzet_paket` DOUBLE, `p_is_ekspedisi` INT(1))
BEGIN

  INSERT INTO tbl_spj
    (NoSPJ,KodeJadwal,TglBerangkat,JamBerangkat,
    JumlahKursiDisediakan,JumlahPenumpang,TglSPJ,
    NoPolisi,CSO,KodeDriver,
    Driver,TotalOmzet,IdJurusan,
    FlagAmbilBiayaOP, JumlahPaket,TotalOmzetPaket,
    IsEkspedisi)
  VALUES(
    p_no_spj,p_kode_jadwal,p_tgl_berangkat, p_jam_berangkat,
    p_layout_kursi,p_jumlah_penumpang,NOW(),
    p_mobil_dipilih,p_cso,p_sopir_dipilih,
    p_nama_sopir,p_total_omzet,f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
    0,p_jumlah_paket,p_total_omzet_paket,
    p_is_ekspedisi);

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_spj_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah`(`p_no_spj` VARCHAR(50), `p_jumlah_penumpang` INT(2), `p_mobil_dipilih` VARCHAR(50), `p_cso` INTEGER, `p_sopir_dipilih` VARCHAR(50), `p_nama_sopir` VARCHAR(50), `p_total_omzet` DOUBLE, `p_jumlah_paket` INT(3), `p_total_omzet_paket` DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    JumlahPenumpang=p_jumlah_penumpang,
    TglSPJ=NOW(),
    NoPolisi=p_mobil_dipilih,
    CSO=p_cso,
    KodeDriver=p_sopir_dipilih,
    Driver=p_nama_sopir,
    TotalOmzet=p_total_omzet,
    JumlahPaket=p_jumlah_paket,
    TotalOmzetPaket=p_total_omzet_paket
  WHERE NoSPJ=p_no_spj;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_spj_ubah_insentif_sopir
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah_insentif_sopir`(`p_no_spj` VARCHAR(50), `p_insentif_sopir` DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    InsentifSopir=p_insentif_sopir
  WHERE NoSPJ=p_no_spj;

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_user_baca_pengumuman_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_baca_pengumuman_tambah`(`p_id_pengumuman` INTEGER, `p_user_id` INTEGER)
BEGIN

  INSERT INTO tbl_user_baca_pengumuman (
    user_id,IdPengumuman)
  VALUES(
    p_user_id, p_id_pengumuman);
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_user_reset_pengumuman_baru
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_reset_pengumuman_baru`(`p_user_id` INTEGER)
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=0
  WHERE user_id=p_user_id;
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_user_tambah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_tambah`(`p_username` VARCHAR(50), `p_user_password` VARCHAR(255), `p_NRP` VARCHAR(20), `p_nama` VARCHAR(255), `p_telp` VARCHAR(20), `p_hp` VARCHAR(20), `p_email` VARCHAR(50), `p_address` VARCHAR(255), `p_KodeCabang` VARCHAR(20), `p_status_online` BIT, `p_berlaku` DATETIME, `p_user_level` DECIMAL(3,1), `p_flag_aktif` INT(1))
BEGIN

  INSERT INTO tbl_user (
    username, user_password, NRP,
    nama, telp, hp,
    email, address, KodeCabang,
    status_online,berlaku,user_level,
    user_active)
  VALUES(
    p_username, p_user_password,p_NRP,
    p_nama,p_telp, p_hp,
    p_email,p_address, p_KodeCabang,
    p_status_online, p_berlaku, p_user_level,
    p_flag_aktif);

END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_user_ubah
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_ubah`(`p_user_id` INTEGER, `p_username` VARCHAR(50), `p_NRP` VARCHAR(20), `p_nama` VARCHAR(255), `p_telp` VARCHAR(20), `p_hp` VARCHAR(20), `p_email` VARCHAR(50), `p_address` VARCHAR(255), `p_KodeCabang` VARCHAR(20), `p_status_online` BIT, `p_berlaku` DATETIME, `p_user_level` DECIMAL(3,1), `p_flag_aktif` INT(1))
BEGIN

  UPDATE tbl_user SET
    username=p_username, NRP=p_NRP,
    nama=p_nama, telp=p_telp, hp=p_hp,
    email=p_email, address=p_address, KodeCabang=p_KodeCabang,
    status_online=p_status_online,berlaku=p_berlaku,user_level=p_user_level,
    user_active=p_flag_aktif
  WHERE user_id=p_user_id;    
END//
DELIMITER ;

-- Dumping structure for procedure tiketux_abimanyu.sp_user_update_pengumuman_baru
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_update_pengumuman_baru`()
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=jumlah_pengumuman_baru+1;
END//
DELIMITER ;

-- Dumping structure for table tiketux_abimanyu.tbl_ba_bop
CREATE TABLE IF NOT EXISTS `tbl_ba_bop` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeBA` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuTransaksi` datetime DEFAULT NULL,
  `JenisBiaya` int(2) DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `NoSPJ` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_ba_bop: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_ba_bop` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ba_bop` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_ba_check
CREATE TABLE IF NOT EXISTS `tbl_ba_check` (
  `NoBA` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `TglBA` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJurusan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahPenumpangCheck` int(11) DEFAULT NULL,
  `JumlahPaketCheck` int(11) DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IsCheck` int(1) DEFAULT NULL,
  `isCetakVoucherBBM` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_ba_check: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_ba_check` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ba_check` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_biaya_harian
CREATE TABLE IF NOT EXISTS `tbl_biaya_harian` (
  `IdBiayaHarian` int(5) NOT NULL AUTO_INCREMENT,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `TglTransaksi` date NOT NULL,
  `IdPetugas` int(5) NOT NULL,
  `NamaPetugas` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Penerima` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `JenisBiaya` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `Keterangan` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IdBiayaHarian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_biaya_harian: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_biaya_harian` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_biaya_harian` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_biaya_insentif_sopir
CREATE TABLE IF NOT EXISTS `tbl_biaya_insentif_sopir` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPenjadwalan` int(11) DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJadwal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuat` datetime DEFAULT NULL,
  `IdApprover` int(11) DEFAULT NULL,
  `NamaApprover` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuApprove` datetime DEFAULT NULL,
  `NominalInsentif` double DEFAULT NULL,
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IDX_1` (`TglBerangkat`) USING BTREE,
  KEY `IDX_2` (`IdJurusan`) USING BTREE,
  KEY `FK_5` (`IdPenjadwalan`) USING BTREE,
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `KodeKendaraan` (`KodeKendaraan`) USING BTREE,
  KEY `KodeSopir` (`KodeSopir`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_biaya_insentif_sopir: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_biaya_insentif_sopir` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_biaya_insentif_sopir` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_biaya_op
CREATE TABLE IF NOT EXISTS `tbl_biaya_op` (
  `IDBiayaOP` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkun` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagJenisBiaya` int(1) DEFAULT NULL,
  `TglTransaksi` date DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeCabang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCatat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisRelease` int(1) DEFAULT NULL,
  PRIMARY KEY (`IDBiayaOP`),
  KEY `FK_1` (`NoSPJ`) USING BTREE,
  KEY `FK_2` (`IdJurusan`) USING BTREE,
  KEY `FK_3` (`TglTransaksi`) USING BTREE,
  KEY `FK_4` (`KodeSopir`) USING BTREE,
  KEY `FK_5` (`NoPolisi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_biaya_op: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_biaya_op` DISABLE KEYS */;
INSERT INTO `tbl_biaya_op` (`IDBiayaOP`, `NoSPJ`, `KodeAkun`, `FlagJenisBiaya`, `TglTransaksi`, `NoPolisi`, `KodeSopir`, `Jumlah`, `Keterangan`, `IdPetugas`, `IdJurusan`, `KodeCabang`, `WaktuCatat`, `IdSetoran`, `JenisRelease`) VALUES
	(1, 'MNFSMG1708149908', '', 2, '2017-08-14', 'NST0001', '0002', 800000, '', 3, 25, 'BDG', '2017-08-14 01:13:58', NULL, 0),
	(2, 'MNFSMG1708142866', '', 2, '2017-08-14', 'NST0001', '0002', 800000, '', 3, 25, 'BDG', '2017-08-14 01:14:32', NULL, 0);
/*!40000 ALTER TABLE `tbl_biaya_op` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_deposit_log_topup
CREATE TABLE IF NOT EXISTS `tbl_deposit_log_topup` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `OTP` varchar(6) COLLATE latin1_general_ci NOT NULL COMMENT 'menyimpan kode ',
  `IsVerified` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'jika OTP benar, akan diverifikasi',
  `WaktuVerifikasi` datetime DEFAULT NULL,
  `PetugasTopUp` int(10) unsigned NOT NULL,
  `PetugasVerifikasi` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_deposit_log_topup: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_deposit_log_topup` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_deposit_log_topup` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_deposit_log_trx
CREATE TABLE IF NOT EXISTS `tbl_deposit_log_trx` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_deposit_log_trx: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_deposit_log_trx` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_deposit_log_trx` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_jenis_discount
CREATE TABLE IF NOT EXISTS `tbl_jenis_discount` (
  `IdDiscount` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeDiscount` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDiscount` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `JumlahDiscount` double NOT NULL,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `IsHargaTetap` int(1) DEFAULT '0' COMMENT 'jika diset=1 maka nilai harganya bukan diskon melainkan harga tetap sesuai dengan field jenisdiskon',
  `IsReturn` int(1) NOT NULL DEFAULT '0',
  `ListJurusan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdDiscount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_jenis_discount: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_jenis_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_jenis_discount` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_kasbon_sopir
CREATE TABLE IF NOT EXISTS `tbl_kasbon_sopir` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeAkun` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `TglTransaksi` date NOT NULL,
  `KodeSopir` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` double NOT NULL,
  `IdKasir` int(10) unsigned NOT NULL,
  `DiberikanDiCabang` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `Pembatal` int(10) unsigned NOT NULL,
  `WaktuBatal` datetime NOT NULL,
  `WaktuCatatTransaksi` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_kasbon_sopir: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_kasbon_sopir` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_kasbon_sopir` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_kendaraan_administratif
CREATE TABLE IF NOT EXISTS `tbl_kendaraan_administratif` (
  `id_kendaraan_administratif` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalAktif` date DEFAULT NULL,
  `StatusKepemilikan` int(1) DEFAULT NULL,
  `Keterangan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglValidSTNK` date DEFAULT NULL,
  `TglValidKIR` date DEFAULT NULL,
  `BiayaLeasing` double DEFAULT NULL,
  `NamaLeasing` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaAngsuran` double DEFAULT NULL,
  `AngsuranKe` int(2) DEFAULT NULL,
  `JumlahAngsuran` int(2) DEFAULT NULL,
  `JatuhTempoAngsuran` date DEFAULT NULL,
  PRIMARY KEY (`id_kendaraan_administratif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_kendaraan_administratif: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_kendaraan_administratif` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_kendaraan_administratif` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_cetakulang_voucher_bbm
CREATE TABLE IF NOT EXISTS `tbl_log_cetakulang_voucher_bbm` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahRupiah` double DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `UserApproval` int(11) DEFAULT NULL,
  `NamaUserApproval` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_cetakulang_voucher_bbm: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_cetakulang_voucher_bbm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_cetakulang_voucher_bbm` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_cetak_manifest
CREATE TABLE IF NOT EXISTS `tbl_log_cetak_manifest` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_cetak_manifest: ~7 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_cetak_manifest` DISABLE KEYS */;
INSERT INTO `tbl_log_cetak_manifest` (`Id`, `NoSPJ`, `KodeJadwal`, `TglBerangkat`, `JamBerangkat`, `JumlahKursiDisediakan`, `JumlahPenumpang`, `JumlahPaket`, `JumlahPaxPaket`, `NoPolisi`, `KodeDriver`, `Driver`, `TotalOmzet`, `TotalOmzetPaket`, `IsSubJadwal`, `WaktuCetak`, `UserCetak`, `NamaUserPencetak`, `CetakanKe`) VALUES
	(1, 'MNFPKL1702043241', 'PKL-TGL0000', '2017-02-04 00:00:00', '00:00', 5, 0, 0, NULL, 'KK001', 'K01', 'Budi', 0, 0, 0, '2017-02-04 20:03:12', 3, 'admin', 1),
	(2, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', 8, 3, 0, NULL, 'NST0001', '0002', 'DIDIK SURYANTORO', 630026, 0, 0, '2017-08-14 01:14:32', 3, 'admin', 1),
	(3, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', 8, 4, 0, NULL, 'NST0001', '0002', 'DIDIK SURYANTORO', 840026, 0, 0, '2017-08-14 01:36:55', 3, 'admin', 2),
	(4, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', 8, 4, 0, NULL, 'NST0001', '0002', 'DIDIK SURYANTORO', 840026, 0, 0, '2017-08-14 01:38:11', 3, 'admin', 3),
	(5, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', 8, 4, 0, NULL, 'NST0001', '0002', 'DIDIK SURYANTORO', 840026, 0, 0, '2017-08-14 01:54:46', 3, 'admin', 4),
	(6, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', NULL, 0, 1, 2, '', '', '', NULL, 0, 0, '2017-08-14 01:58:21', 3, 'admin', 5),
	(7, 'MNFSMG1708142866', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', NULL, 0, 2, 3, '', '', '', NULL, 10000, 0, '2017-08-14 02:01:30', 3, 'admin', 6);
/*!40000 ALTER TABLE `tbl_log_cetak_manifest` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_cetak_tiket
CREATE TABLE IF NOT EXISTS `tbl_log_cetak_tiket` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserPencetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `UserOtorisasi` int(11) DEFAULT NULL,
  `NamaUserOtorisasi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_cetak_tiket: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_cetak_tiket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_cetak_tiket` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_finpay
CREATE TABLE IF NOT EXISTS `tbl_log_finpay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `module` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `action` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `data` text COLLATE latin1_general_ci,
  `user_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_finpay: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_finpay` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_finpay` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_koreksi_disc
CREATE TABLE IF NOT EXISTS `tbl_log_koreksi_disc` (
  `IdLog` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabang` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) unsigned DEFAULT NULL,
  `HargaTiket` double DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `SubTotal` double DEFAULT NULL,
  `DiscountMula` double DEFAULT NULL,
  `DiscountBaru` double DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `PetugasPenjual` int(10) unsigned DEFAULT NULL,
  `CetakTiket` int(1) unsigned DEFAULT NULL,
  `PetugasCetakTiket` int(10) unsigned DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `JenisDiscountMula` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscountBaru` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) unsigned DEFAULT NULL,
  `FlagBatal` int(1) unsigned DEFAULT NULL,
  `JenisPenumpangMula` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpangBaru` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPengkoreksi` int(10) unsigned DEFAULT NULL,
  `PetugasOtorisasi` int(10) unsigned DEFAULT NULL,
  `WaktuKoreksi` datetime DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  PRIMARY KEY (`IdLog`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_koreksi_disc: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_koreksi_disc` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_koreksi_disc` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_mutasi
CREATE TABLE IF NOT EXISTS `tbl_log_mutasi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBookingSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatSebelumnya` date DEFAULT NULL,
  `JamBerangkatSebelumnya` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusanSebelumnya` int(11) DEFAULT NULL,
  `NomorKursiSebelumnya` int(2) DEFAULT NULL,
  `HargaTiketSebelumnya` double DEFAULT NULL,
  `KodeBookingMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatMutasi` date DEFAULT NULL,
  `JamBerangkatMutasi` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursiMutasi` int(2) DEFAULT NULL,
  `IdJurusanMutasi` int(11) DEFAULT NULL,
  `HargaTiketMutasi` double DEFAULT NULL,
  `IsCetakTiket` int(1) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `TotalDiskon` double DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserMutasi` int(11) DEFAULT NULL,
  `NamaUserMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KodeJadwalSebelumnya` (`KodeJadwalSebelumnya`) USING BTREE,
  KEY `FK2` (`KodeJadwalMutasi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_mutasi: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_mutasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_mutasi` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_sms
CREATE TABLE IF NOT EXISTS `tbl_log_sms` (
  `IdLogSms` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuKirim` datetime DEFAULT NULL,
  `JumlahPesan` int(2) DEFAULT NULL,
  `FlagTipePengiriman` int(2) DEFAULT NULL,
  `KodeReferensi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdLogSms`),
  KEY `IDX_WAKTUKIRIM` (`WaktuKirim`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_sms: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_sms` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_token_sms
CREATE TABLE IF NOT EXISTS `tbl_log_token_sms` (
  `IdLog` int(11) NOT NULL AUTO_INCREMENT,
  `WaktuCatat` datetime NOT NULL,
  `JumlahSisaToken` double NOT NULL DEFAULT '0',
  `JumlahSMSDikirim` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdLog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_token_sms: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_token_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_token_sms` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_log_user
CREATE TABLE IF NOT EXISTS `tbl_log_user` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `waktu_login` datetime DEFAULT NULL,
  `waktu_logout` datetime DEFAULT NULL,
  `user_ip` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_log_user: ~90 rows (approximately)
/*!40000 ALTER TABLE `tbl_log_user` DISABLE KEYS */;
INSERT INTO `tbl_log_user` (`id_log`, `user_id`, `waktu_login`, `waktu_logout`, `user_ip`) VALUES
	(1, 3, '2016-12-04 10:24:49', NULL, 'ca50d462'),
	(2, 3, '2016-12-05 15:02:52', NULL, '7660eec8'),
	(3, 3, '2016-12-05 20:10:45', '2016-12-05 23:44:03', 'b4fd1920'),
	(4, 3, '2016-12-06 16:17:30', NULL, '7660eec8'),
	(5, 3, '2016-12-06 16:32:08', NULL, '7660eec8'),
	(6, 3, '2016-12-06 17:06:00', NULL, '7660eec8'),
	(7, 3, '2016-12-06 17:10:28', NULL, '7660eec8'),
	(8, 3, '2016-12-06 17:15:40', NULL, '7660eec8'),
	(9, 3, '2016-12-06 17:16:58', NULL, '7660eec8'),
	(10, 3, '2016-12-07 15:48:22', NULL, 'de7c7e65'),
	(11, 3, '2016-12-07 15:49:56', '2016-12-07 16:00:19', 'de7c7e65'),
	(12, 3, '2016-12-07 16:00:25', NULL, 'de7c7e65'),
	(13, 3, '2016-12-08 14:25:49', NULL, 'de7c7e65'),
	(14, 3, '2016-12-10 15:47:03', NULL, 'b4fd1f8f'),
	(15, 3, '2016-12-13 08:41:26', '2016-12-13 09:22:55', 'b4fd1f8f'),
	(16, 3, '2016-12-13 13:02:43', NULL, 'b4fd1f8f'),
	(17, 3, '2016-12-13 14:34:02', NULL, 'ca50d586'),
	(18, 3, '2016-12-13 14:36:03', NULL, 'b4fd1f8f'),
	(19, 3, '2016-12-13 14:36:30', NULL, 'b4fd1f8f'),
	(20, 3, '2016-12-13 14:37:03', NULL, 'b4fd1f8f'),
	(21, 3, '2016-12-14 09:38:43', NULL, 'b4f60205'),
	(22, 3, '2016-12-15 17:24:22', NULL, 'b4f60205'),
	(23, 3, '2016-12-27 10:38:15', '2016-12-27 10:42:40', '2449231b'),
	(24, 3, '2017-01-04 11:12:02', NULL, '24491bcf'),
	(25, 3, '2017-01-04 15:32:05', NULL, '7da4f633'),
	(26, 3, '2017-01-05 12:50:35', NULL, '2447e687'),
	(27, 3, '2017-01-09 09:50:45', '2017-01-09 10:17:45', 'de7c7a7d'),
	(28, 3, '2017-01-16 08:21:28', NULL, '7da6d8c6'),
	(29, 3, '2017-01-16 17:29:50', NULL, '7da6d8c6'),
	(30, 3, '2017-01-17 14:41:27', NULL, '7da6d8c6'),
	(31, 3, '2017-01-19 07:59:43', NULL, '7da6d8c6'),
	(32, 3, '2017-01-19 11:37:27', NULL, '7da6d8c6'),
	(33, 3, '2017-01-20 14:00:45', NULL, '244f1bd3'),
	(34, 3, '2017-01-20 18:14:19', NULL, '78bc4865'),
	(35, 3, '2017-01-23 10:21:30', NULL, 'b4fd1d84'),
	(36, 3, '2017-01-24 11:24:11', NULL, 'b4fd1d84'),
	(37, 3, '2017-01-25 08:14:57', NULL, 'b4fd1d84'),
	(38, 3, '2017-01-26 10:45:05', NULL, 'b4f5a231'),
	(39, 3, '2017-01-30 12:37:19', NULL, 'b4fde98f'),
	(40, 3, '2017-02-02 08:40:44', NULL, '244fc0ba'),
	(41, 3, '2017-02-02 17:37:51', NULL, '70d79a4f'),
	(42, 3, '2017-02-02 17:38:30', NULL, '70d79912'),
	(43, 3, '2017-02-04 18:39:27', NULL, '78bc4844'),
	(44, 3, '2017-02-04 18:41:37', NULL, '78bc4844'),
	(45, 3, '2017-02-04 19:22:44', NULL, '70d7ac6d'),
	(46, 3, '2017-02-04 19:22:59', NULL, '70d79a01'),
	(47, 3, '2017-02-04 19:23:45', NULL, '70d7ac6d'),
	(48, 3, '2017-02-04 19:48:59', NULL, '70d79a01'),
	(49, 3, '2017-02-04 19:49:17', NULL, '70d7ac6d'),
	(50, 3, '2017-02-04 19:49:34', NULL, '70d7ac6d'),
	(51, 3, '2017-02-04 19:55:01', NULL, '70d79a01'),
	(52, 3, '2017-02-04 19:56:26', NULL, '70d7ac6d'),
	(53, 3, '2017-02-04 19:58:13', NULL, '70d7ac6d'),
	(54, 3, '2017-02-04 19:58:46', NULL, '70d79a01'),
	(55, 3, '2017-02-04 19:59:01', NULL, '70d7ac6d'),
	(56, 3, '2017-02-04 19:59:47', NULL, '70d79a01'),
	(57, 3, '2017-02-04 20:01:24', NULL, '78bc4844'),
	(58, 3, '2017-02-05 18:55:28', NULL, '24492353'),
	(59, 3, '2017-02-05 19:05:24', NULL, '24492353'),
	(60, 3, '2017-02-17 08:38:31', NULL, '7204520a'),
	(61, 3, '2017-02-17 15:15:07', NULL, 'b4f5779e'),
	(62, 3, '2017-02-17 15:15:42', '2017-02-17 15:17:24', 'b4f5779e'),
	(63, 3, '2017-02-17 15:17:31', NULL, 'b4f5779e'),
	(64, 3, '2017-02-20 13:21:02', NULL, '7da33031'),
	(65, 3, '2017-02-20 14:02:45', NULL, '7da33031'),
	(66, 3, '2017-02-20 17:12:32', NULL, '7da33031'),
	(67, 3, '2017-02-21 16:18:04', NULL, '7da33031'),
	(68, 3, '2017-02-22 13:27:29', NULL, '7da33031'),
	(69, 3, '2017-02-22 13:31:14', NULL, '7da33031'),
	(70, 3, '2017-03-10 13:09:28', NULL, '7204528e'),
	(71, 3, '2017-03-11 12:37:54', '2017-03-11 12:52:01', 'ca50d7c6'),
	(72, 3, '2017-03-16 03:41:47', NULL, 'de7c7a8c'),
	(73, 3, '2017-03-16 04:19:08', NULL, 'ca50d7e2'),
	(74, 3, '2017-03-16 09:13:24', NULL, 'de7c7a8c'),
	(75, 3, '2017-03-16 10:44:23', NULL, 'ca50d7e2'),
	(76, 3, '2017-03-16 10:59:39', '2017-03-16 11:23:32', 'de7c7a8c'),
	(77, 3, '2017-03-16 11:23:48', NULL, 'de7c7a8c'),
	(78, 3, '2017-03-17 10:38:21', NULL, '78bc5fb2'),
	(79, 3, '2017-03-20 08:48:06', NULL, '78bc0021'),
	(80, 3, '2017-04-10 15:37:35', NULL, 'b4f5a284'),
	(81, 3, '2017-05-09 11:37:09', NULL, 'a8ebc9a0'),
	(82, 3, '2017-07-12 10:13:52', NULL, '70d7f094'),
	(83, 3, '2017-07-13 19:05:50', NULL, '24480bd9'),
	(84, 3, '2017-08-11 17:59:06', NULL, '244faceb'),
	(85, 3, '2017-08-13 15:47:37', NULL, '7f000001'),
	(86, 3, '2017-08-13 18:46:16', '2017-08-13 18:46:18', '7f000001'),
	(87, 3, '2017-08-13 18:46:44', '2017-08-13 18:46:46', '7f000001'),
	(88, 3, '2017-08-13 18:49:40', '2017-08-13 18:52:54', '7f000001'),
	(89, 3, '2017-08-13 18:53:49', NULL, '7f000001'),
	(90, 3, '2017-08-13 20:53:54', NULL, '00000000'),
	(91, 3, '2017-08-13 22:01:17', NULL, '00000000'),
	(92, 3, '2017-08-13 22:03:03', NULL, '7f000001'),
	(93, 3, '2017-08-13 22:48:13', NULL, '7f000001'),
	(94, 3, '2017-08-14 00:06:29', '2017-08-14 02:05:47', '7f000001');
/*!40000 ALTER TABLE `tbl_log_user` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_mac_address
CREATE TABLE IF NOT EXISTS `tbl_mac_address` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MacAddress` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NamaKomputer` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `AddBy` int(10) unsigned NOT NULL,
  `AddByName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `WaktuTambah` datetime NOT NULL,
  `IsAktif` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_mac_address: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_mac_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mac_address` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_cabang
CREATE TABLE IF NOT EXISTS `tbl_md_cabang` (
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Fax` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAgen` int(1) DEFAULT NULL,
  `IsPusat` int(1) NOT NULL DEFAULT '0',
  `COAAR` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COACash` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COABank` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `SaldoPettyCash` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`KodeCabang`),
  KEY `TbMDCabang_index4992` (`Nama`,`Alamat`,`Kota`) USING BTREE,
  KEY `TbMDCabang_index4994` (`Telp`) USING BTREE,
  KEY `TbMDCabang_index4995` (`FlagAgen`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_cabang: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_cabang` DISABLE KEYS */;
INSERT INTO `tbl_md_cabang` (`KodeCabang`, `Nama`, `Alamat`, `Kota`, `Telp`, `Fax`, `FlagAgen`, `IsPusat`, `COAAR`, `COASalesTiket`, `COAMarkDownTiket`, `COASalesPaket`, `COAMarkDownPaket`, `COACash`, `COABank`, `SaldoPettyCash`) VALUES
	('BDG', 'Bandung', 'Mess Rumah kita , Jalan gumuruh gang maleer VII/95-133 bandung', 'BANDUNG', '082136197633', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('MLG', 'Malang', 'Jln. Rajakwesi 4A ( terusan Kawi ) malang, 586622', 'MALANG', '0341586621', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('PKL', 'Pekalongan', 'Jl. Dr. Cipto 58 Pekalongan', 'PEKALONGAN', '0285413837', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('SBY', 'Surabaya', '', 'SURABAYA', '081567633849', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('SMG', 'Semarang', 'Jl.Jendral Sudirman No.75 Semarang', 'SEMARANG', '02476430727', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('TGL', 'Tegal', 'Jl. Diponegoro 105 tegal', 'TEGAL', '08158700002', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tbl_md_cabang` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_harga_promo
CREATE TABLE IF NOT EXISTS `tbl_md_harga_promo` (
  `KodeHargaPromo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `HargaPromo` double NOT NULL,
  `FlagAktif` int(1) DEFAULT NULL,
  PRIMARY KEY (`KodeHargaPromo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_harga_promo: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_harga_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_md_harga_promo` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_jadwal
CREATE TABLE IF NOT EXISTS `tbl_md_jadwal` (
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeCabangAsal` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `FlagSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir1` double DEFAULT NULL,
  `BiayaSopir2` double DEFAULT NULL,
  `BiayaSopir3` double DEFAULT NULL,
  `IsBiayaSopirKumulatif` int(1) DEFAULT '0',
  `BiayaTol` double DEFAULT NULL,
  `BiayaParkir` double DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `IsBBMVoucher` int(1) DEFAULT '0',
  `Via` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsOnline` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`KodeJadwal`),
  KEY `TbMDJadwal_index5011` (`IdJurusan`,`JamBerangkat`,`FlagAktif`) USING BTREE,
  KEY `TbMDJadwal_index5013` (`IdJurusan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_jadwal: ~19 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_jadwal` DISABLE KEYS */;
INSERT INTO `tbl_md_jadwal` (`KodeJadwal`, `IdJurusan`, `KodeCabangAsal`, `KodeCabangTujuan`, `JamBerangkat`, `IdLayout`, `JumlahKursi`, `FlagSubJadwal`, `KodeJadwalUtama`, `BiayaSopir1`, `BiayaSopir2`, `BiayaSopir3`, `IsBiayaSopirKumulatif`, `BiayaTol`, `BiayaParkir`, `BiayaBBM`, `IsBBMVoucher`, `Via`, `FlagAktif`, `IsOnline`) VALUES
	('MLG-PKL1900', 27, 'C-MLG', 'C-PKL', '19:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('MLG-TGL1900', 28, 'C-MLG', 'C-TGL', '19:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('PKL-MLG1400', 18, 'C-PKL', 'C-MLG', '14:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('PKL-SBY1400', 19, 'C-PKL', 'C-SBY', '14:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('PKL-TGL0000', 17, 'C-PKL', 'C-BDG', '00:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('PKL-TGL1200', 16, 'C-PKL', 'C-TGL', '12:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-BDG1900', 25, 'C-SMG', 'C-BDG', '19:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-MLG1900', 26, 'C-SMG', 'C-MLG', '19:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-PKL0900', 15, 'C-SMG', 'C-PKL', '09:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-PKL1400', 15, 'C-SMG', 'C-PKL', '14:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-PKL1800', 15, 'C-SMG', 'C-PKL', '18:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-SBY1900', 29, 'C-SMG', 'C-SBY', '19:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-TGL0900', 20, 'C-SMG', 'C-TGL', '09:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('SMG-TGL1800', 20, 'C-SMG', 'C-TGL', '18:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('TGL-BDG0100', 21, 'C-TGL', 'C-BDG', '01:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('TGL-MLG1200', 24, 'C-TGL', 'C-MLG', '12:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('TGL-PKL0700', 22, 'C-TGL', 'C-PKL', '07:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('TGL-PKL1600', 22, 'C-TGL', 'C-PKL', '16:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
	('TGL-SBY1200', 23, 'C-TGL', 'C-SBY', '12:00', '8', 8, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0);
/*!40000 ALTER TABLE `tbl_md_jadwal` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_jurusan
CREATE TABLE IF NOT EXISTS `tbl_md_jurusan` (
  `IdJurusan` int(11) NOT NULL AUTO_INCREMENT,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `HargaTiket` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaTiketTuslah` double DEFAULT NULL,
  `FlagTiketTuslah` int(1) DEFAULT '0',
  `KodeAkunPendapatanPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunPendapatanPaket` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunCharge` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunBiayaSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir` double DEFAULT '0',
  `KodeAkunBiayaTol` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaTol` double DEFAULT '0',
  `KodeAkunBiayaParkir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaParkir` double DEFAULT '0',
  `KodeAkunBiayaBBM` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `LiterBBM` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangSopir` double DEFAULT '0',
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT '0',
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT '0',
  `FlagAktif` int(1) DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `HargaPaket1KiloPertama` double DEFAULT '0',
  `HargaPaket1KiloBerikut` double DEFAULT '0',
  `HargaPaket2KiloPertama` double DEFAULT '0',
  `HargaPaket2KiloBerikut` double DEFAULT '0',
  `HargaPaket3KiloPertama` double DEFAULT '0',
  `HargaPaket3KiloBerikut` double DEFAULT '0',
  `HargaPaket4KiloPertama` double DEFAULT '0',
  `HargaPaket4KiloBerikut` double DEFAULT '0',
  `HargaPaket5KiloPertama` double DEFAULT '0',
  `HargaPaket5KiloBerikut` double DEFAULT '0',
  `HargaPaket6KiloPertama` double NOT NULL DEFAULT '0',
  `HargaPaket6KiloBerikut` double NOT NULL DEFAULT '0',
  `FlagOperasionalJurusan` int(1) unsigned DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter',
  `IsBiayaSopirKumulatif` int(1) unsigned DEFAULT '0',
  `IsVoucherBBM` int(1) NOT NULL DEFAULT '0' COMMENT '0=cash; 1=voucher BBM',
  `IsOnline` int(1) NOT NULL DEFAULT '0',
  `HargaPaketPerkoli` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdJurusan`),
  KEY `TbMDJurusan_index4998` (`BiayaSopir`) USING BTREE,
  KEY `TbMDJurusan_index4999` (`KodeJurusan`) USING BTREE,
  KEY `fk1mdjur` (`KodeCabangAsal`) USING BTREE,
  KEY `fk2mdjur` (`KodeCabangTujuan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_jurusan: ~15 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_jurusan` DISABLE KEYS */;
INSERT INTO `tbl_md_jurusan` (`IdJurusan`, `KodeJurusan`, `KodeCabangAsal`, `KodeCabangTujuan`, `HargaTiket`, `HargaTiketTuslah`, `FlagTiketTuslah`, `KodeAkunPendapatanPenumpang`, `KodeAkunPendapatanPaket`, `KodeAkunCharge`, `KodeAkunBiayaSopir`, `BiayaSopir`, `KodeAkunBiayaTol`, `BiayaTol`, `KodeAkunBiayaParkir`, `BiayaParkir`, `KodeAkunBiayaBBM`, `BiayaBBM`, `LiterBBM`, `KodeAkunKomisiPenumpangSopir`, `KomisiPenumpangSopir`, `KodeAkunKomisiPenumpangCSO`, `KomisiPenumpangCSO`, `KodeAkunKomisiPaketSopir`, `KomisiPaketSopir`, `KodeAkunKomisiPaketCSO`, `KomisiPaketCSO`, `FlagAktif`, `FlagLuarKota`, `HargaPaket1KiloPertama`, `HargaPaket1KiloBerikut`, `HargaPaket2KiloPertama`, `HargaPaket2KiloBerikut`, `HargaPaket3KiloPertama`, `HargaPaket3KiloBerikut`, `HargaPaket4KiloPertama`, `HargaPaket4KiloBerikut`, `HargaPaket5KiloPertama`, `HargaPaket5KiloBerikut`, `HargaPaket6KiloPertama`, `HargaPaket6KiloBerikut`, `FlagOperasionalJurusan`, `IsBiayaSopirKumulatif`, `IsVoucherBBM`, `IsOnline`, `HargaPaketPerkoli`) VALUES
	(15, 'SMG-PKL', 'SMG', 'PKL', '65000', 65000, 0, NULL, NULL, NULL, NULL, 235, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(16, 'PKL-TGL', 'PKL', 'TGL', '65000', 65000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 30000, 30000, 30000, 30000, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(17, 'PKL-BDG', 'PKL', 'BDG', '150000', 150000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(18, 'PKL-MLG', 'PKL', 'MLG', '215000', 215000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(19, 'PKL-SBY', 'PKL', 'SBY', '215000', 215000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(20, 'SMG-TGL', 'SMG', 'TGL', '90000', 90000, 0, NULL, NULL, NULL, NULL, 340000, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(21, 'TGL-BDG', 'TGL', 'BDG', '140000', 140000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(22, 'TGL-PKL', 'TGL', 'PKL', '65000', 65000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(23, 'TGL-SBY', 'TGL', 'SBY', '240000', 240000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(24, 'TGL-MLG', 'TGL', 'MLG', '240000', 240000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(25, 'SMG-BDG', 'SMG', 'BDG', '200000', 200000, 0, NULL, NULL, NULL, NULL, 800000, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 10000),
	(26, 'SMG-MLG', 'SMG', 'MLG', '150000', 150000, 0, NULL, NULL, NULL, NULL, 675000, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(27, 'MLG-PKL', 'MLG', 'PKL', '215000', 215000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(28, 'MLG-TGL', 'MLG', 'TGL', '215000', 215000, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0),
	(29, 'SMG-SBY', 'SMG', 'SBY', '150000', 150000, 0, NULL, NULL, NULL, NULL, 675, NULL, 0, NULL, 0, NULL, NULL, '5=;6=;7=;8=;9=;10=;12=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `tbl_md_jurusan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_kendaraan
CREATE TABLE IF NOT EXISTS `tbl_md_kendaraan` (
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jenis` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Merek` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Tahun` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Warna` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `KodeSopir1` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir2` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSTNK` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoBPKB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoRangka` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoMesin` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KilometerAkhir` double DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0',
  PRIMARY KEY (`KodeKendaraan`),
  KEY `TbMDKendaraan_index5022` (`NoPolisi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_kendaraan: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_kendaraan` DISABLE KEYS */;
INSERT INTO `tbl_md_kendaraan` (`KodeKendaraan`, `KodeCabang`, `NoPolisi`, `Jenis`, `Merek`, `Tahun`, `Warna`, `IdLayout`, `JumlahKursi`, `KodeSopir1`, `KodeSopir2`, `NoSTNK`, `NoBPKB`, `NoRangka`, `NoMesin`, `KilometerAkhir`, `Remark`, `FlagAktif`, `IsCargo`) VALUES
	('NSt0001', 'SBY', 'H8698zs', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Coklat muda mtl', '8', 8, '0006', '0006', '', 'G3766702I', 'MHMLOWY39AK004606', '4d56cf38086', 0, NULL, 1, 0),
	('Nst0002', 'BDG', 'H8704ZS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Biru muda mtl', '8', 8, '0003', '0003', '20', '', '', '', 0, NULL, 1, 0),
	('Nst0003', 'MLG', 'H9376KA', 'Mpnp / stasiun wAgon', 'Mitsubishi', '2003', '', '8', 8, '0006', '0006', '00043551', '45945581', 'MHML300DB3R230831', '4d56c343024', 0, NULL, 1, 0),
	('Nst0004', 'BDG', 'H1241EA', 'Mpnp / stasiun wAgon', 'Mitsubishi', '2008', 'Hijau metal kombinasi', '8', 8, '0005', '0005', '03913449', 'F2025158I', 'MHMLOWY398K002208', '4d56cD47602', 0, NULL, 1, 0),
	('Nst0005', 'MLG', 'H9718ZS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Biru muda mtl', '8', 8, '0006', '0006', '00275650', 'G3766707I', 'MHMLOWY39AK004608', '4d56cf38079', 0, NULL, 1, 0),
	('Nst0006', 'BDG', 'H1859GS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Biru muda mtl', '8', 8, '0007', '0007', '01202133', 'G3766853I', 'MHMLOWY39AK004611', '4d56cf38083', 0, NULL, 1, 0),
	('Nst0007', 'SBY', 'H8735ZS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Coklat muda mtl', '8', 8, '0002', '0002', '01093457', 'G37667051', 'MHMLOWY39AK004615', '4d56cf38081', 0, NULL, 1, 0),
	('Nst0008', 'MLG', 'H8729ZS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Biru muda mtl', '8', 8, '0006', '0006', '01373220', 'G3766703I', 'MHMLOWY39AK004617', '4d56cf48577', 0, NULL, 1, 0),
	('Nst0009', 'BDG', 'H1848GS', 'Mpnp / minibus', 'Mitsubishi', '2010', 'Coklat muda mtl', '8', 8, '0006', '0006', '01202134', 'G37668521', 'MHMLowy39ak004648', '4d56cf37720', 0, NULL, 1, 0);
/*!40000 ALTER TABLE `tbl_md_kendaraan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_kendaraan_layout
CREATE TABLE IF NOT EXISTS `tbl_md_kendaraan_layout` (
  `Id` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `LayoutBody` int(3) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_kendaraan_layout: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_kendaraan_layout` DISABLE KEYS */;
INSERT INTO `tbl_md_kendaraan_layout` (`Id`, `LayoutBody`) VALUES
	('10', 10),
	('12', 12),
	('14', 14),
	('5', 5),
	('6', 6),
	('7', 7),
	('8', 8),
	('9', 9);
/*!40000 ALTER TABLE `tbl_md_kendaraan_layout` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_kota
CREATE TABLE IF NOT EXISTS `tbl_md_kota` (
  `KodeKota` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`KodeKota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_kota: ~10 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_kota` DISABLE KEYS */;
INSERT INTO `tbl_md_kota` (`KodeKota`, `NamaKota`) VALUES
	('BDG', 'BANDUNG'),
	('BTG', 'BATANG'),
	('KDL', 'KENDAL'),
	('KWU', 'KALIWUNGU'),
	('MLG', 'MALANG'),
	('PKL', 'PEKALONGAN'),
	('SBY', 'SURABAYA'),
	('SMG', 'SEMARANG'),
	('TGL', 'TEGAL'),
	('WLR', 'WELERI');
/*!40000 ALTER TABLE `tbl_md_kota` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_libur
CREATE TABLE IF NOT EXISTS `tbl_md_libur` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TglLibur` date DEFAULT NULL,
  `KeteranganLibur` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `DiubahOleh` text COLLATE latin1_general_ci,
  `WaktuUbah` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`),
  KEY `IDX_1` (`TglLibur`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_libur: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_libur` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_md_libur` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_maskapai
CREATE TABLE IF NOT EXISTS `tbl_md_maskapai` (
  `KodeMaskapai` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `NamaMaskapai` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`KodeMaskapai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_maskapai: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_maskapai` DISABLE KEYS */;
INSERT INTO `tbl_md_maskapai` (`KodeMaskapai`, `NamaMaskapai`) VALUES
	('AA', 'Air Asia'),
	('CITI', 'CITILINK'),
	('GAR', 'Garuda Indonesia'),
	('LION', 'Lion Air'),
	('WING', 'WINGS AIR');
/*!40000 ALTER TABLE `tbl_md_maskapai` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_member
CREATE TABLE IF NOT EXISTS `tbl_md_member` (
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JenisKelamin` int(1) NOT NULL DEFAULT '0',
  `KategoriMember` int(1) DEFAULT NULL,
  `TempatLahir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `NoKTP` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglRegistrasi` date DEFAULT NULL,
  `Alamat` text COLLATE latin1_general_ci,
  `Kota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePos` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Handphone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `Pekerjaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Point` double DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuTransaksiTerakhir` datetime DEFAULT NULL,
  `IdKartu` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSeriKartu` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KataSandi` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) DEFAULT '1',
  `CabangDaftar` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `SaldoDeposit` double DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdMember`),
  KEY `TbMDMember_index5245` (`IdKartu`) USING BTREE,
  KEY `TbMDMember_index5246` (`NoSeriKartu`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_member: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_member` DISABLE KEYS */;
INSERT INTO `tbl_md_member` (`IdMember`, `Nama`, `JenisKelamin`, `KategoriMember`, `TempatLahir`, `TglLahir`, `NoKTP`, `TglRegistrasi`, `Alamat`, `Kota`, `KodePos`, `Telp`, `Handphone`, `Email`, `Pekerjaan`, `Point`, `ExpiredDate`, `WaktuTransaksiTerakhir`, `IdKartu`, `NoSeriKartu`, `KataSandi`, `FlagAktif`, `CabangDaftar`, `SaldoDeposit`, `Signature`) VALUES
	('0001', 'MUHAMAD BASIR MUFARID', 0, 1, 'SURABAYA', '1978-03-05', '334014123456', '2017-01-04', 'VILLA KALIJUDAN INDAH BLOK M3', 'SURABAYA', '56196', '085228484666', '081515155666', 'basirmufarid@gmail.com', 'driver', 0, '2018-01-04', NULL, '', '', '', 1, 'C-SBY', 0, NULL);
/*!40000 ALTER TABLE `tbl_md_member` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_paket_daftar_layanan
CREATE TABLE IF NOT EXISTS `tbl_md_paket_daftar_layanan` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeLayanan` char(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaLayanan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaKiloPertama` double DEFAULT NULL,
  `HargaKiloBerikutnya` double DEFAULT NULL,
  `FlagAktif` int(1) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_paket_daftar_layanan: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_paket_daftar_layanan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_md_paket_daftar_layanan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_plan_asuransi
CREATE TABLE IF NOT EXISTS `tbl_md_plan_asuransi` (
  `IdPlanAsuransi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NamaPlan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BesarPremi` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  PRIMARY KEY (`IdPlanAsuransi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_plan_asuransi: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_plan_asuransi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_md_plan_asuransi` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_promo
CREATE TABLE IF NOT EXISTS `tbl_md_promo` (
  `KodePromo` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `LevelPromo` int(2) NOT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `JumlahPenumpangMinimum` int(2) DEFAULT NULL,
  `JumlahPoint` double NOT NULL,
  `JumlahDiscount` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `FlagDiscount` int(1) NOT NULL DEFAULT '0',
  `FlagAktif` int(1) DEFAULT NULL,
  `FlagTargetPromo` tinyint(3) unsigned DEFAULT NULL,
  `JamMulai` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JamAkhir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPromo` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisMobil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BerlakuHari` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPromoPP` int(1) DEFAULT '0',
  PRIMARY KEY (`KodePromo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_promo: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_promo` DISABLE KEYS */;
INSERT INTO `tbl_md_promo` (`KodePromo`, `LevelPromo`, `KodeCabangAsal`, `KodeCabangTujuan`, `KodeJadwal`, `BerlakuMula`, `BerlakuAkhir`, `JumlahPenumpangMinimum`, `JumlahPoint`, `JumlahDiscount`, `FlagDiscount`, `FlagAktif`, `FlagTargetPromo`, `JamMulai`, `JamAkhir`, `NamaPromo`, `JenisMobil`, `NomorKursi`, `BerlakuHari`, `IsPromoPP`) VALUES
	('K01', 0, 'C-PKL', 'C-BDG', '', '2017-01-17', '2017-02-17', 150000, 0, '50%', 0, 1, 1, '10.00', '23.59', 'Promo Akhir Zaman', NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `tbl_md_promo` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_md_sopir
CREATE TABLE IF NOT EXISTS `tbl_md_sopir` (
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `HP` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSIM` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAktif` int(1) DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0',
  PRIMARY KEY (`KodeSopir`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_md_sopir: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_md_sopir` DISABLE KEYS */;
INSERT INTO `tbl_md_sopir` (`KodeSopir`, `Nama`, `HP`, `Alamat`, `NoSIM`, `FlagAktif`, `IsCargo`) VALUES
	('0001', 'HARYANTO', '085742877085', 'Wotgandul dalam 1a/43. Gabahan Semarang', '641114211288', 1, 0),
	('0002', 'DIDIK SURYANTORO', '085700237536', 'Jalan tanggul mas barat IX / c 379 rt 9 rw 10 pang', '740614210586', 1, 0),
	('0003', 'Moch Nurohim / Kabul', '085876517880', 'Desa protomulyo rt 4/ 09. Kaliwungu selatan kab ke', '751114330639', 1, 0),
	('0004', 'Rasmidjan ', '085865291005', 'Anjasmoro tengah I / 19 rt.01. ', '521214210701', 1, 0),
	('0005', 'Eko Temi Haryanto', '082133860109', 'Perum ambarawa Asri rt 06. Rw 12. ', '730914590231', 1, 0),
	('0006', 'Basir Mufarid', '081515155666', 'Kalijudan 8 - c/ 20. Rt 03/ 003', '700415144239', 1, 0),
	('0007', 'Kasir', '085220581235', 'Jalan piasa kulon 004/003 soma gedhe banyumas', '591013050709', 1, 0),
	('0008', 'Moch Dimhari', '081615475141', 'Ds. Jelu Ngasem rt /rw 16/04 Bojonegoro', '611215460077', 1, 0);
/*!40000 ALTER TABLE `tbl_md_sopir` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_member_deposit_topup_log
CREATE TABLE IF NOT EXISTS `tbl_member_deposit_topup_log` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `PetugasTopUp` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_member_deposit_topup_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_member_deposit_topup_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member_deposit_topup_log` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_member_deposit_trx_log
CREATE TABLE IF NOT EXISTS `tbl_member_deposit_trx_log` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` int(11) DEFAULT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_member_deposit_trx_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_member_deposit_trx_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member_deposit_trx_log` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_member_frekwensi_berangkat
CREATE TABLE IF NOT EXISTS `tbl_member_frekwensi_berangkat` (
  `IdFrekwensi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `FrekwensiBerangkat` double DEFAULT NULL,
  PRIMARY KEY (`IdFrekwensi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_member_frekwensi_berangkat: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_member_frekwensi_berangkat` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member_frekwensi_berangkat` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_member_mutasi_point
CREATE TABLE IF NOT EXISTS `tbl_member_mutasi_point` (
  `IdMutasiPoint` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Referensi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `JenisMutasi` int(1) DEFAULT NULL,
  `JumlahPoint` double DEFAULT NULL,
  `Operator` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`IdMutasiPoint`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_member_mutasi_point: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_member_mutasi_point` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member_mutasi_point` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_member_promo_point
CREATE TABLE IF NOT EXISTS `tbl_member_promo_point` (
  `KodeProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahPointTukar` double NOT NULL,
  `FlagAktif` int(1) NOT NULL,
  PRIMARY KEY (`KodeProduk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_member_promo_point: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_member_promo_point` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member_promo_point` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket
CREATE TABLE IF NOT EXISTS `tbl_paket` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NamaPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaket` double DEFAULT NULL,
  `Diskon` double NOT NULL DEFAULT '0',
  `TotalBayar` double NOT NULL DEFAULT '0',
  `Dimensi` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKoli` double DEFAULT NULL,
  `Berat` double DEFAULT NULL,
  `KeteranganPaket` text COLLATE latin1_general_ci,
  `InstruksiKhusus` text COLLATE latin1_general_ci,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenjual` int(11) DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT NULL,
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT NULL,
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `FlagBatal` int(1) DEFAULT '0',
  `JenisPembayaran` int(1) DEFAULT NULL,
  `CaraPembayaran` int(1) DEFAULT NULL,
  `CetakTiket` int(1) DEFAULT '0',
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `PetugasPembatalan` int(10) unsigned DEFAULT NULL,
  `StatusDiambil` int(1) DEFAULT '0',
  `NamaPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoKTPPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPengambilan` datetime DEFAULT NULL,
  `PetugasPemberi` int(10) unsigned DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(10) unsigned DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisLayanan` char(4) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMutasi` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuMutasi` text COLLATE latin1_general_ci NOT NULL,
  `UserPemutasi` text COLLATE latin1_general_ci NOT NULL,
  `IsSampai` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuSampai` datetime DEFAULT NULL,
  `UserCheckIn` int(11) DEFAULT NULL,
  `KodePelanggan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` date DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `IsCheck` int(1) DEFAULT '0',
  `LogMutasi` text COLLATE latin1_general_ci,
  `IsVolumetrik` int(10) DEFAULT NULL,
  `Panjang` int(10) DEFAULT NULL,
  `Lebar` int(10) DEFAULT NULL,
  `Tinggi` int(10) DEFAULT NULL,
  PRIMARY KEY (`NoTiket`),
  KEY `tbl_paket_index5692` (`TelpPengirim`,`AlamatPenerima`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket` DISABLE KEYS */;
INSERT INTO `tbl_paket` (`NoTiket`, `KodeCabang`, `KodeJadwal`, `IdJurusan`, `KodeKendaraan`, `KodeSopir`, `TglBerangkat`, `JamBerangkat`, `NamaPengirim`, `AlamatPengirim`, `TelpPengirim`, `WaktuPesan`, `NamaPenerima`, `AlamatPenerima`, `TelpPenerima`, `HargaPaket`, `Diskon`, `TotalBayar`, `Dimensi`, `JumlahKoli`, `Berat`, `KeteranganPaket`, `InstruksiKhusus`, `KodeAkunPendapatan`, `PetugasPenjual`, `KomisiPaketSopir`, `KodeAkunKomisiPaketSopir`, `KomisiPaketCSO`, `KodeAkunKomisiPaketCSO`, `NoSPJ`, `TglCetakSPJ`, `CetakSPJ`, `FlagBatal`, `JenisPembayaran`, `CaraPembayaran`, `CetakTiket`, `WaktuCetakTiket`, `WaktuPembatalan`, `PetugasPembatalan`, `StatusDiambil`, `NamaPengambil`, `NoKTPPengambil`, `TelpPengambil`, `WaktuPengambilan`, `PetugasPemberi`, `WaktuSetor`, `PenerimaSetoran`, `IdSetoran`, `IdRekonData`, `JenisBarang`, `JenisLayanan`, `FlagMutasi`, `WaktuMutasi`, `UserPemutasi`, `IsSampai`, `WaktuSampai`, `UserCheckIn`, `KodePelanggan`, `WaktuCheck`, `IdChecker`, `IsCheck`, `LogMutasi`, `IsVolumetrik`, `Panjang`, `Lebar`, `Tinggi`) VALUES
	('PHA12K1448T1', 'C-BDG', 'PKL-TGL1200', 16, '', '', '2017-02-04', '12:00', 'ratno', 'jl. cipedes tengah', '0817201101', '2017-02-04 20:08:55', 'test', 'jl. semarang', '081234567879', 60000, 0, 60000, '1', 1, 2, 'dokumen', '', '', 3, 0, '', 0, '', '', '0000-00-00 00:00:00', 0, 0, 0, 0, 1, '2017-02-04 20:09:03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'P2P', 0, '', '', 0, NULL, NULL, '', NULL, NULL, 0, NULL, 0, 0, 0, 0),
	('PHF18Z1EE0W1', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'kasdja', 'kajsdksaj', '09989012', '2017-08-14 02:00:58', 'jkahjsadh', 'kjhasjkdasd', '098981919', 10000, 0, 10000, '3', 1, NULL, 'fragile', '', '', 3, 0, '', 0, '', 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 0, 0, 0, 1, '2017-08-14 02:01:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'P2P', 0, '', '', 0, NULL, NULL, '', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL),
	('PHT8OEEV1N', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'Gara', 'Bandung', '08986833510', '2017-08-14 01:57:23', 'Nadya', 'Bandung', '0891231991', 0, 0, 0, '1', 2, NULL, 'qweqw', '', '', 3, 0, '', 0, '', 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 0, 0, 0, 1, '2017-08-14 01:57:25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'P2D', 0, '', '', 0, NULL, NULL, '', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tbl_paket` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket_cargo
CREATE TABLE IF NOT EXISTS `tbl_paket_cargo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoAWB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Asal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Tujuan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(3) COLLATE latin1_general_ci DEFAULT 'PRC' COMMENT 'PRC=PARCEL; DOK=DOKUMEN',
  `Via` varchar(3) COLLATE latin1_general_ci DEFAULT 'DRT' COMMENT 'DRT=Darat; UDR=Udara; LUT=Laut',
  `Layanan` varchar(3) COLLATE latin1_general_ci DEFAULT 'REG' COMMENT 'URG="Urgent"; REG="Reguler"',
  `Koli` int(2) DEFAULT '0' COMMENT 'Pax',
  `IsVolumetric` int(1) DEFAULT '0' COMMENT '0=perhitungan berdasarkan berat aktual; 1= perhitungan berdasarkan berat voume metric',
  `DimensiPanjang` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `DimensiLebar` double DEFAULT '0' COMMENT 'satuan dalam centimeter',
  `DimensiTinggi` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `Berat` double DEFAULT '0' COMMENT 'dalam kilogram',
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT '' COMMENT 'string',
  `BiayaKirim` double DEFAULT '0',
  `BiayaTambahan` double DEFAULT '0',
  `BiayaPacking` double DEFAULT '0',
  `IsAsuransi` int(1) DEFAULT '0' COMMENT '0=tidak pake asuransi; 1=pake asuransi (besarnya asuransi adalah 2 permil dari harga pengakuan)',
  `HargaDiakui` double DEFAULT '0' COMMENT 'harga yang diakui pelanggan utk menghitung asuransi',
  `BiayaAsuransi` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0',
  `IsTunai` int(1) DEFAULT '1' COMMENT '1=Tunai;  0=kredit',
  `DeskripsiBarang` text COLLATE latin1_general_ci,
  `WaktuCatat` datetime DEFAULT NULL,
  `DicatatOleh` int(11) DEFAULT NULL,
  `NamaPencatat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakAWB` datetime DEFAULT NULL,
  `DicetakOleh` int(11) DEFAULT NULL,
  `NamaPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPickedUp` int(1) DEFAULT '0' COMMENT '0=belum di pickup; 1=sudah di pickup',
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenerima` int(11) DEFAULT NULL,
  `NamaPetugasPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `PetugasBatal` int(11) DEFAULT NULL,
  `NamaPetugasBatal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL,
  `IsMutasi` int(1) DEFAULT '0',
  `TglDiterimaLama` date DEFAULT NULL,
  `PoinPickedUpLama` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUpLama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `PetugasMutasi` int(11) DEFAULT NULL,
  `NamaPetugasMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogMutasi` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket_cargo: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket_cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket_cargo` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket_cargo_md_harga
CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_harga` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPerKg` double DEFAULT NULL,
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket_cargo_md_harga: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket_cargo_md_harga` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket_cargo_md_harga` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket_cargo_md_kota
CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_kota` (
  `KodeKota` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeProvinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`KodeKota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket_cargo_md_kota: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket_cargo_md_kota` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket_cargo_md_kota` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket_cargo_md_provinsi
CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_provinsi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NamaProvinsi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket_cargo_md_provinsi: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket_cargo_md_provinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket_cargo_md_provinsi` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_paket_cargo_spj
CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_spj` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `PetugasCetak` int(11) DEFAULT NULL,
  `NamaPetugasCetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BodyUnit` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTransaksi` int(3) DEFAULT NULL,
  `JumlahKoli` int(3) DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalBerat` double DEFAULT NULL,
  `PetugasPickedUp` int(11) DEFAULT NULL,
  `NamaPetugasPickedUp` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogCetak` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_paket_cargo_spj: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket_cargo_spj` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket_cargo_spj` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_pelanggan
CREATE TABLE IF NOT EXISTS `tbl_pelanggan` (
  `IdPelanggan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoHP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTelp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglPertamaTransaksi` date DEFAULT NULL,
  `TglTerakhirTransaksi` date DEFAULT NULL,
  `CSOTerakhir` int(10) unsigned DEFAULT NULL,
  `KodeJurusanTerakhir` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `FrekwensiPergi` double DEFAULT '0',
  `FlagMember` int(1) DEFAULT '0',
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdPelanggan`),
  KEY `IDX_01` (`NoHP`) USING BTREE,
  KEY `IDX_02` (`NoTelp`) USING BTREE,
  KEY `IDX_03` (`FrekwensiPergi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_pelanggan: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_pelanggan` DISABLE KEYS */;
INSERT INTO `tbl_pelanggan` (`IdPelanggan`, `NoHP`, `NoTelp`, `Nama`, `Alamat`, `TglPertamaTransaksi`, `TglTerakhirTransaksi`, `CSOTerakhir`, `KodeJurusanTerakhir`, `FrekwensiPergi`, `FlagMember`, `IdMember`) VALUES
	(1, '0817201101', '0817201101', 'ratno', 'Jl. Jemput', '2017-02-22', '2017-04-10', 3, 'SMG-BDG190', 2, 0, NULL),
	(2, '08986833510', '08986833510', 'Gara', '', '2017-07-13', '2017-08-14', 3, 'SMG-BDG190', 4, 0, NULL);
/*!40000 ALTER TABLE `tbl_pelanggan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_pengaturan_parameter
CREATE TABLE IF NOT EXISTS `tbl_pengaturan_parameter` (
  `NamaParameter` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `NilaiParameter` text COLLATE latin1_general_ci NOT NULL,
  `Deskripsi` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`NamaParameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_pengaturan_parameter: ~18 rows (approximately)
/*!40000 ALTER TABLE `tbl_pengaturan_parameter` DISABLE KEYS */;
INSERT INTO `tbl_pengaturan_parameter` (`NamaParameter`, `NilaiParameter`, `Deskripsi`) VALUES
	('BBM_SOLAR', '5150', 'harga solar per liter'),
	('BBM_SPBU_57', 'SBPU 57', ''),
	('BBM_SPBU_BDO', 'SBPU BDO', ''),
	('DEPOSIT_MIN', '2000000', 'saldo minimum yang harus tersimpan'),
	('DEPOSIT_SALDO', '9550000', 'Saldo Deposit Tiketux'),
	('FEE_TIKET', '1500', 'Fee tiket pertama & kedua'),
	('KOMISITTX1', '10000', 'Besar Komisi untuk tiketux'),
	('PERUSH_ALAMAT', 'alamat', 'Alamat perusahaan'),
	('PERUSH_EMAIL', 'email', 'email perusahaan'),
	('PERUSH_NAMA', 'NAHWA TRAVEL', 'nama perusahaan penyedia jasa transportasi'),
	('PERUSH_TELP', '022', 'telp perusahaan'),
	('PERUSH_WEB', 'web', 'web perusahaan'),
	('PESAN_DITIKET', '', 'Menampilkan pesan di tiket'),
	('POLIS_ASURANSI', '1233232323', 'nomor polis induk asuransi'),
	('TGL_AKHIR_TUSLAH1', '2013-08-12 ', 'Tanggal Akhir Tuslah Luar Kota'),
	('TGL_AKHIR_TUSLAH2', '2013-08-11 ', 'tgl berakhirnya tuslah dalam kota'),
	('TGL_MULAI_TUSLAH1', '2013-08-02 ', 'Tanggal Mulai Tuslah Luar Kota'),
	('TGL_MULAI_TUSLAH2', '2013-08-05 ', 'tgl mulai tuslah dalam kota');
/*!40000 ALTER TABLE `tbl_pengaturan_parameter` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_pengaturan_umum
CREATE TABLE IF NOT EXISTS `tbl_pengaturan_umum` (
  `IdPengaturanUmum` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PesanSambutan` text COLLATE latin1_general_ci,
  `PesanDiTiket` text COLLATE latin1_general_ci,
  `FeeTiket` double DEFAULT NULL,
  `FeePaket` double DEFAULT NULL,
  `FeeSMS` double DEFAULT NULL,
  `NamaPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `EmailPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WebSitePerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaketMinimum` double DEFAULT NULL,
  `TglMulaiTuslah` date DEFAULT NULL,
  `TglAkhirTuslah` date DEFAULT NULL,
  `FlagBlokir` int(1) NOT NULL DEFAULT '0',
  `PemblokirUnblokir` int(11) NOT NULL,
  `WaktuBlokirUnblokir` datetime NOT NULL,
  PRIMARY KEY (`IdPengaturanUmum`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_pengaturan_umum: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_pengaturan_umum` DISABLE KEYS */;
INSERT INTO `tbl_pengaturan_umum` (`IdPengaturanUmum`, `PesanSambutan`, `PesanDiTiket`, `FeeTiket`, `FeePaket`, `FeeSMS`, `NamaPerusahaan`, `AlamatPerusahaan`, `TelpPerusahaan`, `EmailPerusahaan`, `WebSitePerusahaan`, `HargaPaketMinimum`, `TglMulaiTuslah`, `TglAkhirTuslah`, `FlagBlokir`, `PemblokirUnblokir`, `WaktuBlokirUnblokir`) VALUES
	(1, '', 'T001', 5000, 0, 100, 'ZENA TRAVEL', 'Komplek Taman Bumi Prima', '081233333333', 'info@tiketuxx.com', 'www.tiketux.com', 10000, '2011-08-23', '2011-08-31', 0, 25, '2013-09-03 15:04:42');
/*!40000 ALTER TABLE `tbl_pengaturan_umum` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_pengumuman
CREATE TABLE IF NOT EXISTS `tbl_pengumuman` (
  `IdPengumuman` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodePengumuman` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JudulPengumuman` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Pengumuman` text COLLATE latin1_general_ci,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PembuatPengumuman` int(10) unsigned DEFAULT NULL,
  `WaktuPembuatanPengumuman` datetime DEFAULT NULL,
  PRIMARY KEY (`IdPengumuman`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_pengumuman: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_pengumuman` DISABLE KEYS */;
INSERT INTO `tbl_pengumuman` (`IdPengumuman`, `KodePengumuman`, `JudulPengumuman`, `Pengumuman`, `Kota`, `PembuatPengumuman`, `WaktuPembuatanPengumuman`) VALUES
	(1, 'TES', 'TES JADWAL', 'TES JADWAL', '', 6, '2016-05-30 00:00:00'),
	(3, 'P01', 'Pengumuman Operasional', 'Pengumuman Operasional', '', 1, '2016-06-01 00:00:00'),
	(4, '001', 'perubahan jam keberangkatan', 'Jadwal di plot per 1jam sekali\r\nMulai dari 05.00 06.00 07.00 08.00 09.00 10.00\r\n14.00 15.00 16.00 17.00 18.00 19.00\r\nTotal 12 trip.', '', 9, '2016-06-10 00:00:00');
/*!40000 ALTER TABLE `tbl_pengumuman` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_penjadwalan_kendaraan
CREATE TABLE IF NOT EXISTS `tbl_penjadwalan_kendaraan` (
  `IdPenjadwalan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `LayoutKursi` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) DEFAULT NULL,
  `KodeDriver` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusAktif` int(1) DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IdPenjadwalan`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_penjadwalan_kendaraan: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_penjadwalan_kendaraan` DISABLE KEYS */;
INSERT INTO `tbl_penjadwalan_kendaraan` (`IdPenjadwalan`, `TglBerangkat`, `JamBerangkat`, `KodeJadwal`, `IdJurusan`, `KodeKendaraan`, `NoPolisi`, `LayoutKursi`, `JumlahKursi`, `KodeDriver`, `NamaDriver`, `StatusAktif`, `Remark`) VALUES
	(1, '2017-02-04', '00:00:00', 'PKL-TGL0000', 17, 'KK001', 'D6081H', '12', 0, 'K01', 'Budi', 1, '');
/*!40000 ALTER TABLE `tbl_penjadwalan_kendaraan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_penjadwalan_kendaraan_ba_penutupan
CREATE TABLE IF NOT EXISTS `tbl_penjadwalan_kendaraan_ba_penutupan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `Remark` text COLLATE latin1_general_ci,
  `PetugasPenutup` int(11) DEFAULT NULL,
  `NamaPetugasPenutup` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPenutupan` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FX_1` (`TglBerangkat`) USING BTREE,
  KEY `FX_2` (`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `FX_3` (`IdJurusan`) USING BTREE,
  KEY `FX_4` (`PetugasPenutup`) USING BTREE,
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_penjadwalan_kendaraan_ba_penutupan: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_penjadwalan_kendaraan_ba_penutupan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_penjadwalan_kendaraan_ba_penutupan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_posisi
CREATE TABLE IF NOT EXISTS `tbl_posisi` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `TglBerangkat` date NOT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahKursi` int(2) NOT NULL DEFAULT '0',
  `SisaKursi` int(2) NOT NULL DEFAULT '0',
  `TglCetakSPJ` datetime DEFAULT NULL,
  `PetugasCetakSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Memo` text COLLATE latin1_general_ci,
  `PembuatMemo` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuatMemo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMemo` int(1) DEFAULT '0',
  `FlagOperasionalJurusan` int(1) DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter',
  PRIMARY KEY (`ID`),
  KEY `TbPosisi_index5116` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbPosisi_index5117` (`NoSPJ`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_posisi: ~39 rows (approximately)
/*!40000 ALTER TABLE `tbl_posisi` DISABLE KEYS */;
INSERT INTO `tbl_posisi` (`ID`, `NoSPJ`, `KodeJadwal`, `IdJurusan`, `TglBerangkat`, `JamBerangkat`, `KodeKendaraan`, `KodeSopir`, `JumlahKursi`, `SisaKursi`, `TglCetakSPJ`, `PetugasCetakSPJ`, `Memo`, `PembuatMemo`, `WaktuBuatMemo`, `FlagMemo`, `FlagOperasionalJurusan`) VALUES
	(1, NULL, 'BTU-JND0200', NULL, '2016-12-07', '02:00:00', '', '', 12, 12, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(2, NULL, 'BTU-JND1600', NULL, '2016-12-06', '16:00:00', '', '', 12, 12, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(3, NULL, 'PKL-TGL0000', NULL, '2017-01-05', '00:00:00', '', '', 5, 5, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(4, NULL, 'PKL-MLG1400', NULL, '2017-01-05', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(5, NULL, 'PKL-SBY1400', NULL, '2017-01-05', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(6, NULL, 'SMG-BDG1900', NULL, '2017-01-05', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(7, NULL, 'SMG-TGL0900', NULL, '2017-01-05', '09:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(8, NULL, 'TGL-BDG0100', NULL, '2017-01-05', '01:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(9, NULL, 'MLG-TGL1900', NULL, '2017-01-16', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(10, NULL, 'PKL-TGL0000', NULL, '2017-01-16', '00:00:00', '', '', 5, 5, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(11, NULL, 'PKL-MLG1400', NULL, '2017-01-16', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(12, NULL, 'PKL-SBY1400', NULL, '2017-01-16', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(13, NULL, 'PKL-TGL1200', NULL, '2017-01-16', '12:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(14, NULL, 'SMG-BDG1900', NULL, '2017-01-16', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(15, NULL, 'SMG-MLG1900', NULL, '2017-01-16', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(16, NULL, 'SMG-PKL1400', NULL, '2017-01-16', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(17, NULL, 'SMG-PKL0900', NULL, '2017-01-16', '09:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(18, NULL, 'SMG-PKL1800', NULL, '2017-01-16', '18:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(19, NULL, 'TGL-BDG0100', NULL, '2017-01-16', '01:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(20, NULL, 'PKL-TGL0000', NULL, '2017-01-17', '00:00:00', '', '', 5, 5, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(21, NULL, 'PKL-MLG1400', NULL, '2017-01-17', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(22, NULL, 'PKL-MLG1400', NULL, '2017-02-02', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(23, NULL, 'PKL-SBY1400', NULL, '2017-02-02', '14:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(24, NULL, 'MLG-PKL1900', NULL, '2017-02-02', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(25, NULL, 'MLG-TGL1900', NULL, '2017-02-02', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(26, NULL, 'SMG-PKL0900', NULL, '2017-02-04', '09:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(27, 'MNFPKL1702043241', 'PKL-TGL0000', NULL, '2017-02-04', '00:00:00', 'KK001', 'K01', 5, 5, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(28, NULL, 'PKL-TGL1200', NULL, '2017-02-04', '12:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(29, NULL, 'SMG-BDG1900', NULL, '2017-02-17', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(30, NULL, 'SMG-BDG1900', NULL, '2017-02-20', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(31, NULL, 'SMG-BDG1900', NULL, '2017-02-21', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(32, NULL, 'SMG-BDG1900', NULL, '2017-02-22', '19:00:00', '', '', 8, 7, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(33, NULL, 'SMG-MLG1900', NULL, '2017-03-16', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(34, NULL, 'SMG-PKL0900', NULL, '2017-03-16', '09:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(35, NULL, 'SMG-BDG1900', NULL, '2017-04-10', '19:00:00', '', '', 8, 7, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(36, NULL, 'SMG-BDG1900', NULL, '2017-07-13', '19:00:00', '', '', 8, 7, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(37, NULL, 'SMG-TGL0900', NULL, '2017-08-11', '09:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(38, NULL, 'SMG-BDG1900', NULL, '2017-08-13', '19:00:00', '', '', 8, 8, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(39, 'MNFSMG1708142866', 'SMG-BDG1900', NULL, '2017-08-14', '19:00:00', 'NST0001', '0002', 8, 5, NULL, NULL, NULL, NULL, NULL, 0, 0);
/*!40000 ALTER TABLE `tbl_posisi` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_posisi_backup
CREATE TABLE IF NOT EXISTS `tbl_posisi_backup` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date NOT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL DEFAULT '0',
  `SisaKursi` int(2) NOT NULL DEFAULT '0',
  `TglCetakSPJ` datetime DEFAULT NULL,
  `PetugasCetakSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Memo` text COLLATE latin1_general_ci,
  `PembuatMemo` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuatMemo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMemo` int(1) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `TbPosisi_index5116` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbPosisi_index5117` (`NoSPJ`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_posisi_backup: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_posisi_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_posisi_backup` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_posisi_detail
CREATE TABLE IF NOT EXISTS `tbl_posisi_detail` (
  `NomorKursi` int(2) NOT NULL,
  `ID` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `JamBerangkat` time DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date NOT NULL DEFAULT '0000-00-00',
  `StatusKursi` int(1) DEFAULT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Session` int(10) DEFAULT NULL,
  `StatusBayar` int(1) DEFAULT '0',
  `SessionTime` datetime DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NomorKursi`,`KodeJadwal`,`TglBerangkat`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `idx2` (`KodeJadwal`,`TglBerangkat`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_posisi_detail: ~17 rows (approximately)
/*!40000 ALTER TABLE `tbl_posisi_detail` DISABLE KEYS */;
INSERT INTO `tbl_posisi_detail` (`NomorKursi`, `ID`, `KodeJadwal`, `JamBerangkat`, `IsSubJadwal`, `KodeJadwalUtama`, `KodeCabangAsal`, `KodeCabangTujuan`, `TglBerangkat`, `StatusKursi`, `NoTiket`, `Nama`, `Session`, `StatusBayar`, `SessionTime`, `KodeBooking`) VALUES
	(1, NULL, 'BTU-JND0200', '02:00:00', 0, 'BTU-JND0200', 'BTU', 'JND', '2016-12-07', 1, NULL, NULL, 3, 0, '2016-12-04 11:45:27', NULL),
	(1, NULL, 'BTU-JND1600', '16:00:00', 0, 'BTU-JND1600', 'BTU', 'JND', '2016-12-06', 1, NULL, NULL, 3, 0, '2016-12-04 12:06:47', NULL),
	(1, NULL, 'PKL-TGL0000', '00:00:00', 0, 'PKL-TGL0000', 'C-PKL', 'C-BDG', '2017-01-16', 1, NULL, NULL, 3, 0, '2017-01-16 17:38:47', NULL),
	(1, NULL, 'PKL-TGL0000', '00:00:00', 0, 'PKL-TGL0000', 'C-PKL', 'C-BDG', '2017-01-17', 0, NULL, NULL, 3, 0, '2017-01-17 15:36:30', NULL),
	(1, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-07-13', 1, 'TH7DD7ZF1', 'Gara', NULL, 1, '2017-07-13 19:07:25', 'BHP17CDD7Z'),
	(1, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-08-14', 1, 'TH8EEWUFK12', 'Gara', NULL, 1, '2017-08-14 00:26:29', 'BH78NEEWU'),
	(1, NULL, 'SMG-TGL0900', '09:00:00', 0, 'SMG-TGL0900', 'C-SMG', 'C-TGL', '2017-08-11', 1, NULL, NULL, 3, 0, '2017-08-11 18:00:23', NULL),
	(2, NULL, 'PKL-TGL0000', '00:00:00', 0, 'PKL-TGL0000', 'C-PKL', 'C-BDG', '2017-01-16', 1, NULL, NULL, 3, 0, '2017-01-16 17:38:48', NULL),
	(2, NULL, 'PKL-TGL0000', '00:00:00', 0, 'PKL-TGL0000', 'C-PKL', 'C-BDG', '2017-01-17', 1, NULL, NULL, 3, 0, '2017-01-17 15:31:48', NULL),
	(2, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-08-14', 1, 'TH8EE6QLM1', 'Nadya', NULL, 1, '2017-08-14 01:04:02', 'BH389EE6Q'),
	(3, NULL, 'PKL-TGL0000', '00:00:00', 0, 'PKL-TGL0000', 'C-PKL', 'C-BDG', '2017-02-04', 1, NULL, NULL, 3, 0, '2017-02-04 20:01:44', NULL),
	(3, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-08-14', 0, NULL, NULL, NULL, 0, '2017-08-14 01:08:29', NULL),
	(5, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-02-21', 1, NULL, NULL, 3, 0, '2017-02-21 16:20:32', NULL),
	(5, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-02-22', 1, 'TH2MMUCCU1', 'ratno', NULL, 1, '2017-02-22 13:29:26', 'BHL12Z1MMUC'),
	(5, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-04-10', 1, 'TH4AAC17TF', 'ratno', NULL, 1, '2017-04-10 15:38:03', 'BHY4G1AAC17'),
	(5, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-08-14', 1, 'TH8EE9SRV', 'Nadya', NULL, 1, '2017-08-14 01:09:14', 'BHN18K1EE9S'),
	(7, NULL, 'SMG-BDG1900', '19:00:00', 0, 'SMG-BDG1900', 'C-SMG', 'C-BDG', '2017-08-14', 0, NULL, NULL, 3, 0, '2017-08-14 01:50:51', NULL);
/*!40000 ALTER TABLE `tbl_posisi_detail` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_posisi_detail_backup
CREATE TABLE IF NOT EXISTS `tbl_posisi_detail_backup` (
  `NomorKursi` int(2) NOT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `TglBerangkat` date NOT NULL,
  `ID` int(10) unsigned NOT NULL,
  `StatusKursi` int(1) DEFAULT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Session` int(10) unsigned DEFAULT NULL,
  `StatusBayar` int(1) DEFAULT '0',
  `SessionTime` datetime DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NomorKursi`,`KodeJadwal`,`TglBerangkat`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_posisi_detail_backup: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_posisi_detail_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_posisi_detail_backup` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_rekon_data
CREATE TABLE IF NOT EXISTS `tbl_rekon_data` (
  `IdRekonData` int(11) NOT NULL AUTO_INCREMENT,
  `TglRekonData` date DEFAULT NULL,
  `PetugasRekonData` int(10) unsigned DEFAULT NULL,
  `JumlahSPJ` double DEFAULT NULL,
  `JumlahPenumpang` double DEFAULT NULL,
  `JumlahTiketOnline` double DEFAULT NULL,
  `JumlahTiketBatal` double DEFAULT NULL,
  `JumlahFeeTiket` double DEFAULT NULL,
  `JumlahPaket` double DEFAULT NULL,
  `JumlahFeePaket` double DEFAULT NULL,
  `JumlahSMS` double DEFAULT NULL,
  `JumlahFeeSMS` double DEFAULT NULL,
  `TotalFee` double DEFAULT NULL,
  `TotalDiskonFee` double DEFAULT '0',
  `TotalBayarFee` double DEFAULT '0',
  `FlagDibayar` int(1) DEFAULT NULL,
  `WaktuCatatBayar` datetime DEFAULT NULL,
  `PetugasCatatBayar` int(10) unsigned DEFAULT NULL,
  `FeePerTiket` double NOT NULL,
  `FeePerPaket` double NOT NULL,
  `FeePerSMS` double DEFAULT NULL,
  PRIMARY KEY (`IdRekonData`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_rekon_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_rekon_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rekon_data` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_reservasi
CREATE TABLE IF NOT EXISTS `tbl_reservasi` (
  `NoTiket` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` text COLLATE latin1_general_ci,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCodeExpiredTime` datetime DEFAULT NULL,
  `Signature` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0',
  `IdDiscount` int(5) DEFAULT NULL,
  `area_antar` char(10) COLLATE latin1_general_ci DEFAULT NULL,
  `area_jemput` char(10) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatTujuan` text COLLATE latin1_general_ci,
  `AlamatJemput` text COLLATE latin1_general_ci,
  `BiayaJemput` int(11) DEFAULT NULL,
  `BiayaAntar` int(11) DEFAULT NULL,
  `Maskapai` char(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Terminal` char(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuLanding` char(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NoTiket`),
  KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_reservasi: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_reservasi` DISABLE KEYS */;
INSERT INTO `tbl_reservasi` (`NoTiket`, `KodeCabang`, `KodeJadwal`, `IdJurusan`, `KodeKendaraan`, `KodeSopir`, `TglBerangkat`, `JamBerangkat`, `KodeBooking`, `IdMember`, `PointMember`, `Nama`, `Telp`, `HP`, `Alamat`, `WaktuPesan`, `NomorKursi`, `HargaTiket`, `Charge`, `SubTotal`, `Discount`, `PPN`, `Total`, `PetugasPenjual`, `FlagPesanan`, `NoSPJ`, `TglCetakSPJ`, `CetakSPJ`, `CetakTiket`, `PetugasCetakTiket`, `WaktuCetakTiket`, `KomisiPenumpangCSO`, `FlagSetor`, `PetugasCetakSPJ`, `Keterangan`, `JenisDiscount`, `JenisPembayaran`, `WaktuSetor`, `PenerimaSetoran`, `IdSetoran`, `FlagRekonData`, `IdRekonData`, `FlagBatal`, `PetugasPembatalan`, `WaktuPembatalan`, `KodeAkunPendapatan`, `JenisPenumpang`, `KodeAkunKomisiPenumpangCSO`, `PaymentCode`, `PaymentCodeExpiredTime`, `Signature`, `WaktuMutasi`, `Pemutasi`, `MutasiDari`, `OTP`, `OTPUsed`, `IsSettlement`, `Komisi`, `HargaTiketux`, `IdDiscount`, `area_antar`, `area_jemput`, `AlamatTujuan`, `AlamatJemput`, `BiayaJemput`, `BiayaAntar`, `Maskapai`, `Terminal`, `WaktuLanding`) VALUES
	('TH2MMUCCU1', 'C-BDG', 'SMG-BDG1900', 25, '', '', '2017-02-22', '19:00', 'BHL12Z1MMUC', '', 0, 'ratno', '0817201101', '', 'Jl. Jemput', '2017-02-22 13:30:12', 5, 200000, 0, 200000, 0, 0, 222500, 3, 0, '', '0000-00-00 00:00:00', 0, 1, 3, '2017-02-22 13:30:14', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, 'A0001', 'A0001', 'Jl. Antar', 'Jl. Jemput', 12500, 10000, NULL, NULL, NULL),
	('TH4AAC17TF', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-04-10', '19:00', 'BHY4G1AAC17', '', 0, 'ratno', '0817201101', '', '', '2017-04-10 15:38:07', 5, 200000, 0, 200000, 0, 0, 260000, 3, 0, '', '0000-00-00 00:00:00', 0, 1, 3, '2017-04-10 15:39:21', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, 'A0026', 'A0130', '', 'Jl. Jemput', 40000, 20000, NULL, NULL, NULL),
	('TH7DD7ZF1', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-07-13', '19:00', 'BHP17CDD7Z', '', 0, 'Gara', '08986833510', '', '', '2017-07-13 19:07:35', 1, 200000, 0, 200000, 0, 0, 255000, 3, 0, '', '0000-00-00 00:00:00', 0, 1, 3, '2017-07-13 19:07:37', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, 'A0028', 'A0130', 'Cibaduyut', 'Semarang', 40000, 15000, NULL, NULL, NULL),
	('TH8EE6QLM1', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BH389EE6Q', '', 0, 'Nadya', '08986833510', '', '', '2017-08-14 01:06:26', 2, 200000, 0, 200000, 0, 0, 210000, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 1, 3, '2017-08-14 01:06:28', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, '', 'A0213', 'Rumah2', 'Bandara1', 10000, 0, 'Air Asia', '1', '19:00'),
	('TH8EE9SB7', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BHN18K1EE9S', '', 0, 'Nadya', '08986833510', '', '', '2017-08-14 01:09:28', 5, 200000, 0, 200000, 0, 0, 210013, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 1, 3, '2017-08-14 01:09:30', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 1, 3, '2017-08-14 01:55:27', '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, 'A0215', 'A0213', 'Cirb', 'Band', 10000, 13, 'CITILINK', '1', '19:00'),
	('TH8EE9SRV', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BHN18K1EE9S', '', 0, 'Nadya', '08986833510', '', '', '2017-08-14 01:09:28', 5, 200000, 0, 200000, 0, 0, 210013, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 1, 3, '2017-08-14 01:09:30', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, '2017-08-14 01:55:34', 3, 'SMG-BDG1900', NULL, 0, 1, 0, 0, NULL, 'A0215', 'A0213', 'Cirb', 'Band', 10000, 13, 'CITILINK', '1', '19:00'),
	('TH8EEWUFK', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BH78NEEWU', '', 0, 'Gara', '08986833510', '', '', '2017-08-14 00:47:53', 1, 200000, 0, 200000, 0, 0, 210000, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 0, NULL, NULL, 0, 0, 0, '', '', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, '', '10000', 'Rumah', 'Bandara', 0, 0, NULL, NULL, NULL),
	('TH8EEWUFK1', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BH78NEEWU', '', 0, 'Gara', '08986833510', '', '', '2017-08-14 00:54:43', 1, 200000, 0, 200000, 0, 0, 210000, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 0, NULL, NULL, 0, 0, 0, '', '', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, '', '10000', 'Rumah', 'Bandara', 0, 0, NULL, NULL, NULL),
	('TH8EEWUFK12', 'BDG', 'SMG-BDG1900', 25, '', '', '2017-08-14', '19:00', 'BH78NEEWU', '', 0, 'Gara', '08986833510', '', '', '2017-08-14 00:59:55', 1, 200000, 0, 200000, 0, 0, 210000, 3, 0, 'MNFSMG1708142866', '2017-08-14 02:01:30', 1, 1, 3, '2017-08-14 01:18:00', 0, 0, 0, '', '', 0, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, '', 'U', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, NULL, '', 'A0213', 'Rumah', 'Bandara', 10000, 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tbl_reservasi` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_reservasi_olap
CREATE TABLE IF NOT EXISTS `tbl_reservasi_olap` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`NoTiket`),
  KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_reservasi_olap: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_reservasi_olap` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reservasi_olap` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_sessions
CREATE TABLE IF NOT EXISTS `tbl_sessions` (
  `session_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_user_id` float NOT NULL,
  `session_start` float NOT NULL,
  `session_time` float NOT NULL,
  `session_ip` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_page` float NOT NULL,
  `session_logged_in` float NOT NULL,
  `session_admin` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_sessions: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_sessions` DISABLE KEYS */;
INSERT INTO `tbl_sessions` (`session_id`, `session_user_id`, `session_start`, `session_time`, `session_ip`, `session_page`, `session_logged_in`, `session_admin`) VALUES
	('a79d3f622c156f9da3269f50e7919376', -1, 1502650000, 1502650000, '7f000001', 901, 0, 0);
/*!40000 ALTER TABLE `tbl_sessions` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_spj
CREATE TABLE IF NOT EXISTS `tbl_spj` (
  `NoSPJ` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglSPJ` datetime DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CSO` int(10) unsigned DEFAULT NULL,
  `CSOPaket` int(10) DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `FlagAmbilBiayaOP` int(1) DEFAULT NULL,
  `TglTransaksi` datetime DEFAULT NULL,
  `CabangBayarOP` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `InsentifSopir` double DEFAULT '0',
  `IsEkspedisi` int(1) DEFAULT '0' COMMENT '0=travel; 1=paket',
  `IsCetakVoucherBBM` int(1) DEFAULT '0',
  `IsSubJadwal` int(1) DEFAULT '0' COMMENT '0=not verified; 1=verified',
  `IsCheck` int(1) DEFAULT '0',
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` datetime DEFAULT NULL,
  `JumlahPenumpangCheck` int(2) DEFAULT NULL,
  `PathFoto` text COLLATE latin1_general_ci,
  `JumlahPaketCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTambahanCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTanpaTiketCheck` int(2) DEFAULT '0',
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NoSPJ`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `IDX_2` (`NoPolisi`) USING BTREE,
  KEY `IDX_3` (`TglSPJ`) USING BTREE,
  KEY `IDX_4` (`TglSPJ`,`NoPolisi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_spj: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_spj` DISABLE KEYS */;
INSERT INTO `tbl_spj` (`NoSPJ`, `IdJurusan`, `TglSPJ`, `KodeJadwal`, `TglBerangkat`, `JamBerangkat`, `IdLayout`, `JumlahKursiDisediakan`, `JumlahPenumpang`, `JumlahPaket`, `JumlahPaxPaket`, `NoPolisi`, `KodeDriver`, `CSO`, `CSOPaket`, `Driver`, `TotalOmzet`, `TotalOmzetPaket`, `FlagAmbilBiayaOP`, `TglTransaksi`, `CabangBayarOP`, `InsentifSopir`, `IsEkspedisi`, `IsCetakVoucherBBM`, `IsSubJadwal`, `IsCheck`, `IdChecker`, `NamaChecker`, `WaktuCheck`, `JumlahPenumpangCheck`, `PathFoto`, `JumlahPaketCheck`, `JumlahPenumpangTambahanCheck`, `JumlahPenumpangTanpaTiketCheck`, `CatatanTambahanCheck`, `IdSetoran`) VALUES
	('MNFPKL1702043241', 17, '2017-02-04 20:03:12', 'PKL-TGL0000', '2017-02-04 00:00:00', '00:00', NULL, 5, 0, 0, NULL, 'KK001', 'K01', 3, NULL, 'Budi', 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL),
	('MNFSMG1708142866', 25, '2017-08-14 01:14:32', 'SMG-BDG1900', '2017-08-14 00:00:00', '19:00', NULL, 8, 4, 2, 3, 'NST0001', '0002', 3, 3, 'DIDIK SURYANTORO', 840026, 10000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL);
/*!40000 ALTER TABLE `tbl_spj` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_surat_jalan
CREATE TABLE IF NOT EXISTS `tbl_surat_jalan` (
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `TglSJ` date NOT NULL,
  `WaktuSJ` varchar(6) COLLATE latin1_general_ci NOT NULL,
  `WaktuCetak` datetime NOT NULL,
  `IdPool` int(10) unsigned NOT NULL,
  `IdPetugas` int(10) unsigned NOT NULL,
  `NamaPetugas` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaSopir` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `JumlahTrip` int(2) unsigned NOT NULL DEFAULT '0',
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuBatal` datetime NOT NULL,
  `Pembatal` int(10) unsigned NOT NULL,
  `NamaPembatal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`NoSJ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_surat_jalan: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_surat_jalan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_surat_jalan` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_surat_jalan_biaya
CREATE TABLE IF NOT EXISTS `tbl_surat_jalan_biaya` (
  `IdBiaya` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JenisBiaya` int(1) NOT NULL,
  `COA` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NamaBiaya` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `IsRealized` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdBiaya`),
  KEY `Index_2` (`NoSJ`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_surat_jalan_biaya: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_surat_jalan_biaya` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_surat_jalan_biaya` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_tiket_mitra
CREATE TABLE IF NOT EXISTS `tbl_tiket_mitra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NamaPenumpang` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Mitra` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TiketPublish` int(11) DEFAULT '0',
  `Komisi` double DEFAULT NULL,
  `WaktuInput` datetime DEFAULT NULL,
  `PetugasInput` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_tiket_mitra: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_tiket_mitra` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tiket_mitra` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `user_active` float NOT NULL,
  `username` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_time` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_page` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_lastvisit` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_level` decimal(3,1) NOT NULL,
  `user_timezone` float NOT NULL,
  `berlaku` datetime NOT NULL,
  `NRP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `birthday` datetime NOT NULL,
  `telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `hp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status_online` int(1) NOT NULL,
  `waktu_update_terakhir` datetime DEFAULT NULL,
  `waktu_login` datetime NOT NULL,
  `waktu_logout` datetime NOT NULL,
  `path_foto` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `jumlah_pengumuman_baru` int(11) DEFAULT '0',
  `cetak_bbm` int(1) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_user: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `KodeCabang`, `user_active`, `username`, `user_password`, `user_session_time`, `user_session_page`, `user_lastvisit`, `user_level`, `user_timezone`, `berlaku`, `NRP`, `nama`, `birthday`, `telp`, `hp`, `email`, `address`, `status_online`, `waktu_update_terakhir`, `waktu_login`, `waktu_logout`, `path_foto`, `jumlah_pengumuman_baru`, `cetak_bbm`) VALUES
	(3, 'BDG', 1, 'admin', '202cb962ac59075b964b07152d234b70', '1502651147', '900', '1502642745', 0.0, 0, '2017-05-11 00:00:00', 'ADMIN1', 'admin', '0000-00-00 00:00:00', '', '', '', '', 0, '2017-08-14 02:05:47', '2017-08-14 00:06:29', '2017-08-14 02:05:47', NULL, 0, 0),
	(20, 'BKM', 1, 'lorenz', 'e10adc3949ba59abbe56e057f20f883e', '1471594992', '201', '1471344587', 0.0, 0, '2017-06-24 00:00:00', '123456', 'Lorenz', '0000-00-00 00:00:00', '', '', '', '', 1, '2016-08-19 16:02:14', '2016-08-18 09:53:28', '0000-00-00 00:00:00', NULL, 0, 0),
	(21, 'BKM', 1, 'cso', '202cb962ac59075b964b07152d234b70', '', '', '', 2.0, 0, '2017-08-18 00:00:00', 'cso01', 'CSO 1', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_user_baca_pengumuman
CREATE TABLE IF NOT EXISTS `tbl_user_baca_pengumuman` (
  `user_id` int(11) NOT NULL,
  `IdPengumuman` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`IdPengumuman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_user_baca_pengumuman: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_user_baca_pengumuman` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user_baca_pengumuman` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_user_drop_cash
CREATE TABLE IF NOT EXISTS `tbl_user_drop_cash` (
  `IdDropCash` int(255) NOT NULL AUTO_INCREMENT,
  `Jumlah` double DEFAULT NULL,
  `IdSetoran` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `IsSetoran` int(10) DEFAULT NULL,
  `TglDropCash` date DEFAULT NULL,
  `IsClosing` int(5) NOT NULL DEFAULT '0',
  `ResiClosing` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `IdUser` int(5) DEFAULT NULL,
  `NamaUser` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdDropCash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_user_drop_cash: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_user_drop_cash` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user_drop_cash` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_user_setoran
CREATE TABLE IF NOT EXISTS `tbl_user_setoran` (
  `IdSetoran` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `WaktuSetoran` datetime DEFAULT NULL,
  `IdUser` int(11) DEFAULT NULL,
  `NamaUser` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTiketUmum` double DEFAULT '0',
  `JumlahTiketDiskon` double DEFAULT '0',
  `OmzetPenumpangTunai` double DEFAULT '0',
  `OmzetPenumpangDebit` double DEFAULT '0',
  `OmzetPenumpangKredit` double DEFAULT '0',
  `TotalDiskon` double DEFAULT '0',
  `TotalPaket` double DEFAULT '0',
  `OmzetPaket` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0',
  PRIMARY KEY (`IdSetoran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_user_setoran: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_user_setoran` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_user_setoran` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_voucher
CREATE TABLE IF NOT EXISTS `tbl_voucher` (
  `KodeVoucher` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangBerangkat` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangTujuan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPencetak` int(10) unsigned DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuDigunakan` datetime DEFAULT NULL,
  `PetugasPengguna` int(10) unsigned DEFAULT NULL,
  `NilaiVoucher` double NOT NULL,
  `IsHargaTetap` int(1) NOT NULL DEFAULT '0' COMMENT 'jika voucher nilainya adalah sebagai harga tertera, maka nilainya = 1',
  `IsBolehWeekEnd` int(1) NOT NULL DEFAULT '0',
  `IsReturn` int(1) NOT NULL DEFAULT '0',
  `IdJurusanBerangkat` int(11) DEFAULT NULL,
  `KodeJadwalBerangkat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTiketBerangkat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `DibatalkanOleh` int(11) DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL,
  PRIMARY KEY (`KodeVoucher`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_voucher: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_voucher` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.tbl_voucher_bbm
CREATE TABLE IF NOT EXISTS `tbl_voucher_bbm` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeVoucher` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kilometer` double DEFAULT NULL,
  `JenisBBM` int(1) DEFAULT NULL,
  `JumlahLiter` int(3) DEFAULT NULL,
  `JumlahBiaya` double DEFAULT NULL,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `NamaPetugas` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDicatat` date DEFAULT NULL,
  `KodeSPBU` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSPBU` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.tbl_voucher_bbm: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_voucher_bbm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_voucher_bbm` ENABLE KEYS */;

-- Dumping structure for table tiketux_abimanyu.v_laporan_keuangan_tiketux1
CREATE TABLE IF NOT EXISTS `v_laporan_keuangan_tiketux1` (
  `NoTiket` tinyint(4) NOT NULL,
  `KodeCabang` tinyint(4) NOT NULL,
  `KodeJadwal` tinyint(4) NOT NULL,
  `IdJurusan` tinyint(4) NOT NULL,
  `KodeKendaraan` tinyint(4) NOT NULL,
  `KodeSopir` tinyint(4) NOT NULL,
  `TglBerangkat` tinyint(4) NOT NULL,
  `JamBerangkat` tinyint(4) NOT NULL,
  `KodeBooking` tinyint(4) NOT NULL,
  `IdMember` tinyint(4) NOT NULL,
  `PointMember` tinyint(4) NOT NULL,
  `Nama` tinyint(4) NOT NULL,
  `Alamat` tinyint(4) NOT NULL,
  `Telp` tinyint(4) NOT NULL,
  `HP` tinyint(4) NOT NULL,
  `WaktuPesan` tinyint(4) NOT NULL,
  `NomorKursi` tinyint(4) NOT NULL,
  `HargaTiket` tinyint(4) NOT NULL,
  `Charge` tinyint(4) NOT NULL,
  `SubTotal` tinyint(4) NOT NULL,
  `Discount` tinyint(4) NOT NULL,
  `PPN` tinyint(4) NOT NULL,
  `Total` tinyint(4) NOT NULL,
  `PetugasPenjual` tinyint(4) NOT NULL,
  `FlagPesanan` tinyint(4) NOT NULL,
  `NoSPJ` tinyint(4) NOT NULL,
  `TglCetakSPJ` tinyint(4) NOT NULL,
  `CetakSPJ` tinyint(4) NOT NULL,
  `CetakTiket` tinyint(4) NOT NULL,
  `PetugasCetakTiket` tinyint(4) NOT NULL,
  `WaktuCetakTiket` tinyint(4) NOT NULL,
  `KomisiPenumpangCSO` tinyint(4) NOT NULL,
  `FlagSetor` tinyint(4) NOT NULL,
  `PetugasCetakSPJ` tinyint(4) NOT NULL,
  `Keterangan` tinyint(4) NOT NULL,
  `JenisDiscount` tinyint(4) NOT NULL,
  `JenisPembayaran` tinyint(4) NOT NULL,
  `WaktuSetor` tinyint(4) NOT NULL,
  `PenerimaSetoran` tinyint(4) NOT NULL,
  `IdSetoran` tinyint(4) NOT NULL,
  `FlagRekonData` tinyint(4) NOT NULL,
  `IdRekonData` tinyint(4) NOT NULL,
  `FlagBatal` tinyint(4) NOT NULL,
  `PetugasPembatalan` tinyint(4) NOT NULL,
  `WaktuPembatalan` tinyint(4) NOT NULL,
  `KodeAkunPendapatan` tinyint(4) NOT NULL,
  `JenisPenumpang` tinyint(4) NOT NULL,
  `KodeAkunKomisiPenumpangCSO` tinyint(4) NOT NULL,
  `PaymentCode` tinyint(4) NOT NULL,
  `PaymentCodeExpiredTime` tinyint(4) NOT NULL,
  `Signature` tinyint(4) NOT NULL,
  `WaktuMutasi` tinyint(4) NOT NULL,
  `Pemutasi` tinyint(4) NOT NULL,
  `MutasiDari` tinyint(4) NOT NULL,
  `OTP` tinyint(4) NOT NULL,
  `OTPUsed` tinyint(4) NOT NULL,
  `IsSettlement` tinyint(4) NOT NULL,
  `Komisi` tinyint(4) NOT NULL,
  `HargaTiketux` tinyint(4) NOT NULL,
  `IdDiscount` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table tiketux_abimanyu.v_laporan_keuangan_tiketux1: 0 rows
/*!40000 ALTER TABLE `v_laporan_keuangan_tiketux1` DISABLE KEYS */;
/*!40000 ALTER TABLE `v_laporan_keuangan_tiketux1` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
