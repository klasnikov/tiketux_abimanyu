<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberPromoTukarPoint.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$PromoPoin	= new PromoPoin();

switch($mode){

//TAMPILKAN MEMBER BARU ==========================================================================================================
case 'tampilkan_promo':
	
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	$cari 			= $HTTP_GET_VARS['cari'];
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			
			<th><a href='#' onClick='setOrder(\"kode_promo\")'>
				<font color='white'>Kode Promo</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"nama_penukaran_promo\")'>
				<font color='white'>Nama promo poin</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"poin\")'>
				<font color='white'>Poin</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"dibuat_oleh\")'>
				<font color='white'>Dibuat Oleh</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"waktu_dibuat\")'>
				<font color='white'>Waktu dibuat</font></a></th>
				
				<th><a href='#' onClick='setOrder(\"diubah_oleh\")'>
				<font color='white'>Diubah oleh</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"waktu_diubah\")'>
				<font color='white'>Waktu dibuat</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"status_aktif\")'>
				<font color='white'>Status</font></a></th>
			
			<th width='90'>Aksi</th>
    </tr>";
    
	$result	= $PromoPoin->ambilData($sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row['status_aktif']){
			$status	= "Aktif";
		}
		else
		{
			$status	= "Nonaktif";
			$odd='red';
		}
		
		$action = 
			"<input type='button' value='&nbsp;&nbsp;Ubah&nbsp;&nbsp;&nbsp;' onClick='document.location=\"".append_sid('member_promo_poin_detail.'.$phpEx)."&id_promo_poin=$row[id_promo_poin]&mode=ambil_data_promo"."\"' />
			<input type='button' value='&nbsp;&nbsp;Hapus&nbsp;&nbsp;' onclick=\"TanyaHapus('$row[id_promo_poin]');\"  />";
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[kode_promo]</td>
      <td class='$odd'>$row[nama_penukaran_poin]</td>
      <td class='$odd' align='right'>".number_format($row['poin'],0,",",".")."</td>
			<td class='$odd'>$row[dibuat_oleh]</td>
			<td class='$odd'>$row[waktu_dibuat]</td>
			<td class='$odd'>$row[diubah_oleh]</td>
			<td class='$odd'>$row[waktu_diubah]</td>
			<td class='$odd' align='center'><a href='#' onClick='ubahStatus(\"$row[id_promo_poin]\");'>$status</a></td>
			<td class='$odd' align='center'>$action</td>
    </tr>
		<tr>
			<td colspan=14 height=3 bgcolor='D0D0D0'></td>
		</tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=14 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//UBAH STATUS PROMO ==========================================================================================================
case 'ubah_status':
	$id_promo_poin    = $HTTP_GET_VARS['id_promo_poin'];  
	
	if(!$PromoPoin->ubahStatus($id_promo_poin)) echo("false");else echo("true");
	
exit;
//switch mode

//HAPUS MEMBER ==========================================================================================================
case 'hapus_promo':
	
	//ambil isi dari inputan
	$id_promo_poin	= $HTTP_GET_VARS['id_promo_poin'];	
	
	if($PromoPoin->hapus($id_promo_poin)){
		echo("berhasil");
	}
	else{
		echo("gagal");
	}
exit;
}//switch mode

$template->set_filenames(array('body' => 'member_promo_poin.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  	=>$userdata['username'],
   	'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo_poin.'.$phpEx).'">Promo Poin</a>',
   	'U_ADD' 			=>'<a href="'.append_sid('member_promo_poin_detail.'.$phpEx) .'">Tambah Promo Poin</a>',
   	'U_USER_SHOW'	=>append_sid('promo.'.$phpEx.'?mode=tampilkan_promo'),
		'PESAN'				=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>