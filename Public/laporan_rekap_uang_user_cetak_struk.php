<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//METHODS
function getDataSPJ($tgl_berangkat,$kode_jadwal){
	global $db;
	
	$sql=
		"SELECT 
			NoSPJ,KodeDriver,Driver,NoPolisi
		FROM tbl_spj
		WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

	if (!$result= $db->sql_query($sql)){
			echo("Error:".__LINE__);exit;
	}
	
	return $db->sql_fetchrow($result);
	
}

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tanggal		= $HTTP_GET_VARS['p0'];
	
//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$User						= new User();
$useraktif			= $userdata['user_id'];
$data_user			= $User->ambilDataDetail($useraktif);
	
$temp_tanggal_cari	= explode(" ",$tanggal);
$temp_tanggal_cari	= explode("-",$temp_tanggal_cari[0]);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi";
}
	

$data_perusahaan	= $PengaturanUmum->ambilDataPerusahaan();

$template->set_filenames(array('body' => 'laporan_rekap_uang_user_cetak_struk.tpl')); 

//MEMERIKSA APAKAH SUDAH PERNAH SETORAN SEBELUMNYA
//QUERY DATA TIKET
$sql=
	"SELECT COUNT(1)
	FROM $tbl_reservasi
	WHERE DATE(WaktuCetakTiket)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1 
	AND PetugasCetakTiket=$useraktif
	GROUP BY TglBerangkat,KodeJadwal,JamBerangkat
	ORDER BY TglBerangkat,Asal,Tujuan,JamBerangkat ";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

//QUERY DATA TIKET
$sql=
	"SELECT 
		KodeJadwal,TglBerangkat,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
		JamBerangkat,JenisPembayaran,JenisPenumpang,
		COUNT(NoTiket) AS JumlahPenumpang,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,Total,0)),0) AS TotalKartuKredit,
		IS_NULL(SUM(IF(JenisPembayaran!=3,Total,0)),0) AS Total,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IdJurusan
	FROM $tbl_reservasi
	WHERE DATE(WaktuCetakTiket)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1 
	AND PetugasCetakTiket=$useraktif
	GROUP BY TglBerangkat,KodeJadwal,JamBerangkat
	ORDER BY TglBerangkat,Asal,Tujuan,JamBerangkat ";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$jurusan_temp	="";
$tanggal_temp	="";

$total_pendapatan_tiket_tunai	= 0;
$total_pendapatan_tiket_debit	= 0;
$total_pendapatan_tiket_kredit= 0;
$total_pendapatan_tiket				= 0;

while ($data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail)){
	
	//MENGAMBIL DATA SPJ
	$data_spj	= getDataSPJ($data_penumpang_detail['TglBerangkat'],$data_penumpang_detail['KodeJadwal']);
	
	if($data_spj['NoSPJ']!=""){
		$ket_nospj	= $data_spj['NoSPJ'];
		$ket_driver	= $data_spj['KodeDriver']."|".$data_spj['Driver'];;
		$ket_nopol	= $data_spj['NoPolisi'];
	}
	else{
		$ket_nospj	= "Belum Cetak";
		$ket_driver	= "Belum Cetak";
		$ket_nopol	= "Belum Cetak";
	}
	
	if($tanggal_temp==$data_penumpang_detail['TglBerangkat']){
		$show_tanggal	= "";
	}
	else{
		$show_tanggal	= "<font style='font-weight: bold;font-size:16px;'>".dateparse(FormatMySQLDateToTgl($data_penumpang_detail['TglBerangkat']))."</font><br>";
		$tanggal_temp	= $data_penumpang_detail['TglBerangkat'];
	}
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$show_jurusan	= $data_penumpang_detail['Asal'].'-'.$data_penumpang_detail['Tujuan']."<br>";
		$jurusan_temp	= $data_penumpang_detail['IdJurusan'];
	}

	//list jenis penumpang
	$list_jenis_penumpang	= "";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangU']>0?"|U:".$data_penumpang_detail['TotalPenumpangU']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangM']>0?"|M:".$data_penumpang_detail['TotalPenumpangM']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangK']>0?"|K:".$data_penumpang_detail['TotalPenumpangK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangKK']>0?"|KK:".$data_penumpang_detail['TotalPenumpangKK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangG']>0?"|G:".$data_penumpang_detail['TotalPenumpangG']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangT']>0?"|O:".$data_penumpang_detail['TotalPenumpangT']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangR']>0?"|R:".$data_penumpang_detail['TotalPenumpangR']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangV']>0?"|V:".$data_penumpang_detail['TotalPenumpangV']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangVR']>0?"|VR:".$data_penumpang_detail['TotalPenumpangVR']:"";
	
	$total_pendapatan_tiket_tunai	+= $data_penumpang_detail['TotalTunai'];
	$total_pendapatan_tiket_debit	+= $data_penumpang_detail['TotalDebit'];
	$total_pendapatan_tiket_kredit+= $data_penumpang_detail['TotalKartuKredit'];
	$total_pendapatan_tiket				+= $data_penumpang_detail['Total'];
	
	$template->
		assign_block_vars(
			'TIKET',
			array(
				'SHOWTANGGAL'		=>$show_tanggal,
				'JURUSAN'				=>$show_jurusan,
				'JAM'						=>$data_penumpang_detail['JamBerangkat'],
				'KET_SPJ'				=>$ket_driver,
				'LIST_PENUMPANG'=>$data_penumpang_detail['JumlahPenumpang'].$list_jenis_penumpang,
				'TUNAI'					=>number_format($data_penumpang_detail['TotalTunai'],0,",","."),
				'DEBIT'					=>$data_penumpang_detail['TotalDebit']==0?"":"Debit:Rp.".number_format($data_penumpang_detail['TotalDebit'],0,",",".")."<br>",
				'KREDIT'				=>$data_penumpang_detail['TotalKartuKredit']==0?"":"Kredit:Rp.".number_format($data_penumpang_detail['TotalKartuKredit'],0,",",".")."<br>",
			)
	);
}

$show_sub_total_tiket	 = "Penj.Tunai:Rp.". number_format($total_pendapatan_tiket_tunai,0,",",".")."<br>";
$show_sub_total_tiket	.= $total_pendapatan_tiket_debit==0?"":"Penj.Debit:Rp.". number_format($total_pendapatan_tiket_debit,0,",",".")."<br>";
$show_sub_total_tiket	.= $total_pendapatan_tiket_kredit==0?"":"Penj.Kredit:Rp.". number_format($total_pendapatan_tiket_kredit,0,",",".")."<br>";
$show_sub_total_tiket .= "Penj.Tiket:Rp.".number_format($total_pendapatan_tiket,0,",",".")."<br>";

//END TIKET

//QUERY PAKET
$sql=
	"SELECT 
		IS_NULL(COUNT(NoTiket),0) AS JumlahTransasi,
		IS_NULL(SUM(JumlahKoli),0) AS JumlahPax,
		IS_NULL(SUM(Berat),0) AS JumlahKilo,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,TotalBayar,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,TotalBayar,0)),0) AS TotalKartuKredit,
		IS_NULL(SUM(TotalBayar),0) AS Total
	FROM tbl_paket
	WHERE
		DATE(WaktuCetakTiket)='$tanggal'
		AND CetakTiket=1 
		AND FlagBatal!=1
		AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual=$useraktif,PetugasPemberi=$useraktif)
	";

if (!$result_paket= $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$data_paket	= $db->sql_fetchrow($result_paket);

if($data_paket['JumlahTransasi']>0){	
	$template->
		assign_block_vars(
			'PAKET',
				array(
					'TUNAI'					=> number_format($data_paket['TotalTunai'],0,",","."),
					'DEBIT'					=> $data_paket['TotalDebit']==0?"":"Debit:Rp.".number_format($data_paket['TotalDebit'],0,",",".")."<br>",
					'KREDIT'				=> $data_paket['TotalKartuKredit']==0?"":"Penj.Kredit:Rp.". number_format($data_paket['TotalKartuKredit'],0,",",".")."<br>",
					'TOTAL_PENJUALAN'=> number_format($data_paket['Total'],0,",","."),
					'JUMLAH_TRANSAKSI'=> number_format($data_penumpang_detail['JumlahTransasi'],0,",","."),
					'JUMLAH_PAX'		=> number_format($data_penumpang_detail['JumlahPax'],0,",","."),
					'JUMLAH_KILO'		=> number_format($data_penumpang_detail['JumlahKilo'],0,",","."),
		)
	);
}

//END PAKET

//QUERY BIAYA
$sql=
	"SELECT
		IS_NULL(SUM(IF(FlagJenisBiaya=2,Jumlah,0)),0) AS TotalBiayaSopir,
		IS_NULL(SUM(IF(FlagJenisBiaya=1,Jumlah,0)),0) AS TotalBiayaTol,
		IS_NULL(SUM(IF(FlagJenisBiaya=4,Jumlah,0)),0) AS TotalBiayaParkir,
		IS_NULL(SUM(IF(FlagJenisBiaya=5,Jumlah,0)),0) AS TotalBiayaInsentifSopir,
		IS_NULL(SUM(IF(FlagJenisBiaya=7,Jumlah,0)),0) AS TotalBiayaTambahanBBM,
		IS_NULL(SUM(IF(FlagJenisBiaya=8,Jumlah,0)),0) AS TotalBiayaTambahanTol,
		IS_NULL(SUM(IF(FlagJenisBiaya=9,Jumlah,0)),0) AS TotalBiayaTambahanLain,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE IdPetugas='$useraktif' AND TglTransaksi='$tanggal' AND NOT FlagJenisBiaya IN($FLAG_BIAYA_BBM,$FLAG_BIAYA_VOUCHER_BBM)";

if (!$result_biaya = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$data_biaya	= $db->sql_fetchrow($result_biaya);

$show_total_setoran	= $total_pendapatan_tiket_debit+$data_paket['TotalDebit']==0?"":"Setoran Debit:Rp.". number_format($total_pendapatan_tiket_debit+$data_paket['TotalDebit'],0,",",".")."<br>";
$show_total_setoran	.= $total_pendapatan_tiket_kredit+$data_paket['TotalKartuKredit']==0?"":"Setoran Kredit:Rp.". number_format($total_pendapatan_tiket_kredit+$data_paket['TotalKartuKredit'],0,",",".")."<br>";

if($data_biaya['TotalBiayaTambahanBBM']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahanbbm",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanBBM'],0,",",".")));
}

if($data_biaya['TotalBiayaTambahanTol']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahantol",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanTol'],0,",",".")));
}

if($data_biaya['TotalBiayaTambahanLain']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahanlain",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanLain'],0,",",".")));
}



//set template
$template->assign_vars (
	array(
	'NAMA_PERUSAHAAN'			=> $data_perusahaan['NamaPerusahaan'],
	'NAMA_CSO'						=> $data_user['nama'],
	'TANGGAL_TRANSAKSI'		=> dateparse(FormatMySQLDateToTgl($tanggal)),
	'TOTAL_BIAYA'					=> number_format($row_biaya[0],0,",","."),
	'SHOW_SUB_TOTAL_TIKET'=> $show_sub_total_tiket,
	'BIAYA_SOPIR'					=> number_format($data_biaya['TotalBiayaSopir'],0,",","."),
	'BIAYA_INSENTIF_SOPIR'=> number_format($data_biaya['TotalBiayaInsentifSopir'],0,",","."),
	'BIAYA_PARKIR'				=> number_format($data_biaya['TotalBiayaParkir'],0,",","."),
	'BIAYA_TOL'						=> number_format($data_biaya['TotalBiayaTol'],0,",","."),
	'TOTAL_BIAYA'					=> number_format($data_biaya['TotalBiaya'],0,",","."),
	'TOTAL_SETORAN'				=> number_format($total_pendapatan_tiket_tunai+$data_paket['TotalTunai']-$data_biaya['TotalBiaya'],0,",","."),
	'SHOW_TOTAL_SETORAN'	=> $show_total_setoran,
	'WAKTU_CETAK'					=> date("d-M-Y H:i:s")
	)
);

$template->pparse('body');	


?>