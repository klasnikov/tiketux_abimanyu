<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$template->set_filenames(array('body' => 'biayaharian/laporan_biayalain.tpl'));

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}

switch ($mode){
    case "topup":
        $kodecabang = $HTTP_GET_VARS['kode'];
        $topup = $HTTP_GET_VARS['saldo'];

        $sql = "UPDATE tbl_md_cabang SET SaldoPettyCash = SaldoPettyCash+$topup WHERE KodeCabang = '$kodecabang';";

        if(!$db->sql_query($sql)){
            echo (0);
        }else{
            echo (1);
        }
        exit;
}

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"Nama":$sort_by;

$kondisi	=($cari=="")?"":
    " WHERE KodeCabang LIKE '%$cari%'
				OR Nama LIKE '%$cari%'
				OR Alamat LIKE '%$cari%'
				OR Kota LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Fax LIKE '%$cari%'";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"KodeCabang","tbl_md_cabang","&cari=$cari",$kondisi,"pengaturan_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql =
    "SELECT KodeCabang,Nama,Alamat,Kota,Telp,Fax,FlagAgen,SaldoPettyCash
			FROM tbl_md_cabang $kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

$idx_check=0;


if ($result = $db->sql_query($sql)){
    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        $detail ="window.open('".append_sid('reservasi.biayalain.'.$phpEx.'?kode_cabang='.$row['KodeCabang'].'')."','_blank');";

        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'		=>$odd,
                'no'		=>$i,
                'kode'	=>$row['KodeCabang'],
                'nama'	=>$row['Nama'],
                'alamat'=>$row['Alamat'],
                'kota'	=>$row['Kota'],
                'saldo'=>"Rp. ".number_format($row['SaldoPettyCash'],0,',','.'),
                'topup'=>"TOPUP('".$row['KodeCabang']."')",
                'detail'=>$detail,
            )
        );

        $i++;
    }

    if($i-1<=0){
        $no_data	=	"<tr><td colspan=20 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
    }
}
else{
    //die_error('Cannot Load cabang',__FILE__,__LINE__,$sql);
    echo("Err :".__LINE__);exit;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

$array_sort	=
    "'".append_sid('biayaharian.php?sort_by=KodeCabang'.$parameter_sorting)."',".
    "'".append_sid('biayaharian.php?sort_by=Nama'.$parameter_sorting)."',".
    "'".append_sid('biayaharian.php?sort_by=Alamat'.$parameter_sorting)."',".
    "'".append_sid('biayaharian.php?sort_by=Kota'.$parameter_sorting)."',".
    "'".append_sid('biayaharian.php?sort_by=SaldoPettyCash'.$parameter_sorting)."',";

$page_title	= "Pengaturan Cabang";

$template->assign_vars(array(
        'BCRUMP'    		=> '<ul id="breadcrumb"><li><a href="'.append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional') .'">Home</a> </li><li> <a href="'.append_sid('biayaharian.'.$phpEx).'">Biaya Harian</a></li></ul>',
        'ACTION_CARI'		=> append_sid('biayaharian.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'NO_DATA'				=> $no_data,
        'PAGING'				=> $paging,
        'ARRAY_SORT'		=> $array_sort
    )
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>