<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kode_cabang    = isset($HTTP_GET_VARS['kode_cabang'])? $HTTP_GET_VARS['kode_cabang'] : $HTTP_POST_VARS['kode_cabang'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

if($kode_cabang == ""){
    $cabang = $userdata['KodeCabang'];
}else{
    $cabang = $kode_cabang;
}

$mode	= $mode==""?"explore":$mode;

switch ($mode){
    case "explore":

        $kondisi =	$cari==""?"":
            " AND (KodeCabang LIKE '$cari%'
				OR NamaPetugas LIKE '$cari%' 
				OR Penerima LIKE '%$cari%'
				OR Keterangan LIKE '%$cari%' 
				OR Jumlah LIKE '%$cari%'
				OR JenisBiaya LIKE '%$cari%')";

        //PAGING======================================================
        $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
        $paging		= pagingData($idx_page,"IdBiayaHarian","tbl_biaya_harian",
            "&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&kode_cabang=$cabang",
            "WHERE KodeCabang = '$cabang' AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"reservasi.biayalain.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
        //END PAGING======================================================

        $sql	= "SELECT *, f_cabang_get_name_by_kode(KodeCabang) AS CABANG FROM tbl_biaya_harian WHERE KodeCabang = '$cabang' AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi ORDER BY TglTransaksi DESC;";

        if(!$result = $db->sql_query($sql)){
            die_error("Gagal eksekusi query!",__LINE__,"Error Code",mysql_error());
        }

        $i=1;

        while ($row = $db->sql_fetchrow($result)){
            $odd ='odd';

            if (($i % 2)==0){
                $odd = 'even';
            }

            $template->
            assign_block_vars(
                'ROW',
                array(
                    'odd'=>$odd,
                    'no'=>$i+$idx_page*$VIEW_PER_PAGE,
                    'id'=>$row['IdBiayaHarian'],
                    'Cabang'=>$row['CABANG'],
                    'TglTransaksi'=>date_format(date_create($row['TglTransaksi']),'d-m-Y'),
                    'Petugas'=>$row['NamaPetugas'],
                    'Penerima'=>$row['Penerima'],
                    'JenisBiaya'=>$row['JenisBiaya'],
                    'Jumlah'=>"Rp. ".number_format($row['Jumlah'],0,',','.'),
                    'Keterangan'=>$row['Keterangan'],
                    )
            );
            $i++;
        }

        if($i-1<=0){
            $no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
        }

        $page_title	= "Biaya Harian";

        //KOMPONEN UNTUK EXPORT
        $parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&cari=".$cari.
            "&kode_cabang=".$kode_cabang."";

        $script_cetak_excel="Start('reservasi.biayalain.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
        //--END KOMPONEN UNTUK EXPORT

        $sql = "SELECT Nama, SaldoPettyCash FROM tbl_md_cabang WHERE KodeCabang = '$cabang';";
        if(!$result = $db->sql_query($sql)){
            die_error("Gagal eksekusi query!",__LINE__,"Error Code",mysql_error());
        }else{
            $row = $db->sql_fetchrow($result);
        }

        $template->assign_vars(array(
                'ACTION_CARI'		=> append_sid('reservasi.biayalain.'.$phpEx),
                'PAGING'			=> $paging,
                'TGL_AWAL'			=> $tanggal_mulai,
                'TGL_AKHIR'			=> $tanggal_akhir,
                'TXT_CARI'			=> $cari,
                'CABANG'            => $row[0],
                'SALDOCABANG'       => ($row[1] == "")?0:number_format($row[1],0,',','.'),
                'SALDO'             => ($row[1] == "")?0:$row[1],
                'EXPORT'            => $script_cetak_excel,
                'NO_DATA'			=> $no_data,
            )
        );
        
        $template->set_filenames(array('body' => 'biayaharian/biayalain.tpl'));
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
    exit;
    case "create":
        global $db;

        $jenis = $HTTP_GET_VARS['jenis'];
        $penerima = $HTTP_GET_VARS['penerima'];
        $jumlah = $HTTP_GET_VARS['jumlah'];
        $keterangan = $HTTP_GET_VARS['keterangan'];
        $sisa_saldo = $HTTP_GET_VARS['saldo'];

        $cabang = $userdata['KodeCabang'];
        $userid = $userdata['user_id'];
        $petugas = $userdata['nama'];

        $sql = "INSERT INTO tbl_biaya_harian (KodeCabang, TglTransaksi, IdPetugas, NamaPetugas, Penerima, JenisBiaya, Jumlah, Keterangan) 
                VALUES ('$cabang',NOW(),$userid,'$petugas','$penerima','$jenis',$jumlah,'$keterangan');";


        if(!$db->sql_query($sql)){
            echo (0);
        }

        $sql = "UPDATE tbl_md_cabang SET SaldoPettyCash = $sisa_saldo WHERE KodeCabang = '$cabang';";

        if(!$db->sql_query($sql)){
            echo (0);
        }else{
            echo (1);
        }

    exit;
    case "cetak":
        $id = $HTTP_GET_VARS['IdBiayaHarian'];

        $sql	= "SELECT *, f_cabang_get_name_by_kode(KodeCabang) AS CABANG FROM tbl_biaya_harian WHERE IdBiayaHarian = $id;";

        if(!$result = $db->sql_query($sql)){
            die_error("Gagal eksekusi query!",__LINE__,"Error Code",mysql_error());
        }
        
        $row = $db->sql_fetchrow($result);

        $template->assign_vars(array(
                'PETUGAS' => $row['NamaPetugas'],
                'CABANG'=>$row['CABANG'],
                'TGL'=>date_format(date_create($row['TglTransaksi']),'d-m-Y'),
                'PENERIMA'=>$row['Penerima'],
                'JENIS'=>$row['JenisBiaya'],
                'JUMLAH'=>number_format($row['Jumlah'],0,',','.'),
                'KETERANGAN'=>$row['Keterangan'],
                'CETAK'=>date('d-m-Y')
            )
        );

        $template->set_filenames(array('body' => 'biayaharian/cetak.tpl'));
        $template->pparse('body');
    exit;
}
?>