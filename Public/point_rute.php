<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act    = $HTTP_GET_VARS['act'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'tampilkan_point_rute';

switch($mode){

//TAMPILKAN MEMBER BARU ==========================================================================================================
case 'tampilkan_point_rute':
	
	$cari    							= $HTTP_POST_VARS['txt_cari'];
	
	
	
	if($cari!=''){
		$kondisi_pencarian =  " (kode LIKE '%$cari%')";
		$kondisi_pencarian .= " OR(nama LIKE '%$cari%')";
		$kondisi_pencarian .= " OR(namaKota LIKE '%$cari%')";
	}
	
	$kondisi	= ($kondisi_pencarian=='')? '':'WHERE '.$kondisi_pencarian;
	
	$sql = 
		"SELECT 
			kode,nama,NamaKota
		FROM	TbMDKota ".$kondisi." ORDER BY nama";
				
	if (!$result = $db->sql_query($sql)){
		die_error('AGAL mengambil data point_rute',__FILE__,__LINE__,$sql);
	}
	else {
		$i = ($idx_halaman_sekarang-1)*$JUM_DATA_DITAMPILKAN;
		
		while ($row=$db->sql_fetchrow($result)){   
			$i++;
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
						
			$action = "<a href='".append_sid('point_rute_detail.'.$phpEx)."&kode_point_rute=$row[0]&mode=ambil_data_point_rute'>Edit</a>+<a onclick=\"TanyaHapus('$row[0]');\" href='##'>Delete</a>";
			
			$template->set_filenames(array('body' => 'point_rute.tpl'));        
			$template->assign_block_vars('ROW',
				array(
						'odd'					=>$odd,
						'no'					=>$i,
						'kode'				=>$row['kode'],
						'nama_point'	=>$row['nama'],
						'kota'				=>$row['NamaKota'],
						'act'					=>$action
					));
			
		}
		
		//jika tidak ditemukan data pada database
		if($i==0){
			$pesan=
				"<table width='100%' class='border'>
					<tr><td align='center' bgcolor='EFEFEF'>
						<font color='red'><strong>Data tidak ditemukan!</strong></font>
					</td></tr>
				</table><br><br>";
		}
	}
break;

//HAPUS MEMBER BARU ==========================================================================================================
case 'hapus_point_rute':
	$kode_point_rute    = $HTTP_GET_VARS['kode_point_rute'];  
	
	//memeriksa id point_rute,point_rute aktif tidak dapat menghapus datanya sendiri
	if($kode_point_rute==$userdata['kode_point_rute']){
		echo("Anda tidak dapat menghapus data anda sendiri!"); 
		exit;
	}
	
	$sql =
		"DELETE FROM TbMDkota 
		WHERE (kode ='$kode_point_rute')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('GAGAL menghapus data');
	}

exit;
}//switch mode

$template->set_filenames(array('body' => 'point_rute.tpl')); 
$template->assign_vars(array
  ( 'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('point_rute.'.$phpEx).'">Point Rute</a>',
   	'U_ADD' =>'<a href="'.append_sid('point_rute_detail.'.$phpEx) .'">Tambah Point Rute</a>',
   	'U_USER_SHOW'=>append_sid('point_rute.'.$phpEx.'?mode=tampilkan_point_rute'),
		'CARI'	=>$cari,
		'SID'=>$userdata['sid'],
		'PESAN'	=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>