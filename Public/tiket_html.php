<?php
//
// CETAK TIKET UNTUK LINUX
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassJurusan.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso						= $userdata['nama'];

function getDiscountGroup($kode_jadwal,$tgl_berangkat){
		
		global $db;
		
		//mengambil tanggal tuslah
		$sql	= 
			"SELECT TglMulaiTuslah,TglAkhirTuslah
				FROM tbl_pengaturan_umum LIMIT 0,1";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$tgl_mulai_tuslah	= $row['TglMulaiTuslah'];
		$tgl_akhir_tuslah	= $row['TglAkhirTuslah'];
		
		//mengambil idjurusan dari kodejadwal
		$sql	= 
			"SELECT  IdJurusan
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal'";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$id_jurusan	= ($row['IdJurusan']!="")?$row['IdJurusan']:0;
		
		//mengambil harga tiket
		$sql	= 
			"SELECT IF($tgl_berangkat NOT BETWEEN $tgl_mulai_tuslah AND $tgl_akhir_tuslah,HargaTiket,HargaTiketTuslah) AS HargaTiket
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan'";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$harga_tiket	= ($row['HargaTiket']!="")?$row['HargaTiket']:0;
		
		$sql = "SELECT FlagLuarKota
            FROM   tbl_md_jurusan
						WHERE IdJurusan='$id_jurusan'";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,IS_NULL(JumlahDiscount,0) AS JumlahDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND KodeDiscount='M'";
	  
	  if (!$result = $db->sql_query($sql)){
			return 0;
		}
		
		$row = $db->sql_fetchrow($result);
		
		return $row;
}

$Reservasi= new Reservasi();
$Asuransi	= new Asuransi();
$User			= new User();
$Jurusan	= new Jurusan();

	//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING
	if($cetak_tiket!=1){
		//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
		$result	= $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
	}
	else{
		//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
		$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	}
	
	if ($result){
					
		$i=0;
		
		$list_no_tiket		="";
		$list_kode_booking="";
		$output_tiket	= "<table width='300' cellspacing='0' cellpadding='0'>";
		
		$sudah_diperiksa	= false;
		
		//MENGAMBIL JUMLAH TIKET YANG DIPESAN
		$jum_pesanan	= @mysql_num_rows($result);
		
		while ($row = $db->sql_fetchrow($result)){
			
			
			$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($row['IdJurusan']);
			
			$no_tiket=$row['NoTiket'];
			$kode_booking=$row['KodeBooking'];
			$id_member=$row['IdMember'];
			$nama=$row['Nama'];
			$alamat=$row['Alamat'];
			$telp=$row['Telp'];
			$tanggal=$row['TglBerangkat'];
			$jam=$row['JamBerangkat'];
			$asal=$array_jurusan['Asal'];
			$tujuan=$array_jurusan['Tujuan'];
			$harga_tiket=number_format($row['HargaTiket'],0,",",".");
			$discount=number_format($row['Discount'],0,",",".");
			$total_bayar=$row['Total'];
			$bayar=number_format($row['Total'],0,",",".");
			$nomor_kursi=$row['NomorKursi'];
			$cetak_tiket=$row['CetakTiket'];
			$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
			$kode_jadwal=$row['KodeJadwal'];
			
			$nama_cso	= ($cetak_tiket!=1)?$cso:$User->ambilNamaUser($row['PetugasCetakTiket']);
			$cso	= $nama_cso;
			
			//MEMBERIKAN DISKON JIKA KURSI YANG DIPESAN LEBIH DARI JUMLAH MINIMUM DISCOUNT GRUP
			if($jum_pesanan>=$JUMLAH_MINIMUM_DISCOUNT_GROUP && ($row['Discount']=='' || $row['Discount']==0)){
				$data_discount_group	= getDiscountGroup($kode_jadwal,$tanggal);
				
				//MENGUBAH DATA DISCOUNT
				$sql = "CALL sp_reservasi_ubah_discount('$no_tiket', 'M', '$data_discount_group[NamaDiscount]','$data_discount_group[JumlahDiscount]');";
				
				if (!$db->sql_query($sql)){
					die_error("Err $this->ID_FILE".__LINE__);
				}
				
				if($data_discount_group['JumlahDiscount']!=''){
					$discount			= number_format($data_discount_group['JumlahDiscount'],0,",",".");
					$total_bayar	= $row['HargaTiket']-$data_discount_group['JumlahDiscount'];
					$bayar				= number_format($total_bayar,0,",",".");
				}
			}
			
			//MENGAMBIL DATA ASURANSI
			$data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);
			
			if(!$sudah_diperiksa && $cetak_tiket!=1){
				if($id_member==''){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();
					
					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
						
						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
						  "", $tanggal,
						  $userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
						  $alamat, $tanggal, $tanggal,
						  $userdata['user_id'], $kode_jadwal,1,
						  0);
					}
					
					$sudah_diperiksa	= true;
				}
				else{
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
				}
			}
			
			$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
			
				
			switch($jenis_pembayaran){
				case 0:
					$ket_jenis_pembayaran="TUNAI";
				break;
				case 1:
					$ket_jenis_pembayaran="DEBIT CARD";
				break;
				case 2:
					$ket_jenis_pembayaran="KREDIT CARD";
				break;
				case 3:
					$ket_jenis_pembayaran="VOUCHER";
				break;
			}
			
			$list_no_tiket			.="'$no_tiket',";
			$list_kode_booking	.="'$kode_booking',";
			
			$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
			
			$output_tiket	.= "
			<tr>
				<td colspan='3' class='tiket_body'>
					$data_perusahaan[NamaPerusahaan]<br>
					Pelopor On Time Shuttle<br>
					<br>
					$no_tiket<br>
					$ket_jenis_pembayaran
				</td>
			</tr>
			<tr>
				<td class='tiket_body'>Pergi</td><td width='1' class='tiket_body'>:</td><td class='tiket_body'>".dateparse(FormatMySQLDateToTgl($tanggal))."</td>
			</tr>
			<tr>
				<td colspan='3' class='tiket_big_mark'>".substr($asal,0,20)."-".substr($tujuan,0,20)."</td>
			</tr>
			<tr>
				<td colspan='3' class='tiket_big_mark' align='center'>$jam</td>
			</tr>
			<tr>
				<td colspan='3' class='tiket_body' align='center'>Kursi</td>
			</tr>
			<tr>
				<td colspan='3' class='tiket_big_mark'>$nomor_kursi</td>
			</tr>
			<tr><td colspan='3'><br></td></tr>
			";
			
			if($data_asuransi['IdAsuransi']!=""){
				$output_tiket	.=
					"<tr><td class='tiket_body'>Tiket</td><td class='tiket_body'>:</td><td align='right' class='tiket_body'>Rp. $bayar</td></tr>
					<tr><td class='tiket_body'>Premi</td><td class='tiket_body'>:</td><td align='right' class='tiket_body'>Rp. ".number_format($data_asuransi['BesarPremi'],0,",",".")."</td></tr>
					<tr><td colspan='3' class='tiket_body'>Asuransi ".$data_asuransi['NamaPlanAsuransi']."</td></tr>";
				$bayar	= number_format($total_bayar + $data_asuransi['BesarPremi'] ,0,",",".");
			}
			
			$output_tiket	.=
				"<tr><td class='tiket_body'>Bayar</td><td class='tiket_body'>:</td><td align='right' class='tiket_body'>Rp. $bayar</td></tr>
				<tr><td class='tiket_body' valign='top'>CSO</td><td class='tiket_body' valign='top'>:</td><td class='tiket_body'>".substr($cso,0,20)."</td></tr>
				<tr><td colspan='3' class='tiket_body' ><br></td></tr>";
			
			
			//PESAN SPONSOR
			
			$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
			
			$output_tiket	.= 
				"<tr><td colspan='3' class='tiket_footer'>$pesan_sponsor</td></tr>
				<tr><td colspan='3' class='tiket_footer'>-- Terima Kasih --</td></tr>
				<tr><td colspan='3'><br><br></td></tr>";
			
			$i++;
		}
		
		//UPDATE FLAG TIKET DI LAYOUT KURSI
		$list_no_tiket	= substr($list_no_tiket,0,-1);
		
		//mengambil jadwal utama
		$sql	=
			"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
			
		$row = $db->sql_fetchrow($result);
		$kode_jadwal_utama	= $row[0];
		
		$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
			  KodeJadwal='$kode_jadwal_utama' 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN ($list_no_tiket);";
		
		if(!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		//mengupate flag cetak tiket
		if($cetak_tiket!=1){
			$list_kode_booking	= substr($list_kode_booking,0,-1);
			$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
		}
		
		//mengupdate status asuransi menjadi OK
		$Asuransi->ubahFlagBatalAsuransi($list_no_tiket, 0, $userdata['user_id']);
		
	} 
	else{
		//die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
		echo("Error :".__LINE__);
		exit;
	}
	
	$template->set_filenames(array('body' => 'tiket.tpl')); 
	$template->assign_vars(array(
	   'OUTPUT_TIKET'    				=>$output_tiket,
	   )
	);

	$template->pparse('body');	
?>