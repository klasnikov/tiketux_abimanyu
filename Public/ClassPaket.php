<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Paket{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Paket(){
		$this->ID_FILE="C-PKT";
	}
	
	//BODY
	
	function periksaDuplikasi($no_tiket){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_paket_periksa_duplikasi('$no_tiket') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah(
		$no_tiket, $kode_cabang, $kode_jadwal,
		$id_jurusan, $kode_kendaraan , $kode_sopir ,
		$tgl_berangkat, $jam_berangkat , $nama_pengirim ,
		$alamat_pengirim, $telp_pengirim,
		$nama_penerima, $alamat_penerima, $telp_penerima,
		$harga_paket,$keterangan_paket, $petugas_penjual,
		$komisi_paket_CSO, $komisi_paket_sopir,
		$kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
		$jumlah_koli,$berat,$jenis_barang,
		$layanan,$cara_bayar){
	  
		/*
		IS	: data paket belum ada dalam database
		FS	:Data paket baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_paket_tambah(
				'$no_tiket', '$kode_cabang', '$kode_jadwal',
				'$id_jurusan', '', '',
				'$tgl_berangkat', '$jam_berangkat', '$nama_pengirim',
				'$alamat_pengirim', '$telp_pengirim', NOW(),
				'$nama_penerima', '$alamat_penerima', '$telp_penerima',
				'$harga_paket','$keterangan_paket', '$petugas_penjual',
				'', '',
				'0', '$komisi_paket_CSO', '$komisi_paket_sopir',
				'$kode_akun_pendapatan', '$kode_akun_komisi_paket_CSO', '$kode_akun_komisi_paket_sopir',
				'$jumlah_koli','$berat','$jenis_barang',
				'$layanan','$cara_bayar',0);";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function pembatalan($no_tiket,$petugas_pembatalan){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_paket_batal('$no_tiket','$petugas_pembatalan',NOW());";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function  updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi=""){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"CALL sp_paket_update_cetak_tiket('$no_tiket','$jenis_pembayaran','$cabang_transaksi');";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($kondisi){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$kondisi	= ($kondisi=='')?1:$kondisi;
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PetugasPemberi) AS PetugasPemberi
			FROM tbl_paket
			WHERE $kondisi;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataDetail($no_tiket){
		
		/*
		Desc	:Mengembalikan data paket sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,
			f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan,
			f_user_get_nama_by_userid(PetugasPemberi) AS NamaPetugasPemberi
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function  updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$petugas_pemberi,$cabang_ambil){
	  
		/*
		IS	: data paket sudah ada dalam database
		FS	:Data paket dibatalkan 
		*/
		
		//kamus
		global $db;
		global $PAKET_CARA_BAYAR_DI_TUJUAN;
		
		//memeriksa Cara  bayar
		$sql = 
			"SELECT CaraPembayaran
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		$cara_bayar	= $row[0];
		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		if($cara_bayar!=$PAKET_CARA_BAYAR_DI_TUJUAN){
			$sql = 
				"CALL sp_paket_ambil_paket('$no_tiket','$nama_pengambil','$no_ktp_pengambil','$petugas_pemberi');";
		}
		else{
			$sql =
				"UPDATE tbl_paket
			  SET
			    NamaPengambil='$nama_pengambil',
			    NoKTPPengambil='$no_ktp_pengambil',
			    PetugasPemberi='$petugas_pemberi',
			    WaktuPengambilan=NOW(),
			    StatusDiambil=1,
					KodeCabang='$cabang_ambil'
			  WHERE
			    NoTiket = '$no_tiket'";
		}
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilDaftarHarga($id_jurusan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
				HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
				HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
				HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
				HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
				HargaPaket6KiloPertama,HargaPaket6KiloBerikut
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDaftarHarga
	
	function ambilHargaPaketByKodeLayanan($id_jurusan,$kode_layanan){
		
		/*
		Desc	:Mengembalikan data-data harga paket
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				HargaPaket".$kode_layanan."KiloPertama,HargaPaket".$kode_layanan."KiloBerikut
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDaftarHarga
}
?>