<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CSO,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_POST_VARS['tanggal_mulai'])?$HTTP_POST_VARS['tanggal_mulai']:$HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_POST_VARS['tanggal_akhir'])?$HTTP_POST_VARS['tanggal_akhir']:$HTTP_GET_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_user/laporan_uang_user_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= 
	"WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND PetugasCetakTiket='$userdata[user_id]'";
 
$kondisi_cari	=($cari=="")?"":
	" AND NoTiket LIKE '%$cari' 
	OR KodeBooking LIKE '%$cari'";
	
$kondisi	= $kondisi.$kondisi_cari;


			
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"NoTiket","tbl_reservasi","&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir",
$kondisi,"laporan_uang_user.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuCetakTiket,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal
	FROM 
		tbl_reservasi
	$kondisi
	ORDER BY WaktuPesan ASC LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row['FlagBatal']!=1){
			$status	="OK";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_cetak_tiket'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
					'no_tiket'=>$row['NoTiket'],
					'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
					'kode_jadwal'=>$row['KodeJadwal'],
					'nama'=>$row['Nama'],
					'no_kursi'=>$row['NomorKursi'],
					'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
					'discount'=>number_format($row['Discount'],0,",","."),
					'total'=>number_format($row['Total'],0,",","."),
					'tipe_discount'=>$row['JenisDiscount'],
					'status'=>$status
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_uang_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$sql=	
	"SELECT 
		f_laporan_user_get_summary($userdata[user_id],'$tanggal_mulai_mysql','$tanggal_akhir_mysql',0) AS total_tunai,	
		f_laporan_user_get_summary($userdata[user_id],'$tanggal_mulai_mysql','$tanggal_akhir_mysql',1) AS total_debit,
		f_laporan_user_get_summary($userdata[user_id],'$tanggal_mulai_mysql','$tanggal_akhir_mysql',2) AS total_kredit,
		f_laporan_user_get_total_discount($userdata[user_id],'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS total_discount";	


if ($result = $db->sql_query($sql)){
	while ($row = $db->sql_fetchrow($result)){
		$total_tunai	= $row['total_tunai'];
		$total_debit	= $row['total_debit'];
		$total_kredit	= $row['total_kredit'];
		$total_discount= $row['total_discount'];
	}
	
	$total_setor	= $total_tunai+$total_debit+$total_kredit;
	$total_omzet	= $total_setor+$total_discount;
	
	$summary = "
		<table width=1000>
			<tr><td colspan=4><h2>SUMMARY</h2></td></tr>
			<tr><td width='300'>
				<table width='300' class='border'>
					<tr>
						<td><font size=3>Tunai</font><td>
						<td width=150 align='right'>Rp. ".number_format($total_tunai,0,",",".")."</td>
					</tr>
				</table>
			</td>
			<td width=1 ></td>
			<td width='300'>
				<table width='300' class='border'>
					<tr>
						<td><font size=3>Debit Card</font><td>
						<td width=150 align='right'>Rp. ".number_format($total_debit,0,",",".")."</td>
					</tr>
				</table>
			</td>
			<td width=1></td>
			<td width='300'>
				<table width='300' class='border'>
					<tr>
						<td><font size=3>Kredit Card</font><td>
						<td width=150 align='right'>Rp. ".number_format($total_kredit,0,",",".")."</td>
					</tr>
				</table>
			</td></tr>
			<tr><td colspan=7></td></tr>
			<tr>
				<td>
					<table width='300' class='border'>
						<tr><td width=100><strong>Total Setor</strong></td><td width=150 align='right'><strong>Rp.".number_format($total_setor,0,",",".")."</strong></td></tr>
					</table>
				</td>
				<td width=1></td>
				<td>
					<table width='300' class='border'>
						<tr><td width=100><strong>Total Discount</strong></td><td width=150 align='right'><strong>Rp. ".number_format($total_discount,0,",",".")."</strong></td></tr>
					</table>
				</td>
				<td width=1></td>
				<td>
					<table width='300' class='border'>
						<tr><td width=100><strong>Total Omzet</strong></td><td width=150 align='right'><strong>Rp. ".number_format($total_omzet,0,",",".")."</strong></td></tr>
					</table>
				</td>
			</tr>
		</table>
	";
}
else{
	//die_error('Cannot Load laporan_uang_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('laporan_uang_user.'.$phpEx).'">Laporan Pencetakan Tiket</a>',
	'ACTION_CARI'		=> append_sid('laporan_uang_user.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'					=> $summary,
	'PAGING'				=> $paging
	)
);
	      

include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>