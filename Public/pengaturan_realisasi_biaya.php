<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassArea.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassRealisasiBiaya.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################
	$cabang	 		= "";
	$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya x

	//GET LIST CABANG
	$Cabang	 		= new Cabang();
	$Jurusan 		= new Jurusan();
	$RealisasiBiaya = new RealisasiBiaya();

	$template->set_filenames(array('body' => 'realisasi_biaya/realisasi_body.tpl'));

	$data_cabang		 = $Cabang->ambilDataDetail($cabang);
	$id_jurusan 		 = $HTTP_POST_VARS['idjurusan'];
	$tanggal_mulai  	 = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
	$tanggal_mulai		 = ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
	$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);

	function setComboCabangTujuan($cabang_asal,$cabang_dipilih)
	{
		//SET COMBO cabang
		global $db;
		global $Jurusan;

		$result=$Jurusan->ambilDataByCabangAsal($cabang_asal," FlagOperasionalJurusan IN (0,2,3)");
		$opt_cabang="";

		if($result)
		{
			while ($row = $db->sql_fetchrow($result))
			{
				$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
				$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
			}
		}
		else
		{
			echo("Error :".__LINE__);exit;
		}
		return $opt_cabang;
		//END SET COMBO CABANG
	}
	
	switch($mode)
	{
		case "get_tujuan": /*ACTION GET TUJUAN********************************************************************************/
		$cabang_asal		= $HTTP_GET_VARS['asal'];

		$opt_cabang_tujuan=
			"<select class='form-control input-sm' id='idjurusan' name='idjurusan'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";

		echo($opt_cabang_tujuan);

		exit; /*=============================================================================================================*/

		case "realisasi_dialog":
			$NoSPJ = $HTTP_GET_VARS['NoSPJ'];
			
			$template->assign_vars (
            array(
                'NO_SPJ' => $NoSPJ,
                ));
			$template->set_filenames(array('body' => 'realisasi_biaya/realisasi_dialog.tpl')); 
			$template->pparse('body');
		exit;

		case "simpan_realisasi":
			$NoSPJ  = $HTTP_GET_VARS['NoSPJ'];
			$biaya[1] = $HTTP_GET_VARS['tol'];
			$biaya[2] = $HTTP_GET_VARS['sopir'];
			$biaya[3] = $HTTP_GET_VARS['bbm'];
			$biaya[4] = $HTTP_GET_VARS['parkir'];

			for($i=1;$i<=4;$i++)
			{
				$sql = "update tbl_biaya_op set JumlahRealisasi = '$biaya[$i]', WaktuRealisasi = Now(),FlagRealisasi='1' where NoSPJ='$NoSPJ' and FlagJenisBiaya='$i'";
				$db->sql_query($sql);
			}
		exit;
	}	

	$sql = "select NoSPJ,TglTransaksi,NoPolisi,KodeSopir,KodeCabang,IdPetugas,IdJurusan,JenisRelease,FlagRealisasi,WaktuRealisasi from tbl_biaya_op where IdJurusan='$id_jurusan' and TglTransaksi='$tanggal_mulai_mysql' group by NoSPJ";

	$result = $db->sql_query($sql);
	$no=1;
	while($row=$db->sql_fetchrow($result))
	{
		$biaya_release		= $RealisasiBiaya->ambilBiayaRelease($row['NoSPJ']);
		$biaya_realisasi	= $RealisasiBiaya->ambilBiayaRealisasi($row['NoSPJ']);
		
		if($row[FlagRealisasi]!="1")
		{
			$script_action	= "setDialogRealisasi('$row[NoSPJ]');";
			$act 			= '<a onclick="'.$script_action.'">Realisasi</a>';
		}
		else
		{
			$act = "<div style='color:green'>Sudah Realisasi</div>";
		}
		
		$template->
	                assign_block_vars(
	                'ROW',
	                array(
	                	'NO'					=> $no,
	                    'no_spj'     			=> $row['NoSPJ'],
	                    'tgl_transaksi'   		=> $row['TglTransaksi'],
	                    'no_polisi'				=> $row['NoPolisi'],
	                    'kode_sopir'			=> $row['KodeSopir'],
	                    'id_petugas'			=> $row['IdPetugas'],
	                    'id_jurusan'			=> $row['IdJurusan'],
	                    'kode_cabang'			=> $row['KodeCabang'],
	                    'jenis_release'			=> ($row['JenisRelease']=="1"?"Storan Tiket":"Loket"),
	                    'waktu_realisasi'	 	=> $row['WaktuRealisasi'],
	                    'tol_release' 			=> ($biaya_release['1']!=""?$biaya_release['1']:"0"),
	                    'sopir_release' 		=> ($biaya_release['2']!=""?$biaya_release['2']:"0"),
	                    'bbm_release' 			=> ($biaya_release['3']!=""?$biaya_release['3']:"0"),
	                    'parkir_release' 		=> ($biaya_release['4']!=""?$biaya_release['4']:"0"),
	                    'tol_realisasi' 		=> ($biaya_realisasi['1']!=""?$biaya_realisasi['1']:"0"),
	                    'sopir_realisasi' 		=> ($biaya_realisasi['2']!=""?$biaya_realisasi['2']:"0"),
	                    'bbm_realisasi' 		=> ($biaya_realisasi['3']!=""?$biaya_realisasi['3']:"0"),
	                    'parkir_realisasi' 		=> ($biaya_realisasi['4']!=""?$biaya_realisasi['4']:"0"),
	                    'realisasi_action'		=> $act
	                    ));
	    $no++;
	}


	//$id_area=$Area->generateIdArea();

	//$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
	//$parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

	$template->assign_vars (
            array(
                'BCRUMP'    			  => '<ul id="breadcrumb"><li><a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a></li></ul>',
                'ACTION_CARI'			  => append_sid('pengaturan_realisasi_biaya.'.$phpEx),
                'idx_page'				  => $idx_page,
                'TGL_AWAL'				  => $tanggal_mulai,
                'OPT_CABANG'			  => $Cabang->setInterfaceComboCabang($cabang)
                ));


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
