<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Maskapai{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Maskapai(){
		$this->ID_FILE="C-MASKAPAI";
	}
	
	//BODY
	
	function periksaDuplikasi($KodeMaskapai){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS JumlahData FROM tbl_md_maskapai WHERE KodeMaskapai='$KodeMaskapai'";
				
		if (!$result = $db->sql_query($sql)){
		  die_error("Err:$this->ID_FILE".__LINE__);
		}

    $row = $db->sql_fetchrow($result);

    $ditemukan = ($row['JumlahData']<=0)?false:true;

		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($KodeMaskapai,$NamaMaskapai){
	  /*INSERT DATA BARU*/

		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "INSERT INTO tbl_md_maskapai (KodeMaskapai,NamaMaskapai) VALUES('$KodeMaskapai','$NamaMaskapai')";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	function ubah($KodeMaskapai,$NamaMaskapai,$KodeMaskapaiOld){
	  
		/*MENGUBAH DATA*/
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql ="UPDATE tbl_md_maskapai SET KodeMaskapai='$KodeMaskapai',NamaMaskapai='$NamaMaskapai' WHERE KodeMaskapai='$KodeMaskapaiOld'";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_maskapai
			WHERE KodeMaskapai IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus

  function ambilData($kondisi_tambahan=""){

    /*
    ID	:007
    Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $kondisi_tambahan = $kondisi_tambahan==""?"":" AND ".$kondisi_tambahan;

    $sql =
      "SELECT *
			FROM tbl_md_maskapai
			WHERE 1 $kondisi_tambahan
			ORDER BY Namamaskapai;";

    if (!$result = $db->sql_query($sql,TRUE)){
      die_error("Err:$this->ID_FILE ".__LINE__);
    }

    return $result;

  }//  END ambilData

	function ambilDataDetail($KodeMaskapai=""){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_maskapai
			WHERE KodeMaskapai='$KodeMaskapai';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
	}//  END ambilData

  function setComboMaskapai($MaskapaiDipilih="")
  {
    /*MENGEMBALIKAN STRING OPTION KOTA */

    global $db;

    $sql  = "SELECT * FROM tbl_md_maskapai ORDER BY NamaMaskapai ASC";

    if(!$result = $db->sql_query($sql))
    {
      echo("Err:".__LINE__);exit;
    }

    $opt_kota="";

    while($row=$db->sql_fetchrow($result))
    {
      $selected	=($MaskapaiDipilih!=$row['NamaMaskapai'])?"":"selected";
      $opt_kota .="<option value='$row[NamaMaskapai]' $selected>$row[NamaMaskapai]</option>";
    }

    return $opt_kota;
  }//END SET COMBO KOTA
}
?>