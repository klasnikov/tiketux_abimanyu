<?php
/**
 * Jadwal controller.
 *
 * Last update: Jan 10, 2013 09:51 AM
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
class JadwalController extends Controller
{
	private $_jadwalModel;
	private $_jurusanModel;

	public function __construct()
	{
		parent::__construct();

		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_jurusanModel 	= $this->loadModel('Jurusan');
	}

	/**
	 * Medapatkan jadwal keberangkatan untuk jurusan dan tanggal tertentu.
	 *
	 * URL: GET /jadwal
	 *
	 * Parameters:
	 * - jurusan = id jurusan
	 * - tanggal = tanggal keberangkatan, format: yyyy-mm-dd
	 * - min = Batas minimum tanggal jadwal ditampilkan
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function index()
	{
		$this->setRequestMethod('GET');

		$jurusan 	= $_GET['jurusan'];
		$tanggal    = $_GET['tanggal'];
		$jam 		= $_GET['jam'];
		$balik 		= $_GET['balik'];

		$data		= array('status' => 'ZERO_RESULTS');

		if (!empty($balik)) {
			$jurusanDetail 	= $this->_jurusanModel->getDetail($jurusan);

			if (empty($jurusanDetail)) {
				$this->sendResponse($data);
			}

			$kodeJurusan 		= $jurusanDetail->KodeJurusan; 
			$kodeJurusans 		= explode('-', $kodeJurusan);
			$kodeJurusanBalik 	= $kodeJurusans[1] . '-' . $kodeJurusans[0];

			$jurusanBalikDetail = $this->_jurusanModel->getDetailByKode($kodeJurusanBalik);
			$jurusan 			= $jurusanBalikDetail->IdJurusan;
			
			if (empty($jurusanBalikDetail)) {
				$jadwalUtama 		= $this->_jadwalModel->getJadwalUtama($jurusanDetail->IdJurusan);
				$jadwalUtamaDetail	= $this->_jadwalModel->getDetail($jadwalUtama);

				$kodeJurusanUtama 	= $jadwalUtamaDetail->KodeJurusan; 
				$kodeJurusansUtama 	= explode('-', $kodeJurusanUtama);

				$kodeJurusanBalik 	= $kodeJurusansUtama[1] . '-' . $kodeJurusansUtama[0];
				$jurusanBalikDetail = $this->_jurusanModel->getDetailByKode($kodeJurusanBalik);

				if (empty($jurusanBalikDetail)) {
					$this->sendResponse($data);
				}

				$jurusan 			= $jurusanBalikDetail->IdJurusan;
			}
		}

		$jadwalList      = $this->_jadwalModel->getListByJurusan($jurusan, $jam);
		$reservasiList   = $this->_jadwalModel->getReservasiList($jurusan, $tanggal);

		if (is_array($jadwalList) && sizeof($jadwalList)) {
			$data['status']	 = 'OK';
			$j = 0;

			for ($i = 0; $i < sizeof($jadwalList); $i++) {
				$kode 			= $jadwalList[$i]->KodeJadwal;
				$aktif			= $jadwalList[$i]->FlagAktif;
				$penjadwalan 	= $this->_jadwalModel->getPenjadwalan($jurusan, $kode, $tanggal);
				$jadwalDetail 	= $this->_jadwalModel->ambilDetailJadwal($kode);

				$filter_head  	= "''";
			 	$filter_tail  	= "''";

				if ($jadwalDetail['Rute'] != "") {
					$filter_head  	= "";
					$filter_tail  	= "";

 					$rute_arr 		= explode(",",$jadwalDetail['Rute']);
 					$key_asal  		= array_search($jadwalDetail["KodeCabangAsal"],$rute_arr);
 					$key_tujuan 	= array_search($jadwalDetail["KodeCabangTujuan"],$rute_arr);

 					//FILTER ASAL 
 					for ($k=0;$k <= $key_asal; $k++) {
   						$filter_head .="'".$rute_arr[$k]."',";
 					}

 					$filter_head  = $filter_head!=""?substr($filter_head,0,-1):"''";

 					//FILTER TUJUAN
 					$jumlah_transit = count($rute_arr);
 					$key_tujuan 	= $key_tujuan==""?$jumlah_transit:$key_tujuan;

 					for ($k = $key_tujuan; $k < $jumlah_transit; $k++) {
   						$filter_tail .="'".$rute_arr[$k]."',";
 					}

 					$filter_tail  = $filter_tail!=""?substr($filter_tail,0,-1):"''";
				}

				if (!empty($penjadwalan)) {
					if ($penjadwalan->StatusAktif == 0) {						
						continue;
					}
				} else {
					if ($aktif == 0) {
						continue;
					}
				}

				$kursiTersedia 	= $this->_jadwalModel->getKursiTersedia($jadwalDetail['KodeJadwalUtama'], $kode, $tanggal, $filter_head, $filter_tail);
				$jumlahBooking 	= sizeof($kursiTersedia);

				$kursi 			= (int) $jadwalList[$i]->JumlahKursi;
				$kursi			= ($kursi == 131 || $kursi == 132) ? 13 : $kursi;
				$kursi			= ($kursi == 101) ? 10 : $kursi;

				$jadwals[$j]['kode'] 	 		 = $kode;
				$jadwals[$j]['jam_berangkat'] 	 = $jadwalList[$i]->JamBerangkat;
				$jadwals[$j]['layout_kursi'] 	 = $jadwalList[$i]->JumlahKursi; //digunakan sebagai kode layout, layout didefinisikan di config/config_var.php
				$jadwals[$j]['layout_id'] 	 	 = $jadwalList[$i]->IdLayout; //digunakan sebagai kode layout, layout didefinisikan di config/config_var.php
				$jadwals[$j]['jumlah_kursi'] 	 = $kursi;
				$jadwals[$j]['jumlah_booking'] 	 = (int) $jumlahBooking;
				$jadwals[$j]['harga_tiket'] 	 	 = (int) $this->_getHargaTiket2($kode, $jadwalList[$i]->IdLayout, $tanggal);

				$j++;
			}

			$data['results'] = array('jadwal' 		=> $jadwals,
									 'hargaTiket'	=> (int) $this->_getHargaTiket($jurusan, $tanggal));
		}

		$this->sendResponse($data);
	}

	/**
	 * Medapatkan harga tiket untuk jurusan dan tanggal tertentu.
	 *
	 * URL: GET /jadwal
	 *
	 * Parameters:
	 * - jurusan = id jurusan
	 * - tanggal = tanggal keberangkatan, format: yyyy-mm-dd
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function hargatiket()
	{
		$this->setRequestMethod('GET');

		$jurusan 	= $_GET['jurusan'];
		$tanggal    = $_GET['tanggal'];

		$data            = array();
		$data['status']  = 'OK';
		$data['results'] = array('hargaTiket'	=> (int) $this->_getHargaTiket($jurusan, $tanggal));


		$this->sendResponse($data);
	}

	private function _getHargaTiket($jurusan, $tanggal)
	{
		global $cfg;

		$sql = "select f_jurusan_get_harga_tiket_by_id_jurusan($jurusan, '$tanggal') as harga";

		$res = 0;

		try {
			$this->dbObj->query($sql);

			$data = $this->dbObj->fetch();
			$res  = $data->harga;
		} catch (DbException $e) {
			Error::store('Harga', $e->getMessage());
		}

		return $res;
	}

	private function _getHargaTiket2($kode_jadwal,$id_layout,$tgl_berangkat)
	{
   		global $cfg;

		$sql 		= "SELECT f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat') as harga";
		$hargaTiket = "";

   		try {
			$this->dbObj->query($sql);

			$data 	= $this->dbObj->fetch();
			$harga  = $data->harga;

			$idx_layout   = strpos($harga,$id_layout);
   			$string_temp  = substr($harga,$idx_layout);
   			$idx_equal    = strpos($string_temp,"=");
   			$idx_delimiter= strpos($string_temp,";");
   			$hargaTiket   = substr($string_temp,$idx_equal+1,$idx_delimiter-$idx_equal-1);
		} catch (DbException $e) {
			Error::store('Harga', $e->getMessage());
		}

   		return $hargaTiket;
	}

	/**
	 * Mendapatkan jumlah booking untuk suatu kode keberangkatan
	 *
	 * @param array $list Daftar reservasi
	 * @param string $kode Kode jadwal
	 *
	 * @return int Jumlah booking
	 */
	private function _getJumlahBooking($list, $kode)
	{
		$res = 0;

		if (is_array($list) && sizeof($list)) {
			for ($i = 0; $i < sizeof($list); $i++) {
				if ($list[$i]->KodeJadwal == $kode) {
					$res = $list[$i]->JumlahBooking;

					break;
				}
			}
		}

		return $res;
	}

}

?>