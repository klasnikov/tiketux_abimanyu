<?php
/**
 * Reservasi controller.
 *
 * Last update: Jun 29, 2012 16:15
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class ReservasiController extends Controller
{
	private $_reservasiModel;
	private $_jadwalModel;
	private $_posisiModel;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->_reservasiModel = $this->loadModel('Reservasi');
		$this->_jadwalModel    = $this->loadModel('Jadwal');
		$this->_posisiModel    = $this->loadModel('Posisi');
	}

	/**
	 * Medapatkan nomor kursi yang telah dibooking.
	 *
	 * URL: GET /reservasi/kursi
	 *
	 * Parameters:
	 * - kodejadwal = kode jadwal, ex: DU-CPT06
	 * - tanggal = tanggal keberangkatan, format: yyyy-mm-dd
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: GET
	 */
	public function kursi()
	{
		$this->setRequestMethod('GET');
		//$this->authenticate(1);

		$kode       = $_GET['kodejadwal'];
		$tanggal    = $_GET['tanggal'];

		$data       = array();
		$results    = array();
		$status     = 'ZERO_RESULTS';

		$jadwalDetail 	= $this->_jadwalModel->ambilDetailJadwal($kode);

		$filter_head  	= "''";
		$filter_tail  	= "''";

		if ($jadwalDetail['Rute'] != "") {
			$filter_head  = "";
			$filter_tail  = "";

 			$rute_arr 	= explode(",",$jadwalDetail['Rute']);
 			$key_asal  	= array_search($jadwalDetail["KodeCabangAsal"],$rute_arr);
 			$key_tujuan = array_search($jadwalDetail["KodeCabangTujuan"],$rute_arr);

 			//FILTER ASAL 
 			for ($j=0;$j <= $key_asal; $j++) {
 				// if (empty($rute_arr[$j])) continue;
 						
   				$filter_head .= "'" . $rute_arr[$j] . "',";
 			}

 			$filter_head  = $filter_head!=""?substr($filter_head,0,-1):"''";

 			//FILTER TUJUAN
 			$jumlah_transit = count($rute_arr);
 			$key_tujuan 	= $key_tujuan==""?$jumlah_transit:$key_tujuan;

 			for ($j = $key_tujuan; $j < $jumlah_transit; $j++) {
 				// if (empty($rute_arr[$j])) continue;

   				$filter_tail .="'".$rute_arr[$j]."',";
 			}

 			$filter_tail  = $filter_tail!=""?substr($filter_tail,0,-1):"''";
		}

		$nomorKursi 	= $this->_jadwalModel->getKursiTersedia($jadwalDetail['KodeJadwalUtama'], $kode, $tanggal, $filter_head, $filter_tail);

		if (!empty($nomorKursi)) {
			$status	 = 'OK';
			$results = array('nomorkursi' => implode(",", $nomorKursi));
		}

		$data['results'] = $results;
		$data['status']  = $status;

		$this->sendResponse($data);
	}

	/**
	 * Insert data reservasi.
	 *
	 * URL: /reservasi/add
	 *
	 * Parameters:
	 * - kode_jadwal = Kode jadwal
	 * - tgl_berangkat = Tanggal keberangkatan (yyyy-mm-dd)
	 * - nama_pemesan = Nama pemesan
	 * - alamat_pemesan = Alamat pemesan
	 * - telp_pemesan = Telpon pemesan
	 * - nomor_kursi = Nomor kursi, dipisahkan koma (,)
	 * - nama_penumpang = Nama penumpang, dipisahkan koma (,)
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: POST
	 *
	 * Response Status
	 * - OK
	 * - ERROR
	 *
	 * Error messages:
	 * - Tidak ada kursi yang dipilih
	 * - Kursi sudah dibooking pengguna lain
	 * - Internal transaction error
	 */
	public function add()
	{
		$this->setRequestMethod('POST');
		//$this->authenticate(1);

		$kodeJadwal     = $_POST['kode_jadwal'];
		$tglBerangkat	= $_POST['tgl_berangkat'];
		$namaPemesan	= addslashes(strip_tags($_POST['nama_pemesan']));
		$alamatPemesan	= addslashes(strip_tags($_POST['alamat_pemesan']));
		$telpPemesan	= $_POST['telp_pemesan'];
		$nomorKursi		= $_POST['nomor_kursi']; //separated by coma (1,2,3)
		$idLayout 		= $_POST['id_layout'];
		$namaPenumpang  = addslashes(strip_tags($_POST['nama_penumpang'])); //separated by coma (1,2,3)
		$discount		= $_POST['diskon'];
		$discount		= (empty($discount)) ? 0 : $discount;
		$namaDiscount	= $_POST['nama_diskon'];
		$paycode		= $_POST['paycode'];
		$harga     		= $_POST['harga'];

		//pulang pergi
		$isPP 				= $_POST['is_pp'];
		$kodeJadwalPulang   = $_POST['kode_jadwal_pulang'];
		$tglPulang			= $_POST['tgl_pulang'];
		$nomorKursiPulang	= $_POST['nomor_kursi_pulang'];
		$idLayoutPulang 	= $_POST['id_layout_pulang'];
		$hargaPulang 		= $_POST['harga_pulang'];
		
		$data				= array('status' => 'ERROR');

		$nomorKursis    	= explode(',', $nomorKursi);
		$namaPenumpangs 	= explode(',', $namaPenumpang);

		if (!is_array($nomorKursis) && !sizeof($nomorKursis)) {
			$data['error'] = 'Tidak ada kursi yang dipilih';

			$this->sendResponse($data);
		}

		if (!empty($isPP)) {
			$nomorKursisPulang   = explode(',', $nomorKursiPulang);

			if (!is_array($nomorKursisPulang) && !sizeof($nomorKursisPulang)) {
				$data['error'] = 'Tidak ada kursi pulang yang dipilih';

				$this->sendResponse($data);
			}
		}

		try {
			$jadwal 		= $this->_jadwalModel->ambilDetailJadwal($kodeJadwal);
			$jadwalDetail 	= $jadwal;

			$filter_head  	= "''";
			$filter_tail  	= "''";

			if ($jadwalDetail['Rute'] != "") {
				$filter_head  	= "";
				$filter_tail  	= "";

 				$rute_arr 		= explode(",",$jadwalDetail['Rute']);
 				$key_asal  		= array_search($jadwalDetail["KodeCabangAsal"],$rute_arr);
 				$key_tujuan 	= array_search($jadwalDetail["KodeCabangTujuan"],$rute_arr);

 				//FILTER ASAL 
 				for ($k=0;$k <= $key_asal; $k++) {
   					$filter_head .="'".$rute_arr[$k]."',";
 				}

 				$filter_head  = $filter_head!=""?substr($filter_head,0,-1):"''";

 				//FILTER TUJUAN
 				$jumlah_transit = count($rute_arr);
 				$key_tujuan 	= $key_tujuan==""?$jumlah_transit:$key_tujuan;

 				for ($k = $key_tujuan; $k < $jumlah_transit; $k++) {
   					$filter_tail .="'".$rute_arr[$k]."',";
 				}

 				$filter_tail  = $filter_tail!=""?substr($filter_tail,0,-1):"''";
			}

			$kursiTerpilih = $this->_jadwalModel->getKursiTersedia($jadwalDetail['KodeJadwalUtama'], $kodeJadwal, $tglBerangkat, $filter_head, $filter_tail);

			if (is_array($kursiTerpilih) && sizeof($kursiTerpilih) > 0) {
				$kursiBooked = false;
				for ($k = 0; $k < sizeof($nomorKursis); $k++) {
					if (in_array($nomorKursis[$k], $kursiTerpilih)) {
						$kursiBooked = true;
						break;
					}
				}

				if ($kursiBooked) {
					$data['error']	= 'Kursi sudah dibooking pengguna lain';

					$this->sendResponse($data);
				}				
			}

			$this->dbObj->beginTrans();

			//PERGI
			$noTikets 		= array();

			// $jadwal			= $this->_jadwalModel->getDetail($kodeJadwal);
			
			$jamBerangkat 	= $jadwal['JamBerangkat'];
			$cabangAsal 	= $jadwal['KodeCabangAsal'];
			$cabangTujuan 	= $jadwal['KodeCabangTujuan'];
			$subJadwal 		= (empty($jadwal['FlagSubJadwal'])) ? '0' : $jadwal['FlagSubJadwal'];

			$kodeBooking	= $this->_createKodeBooking($namaPemesan . (rand(1, 10000) * $nomorKursis[0]));
			$hargaTiket		= $this->_getHargaTiket2($kodeJadwal, $idLayout, $tglBerangkat);

			$j = 0;

			for ($i = 0; $i < sizeof($nomorKursis); $i++) {
				$nomorTiket 	= $this->_createKodeTiket($namaPemesan . (rand(1, 10000) * $nomorKursis[$i]));
				$noTikets[$i] 	= $nomorTiket;

				$sql	= "CALL sp_reservasi_update_status_kursi($nomorKursis[$i],0, '$kodeJadwal', '$jamBerangkat', '$cabangAsal', '$cabangTujuan',$subJadwal, '$jadwal[KodeJadwalUtama]',
					'$tglBerangkat',0,600)";

				$sqlx = $sql;
				$this->dbObj->query($sql);

				//Insert ke tbl reservasi
				$sql 	=
							"CALL sp_reservasi_tambah_with_komisi(
							  '$nomorTiket', '".$jadwal['KodeCabangAsal']."', '$kodeJadwal',
							  ".$jadwal['IdJurusan'].", '', '',
							  '$tglBerangkat', '".$jadwal['JamBerangkat']."' , '$kodeBooking',
							  '', '', '".$namaPenumpangs[$i]."' ,
							  '$alamatPemesan', '$telpPemesan', '$telpPemesan',
							  NOW(), ".$nomorKursis[$i].",".$hargaTiket.",
							  '0', ".$hargaTiket.", '0',
							  '0', ".$hargaTiket.",0,
							  0, '', '','', 0, 0,
							  '', '', '',
							  '', 'T', '',
							  '',".$harga.");";

				$this->dbObj->query($sql);

				if ($discount != 0) {
					$values 	= array();

					$values[]	= "Discount 		= $discount";
					$values[]	= "Total 			= HargaTiket-Discount";
					$values[]	= "JenisDiscount 	= '$namaDiscount'";
					$values[]	= "JenisPenumpang 	= 'T'";

					$this->dbObj->updateRecord('tbl_reservasi', $values, array("KodeBooking = '$kodeBooking'", "CetakTiket = 0"));
				}

				$j++;
			}

			//PULANG
			if ($isPP) {
				$jadwal 		= $this->_jadwalModel->ambilDetailJadwal($kodeJadwalPulang);
				$jadwalDetail 	= $jadwal;

				$filter_head  	= "''";
				$filter_tail  	= "''";

				if ($jadwalDetail['Rute'] != "") {
					$filter_head  	= "";
					$filter_tail  	= "";

	 				$rute_arr 		= explode(",",$jadwalDetail['Rute']);
	 				$key_asal  		= array_search($jadwalDetail["KodeCabangAsal"],$rute_arr);
	 				$key_tujuan 	= array_search($jadwalDetail["KodeCabangTujuan"],$rute_arr);

	 				//FILTER ASAL 
	 				for ($k=0;$k <= $key_asal; $k++) {
	   					$filter_head .="'".$rute_arr[$k]."',";
	 				}

	 				$filter_head  = $filter_head!=""?substr($filter_head,0,-1):"''";

	 				//FILTER TUJUAN
	 				$jumlah_transit = count($rute_arr);
	 				$key_tujuan 	= $key_tujuan==""?$jumlah_transit:$key_tujuan;

	 				for ($k = $key_tujuan; $k < $jumlah_transit; $k++) {
	   					$filter_tail .="'".$rute_arr[$k]."',";
	 				}

	 				$filter_tail  = $filter_tail!=""?substr($filter_tail,0,-1):"''";
				}

				$kursiTerpilih = $this->_jadwalModel->getKursiTersedia($jadwalDetail['KodeJadwalUtama'], $kodeJadwalPulang, $tglPulang, $filter_head, $filter_tail);

				if (is_array($kursiTerpilih) && sizeof($kursiTerpilih) > 0) {
					$kursiBooked = false;
					for ($k = 0; $k < sizeof($nomorKursis); $k++) {
						if (in_array($nomorKursis[$k], $kursiTerpilih)) {
							$kursiBooked = true;
							break;
						}
					}

					if ($kursiBooked) {
						$data['error']	= 'Kursi sudah dibooking pengguna lain';

						$this->sendResponse($data);
					}				
				}
				
				//$jadwal			= $this->_jadwalModel->getDetail($kodeJadwalPulang);
				$jadwal 		= $this->_jadwalModel->ambilDetailJadwal($kodeJadwalPulang);
				$jamBerangkat 	= $jadwal['JamBerangkat'];
				$cabangAsal 	= $jadwal['KodeCabangAsal'];
				$cabangTujuan 	= $jadwal['KodeCabangTujuan'];
				$subJadwal 		= $jadwal['FlagSubJadwal'];

				$hargaTiket		= $this->_getHargaTiket2($kodeJadwalPulang, $idLayoutPulang, $tglPulang);

				for ($i = 0; $i < sizeof($nomorKursisPulang); $i++) {
					$nomorTiket 	= $this->_createKodeTiket($namaPemesan . (rand(1, 10000) * $nomorKursisPulang[$i]));
					$noTikets[$j] 	= $nomorTiket;

					//update status kursi  di tbl_posisi_detail
					$sql	= "CALL sp_reservasi_update_status_kursi(
								$nomorKursisPulang[$i],0, '$kodeJadwalPulang', '$jamBerangkat', '$cabangAsal', '$cabangTujuan',$subJadwal, '$jadwal[KodeJadwalUtama]',
								'$tglPulang',0,600)";

					$this->dbObj->query($sql);

					//Insert ke tbl reservasi
					$sql 	=
							"CALL sp_reservasi_tambah_with_komisi(
							  '$nomorTiket', '".$jadwal['KodeCabangAsal']."', '$kodeJadwalPulang',
							  ".$jadwal['IdJurusan'].", '', '',
							  '$tglPulang', '".$jadwal['JamBerangkat']."' , '$kodeBooking',
							  '', '', '".$namaPenumpangs[$i]."' ,
							  '$alamatPemesan', '$telpPemesan', '$telpPemesan',
							  NOW(), ".$nomorKursisPulang[$i].",".$hargaTiket.",
							  '0', ".$hargaTiket.", '0',
							  '0', ".$hargaTiket.",0,
							  0, '', '','', 0, 0,
							  '', '', '',
							  '', 'T', '',
							  '',".$harga.");";

					$this->dbObj->query($sql);	

					$j++;				
				}
			}

			$results = array('kode_booking' => $kodeBooking, 'no_tiket' => implode(',', $noTikets));

			if (!empty($paycode)) {
				$kodePayment = strtoupper($kodeBooking . substr(md5($kodeBooking), 0, 5));

				$results['kode_pembayaran'] = $kodePayment;

				$sql = "UPDATE
								tbl_reservasi
						SET
								PaymentCode='$kodePayment'
						WHERE
								KodeBooking='$kodeBooking'";

				$this->dbObj->query($sql);
			}

			$this->dbObj->commitTrans();

			$data['status']     = 'OK';
			$data['results']	= $results;
			$data['sql'] = $sqlx;
		} catch (DbException $ex) {
			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $ex->getMessage());

			$data['error']  = 'Internal transaction error ' . $ex->getMessage();
		}

		$this->sendResponse($data);
	}

	/**
	 * Update payment code di tbl reservasi.
	 *
	 * URL: /reservasi/paycode
	 *
	 * Parameters:
	 * - kode_booking = Kode booking
	 * - kode_payment = kode payment
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: POST
	 */
	public function paycode()
	{
		global $cfg;

		$this->setRequestMethod('POST');
		//$this->authenticate(1);

		$kodeBooking = $_POST['kode_booking'];
		$kodePayment = $_POST['kode_payment'];

		$data		 = array('status' => 'ERROR');

		try {
			$sql	 = "UPDATE
								tbl_reservasi
						SET
								PaymentCode='$kodePayment'
						WHERE
								KodeBooking='$kodeBooking'";

			$this->dbObj->query($sql);

			$data['status'] = 'OK';
		} catch (DbException $e) {
			Error::store('Reservasi', $e->getMessage());

			$data['error'] = 'Internal transaction error';
		}

		$this->sendResponse($data);
	}

	/**
	 * Update status pembayaran.
	 *
	 * URL: /reservasi/pay
	 *
	 * Parameters:
	 * - kode_booking = Kode booking
	 *
	 * Requires authentication: No
	 *
	 * Response formats:
	 * - json
	 * - xml
	 *
	 * HTTP method: POST
	 */
	public function pay()
	{
		global $cfg;

		$this->setRequestMethod('POST');
		//$this->authenticate(1);

		$kodeBooking = $_POST['kode_booking'];
		$kodePayment = $_POST['kode_payment'];
		$otp		 = $_POST['otp'];

		$data		 = array('status' => 'ERROR');

		try {
			$detail  = $this->_reservasiModel->getDetail($kodeBooking);

			$sql	 = "UPDATE
								tbl_reservasi
						SET
								CetakTiket=1,
								WaktuCetakTiket=Now(),
								PetugasCetakTiket=0,
								JenisPembayaran=99,
								otp='$otp'
						WHERE
								KodeBooking='$kodeBooking'
								AND
								PaymentCode='$kodePayment' ";

			$this->dbObj->query($sql);

			$sql	 = "UPDATE
								tbl_posisi_detail
						SET
								StatusBayar=1
						WHERE
								KodeBooking='$kodeBooking'";

			$this->dbObj->query($sql);

			$hargaTiket		= $detail[0]->HargaTiket;
			$jmlPenumpang	= sizeof($detail);
			$totalHarga		= $hargaTiket * $jmlPenumpang;
			$nama			= "Pembelian tiket $kodeBooking, Nama: " . addslashes($detail[0]->Nama)
							. ", Kode Jadwal " . $detail[0]->KodeJadwal . ", Tgl Berangkat: " . $detail[0]->TglBerangkat;

			$this->dbObj->query("CALL sp_deposit_debit('$kodeBooking', '$totalHarga', '$nama')");

			$data['status']  = 'OK';
		} catch (DbException $e) {
			Error::store('Reservasi', $e->getMessage());

			$data['error'] = 'Internal transaction error';
		}

		$this->sendResponse($data);
	}

	public function batal()
	{
		global $cfg;

		$this->setRequestMethod('POST');
		//$this->authenticate(1);

		$kodeBooking = $_POST['kode_booking'];

		$data		 = array('status' => 'ERROR');

		try {
			$sql	 = "SELECT
								*
						FROM
								tbl_reservasi
						WHERE
								KodeBooking='$kodeBooking'";

			$this->dbObj->query($sql);

			$rows	 = $this->dbObj->fetchAll();

			if (sizeof($rows)) {
				$this->dbObj->beginTrans();

				for ($i = 0; $i < sizeof($rows); $i++) {
					$noTiket	= $rows[$i]->NoTiket;
					$sql 		= "CALL sp_reservasi_batal('$noTiket',NULL,NOW());";

					$this->dbObj->query($sql);

					$sql = "UPDATE
									tbl_posisi_detail
							SET
									StatusKursi = 0, Nama=NULL, NoTiket=NULL,
									KodeBooking=NULL,Session=NULL,StatusBayar=0
							WHERE
									NoTiket='$noTiket';";

					$this->dbObj->query($sql);

					//$sql = "UPDATE
					//				tbl_posisi_detail_backup
					//		SET
					//				StatusKursi = 0, Nama=NULL, NoTiket=NULL,
					//				KodeBooking=NULL,Session=NULL,StatusBayar=0
					//		WHERE
					//				NoTiket='$noTiket';";
					//
					//$this->dbObj->query($sql);
				}

				$this->dbObj->commitTrans();

				$data['status']  = 'OK';
			} else {
				$data['error'] = 'Kode booking tidak valid';
			}

		} catch (DbException $e) {
			Error::store('Reservasi', $e->getMessage());

			$data['error'] = 'Internal transaction error';
		}

		$this->sendResponse($data);
	}

	/**
	 * Membuat kode booking
	 *
	 * @param string $data Data
	 *
	 * @return string Kode booking
	 */
	private function _createKodeBooking($data)
	{
		list($usec,$sec) = explode(' ',microtime());

		$str    = 'XTR' . dechex($usec).dechex($sec);
		$str	= substr(md5($str.$data).'4A',9,-18).'TTX';
		$val	= strtoupper($str);

		return $val;
	}

	/**
	 * Membuat nomor tiket
	 *
	 * @param string $data Data
	 *
	 * @return string Nomor tiket
	 */
	function _createKodeTiket($data)
	{
		list($usec,$sec) = explode(' ',microtime());

        $key    = 'XTR' . dechex($usec).dechex($sec);

		$data   = $data . $key;
		$str	= "TM".substr(md5($data).'4A',9,-18).'TTX';
		$val	= strtoupper($str);

		return $val;
	}

	private function _getHargaTiket($jadwal, $tanggal)
	{
		global $cfg;

		$sql = "select f_jurusan_get_harga_tiket_by_kode_jadwal('$jadwal', '$tanggal') as harga";

		$res = 0;

		try {
			$this->dbObj->query($sql);

			$data = $this->dbObj->fetch();
			$res  = $data->harga;
		} catch (DbException $e) {
			Error::store('Harga', $e->getMessage());
		}

		return $res;
	}

	private function _getHargaTiket2($kode_jadwal,$id_layout,$tgl_berangkat)
	{
   		global $cfg;

		$sql 		= "SELECT f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat') as harga";
		$hargaTiket = "";

   		try {
			$this->dbObj->query($sql);

			$data 	= $this->dbObj->fetch();
			$harga  = $data->harga;

			$idx_layout   = strpos($harga,$id_layout);
   			$string_temp  = substr($harga,$idx_layout);
   			$idx_equal    = strpos($string_temp,"=");
   			$idx_delimiter= strpos($string_temp,";");
   			$hargaTiket   = substr($string_temp,$idx_equal+1,$idx_delimiter-$idx_equal-1);
		} catch (DbException $e) {
			Error::store('Harga', $e->getMessage());
		}

   		return $hargaTiket;
	}

}
?>
