<?php
/**
 * Posisi model.
 *
 * Last update: Feb 19, 2012 14:20
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class PosisiModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Cek jika sudah ada entry posisi untuk kode jadwal dan tanggal keberangkatan tertentu
	 *
	 * @param string $kode Kode jadwal
	 * @param string $tgl Tanggal keberangkatan (yyyy-mm-dd)
	 *
	 * @return void
	 */
	public function exist($kode, $tgl)
	{
		global $cfg;

		$sql = "SELECT
						*
				FROM
						tbl_posisi
				WHERE
						KodeJadwal = '$kode'
						AND
						TglBerangkat = '$tgl'";

		$res = false;

		try {
			$this->_dbObj->query($sql);

			$res = ($this->_dbObj->getNumRows()) ? true : false;
		} catch (DbException $e) {
			Error::store('Posisi', $e->getMessage());
		}

		return $res;
	}
}
?>