<?php
/**
 * Cabang model.
 *
 * Last update: May 29, 2012 22:55
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

class CabangModel extends Model
{
	public function __construct()
	{
		parent::__construct();

		$this->_table = 'tbl_md_cabang';
	}

	/**
	 * Mendapatkan daftar cabang
	 *
	 * @return array Daftar cabang
	 */
	public function getList()
	{
		return $this->findAll();
	}
}
?>