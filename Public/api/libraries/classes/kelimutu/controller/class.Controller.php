<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: May 29, 2012, 15:40
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 */

/**
 * Registry class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.Registry.php');

/**
 * Registry class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.Loader.php');

/**
 * Exception class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.SystemException.php');

/**
 * Array to XML
 */
require_once(CLASS_DIR . '/kelimutu/utils/class.ArrayXML.php');

/**
 * Authentication class
 */
require_once(CLASS_DIR . '/kelimutu/auth/class.Auth.php');


/**
 * Controller class
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 *
 */
class Controller
{
    /**
     * Content type
     *
     * @var string
     */
    private $_contentType;

	/**
	 * Enable debug
	 */
	private $_enableDebug;

    /**
	 * Authentication object
	 */
	private $_authObj;

	/**
     * Database object
     */
    protected $dbObj;

	/**
	 * User id
	 */
	private $_userId;

	/**
	 * Log parameters
	 */
	private $_logParams;


    /**
     * Constructor.
     *
     * Create a new instance of this class
     */
    public function __construct()
    {
		global $cfg;

        $this->params = array();
        $this->dbObj  = Registry::get('db');

		if ($cfg['sys']['enableAuth']) {
			$this->_authObj	= new Auth();
		}

		$this->_initLog();
    }

    /**
     * Set content type
     *
     * @param string $type Content type (xml, json, etc)
     *
     * @return void
     */
    public function setContentType($type)
    {
        $this->_contentType = $type;
    }

	/**
	 * Get content type
	 *
	 * @return string Content type
	 */
	public function getContentType()
	{
		return $this->_contentType;
	}

	/**
	 * Enable debugging
	 *
	 * @param boolean $enable Enable debug
	 *
	 * @return void
	 */
	public function enableDebug($enable)
	{
		$this->_enableDebug = $enable;
	}

	/**
	 * Load model
	 *
	 * @param string $model Model
	 *
	 * @return object Model object
	 */
	protected function loadModel($model)
    {
        try {
            $className = $model . 'Model';

            Loader::loadClass($className, ROOT_DIR . '/models');

            if (class_exists($className)) {
                $modelObj  = new $className;

                return $modelObj;
            } else {
                app_shutdown("Model class <i>$className</i> does not exists!");
            }
        } catch (SystemException $e) {
            app_shutdown("Model file <i>$className</i> does not exists!");
        }
    }

	/**
	 * Send response
	 *
	 * @param mixed $data Data (response body)
	 * @param int $status HTTP status code
	 * $param string $contentType Content type ()
	 *
	 * @return void
	 */
	public function sendResponse($data, $status=200, $message='', $contentType='')
	{
		global $cfg;

		$this->dbObj->disconnect();

		$this->_contentType = (empty($contentType)) ? $this->_contentType : $contentType;
		$this->_contentType = strtolower($this->_contentType);

		if (!$this->_getContentType($this->_contentType)) {
			$status = 400;
			$data   = $this->_getStatusCodeMessage($status);
		}

		$statusHeader       = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);


		header($statusHeader);
		header('Content-type:' . $this->_getContentType($this->_contentType));

		if ($this->_contentType == 'json' || $this->_contentType == 'xml') {
			$data = (!$data) ? array() : $data;

			if ($status != 200) {
				$data = array('status' => 'ERROR', 'error' => $message);
			}

			$data   = array_merge($data, array('url'  => HTTP::getCurrentURL()));
			$data   = array_merge($data, array('time' => date('Y-m-d H:i:s')));
		} else if ($this->_contentType == 'html') {
			$site = $cfg['sys']['siteName'];
			$data = "<!DOCTYPE html><html><head><title>$site</title></head><body>$data</body></html>";
		}

		$errors = Error::getAll();

		if (sizeof($errors) && $this->_enableDebug && is_array($data)) {
			$data = array_merge($data, array('debug' => $errors));
		}

		if ($this->_contentType == 'xml') {
			$result = ArrayToXML::toXML($data, $cfg['sys']['responsePrefix']);
		} elseif ($this->_contentType == 'json') {
			$result = json_encode(array($cfg['sys']['responsePrefix'] => $data));
		} else {
			$result = $data;
		}

		die($result);
	}

	/**
	 * Parse log
	 *
	 * @return void
	 */
	public function parseLog()
    {
        $agents     = $_SERVER['HTTP_USER_AGENT'];
        $log        = explode("***", $agents);

        if (sizeof($log)) {
            $params = explode('&', $log[1]);

            for ($i = 0; $i < sizeof($params); $i++) {
                $keyval = explode('=', $params[$i]);

                if ($keyval[0] == 'manufacturer') {
                    $this->_logParams['client_manufacturer'] = $keyval[1];
                } elseif ($keyval[0] == 'model') {
                    $this->_logParams['client_model'] = $keyval[1];
                } elseif ($keyval[0] == 'sdk') {
                    $this->_logParams['client_sdk'] = $keyval[1];
                } elseif ($keyval[0] == 'os') {
                    $this->_logParams['client_os'] = $keyval[1];
                } elseif ($keyval[0] == 'type') {
                    $this->_logParams['client_type'] = $keyval[1];
                }
            }
        }
    }

	/**
	 * Get log parameter.
	 *
	 * @return array Log params
	 */
	public function getLogParams()
	{
		return $this->logParams;
	}

	/**
	 * Set http request method (GET, POST, PUT, DELETE)
	 *
	 * @param string $request Request type
	 *
	 * @return void
	 */
	protected function setRequestMethod($request)
	{
		$request		= strtoupper($request);
		$requestMethod	= $_SERVER['REQUEST_METHOD'];

		if ($requestMethod != $request) {
			$this->sendResponse('', 405, 'The requested method is not allowed');
		}
	}

	/**
	 * Authenticate incoming request
	 *
	 * @param int $level Authentication type. Where 1=authenticate request for non user protected resource,
	 *                   2 = authenticate for user protected source
	 *
	 * @return void
	 */
	protected function authenticate($type)
	{
		global $cfg;

		if (!$cfg['sys']['enableAuth']) return;

		if ($this->_authObj) {
			$status = $this->_authObj->authenticate($type);

			if ($status != 1) {
				$this->sendResponse('', $this->_authObj->getHttpStatusCode($status),
									$this->_authObj->getStatusMessage($status));
			} else {
				$this->_userId = $this->_authObj->getUserId();
			}
		}
	}

	/**
	 * Get user id
	 *
	 * @return string User id
	 */
	protected function getUserId()
	{
		return $this->_userId;
	}

	/**
	 * Fetch remote url using curl
	 *
	 * @param string $method Request method: GET or POST
	 * @param string $url Remote URL
	 * @param string $params Parameters for POST request
	 *
	 * @return string Response
	 */
	protected function callRemote($method, $url, $params='')
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}

		$response = curl_exec($ch);

		return $response;
	}

	/**
	 * Get http status message
	 *
	 * @param string $status Status code
	 *
	 * @return string Status message
	 */
	private function _getStatusCodeMessage($status)
    {
        $codes = array( 200 => 'OK',
                        400 => 'Bad Request',
                        401 => 'Unauthorized',
                        402 => 'Payment Required',
                        403 => 'Forbidden',
                        404 => 'Not Found',
						405 => 'Method Not Allowed',
                        500 => 'Internal Server Error',
                        501 => 'Not Implemented');

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

	/**
	 * Get http content type
	 *
	 * @param string $type Content type
	 *
	 * @return string Http content type
	 */
	private function _getContentType($type)
	{
		$types = array('xml' 	=> 'text/xml',
					   'json' 	=> 'application/json',
					   'html' 	=> 'text/html',
					   'text' 	=> 'text');

		return (isset($types[$type])) ? $types[$type] : '';
	}

	/**
	 * Get HTTP POST value
	 *
	 * @param string $key Key / parameter name
	 *
	 * @return mixed Parameter value
	 */
	protected function postParam($key)
    {
        return $this->_cleanXSS($_POST[$key]);
    }

	/**
	 * Get HTTP GET value
	 *
	 * @param string $key Key / parameter name
	 *
	 * @return mixed Parameter value
	 */
    protected function getParam($key)
    {
        return $this->_cleanXSS($_GET[$key]);
    }

	/**
	 * Clean xss attack
	 *
	 * @param mixed $val Value
	 *
	 * @return mixed Value
	 */
    private function _cleanXSS($val)
    {
        if (empty($val)) return '';

        # source from http://quickwired.com/kallahar/smallprojects/php_xss_filter_function.php
        $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
        $search = 'abcdefghijklmnopqrstuvwxyz';
        $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $search .= '1234567890!@#$%^&*()';
        $search .= '~`";:?+/={}[]-_|\'\\';
        for ($i = 0; $i < strlen($search); $i++) {
            $val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val);
            $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val);
        }
        $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
        $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
        $ra = array_merge($ra1, $ra2);
        $found = true;
        while ($found == true) {
            $val_before = $val;
            for ($i = 0; $i < sizeof($ra); $i++) {
                $pattern = '/';
                for ($j = 0; $j < strlen($ra[$i]); $j++) {
                    if ($j > 0) {
                        $pattern .= '(';
                        $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                        $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                        $pattern .= ')?';
                    }
                    $pattern .= $ra[$i][$j];
                }
                $pattern .= '/i';
                $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2);
                $val = preg_replace($pattern, $replacement, $val);
                if ($val_before == $val) {$found = false;}
            }
        }
        $allowedtags = "<strong><em><ul><li><pre><hr><blockquote><span>";
        $cstring = strip_tags($val, $allowedtags);
        $cstring = nl2br($cstring);
        return $cstring;
    }

	protected function formatArray($array)
    {
        if ($this->_contentType == 'xml') {
            return (sizeof($array)) ? $array : '';
        }

        return $array;
    }

	/**
	 * Init log
	 *
	 * @return void
	 */
	private function _initLog()
	{
		$this->_logParams = array('client_manufacturer' => '',
								  'client_model' => '',
								  'client_sdk' => '',
								  'client_os' => '',
								  'client_type' => '');
	}
}
?>