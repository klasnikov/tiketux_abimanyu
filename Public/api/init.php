<?php
/**
 * Kelimutu Rest-like PHP Framework
 *
 * init.php
 *
 * System initialization
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

date_default_timezone_set('Asia/Jakarta');

//define constants
$scriptFileName = preg_replace("/\/c/", " /home/sloki/spanel", $_SERVER['SCRIPT_FILENAME']);

define('ROOT_DIR',      str_replace("/usr", "", dirname(__FILE__)));
//define('ROOT_URL',      substr($_SERVER['PHP_SELF'], 0, - (strlen($scriptFileName) -1 - strlen(ROOT_DIR))));
define('ROOT_URL',      substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(ROOT_DIR))));
define('SERVER_NAME',   $_SERVER['SERVER_NAME']);
define('FILE_NAME',     substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '/') + 1));
define('LIB_DIR',       ROOT_DIR . '/libraries');
define('CLASS_DIR',     LIB_DIR  . '/classes');
define('FUNCTION_DIR',  LIB_DIR  . '/functions');
define('CONFIG_DIR',    ROOT_DIR . '/configs');

//echo ROOT_DIR;
include_once(FUNCTION_DIR . '/lib.php');

include_once CONFIG_DIR . '/config_system.php';
include_once CONFIG_DIR . '/config_db.php';
include_once CONFIG_DIR . '/config_var.php';

//class
include_once CLASS_DIR . '/kelimutu/system/class.Registry.php';
include_once CLASS_DIR . '/kelimutu/system/class.Loader.php';
include_once CLASS_DIR . '/kelimutu/system/class.Error.php';
include_once CLASS_DIR . '/kelimutu/database/class.DbConnection.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Dispatcher.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Controller.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Router.php';
include_once CLASS_DIR . '/kelimutu/model/class.Model.php';
include_once CLASS_DIR . '/kelimutu/transport/class.HTTP.php';
include_once CLASS_DIR . '/kelimutu/auth/class.Auth.php';

try {
    $dbObj = DbConnection::getInstance('MySQL');
    //$dbObj->setConnectionParameters('localhost', 'root', '', 'tiketux-xtrans', '3306');
    $dbObj->setConnectionParameters($cfg['db']['host'],
                                    $cfg['db']['user'],
                                    $cfg['db']['password'],
                                    $cfg['db']['name'],
                                    $cfg['db']['port']);
    $dbObj->connect();

    Registry::set('db', $dbObj);

    Router::add('/', array('controller' => 'Home', 'action' => 'index'));
} catch (KelimutuException $e) { die ($e->getMessage()); }
