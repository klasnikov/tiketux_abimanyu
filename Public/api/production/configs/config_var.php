<?php
/**
 * Global varible.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */


/**
 * rows = Jumlah baris
 * cols = Jumlah kolom
 * seat = layout kursi
 * type = P = Penumpang, K = Kosong, S = Sopir
 *
 */

$cfg['seat']['8'] =  array('rows'  => 4,
                           'cols'  => 3,
                           'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                            array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                            array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                            array('row' => 2, 'col' => 1, 'no' => '',  'type' => 'K'),
                                            array('row' => 2, 'col' => 2, 'no' => '2', 'type' => 'P'),
                                            array('row' => 2, 'col' => 3, 'no' => '3', 'type' => 'P'),

                                            array('row' => 3, 'col' => 1, 'no' => '4', 'type' => 'P'),
                                            array('row' => 3, 'col' => 2, 'no' => '',  'type' => 'K'),
                                            array('row' => 3, 'col' => 3, 'no' => '5', 'type' => 'P'),

                                            array('row' => 4, 'col' => 1, 'no' => '6', 'type' => 'P'),
                                            array('row' => 4, 'col' => 2, 'no' => '7', 'type' => 'P'),
                                            array('row' => 4, 'col' => 3, 'no' => '8', 'type' => 'P')
                                            )
                           );

$cfg['seat']['10']  = array('rows'  => 5,
                            'cols'  => 3,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 2, 'no' => '2', 'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '3', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '4',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 3, 'col' => 3, 'no' => '5',  'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '6', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 4, 'col' => 3, 'no' => '7', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '8', 'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '9',  'type' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '10', 'type' => 'P')
                                             )
                            );


$cfg['seat']['14']  = array('rows'  => 5,
                            'cols'  => 4,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '',   'type' => 'K'),
                                             array('row' => 2, 'col' => 2, 'no' => '2',  'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '3',  'type' => 'P'),
                                             array('row' => 2, 'col' => 4, 'no' => '4',  'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '5',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 3, 'col' => 3, 'no' => '6',  'type' => 'P'),
                                             array('row' => 3, 'col' => 4, 'no' => '7',  'type' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '8',  'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 4, 'col' => 3, 'no' => '9',  'type' => 'P'),
                                             array('row' => 4, 'col' => 4, 'no' => '10', 'type' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '11',   'type' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '12',   'type' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '13',   'type' => 'P'),
                                             array('row' => 5, 'col' => 4, 'no' => '14',   'type' => 'P'),
                                            )
                            );

$cfg['seat']['101']  = array('rows'  => 4,
                            'cols'  => 4,
                            'seat'  => array(array('row' => 1, 'col' => 1, 'no' => '1', 'type' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'type' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '',  'type' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '',  'type' => 'K'),
                                             array('row' => 2, 'col' => 2, 'no' => '2', 'type' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '3', 'type' => 'P'),
                                             array('row' => 2, 'col' => 4, 'no' => '4', 'type' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '5',  'type' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '',   'type' => 'K'),
                                             array('row' => 3, 'col' => 3, 'no' => '6',  'type' => 'P'),
                                             array('row' => 3, 'col' => 4, 'no' => '7',  'type' => 'P'),                                             

                                             array('row' => 4, 'col' => 1, 'no' => '8', 'type' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '',  'type' => 'K'),
                                             array('row' => 4, 'col' => 3, 'no' => '9', 'type' => 'P'),
                                             array('row' => 4, 'col' => 4, 'no' => '10', 'type' => 'P')
                                             )
                            );
?>