<?php
/**
 * Database config file.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

$cfg['db']['host']     = 'localhost';
$cfg['db']['name']     = 'tiketux_daytrans';
$cfg['db']['user']     = 'root';
$cfg['db']['password'] = 'SuperDew4IDCDB';
$cfg['db']['port']     = '3306';

// Development
// $cfg['db']['host']     = 'localhost';
// $cfg['db']['name']     = 'tiketux_daytrans_dev';
// $cfg['db']['user']     = 'root';
// $cfg['db']['password'] = 'SuperDew4IDCDB';
// $cfg['db']['port']     = '3306';
?>
