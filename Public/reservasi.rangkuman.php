<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CALL_CENTER'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$id_jurusan		= isset($HTTP_GET_VARS['idjurusan'])? $HTTP_GET_VARS['idjurusan'] : $HTTP_POST_VARS['idjurusan'];

// LIST
$template->set_filenames(array('body' => 'reservasi.rangkuman/index.tpl')); 

$tanggal_mulai		 = ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$cabang			     = $cabang==""?"CHP":$cabang;

//GET LIST CABANG
$Cabang	 		= new Cabang();
$Jurusan 		= new Jurusan();
$data_cabang	= $Cabang->ambilDataDetail($cabang);

function setComboCabangAsal($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;

	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;

	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal," FlagOperasionalJurusan IN (0,2,3)");
	$opt_cabang="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

switch($mode){
	case "get_tujuan": /*ACTION GET TUJUAN********************************************************************************/
	$cabang_asal		= $HTTP_GET_VARS['asal'];
	$id_jurusan			= $HTTP_GET_VARS['jurusan'];

		$opt_cabang_tujuan=
			"<select class='form-control input-sm' id='idjurusan' name='idjurusan'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";

		echo($opt_cabang_tujuan);

		exit; /*=============================================================================================================*/
}

$show_dashboard_cabang	= "";

$idx	= 0 ;

if($id_jurusan != ''){
	$data_jurusan = $Jurusan->ambilDataDetail($id_jurusan);

	$show_dashboard_cabang	.=
			"<table width='1300'>
			<tr><td colspan='2' style='font-size: 36px; text-align:center;'>$data_jurusan[NamaCabangAsal] ke $data_jurusan[NamaCabangTujuan]</td></tr>
			<tr>";

	$id_rewrite	= "rewrite_".$id_jurusan;

	$show_dashboard_cabang =
		"<td valign='top' align='center'>
			<span id='".$id_rewrite."_0'><img src='./templates/images/loading_bar.gif' /></span>
			<script type='text/javascript'>setDashboardCabang('".$id_jurusan."','".$id_rewrite."_0',1);</script>
		</td>";

	$show_dashboard_cabang .="<tr><td colspan='2'><br></td></tr></tr>";
}

$page_title = "Dashboard Operasional";

$template->assign_vars(array(
	'BCRUMP'    		=> '<ul id="breadcrumb"><li><a href="'.append_sid('main.'.$phpEx) .'">Home</a></li><li><a href="'.append_sid('reservasi.rangkuman.'.$phpEx).'">Rangkuman Reservasi</a></li></ul>',
	'ACTION_CARI'		=> append_sid('reservasi.rangkuman.'.$phpEx),
	'TGL_AWAL'			=> $tanggal_mulai,
	'LIST_DASHBOARD'    => $show_dashboard_cabang,
	'OPT_CABANG'		=> $Cabang->setInterfaceComboCabang($cabang)
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>