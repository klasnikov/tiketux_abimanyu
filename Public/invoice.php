<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] )
{  
  redirect(append_sid('index.'.$phpEx),true); 
}

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

if ($mode=='show')
{
	if ($submode=='admin'){
	$nm = $HTTP_POST_VARS['aa'];
	$sql = "SELECT user_id FROM tbl_user where username like '%$nm%'";
	if (!$result = $db->sql_query($sql)){
		die_error('Cannot Load username',__FILE__,__LINE__,$sql);
	}
	else
	{   
		//$template->assign_block_vars('mod_show',array());
		$i=1;
		//$template->assign_block_vars('ROW',array());
		while($row = $db->sql_fetchrow($result))
		{
			$sqla = "SELECT     kode0, Nama0, CONVERT(char(20), TGlPesan, 104), CONVERT(char(20), JamPEsan, 108), CONVERT(char(20), TGLBerangkat, 104) 
                      , CONVERT(char(20), JamBerangkat, 108), KodeJadwal
					  FROM         TbReservasi where idUser = $row[0]";

			if (!$resulta = $db->sql_query($sqla)){
				die_error('Cannot Load data',__FILE__,__LINE__,$sqla);
			}
			else
			{   
				while($rowa = $db->sql_fetchrow($resulta))
				{
					$odd ='odd';
			 		if (($i % 2)==0)
			 		{
			   			$odd = 'even';
					}
			 
					$template->set_filenames(array('body' => 'invoice_m.tpl')); 
					$template->assign_block_vars('ROW',array('odd'=>$odd,'no'=>$i,'book'=>$rowa[0],'name'=>$rowa[1],'kode'=>$rowa[6],
					'tglpsn'=>$rowa[2],'jampsn'=>$rowa[3],'tglbrk'=>$rowa[4],'jambrk'=>$rowa[5]));
					$i++;
				}
			}
		}
	}
	}
}

$template->set_filenames(array('body' => 'invoice.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  =>$userdata['username'],
   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('invoice.'.$phpEx).'">Invoice</a>',
   	'U_INVOICE' =>append_sid('invoice.'.$phpEx.'?mode=show&submode=admin'),
  ));
  include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
