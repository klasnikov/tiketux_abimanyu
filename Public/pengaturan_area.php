<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassArea.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

$mode=$HTTP_GET_VARS['mode'];

if($mode=="tambah_area")
{
	$id_area 		= $HTTP_POST_VARS['id_area'];
	$nama_area	 	= $HTTP_POST_VARS['nama_area'];
	$kota_area	 	= $HTTP_POST_VARS['kota_area'];
	$jurusan_area 	= $HTTP_POST_VARS['jurusan_area'];
	$biaya_jemput 	= $HTTP_POST_VARS['biaya_jemput'];
	$biaya_antar 	= $HTTP_POST_VARS['biaya_antar'];
	$is_bandara		= ($HTTP_POST_VARS['is_bandara'] !=1 ? 0 : 1);

	$idx_page = $HTTP_POST_VARS['page'];

	$sql = "insert into maps_area values ('$id_area','$nama_area','$kota_area','$jurusan_area','$biaya_jemput','$biaya_antar','$is_bandara')";
	
	$db->sql_query($sql);
	
	redirect(append_sid('pengaturan_area.'.$phpEx.'?page='.$idx_page,true));
}

elseif($mode=="hapus_area")
{
	$id_area=$HTTP_GET_VARS['id_area'];
	$sql = "delete from maps_area where id_area='$id_area'";
	$sql2 = "delete from maps_koordinat where id_area='$id_area'";

	$db->sql_query($sql);
	$db->sql_query($sql2);
	redirect(append_sid('pengaturan_area.'.$phpEx));
}
elseif($mode=="edit_area")
{
	$id_area 		= $HTTP_POST_VARS['id_area'];
	$nama_area		= $HTTP_POST_VARS['nama_area'];
	$kota_area		= $HTTP_POST_VARS['kota_area'];
	$jurusan_area	= $HTTP_POST_VARS['jurusan_area'];
	$biaya_jemput	= $HTTP_POST_VARS['biaya_jemput'];
	$biaya_antar 	= $HTTP_POST_VARS['biaya_antar'];
	$is_bandara 	= $HTTP_POST_VARS['is_bandara'];
 
	$idx_page = $HTTP_POST_VARS['page'];

	$sql = "update maps_area set nama_area='$nama_area',kota_area='$kota_area',jurusan_area='$jurusan_area',biaya_antar='$biaya_antar',biaya_jemput='$biaya_jemput',is_bandara='$is_bandara' where id_area='$id_area'";

	$db->sql_query($sql);
	 

	redirect(append_sid('pengaturan_area.'.$phpEx.'?page='.$idx_page,true));
}
else
{
	$Area = new Area();

	if($HTTP_POST_VARS["txt_cari"]!="")
	{
		$cari=$HTTP_POST_VARS["txt_cari"];
	}
	else
	{
		$cari=$HTTP_GET_VARS["cari"];
	}

	if($cari!='')
	{
		$kondisi_cari = 
		   "where
		    id_area LIKE '%$cari%' 
			OR nama_area LIKE '%$cari%' 
			OR biaya_jemput LIKE '%$cari%'";
	}
	else
	{
		$kondisi_cari='';
	}
	
	$sort_by	= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
	$sort_by 	= ($sort_by=='')?"id_area":$sort_by;

	$order		= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
	$order		= ($order=='')?"asc":$order;
		
	$VIEW_PER_PAGE= 10;
	$PAGE_PER_SECTION= 10;
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging	  = pagingData($idx_page,"id_area","maps_area","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi_cari,"pengaturan_area.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING==================================================	

	$sql = "select * from maps_area $kondisi_cari order by $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	$result = $db->sql_query($sql);
	$no=1;
	while($row=$db->sql_fetchrow($result))
	{
		$act 	="<input type='submit' value='Edit' style='background:transparent;color:orange;font-weight:bold;'> + ";
		$act   .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
		$template->
	                assign_block_vars(
	                'ROW',
	                array(		
	                	'NO'			=> $no+$idx_awal_record,
	                    'id_area'       => $row['id_area'],
	                    'nama_area'     => $row['nama_area'],
	                    'kota_area'     => $Area->getOptKota($row['kota_area']),
	                    'biaya_jemput'  => $row['biaya_jemput'],
	                    'biaya_antar'   => $row['biaya_antar'],
	                    'action'		=> $act,
	                    'opt_kota'		=> $Area->getOptKota($row['kota_area']),
	                    'opt_jurusan'   => $Area->getOptJurusan($row['jurusan_area']),
	                    'is_bandara'	=> ($row['is_bandara'] != 1) ? "" : "checked",
	                    ));
	    $no++;
	}


	$id_area=$Area->generateIdArea();

	$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
	$parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

	$template->set_filenames(array('body' => 'area/area_body.tpl'));
	$template->assign_vars (
            array(
                'BCRUMP'    			  => '<ul id="breadcrumb"><li><a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a></li><li><a href="'.append_sid('pengaturan_area.'.$phpEx) .'">Pengaturan Area</a></li></ul>',
                'ADD_ACTION'			  => append_sid('pengaturan_area.'.$phpEx.'?mode=tambah_area'),
                'ID_AREA'				  => $id_area,
                'EDIT_ACTION'			  => append_sid('pengaturan_area.'.$phpEx.'?mode=edit_area'),
                'PAGING'			      => $paging,
                'idx_page'				  => $idx_page,
                'A_SORT_1'				  => append_sid('pengaturan_area.'.$phpEx.'?sort_by=id_area'.$parameter_sorting),
                'A_SORT_2'				  => append_sid('pengaturan_area.'.$phpEx.'?sort_by=nama_area'.$parameter_sorting),
                'A_SORT_3'				  => append_sid('pengaturan_area.'.$phpEx.'?sort_by=biaya_jemput'.$parameter_sorting),
                'A_SORT_4'				  => append_sid('pengaturan_area.'.$phpEx.'?sort_by=biaya_antar'.$parameter_sorting),
                'OPT_KOTA'			      => $Area->getOptKota(),
                'OPT_JURUSAN'		      => $Area->getOptJurusan()
                ));
}


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
