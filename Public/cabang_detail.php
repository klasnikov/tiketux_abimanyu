<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$act    		= $HTTP_GET_VARS['act'];
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

function PeriksaKodeCabang($kode){
	global $db;
	
	$sql = "SELECT COUNT(kode)
						FROM	tbl_md_cabang
						WHERE kode='$kode'";
					
	if ($result = $db->sql_query($sql)){
			
		while ($row=$db->sql_fetchrow($result)){   
			$data_ditemukan=$row[0];
		}
	}
	else{
		die_error('GAGAL memeriksa Kode');//,__FILE__,__LINE__,$sql);exit;
	}
	
	//jika kode_point_rute belum ada, maka akan memberikan nilai valid (1), jika usernam sudah ada, akan memberikan nilai false (0)
	return (1-$data_ditemukan);
}

function setOptKota($kota){
	global $db;
	
	$sql = "SELECT NamaKota
						FROM	TbMDKotaMaster
					ORDER BY NamaKota";
					
	if ($result = $db->sql_query($sql)){
		$opt_kota;
		while ($row=$db->sql_fetchrow($result)){   
			$selected	=($row['NamaKota']==$kota)?"selected":"";
			
			$opt_kota .="<option value='$row[NamaKota]' $selected>$row[NamaKota]</option>";
		}
	}
	else{
		die_error('GAGAL Mengambil Kota');//,__FILE__,__LINE__,$sql);exit;
	}
	
	//jika kode_point_rute belum ada, maka akan memberikan nilai valid (1), jika usernam sudah ada, akan memberikan nilai false (0)
	return $opt_kota;
}

switch($mode){

//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':
		
	$pesan="Penambahan";
	
	$area=setOptKota("");
	
	$template->set_filenames(array('body' => 'cabang_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  =>$userdata['username'],
	   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('cabang.'.$phpEx).'">Cabang</a>',
			'OPT_KOTA'=>$area,
			'PESAN'	=>$pesan
		));
break;

//TAMBAH PENGGUNA BARU ==========================================================================================================
case 'tambah':
	
	//ambil isi dari parameter
	
	$kode		=$HTTP_GET_VARS['kode'];
	$area		=$HTTP_GET_VARS['kota'];
	$nama		=$HTTP_GET_VARS['nama'];
	$alamat	=$HTTP_GET_VARS['alamat'];
	$telp		=$HTTP_GET_VARS['telp'];
	$fax		=$HTTP_GET_VARS['fax'];
	
	//memeriksa  apakah kode_point_rute sudah ada dalam sistem atau belum
	if(!PeriksaKodeCabang($kode)){
		echo("duplikasi"); 
		exit;
	}
	
	$nama		=$HTTP_GET_VARS['nama'];
	$kota		=$HTTP_GET_VARS['kota'];
	
	$sql = 
		"INSERT INTO tbl_md_cabang(
			Area,Kode,Nama,Alamat,Telp,fax)
		VALUES (
			'$area','$kode','$nama','$alamat','$telp','$fax'
		);";
	
	if (!$result = $db->sql_query($sql)){
		die_error('Gagal menyimpan data');//,__FILE__,__LINE__,$sql);
	}
	
exit;

//UBAH POINT KOTA ==========================================================================================================
case 'ubah':
	
	//ambil isi dari inputan
	$kode_old	=$HTTP_GET_VARS['kode_old'];
	$kode			=$HTTP_GET_VARS['kode'];
	$area			=$HTTP_GET_VARS['kota'];
	$nama			=$HTTP_GET_VARS['nama'];
	$alamat		=$HTTP_GET_VARS['alamat'];
	$telp			=$HTTP_GET_VARS['telp'];
	$fax			=$HTTP_GET_VARS['fax'];
	
	
	//memeriksa  apakah kode_point_rute sudah ada dalam sistem atau belum
	if(!PeriksaKodeCabang($kode) && $kode!=$kode_old){
		echo("duplikasi"); 
		exit;
	}
	
	$sql = 
		"UPDATE
			tbl_md_cabang
		SET 
			kode='$kode',
			Area='$area',
			Nama='$nama',
			Alamat='$alamat',
			Telp='$telp',
			fax='$fax'
		WHERE kode='$kode_old'";
		
	if(!$result = $db->sql_query($sql)){
		//die_error('Gagal mengubah data anggota',__FILE__,__LINE__,$sql);
		die_error('Gagal mengubah data point_rute');
	}
	
exit;

//Ambil data anggota ==========================================================================================================
case 'ambil_data_cabang':
	
	$kode	= $HTTP_GET_VARS['kode'];
	
	$sql = 
		"SELECT 
			Area,Kode,Nama,Alamat,Telp,Fax
		FROM	tbl_md_cabang WHERE kode='$kode'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
			$kode			=$row['Kode'];
			$area			=setOptKota($row['Area']);
			$nama			=$row['Nama'];
			$alamat		=$row['Alamat'];
			$telp			=$row['Telp'];
			$fax			=$row['Fax'];
		}
		
	}
	else {
		die_error('Gagal mengamnbil data');
	}
	
	//memeriksa apakah data point_rute dengan kode_point_rute ditemukan dalam database
	if($kode==""){
		//jika tidak ditemukan, akan langsung didirect ke halaman point_rute
		redirect(append_sid('cabang.'.$phpEx),true); 
	}
	
	$pesan="Pengubahan";
	
	$template->set_filenames(array('body' => 'cabang_detail.tpl')); 
	$template->assign_vars(array
	  ( 'BCRUMP'	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('cabang.'.$phpEx).'">Cabang</a>',
			'PESAN'		=>$pesan,
			'KODE'		=>$kode,
			'OPT_KOTA'=>$area,
			'NAMA'		=>$nama,
			'ALAMAT'	=>$alamat,
			'TELP'		=>$telp,
			'FAX'			=>$fax
  ));
	
break;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>