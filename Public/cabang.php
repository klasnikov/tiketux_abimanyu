<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act    = $HTTP_GET_VARS['act'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'show';

switch($mode){

//TAMPILKAN MEMBER BARU ==========================================================================================================
case 'show':
	
	$cari	= $HTTP_POST_VARS['txt_cari'];
	
	if($cari!=''){
		$kondisi_pencarian =  " (kode LIKE '%$cari%')";
		$kondisi_pencarian .= " OR(Area LIKE '%$cari%')";
		$kondisi_pencarian .= " OR(Nama LIKE '%$cari%')";
		$kondisi_pencarian .= " OR(Alamat LIKE '%$cari%')";
	}
	
	$kondisi	= ($kondisi_pencarian=='')? '':'WHERE '.$kondisi_pencarian;
	
	$sql = 
		"SELECT 
			Kode,Nama,Area
		FROM	tbl_md_cabang ".$kondisi." ORDER BY Area,Nama";
				
	if (!$result = $db->sql_query($sql)){
		die_error('GAGAL mengambil data cabang');//,__FILE__,__LINE__,$sql);
	}
	else {
		$i = ($idx_halaman_sekarang-1)*$JUM_DATA_DITAMPILKAN;
		
		while ($row=$db->sql_fetchrow($result)){   
			$i++;
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
						
			$action = "<a href='".append_sid('cabang_detail.'.$phpEx)."&kode=$row[Kode]&mode=ambil_data_cabang'>Edit</a>+<a onclick=\"TanyaHapus('$row[Kode]');\" href='##'>Delete</a>";
			
			$template->set_filenames(array('body' => 'cabang.tpl'));        
			$template->assign_block_vars('ROW',
				array(
						'odd'	=>$odd,
						'no'	=>$i,
						'kode'=>$row['Kode'],
						'nama'=>$row['Nama'],
						'kota'=>$row['Area'],
						'act'	=>$action
					));
			
		}
		
		//jika tidak ditemukan data pada database
		if($i==0){
			$pesan=
				"<table width='100%' class='border'>
					<tr><td align='center' bgcolor='EFEFEF'>
						<font color='red'><strong>Data tidak ditemukan!</strong></font>
					</td></tr>
				</table><br><br>";
		}
	}
break;

//HAPUS MEMBER BARU ==========================================================================================================
case 'hapus':
	$kode    = $HTTP_GET_VARS['kode'];  
	
	$sql =
		"DELETE FROM tbl_md_cabang 
		WHERE (kode ='$kode')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('GAGAL menghapus data');
	}

exit;
}//switch mode

$template->set_filenames(array('body' => 'cabang.tpl')); 
$template->assign_vars(array
  ( 'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('cabang.'.$phpEx).'">Cabang</a>',
   	'U_ADD' =>'<a href="'.append_sid('cabang_detail.'.$phpEx) .'">Tambah Cabang</a>',
   	'U_USER_SHOW'=>append_sid('cabang.'.$phpEx.'?mode=show'),
		'CARI'	=>$cari,
		'SID'=>$userdata['sid'],
		'PESAN'	=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>