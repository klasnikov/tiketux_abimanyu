<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;
$Member					= new Member();

switch ($mode){
	case 'blank':
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='center'><h1>Silahkan gesekkan kartu member ke alat pembaca</h1></td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action='".append_sid('member_cek_saldo.'.$phpEx)."&mode=cek_saldo' method='post'>
							<input type='hidden' id='hdn_id_member' name='hdn_id_member' value='$id_member' />
							<input type='password' id='id_kartu' name='id_kartu' />
						</form>
					</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>ATAU</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>Silahkan masukkan no seri kartu member pada kolom dibawah ini</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu1' name='frm_input_kartu1' action='".append_sid('member_cek_saldo.'.$phpEx)."&mode=cek_saldo' method='post'>
							<input type='text' id='no_seri_kartu' name='no_seri_kartu' />
						</form>
					</td>
				</tr>
			</table>";
		
	break;
	
	//KONFIRMASI TRANSAKSI  =============================================================================================
	case 'cek_saldo':
		
		$id_kartu			= md5(trim($HTTP_POST_VARS['id_kartu']));		
		$no_seri_kartu= ($HTTP_POST_VARS['no_seri_kartu']);		
		
		if($no_seri_kartu!=""){
			$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
		}
		elseif($id_kartu!=""){
			$data_member=$Member->ambilDataByIdKartu($id_kartu);
		}
		
		if(count($data_member)>1 && $data_member['status_member']==1){
			$body =
				"<input type='hidden' id='hdn_flag' value=1 />
				<table width='400' border=0 class='border' bgcolor='white'>
					<tr>
						<td width='30%'><h3>ID MEMBER</h3></td><td width='5%'><h3>:</h3></td><td width='65%'><h3>$data_member[id_member]</h3></td>
					</tr>
					<tr>
						<td><h3>Nama<h3/></td><td><h3>:<h3/></td><td><h3>$data_member[nama]</h3></td>
					</tr>
					<tr>
						<td><h3>Kategori Member<h3/></td><td><h3>:<h3/></td><td><h3>$data_member[kategori_member]</h3></td>
					</tr>
					<tr>
						<td><h3>Alamat</h3></td><td><h3>:<h3/></td><td><h3>$data_member[alamat] $data_member[kota]</h3></td>
					</tr>
						<tr>
						<td><h3>Telp</h3></td><td><h3>:<h3/></td><td><h3>$data_member[telp_rumah] / $data_member[handphone]</h3></td>
					</tr>
					<tr>
						<td><h3>Deposit</h3></td><td><h3>:<h3/></td><td><h3>Rp. ".number_format($data_member['saldo'],0,",",".")."</h3></td>
					</tr>
					<tr>
						<td><h3>Point</h3></td><td><h3>:<h3/></td><td><h3>".number_format($data_member['point'],0,",",".")."</h3></td>
					</tr>
				</table>";
		}
		else{
			if($data_member['status_member']!=""){
				$pesan="STATUS KEANGGOTAAN ANDA SEDANG DI NON-AKTIFKAN";
			}
			else{
				$pesan="KARTU ANDA BELUM TERDAFTAR";
			}
				
			$body =
				"<input type='hidden' id='hdn_flag' value=1 />
				<table width='400' border=0 class='border' bgcolor='white'>
					<tr>
						<td align='center'><h1><font color='red'>$pesan</font></h1></td>
					</tr>
				</table>";
		}
					
	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_cek_saldo.tpl')); 
$template->assign_vars (
	array(
		'BODY'			=> $body
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>