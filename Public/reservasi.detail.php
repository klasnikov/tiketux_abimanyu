<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CALL_CENTER'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$id_jurusan		= isset($HTTP_GET_VARS['IdJurusan'])? $HTTP_GET_VARS['IdJurusan'] : $HTTP_POST_VARS['IdJurusan'];

// LIST
$template->set_filenames(array('body' => 'reservasi.detail/index.tpl')); 

$tanggal_mulai		 = ($tanggal_mulai!='')?$tanggal_mulai:date('d-m-y');
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$cabang			     = $cabang==""?"CHP":$cabang;

$Reservasi = new Reservasi();
$Cabang    = new Cabang();
$Jurusan   = new Jurusan();


$page_title = "Detail Reservasi";

function setComboCabangAsal($cabang_dipilih)
{
	//SET COMBO cabang
	global $db;
	global $Cabang;

	$result     = $Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang = "";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else
	{
		echo("Err :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih)
{
	//SET COMBO cabang
	global $db;
	global $Jurusan;

	$result = $Jurusan->ambilDataByCabangAsal($cabang_asal," FlagOperasionalJurusan IN (0,2,3)");
	$opt_cabang="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else
	{
		echo("Error :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

switch($mode)
{
	case "get_tujuan": /*ACTION GET TUJUAN********************************************************************************/
	$cabang_asal		= $HTTP_GET_VARS['asal'];
	$id_jurusan			= $HTTP_GET_VARS['jurusan'];

		$opt_cabang_tujuan=
			"<select class='form-control input-sm' id='IdJurusan' name='IdJurusan'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";

		echo($opt_cabang_tujuan);

		exit; /*=============================================================================================================*/
}

$show_dashboard_cabang	= "";

global $db;

$sql = "select KodeJadwal,JamBerangkat,
			   f_cabang_get_kota_by_kode_cabang(KodeCabangAsal) as CabangAsal,
			   f_cabang_get_kota_by_kode_cabang(KodeCabangTujuan) as CabangTujuan 
		from tbl_md_jadwal
		where 
		IdJurusan='$id_jurusan'";

if ($result = $db->sql_query($sql))
{
	$i = $idx_page*$VIEW_PER_PAGE+1;
	while ($row = $db->sql_fetchrow($result))
	{
		$template->
		assign_block_vars(
			'ROW',
				array(
					'HEADER' => "<tr>
									<td style='background:grey; color:white;' colspan='8'><h5>&nbsp;&nbsp;&nbsp;&nbsp;".$row['CabangAsal']." - ".$row['CabangTujuan']." ".$row['JamBerangkat']."</h5>
									</td>
								</tr>
								<tr><center>
									<th style='background:#da251c;color:white;text-align:center;width:30px;' >NO</th>
									<th style='background:#da251c;color:white;text-align:center;' >Nama</th>
									<th style='background:#da251c;color:white;text-align:center;' >Telp</th>
									<th style='background:#da251c;color:white;text-align:center;' >Alamat Jemput</th>
									<th style='background:#da251c;color:white;text-align:center;' >Alamat Antar</th>
									</center>
								</tr>",
					'BODY'	 => $Reservasi->getReservasi($row['KodeJadwal'],$tanggal_mulai_mysql),
				));
				
			$i++;
	}
} 
else
{
	//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 
		
$template->assign_vars(array(
	'BCRUMP'    		=> '<ul id="breadcrumb"><li><a href="'.append_sid('main.'.$phpEx) .'">Home</a></li><li><a href="'.append_sid('reservasi.rangkuman.'.$phpEx).'">Detail Reservasi</a></li></ul>',
	'ACTION_CARI'		=> append_sid('reservasi.detail.'.$phpEx),
	'OPT_CABANG'		=> $Cabang->setInterfaceComboCabang($cabang),
	'TGL_AWAL'			=> $tanggal_mulai
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>