<?php
 //
 // File Konfigurasi  
 //VERSI UPDATE: 29 September 2910 20:14
 //
 
 // PHP
 $phpEx = 'php';

 $dbms = "mysql";
 
 // langguage
 $lang['ENCODING']    = 'iso-8859-1';
 $lang['DIRECTION']   = 'ltr';
 $lang['LEFT']        = 'left';
 $lang['RIGHT']       = 'right';
 $lang['DATE_FORMAT'] = 'd M Y';
 $lang['Title']       = 'Travel Manager';
 $lang['Next']        = ' >';
 $lang['Previous']    = '< ';
 $lang['Goto_page']   = '';
 
 $lang['Error_login'] = 'Username tidak terdaftar atau password anda salah!';
 $lang['Click_return_login'] = 'Click <a href="./index.php">Here</a> To Return To Login';
 
 // Error codes
 define('GENERAL_MESSAGE', 'General');
 define('GENERAL_ERROR', 'Error');
 define('CRITICAL_MESSAGE', 'Critical');
 define('CRITICAL_ERROR', 'Critical'); 

 // public config
 $config['nama_aplikasi'] 			 	= "Abimanyu Travel";
 $config['nama_perusahaan']     	 	= "Abimanyu Travel";
 $config['perpage'] 				 	= 10; // jumlah item per halaman
 $config['repeatshowheader']		 	= 31; //BARIS
 $config['zip'] 					 	= FALSE;   // kita lakukan gzip encoding ?
 $config['protocol'] 				 	= '';
 $config['name']   					 	= '.';
 $config['script'] 					 	= '';	 
 $config['template'] 				 	= '';	 
 $config['jadwal'] 					 	= '';
 $config['range_show_sisa_kursi'] 	 	= 2; //jam
 $config['limit_kasbon_sopir']	   		= 500000; //Rupiah
 $config['email_to_rekon']    	   		= "";
 $config['email_to_name']     	   		= "Keuangan Abimanyu Travel";
 $config['email_host']        	   		= "smtp.gmail.com";
 $config['email_smtp_debug']  	   		= 2;
 $config['email_smtp_auth']   	   		= true;
 $config['email_smtp_secure'] 	   		= "ssl";
 $config['email_port']        	   		= 465;
 $config['email_username']    	   		= "keuangan@tiketux.com";
 $config['email_password']    	   		= "k3u4ng4n@tiketux";
 $config['email_display_name']	   		= "Keuangan PT. TBK";
 $config['limit_utang_rekon'] 	   		= 4000000;
 $config['limit_max_fee_rekon']    		= 180000;
 $config['key_token']         	   		= "indonesiatanahairbeta";
 $config['sms_to_tiketux']    	   		= "628170525609"; //no barton
 $config['masa_berlaku_voucher']   		= 30; //HARI
 $config['masa_berlaku_voucher_return'] = 30; //HARI
 $config['jenis_penumpang']				= array("U"=>"UMUM","M"=>"Mahasiswa dll","V"=>"Voucher","R"=>"Pergi-Pulang","MBR"=>"MEMBER");
 $config['kota_default']		     	=  "BANDUNG";

	$LIST_JENIS_PEMBAYARAN=array(
		"TUNAI",
		"DEBIT",
		"KREDIT",
		"VOUCHER");
		
	$LIST_JENIS_BIAYA=array(
		"JASA",
		"TOL",
		"SOPIR",
		"BBM",
		"PARKIR",
		"INSENTIF SOPIR",
    	"VOUCHER BBM",
		"TAMBAHAN BBM",
		"TAMBAHAN TOL",
		"LAINNYA");
	
	/*$LIST_POOL=array(
		"SETIABUDHI",
		"KARAWANG"
	);*/
	
	$FLAG_BIAYA_JASA	= 0;
	$FLAG_BIAYA_TOL		= 1;
	$FLAG_BIAYA_SOPIR	= 2;
	$FLAG_BIAYA_BBM		= 3;	
	$FLAG_BIAYA_PARKIR= 4;
	$FLAG_BIAYA_SOPIR_INSENTIF	= 5;
  	$FLAG_BIAYA_VOUCHER_BBM=6;
	$FLAG_BIAYA_TAMBAHAN_BBM=7;
	$FLAG_BIAYA_TAMBAHAN_TOL=8;
	$FLAG_BIAYA_TAMBAHAN_LAINNYA=9;
	
	$LEVEL_ADMIN					= 0.0;
	$LEVEL_MANAJEMEN			= 1.0;
	$LEVEL_MANAJER				= 1.2;
	$LEVEL_SUPERVISOR			= 1.3;
	$LEVEL_SUPERVISOR_OPS	= 1.4;
	$LEVEL_CSO						= 2.0;
	$LEVEL_CSO_PAKET			= 2.1;
	$LEVEL_SCHEDULER			= 3.0;
	$LEVEL_KASIR					= 4.0;
	$LEVEL_KEUANGAN				= 5.0;
	$LEVEL_CCARE					= 6.0;
	
	$USER_LEVEL= array(
		'0.0'=>"Admin",
		'1.0'=>"Manajemen",
		'1.2'=>"Manajer",
		'1.3'=>"Spv.Reservasi",
		'1.4'=>"Spv.Operasional",
		'2.0'=>"CSO",
		'2.1'=>"CSO Paket",
		'2.2'=>"CSO2",
		'3.0'=>"Scheduler",
		'4.0'=>"Kasir",
		'5.0'=>"Keuangan",
		'6.0'=>"Customer Care",
		'7.0'=>"Mekanik",
		'8.0'=>"Checker",
		'9.0'=>"Picker",
    "ADMIN"				=> 0.0,
    "MANAJEMEN"			=> 1.0,
    "MANAJER"			=> 1.2,
    "SPV_RESERVASI"		=> 1.3,
    "SPV_OPERASIONAL"	=> 1.4,
    "CSO"				=> 2.0,
    "CSO_PAKET"			=> 2.1,
    "CSO2"				=> 2.2,
    "SCHEDULER"			=> 3.0,
    "KASIR"				=> 4.0,
    "KEUANGAN"			=> 5.0,
    "CUSTOMER_CARE"		=> 6.0,
    "MEKANIK"			=> 7.0,
    "CHECKER"			=> 8.0,
    "PICKER"			=> 9.0);
	
	$USER_LEVEL_INDEX= array(
		"ADMIN"				=>0.0,
		"MANAJEMEN"			=>1.0,
		"MANAJER"			=>1.2,
		"SPV_RESERVASI"		=>1.3,
		"SPV_OPERASIONAL" 	=>1.4,
		"CSO"				=>2.0,
		"CSO_PAKET"			=>2.1,
		"CSO2"				=>2.2,
		"SCHEDULER"			=>3.0,
		"KASIR"				=>4.0,
		"KEUANGAN"			=>5.0,
		"CUSTOMER_CARE"		=>6.0,
		"MEKANIK"			=>7.0,
		"CHECKER"			=>8.0,
		"PICKER"			=>9.0);
	
	$TOLERANSI_KEBERANGKATAN=125; //menit
	
	$TOP_UP_AWAL=150000; //menit
	$BIAYA_PENDAFTARAN=10000; //menit
	
	//PAGING
	$VIEW_PER_PAGE=50;
	$PAGE_PER_SECTION=7;
	$MAX_DATA_EXPORTED_PER_PAGE=1;
	
	//LAYOUT MAKSIMUM UNTUK TBL_POSISI
	$LAYOUT_MAKSIMUM=17;
	$LAYOUT_KURSI_DEFAULT=8;
	
	//INSENTIF SOPIR
	$INSENTIF_SOPIR_JUMLAH=2500;
	$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM=6;
	$INSENTIF_SOPIR_LAYOUT_MAKSIMUM=20;
	
	//INTENSIF PAKET
	$INSENTIF_SOPIR_PAKET_JUMLAH=2500;
	$INSENTIF_SOPIR_JUMLAH_PAKET_MINIMUM=6;
	$INSENTIF_SOPIR_JUMLAH_PAKET_MAKSIMUM=20;
	
	//PENGATURAN MEMBER
	$MINIMUM_KEBERANGKATAN_JADI_MEMBER	= 3;
	$THRESHOLD_MEMBER_EXPIRED	= 90; //hari sebelum expired
	
	//PENGATURAN PAKET
	$LIST_JENIS_PEMBAYARAN_PAKET=array(
		"TUNAI",
		"LANGGANAN",
		"BAYAR DI TUJUAN");
	
	$PAKET_CARA_BAYAR_TUNAI		= 0;
	$PAKET_CARA_BAYAR_LANGGANAN	= 1;
	$PAKET_CARA_BAYAR_DI_TUJUAN	= 2;
	
	$LIST_JENIS_LAYANAN_PAKET=array(
		"1"=>"DOKUMEN",
		"2"=>"BARANG",
		"3"=>"BAGASI");
	
	//SESSION TIME
	$SESSION_TIME_EXPIRED	= 600; // Detik
	$JUMLAH_MINIMUM_DISCOUNT_GROUP	= 4;
  
  #KONFIGURASI SMS GATEWAY
	$sms_config['url']				= "http://sms.3trust.com:88/";
	//$sms_config['pengirim']		= "BIMOTRANS";
	//$sms_config['pengirim2']		= "%2B3125689652";
	$sms_config['user']				= "tiketux";
	$sms_config['password']			= "Or10n";
	$sms_config['range_reminder']	= 35;
	$HEADER_NO_TELP					= array("02","03","04","05","06","07","08","09");
 
?>
