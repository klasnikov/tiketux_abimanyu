<?php
require_once('includes/sha256.inc.php');

class WebService{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $LIST_RESPONSE;
	var $URL_PARAMETER_RECEIVER;
	//CONSTRUCTOR
	function WebService(){
		$this->ID_FILE="WS";
		
		$this->LIST_RESPONSE	= array(
			0	=> "Gagal", 
			1	=> "Berhasil",
			2	=> "Sudah dibayar",
			3	=> "Expired",
			4	=> "Salah format",
			5	=> "Mal function",
			6	=> "Tidak terdaftar",
			"GAGAL"						=>0,
			"BERHASIL"				=>1,
			"SUDAH_DIBAYAR"		=>2,
			"EXPIRED"					=>3,
			"SALAH_FORMAT"		=>4,
			"MAL_FUNCTION"		=>5,
			"TIDAK_TERDAFTAR"	=>6
		);
		
		$this->URL_PARAMETER_RECEIVER	= "http://111.68.113.35/parameter_receiver.php";
		//$this->URL_PARAMETER_RECEIVER	= "http://localhost/tiketuxmobile/parameter_receiver.php";
	}
	
	//BODY
	function ucase($text){
		$len_str	= strlen($text);
		
		$my_return	= "";
		
		for($i=0;$i<$len_str;$i++){
			$my_return.= (ord($text[$i])>=97 && ord($text[$i])<=122)?chr(ord($text[$i])-32):$text[$i];
		}
		
		return $my_return;
	}
	
	function generateSignature($id_alur,$password,$tgl_berangkat="",$jumlah_tiket="",$kode_asal="",$kode_tujuan="",$kode_jadwal="",$jumlah_bayar=""){
		$singnature = $this->ucase($this->ucase(hash('sha256',$this->ucase($id_alur).'%'.$this->ucase($password).'%'.$tgl_berangkat.'%'.$jumlah_tiket.'%'.$this->ucase($kode_asal.$kode_tujuan.$kode_jadwal).'%'.$jumlah_bayar)));
		return $singnature;
	}
	
	function sendHttpPost($url,$parameter){
		
		$length = strlen($parameter);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
			curl_setopt($ch, CURLOPT_ENCODING, "");
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			return $response;
	}
	
	function formatTglRawToTglShort($tgl_raw){
		$bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
						'1' => 'jan',
  					'2' => 'feb',
  					'3' => 'mar',
  					'4' => 'apr',
  					'5' => 'may',
  					'6' => 'jun',
  					'7' => 'jul',
  					'8' => 'aug',
  					'9' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec'
  				);
		
		$tgl	= substr($tgl_raw,0,2);
		$bln	= substr($tgl_raw,2,2);
		$bln	= $bulan[$bln];
		$thn	= substr($tgl_raw,4,2);
		
		return $tgl."-".$bln."-".$thn;
	}
	
	function formatTglToMysqlDate($tgl_raw){
		
		$tgl	= substr($tgl_raw,0,2);
		$bln	= substr($tgl_raw,2,2);
		$thn	= substr($tgl_raw,4,2);
		
		return "20".$thn."-".$bln."-".$tgl;
	}
	
	//MENGENAI ASAL KEBERANGKATAN=====================================================
	function sendRequestPointAsal($password){
		global $URL_PARAMETER_RECEIVER;
		
		$id_alur	= "1";
		
		$my_signature	= $this->generateSignature($id_alur,$password);
		
		$parameter_dikirim	= "id_alur=$id_alur&signature=$my_signature";
		
		return $this->sendHttpPost($this->URL_PARAMETER_RECEIVER,$parameter_dikirim);
	}
	
	function getPointAsal($action="",$kota=""){
		
		global $db;
		
		$kondisi_kota	= ($kota=="")?"":" AND Kota='$kota'";
		
		$sql = 
				"SELECT *
				FROM tbl_md_cabang
				WHERE FlagAgen!=1 $kondisi_kota
				ORDER BY Kota,Nama ASC";
		
		if($result = $db->sql_query($sql)){
			
			$kota="";
			
			while ($row = $db->sql_fetchrow($result)){
				
				if($kota!=$row['Kota']){
					
					$kota=$row['Kota'];
					$opt_asal .="<div class='judul_menu'>$kota</div>";
				}
				
				$temp_action	= str_replace("#id","\"$row[KodeCabang]\"",$action);
				$opt_asal .=
					"<div class='menu_pilihan'>
						<a class='menu' href='#' onClick='$temp_action;return false;'>$row[Nama]</a><br>
						<span class='noticeMessage'>$row[Alamat]</span>
					</div>";
			}
		}
		else{
			echo("Error :".__LINE__);exit;
		}		
		
		return $opt_asal;
	}
	
	//MENGENAI CABANG===================
	function sendRequestCabangDetail($kode_cabang,$password){
		global $URL_PARAMETER_RECEIVER;
		
		$id_alur	= "20";
		
		$my_signature	= $this->generateSignature($id_alur,$password);
		
		$parameter_dikirim	= "id_alur=$id_alur&kode_cabang=$kode_cabang&signature=$my_signature";
		
		return $this->sendHttpPost($this->URL_PARAMETER_RECEIVER,$parameter_dikirim);
	}
	
	
	function getCabangDetail($kode_cabang){
		
		global $db;
		
		$sql = 
				"SELECT *
				FROM tbl_md_cabang
				WHERE KodeCabang='$kode_cabang'";
		
		if(!$result = $db->sql_query($sql)){
			return null;
		}		
		
		$row = $db->sql_fetchrow($result);
		
		if($db->sql_numrows()>0){
			return $row['KodeCabang']."#".$row['Nama']."#".$row['Alamat']."#".$row['Kota'];
		}
		else{
			return null;
		}
	}	
	
	function sendRequestAsalTujuanDetail($id_jurusan,$password){
		global $URL_PARAMETER_RECEIVER;
		
		$id_alur	= "21";
		
		$my_signature	= $this->generateSignature($id_alur,$password);
		
		$parameter_dikirim	= "id_alur=$id_alur&id_jurusan=$id_jurusan&signature=$my_signature";
		
		return $this->sendHttpPost($this->URL_PARAMETER_RECEIVER,$parameter_dikirim);
	}
		
	function getAsalTujuanDetail($id_jurusan){
		
		global $db;
		
		$sql = 
				"SELECT KodeCabangAsal,KodeCabangTujuan,HargaTiket,HargaTiketTuslah
				FROM tbl_md_jurusan
				WHERE IdJurusan='$id_jurusan'";
		
		if(!$result = $db->sql_query($sql)){
			return null;
		}		
		
		$row = $db->sql_fetchrow($result);
		
		if($db->sql_numrows()<=0){
			return null;
		}
		
		$kode_cabang_asal		= $row['KodeCabangAsal'];
		$kode_cabang_tujuan	= $row['KodeCabangTujuan'];
		$harga_tiket				= $row['HargaTiket'];
		$harga_tiket_tuslah	= $row['HargaTiketTuslah'];
		
		//ambil data cabang asal
		$sql = 
				"SELECT KodeCabang,Nama,Alamat,Kota
				FROM tbl_md_cabang
				WHERE KodeCabang IN('$kode_cabang_asal','$kode_cabang_tujuan')";
		
		if(!$result = $db->sql_query($sql)){
			return null;
		}		
		
		while ($row = $db->sql_fetchrow($result)){
			if($row['KodeCabang']==$kode_cabang_asal){
				$data_cabang_asal		= $row['KodeCabang']."#".$row['Nama']."#".$row['Alamat']."#".$row['Kota'];
			}
			else{
				$data_cabang_tujuan	= $row['KodeCabang']."#".$row['Nama']."#".$row['Alamat']."#".$row['Kota'];
			}
		}
		
		return $data_cabang_asal."|".$data_cabang_tujuan."|".$harga_tiket."|".$harga_tiket_tuslah;
	}
	
	//MENGENAI TUJUAN =============================================
	function sendRequestPointTujuan($asal, $password){
		
		$id_alur	= "2";
		
		$my_signature	= $this->generateSignature($id_alur,$password);
		
		$parameter_dikirim	= "id_alur=$id_alur&point_asal=$asal&signature=$my_signature";
		
		return $this->sendHttpPost($this->URL_PARAMETER_RECEIVER,$parameter_dikirim);
	}
	
	function getPointTujuan($action="",$point_asal=""){
	  global $db;
		
		$kondisi_point_asal	= ($point_asal!="")?" WHERE KodeCabangAsal='$point_asal'":"";
		
		$sql = 
			"SELECT IdJurusan, KodeCabangTujuan,KodeJurusan
			FROM tbl_md_jurusan
			$kondisi_point_asal
			ORDER BY KodeJurusan;";
		
		$idx=0;
		
	  if (!$result = $db->sql_query($sql)){
			return "Silahkan coba kembali beberapa saat lagi...";
		} 
		else{
			if($db->sql_numrows()<=0){
				return "<br>Tidak ada jurusan untuk point keberangkatan ini";
			}
			
			while ($row = $db->sql_fetchrow($result)){
	      $list_kode_cabang	.= "'$row[KodeCabangTujuan]',";
				
				$data_jurusan[$idx]['IdJurusan']		= $row['IdJurusan'];
				$data_jurusan[$idx]['KodeCabang']		= $row['KodeCabangTujuan'];
				$data_jurusan[$idx]['KodeJurusan']	= $row['KodeJurusan'];
				
				$idx++;
	    }    
	  }
		
		//get data cabang
		$list_kode_cabang	= substr($list_kode_cabang,0,-1);
		
		$sql = 
				"SELECT *
				FROM tbl_md_cabang
				WHERE FlagAgen!=1 AND KodeCabang IN($list_kode_cabang)
				ORDER BY Kota,Nama ASC";
		
		if (!$result = $db->sql_query($sql)){
			return "Silahkan coba kembali beberapa saat lagi... $sql";
		} 
		else{
			while ($row = $db->sql_fetchrow($result)){
				$data_cabang[$row['KodeCabang']]['Nama']		= $row['Nama'];
				$data_cabang[$row['KodeCabang']]['Alamat']	= $row['Alamat'];
				$data_cabang[$row['KodeCabang']]['Kota']		= $row['Kota'];
	    }    
	  }
		
		$kota = "";
		
		for($idx=0;$idx<count($data_jurusan);$idx++){
			$kode_cabang	= $data_jurusan[$idx]['KodeCabang'];
			$nama					= $data_cabang[$kode_cabang]['Nama'];
			$alamat				= $data_cabang[$kode_cabang]['Alamat'];
			$kota_now			= $data_cabang[$kode_cabang]['Kota'];
			
			if($kota!=$kota_now){
					
					$kota=$kota_now;
					$opt .="<div class='judul_menu'>$kota</div>";
				}
				
				$temp_action	= str_replace("#id","\"".$data_jurusan[$idx]['IdJurusan']."\"",$action);
				$opt .=
					"<div class='menu_pilihan'>
						<a class='menu' href='#' onClick='$temp_action;return false;'>$nama</a><br>
						<span class='noticeMessage'>$alamat</span>
					</div>";
		}		
		
		return $opt;
	}
	
	function getCabangTujuanByIdJurusan($id_jurusan){
		
		global $db;
		
		$sql = 
				"SELECT KodeCabangTujuan
				FROM tbl_md_jurusan
				WHERE IdJurusan='$id_jurusan'";
		
		if(!$result = $db->sql_query($sql)){
			return 0;
		}		
		
		$row = $db->sql_fetchrow($result);
		return $row[0];
	}
	
	//MENGENAI JADWAL =============================================
	function sendRequestJadwal($tgl_berangkat,$id_jurusan,$jumlah_pesan,$password){
		
		$id_alur	= "3";
		
		$my_signature	= $this->generateSignature($id_alur,$password);
		
		$parameter_dikirim	= "id_alur=$id_alur&tgl_berangkat=$tgl_berangkat&id_jurusan=$id_jurusan&jumlah_pesan=$jumlah_pesan&signature=$my_signature";
		
		return $this->sendHttpPost($this->URL_PARAMETER_RECEIVER,$parameter_dikirim);
	}
	
	function getJadwal($action="",$tgl_berangkat="",$id_jurusan="",$jumlah_pesan=""){
	  global $db;
		
		$tgl_berangkat	= $this->formatTglToMysqlDate($tgl_berangkat);
		
		$kondisi_jurusan	= ($id_jurusan!="")?" AND IdJurusan='$id_jurusan'":"";
		
		$sql = 
			"SELECT KodeJadwal, JamBerangkat,Left(JumlahKursi,2)-1 AS JumlahKursi
			FROM tbl_md_jadwal
			WHERE 
				FlagAktif=1 
				AND timediff(CONCAT('$tgl_berangkat ',JamBerangkat,':00'),NOW())>'00:30:00' 
				$kondisi_jurusan
			ORDER BY JamBerangkat,KodeJadwal";
		
		$idx=0;
		
	  if (!$result = $db->sql_query($sql)){
			return "Silahkan coba kembali beberapa saat lagi...";
		} 
		else{
			if($db->sql_numrows()<=0){
				return "<br>Tidak ada jadwal untuk point keberangkatan ini";
			}
			
			//create array
			$idx=0;
			while ($row = $db->sql_fetchrow($result)){
				
				$data_jadwal[$idx]['KodeJadwal']		= $row['KodeJadwal'];
				$data_jadwal[$idx]['JamBerangkat']	= $row['JamBerangkat'];
				$data_jadwal[$idx]['JumlahKursi']		= $row['JumlahKursi'];
				$data_jadwal[$idx]['SisaKursi']			= $row['JumlahKursi'];
				
				$idx++;
	    } 

			//get sisa kursi
			$sql = 
				"SELECT KodeJadwal, COUNT(1) AS KursiTerisi
				FROM tbl_posisi_detail
				WHERE 
					TglBerangkat='$tgl_berangkat'
					AND StatusKursi=1
				GROUP BY KodeJadwal
				ORDER BY KodeJadwal";
		
		  if (!$result = $db->sql_query($sql)){
				return "Silahkan coba kembali beberapa saat lagi... $sql";
			} 
			else{
				$idx=0;
			
				while ($row = $db->sql_fetchrow($result)){
				
					$data_kursi[$row['KodeJadwal']]['KursiTerisi']	= $row['KursiTerisi'];
					
					$idx++;
		    } 
			}
			
			for($idx=0;$idx<count($data_jadwal);$idx++){
				
				$sisa_kursi	= $data_jadwal[$idx]['JumlahKursi']-$data_kursi[$data_jadwal[$idx]['KodeJadwal']]['KursiTerisi'];
				
				if($sisa_kursi>=$jumlah_pesan){
					$temp_action	= str_replace("#id","\"$data_jadwal[$idx][KodeJadwal]\"",$action);
					$opt .=
						"<div class='menu_pilihan'>
							<a class='menu' href='#' onClick='$temp_action;return false;'>".$data_jadwal[$idx]['JamBerangkat']."</a>
							<span class='noticeMessage'>Sisa Kursi:<b>$sisa_kursi</b></span><br>
							<span class='noticeMessage'>".$data_jadwal[$idx]['KodeJadwal']."</span>
						</div>";
				}
				
	    }    
	  }
		
		$opt	=($opt!="")?$opt:"Tidak ada jadwal yang sesuai"; 
		
		return $opt;
	}

}
?>