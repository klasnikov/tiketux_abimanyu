<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
	//redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################
class RealisasiBiaya
{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas

	//CONSTRUCTOR
	function RealisasiBiaya(){
		$this->ID_FILE="C-RLB";
	}

	function ambilBiayaRelease($no_spj)
	{
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		while($row=$db->sql_fetchrow($result))
		{
			$data_biaya["$row[FlagJenisBiaya]"]+=$row["Jumlah"];
		}
		
		return $data_biaya;
		
	}

	function ambilBiayaRealisasi($no_spj)
	{
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		while($row=$db->sql_fetchrow($result)){
			$data_biaya["$row[FlagJenisBiaya]"]	+=$row["JumlahRealisasi"];
		}
		
		return $data_biaya;
		
	}

}
?> 
