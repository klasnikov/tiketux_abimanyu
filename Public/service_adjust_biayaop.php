<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
chdir("/home/selamat/selamat/");
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassPromo.php');	


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

$pass   = $HTTP_GET_VARS['pass'];
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!="" /*&& $pass!="hanyauntukmuindonesiaku"*/){ 
  echo("DENIED");
	exit;
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination


$tgl_berangkat		= "2014-10-30";

$sql	=
	"SELECT * FROM tbl_spj WHERE tglberangkat='$tgl_berangkat' AND NOT NoSPJ IN(SELECT NoSPJ FROM tbl_biaya_op WHERE DATE(TglTransaksi)='$tgl_berangkat')";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($data_spj	= $db->sql_fetchrow($result)){
	$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($tgl_berangkat,$data_spj['KodeJadwal']);
	
	$no_spj	= $data_spj['NoSPJ'];
	$mobil_dipilih	= $data_spj['NoPolisi'];
	$sopir_dipilih = $data_spj['KodeDriver'];
	$useraktif=$data_spj['CSO'];
	$kode_jadwal_utama=$data_spj['KodeJadwal'];
	
	if($data_biaya['BiayaSopir']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaSopir'],$FLAG_BIAYA_SOPIR,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaSopir'],
			$useraktif,$kode_jadwal_utama,$data_spj['CabangBayarOP']);
		
		$total_biaya += $data_biaya['BiayaSopir'];
	}
	
	//biaya tol
	if($data_biaya['BiayaTol']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaTol'],
			$useraktif,$kode_jadwal_utama,$data_spj['CabangBayarOP']);
		
		$total_biaya += $data_biaya['BiayaTol'];
	}
	
	//biaya parkir
	if($data_biaya['BiayaParkir']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaParkir'],
			$useraktif,$kode_jadwal_utama,$data_spj['CabangBayarOP']);
		
		$total_biaya += $data_biaya['BiayaParkir'];
	}
	
	//biaya bbm
	if($data_biaya['BiayaBBM']>0){
		
		if($data_biaya['IsVoucherBBM']==1){
			$temp_flag_biaya 	= $FLAG_BIAYA_VOUCHER_BBM;
		}
		else{
			$temp_flag_biaya 	= $FLAG_BIAYA_BBM;
			$total_biaya 			+= $data_biaya['BiayaBBM'];
		}
		
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaBBM'],$temp_flag_biaya,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaBBM'],
			$useraktif,$kode_jadwal_utama,$data_spj['CabangBayarOP']);
	}
}