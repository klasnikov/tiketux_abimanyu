<?php
//redirect ke server baru
//header('location:http://202.78.200.77');

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');
	
// SESSION 
$userdata = session_pagestart($user_ip,901); 
init_userprefs($userdata);

if( $userdata['session_logged_in'] )
{  
  //redirect(append_sid('main.'.$phpEx),true);  // user aktif ga usa login, langsung redirect ke main
}

// HEADER
include($adp_root_path . 'includes/page_header.php');

$images_random	= rand(1,15);

function setComboCabang($cabang_dipilih){
		//SET COMBO cabang

		global $db;

		$pencari = "";
		$order_by = "Nama,Kota";
		$asc = "ASC";

		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';

		$sql =
			"SELECT *
			FROM tbl_md_cabang
			WHERE
				KodeCabang LIKE '$pencari'
				OR Nama LIKE '%$pencari%'
				OR Alamat LIKE '%$pencari%'
				OR Telp LIKE '$pencari'
				OR Kota LIKE '$pencari'
			$order;";

		$opt_cabang="";

		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
				$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
			}
		}
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:". __LINE__);
		}

		return $opt_cabang;
		//END SET COMBO CABANG
	}

// TEMPLATE
$template->set_filenames(array('body' => 'main_body.tpl')); 
$template->assign_vars(array(
	'U_LOGIN'						=>append_sid('auth.php'),
	'IMAGE_BACKGROUND'	=>"bg_img".$images_random.".gif",
	'OPT_CABANG'			=>setComboCabang(""),
  'MENU_BLOKIR'       =>isset($HTTP_GET_VARS['menu_blokir'])?1:0
	));

// PARSE
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>