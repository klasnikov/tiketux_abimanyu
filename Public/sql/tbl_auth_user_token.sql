/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : arnes

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-23 19:12:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_auth_user_token
-- ----------------------------
DROP TABLE IF EXISTS `tbl_auth_user_token`;
CREATE TABLE `tbl_auth_user_token` (
  `access_token` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` varchar(150) NOT NULL,
  `access_token_secret` varchar(150) NOT NULL,
  `access_token_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_token_expire` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_auth_user_token
-- ----------------------------
INSERT INTO `tbl_auth_user_token` VALUES ('95669b0dafb52f119df597b735fd9cca056c7f34d', '43', 'ANDROID', '7bd71736cf76c6bd6782bfcb377e000b056c7f34d', '2016-02-20 12:02:05', '2016-02-20 12:02:05');
