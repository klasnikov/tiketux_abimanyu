/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : arnes

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-23 19:13:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_api_log_out
-- ----------------------------
DROP TABLE IF EXISTS `tbl_api_log_out`;
CREATE TABLE `tbl_api_log_out` (
  `out_id` int(11) NOT NULL AUTO_INCREMENT,
  `out_url` text NOT NULL,
  `out_params` text NOT NULL,
  `out_method` varchar(5) NOT NULL,
  `out_response` text NOT NULL,
  `out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
