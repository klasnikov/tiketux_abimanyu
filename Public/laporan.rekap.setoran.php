<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["CSO"],$USER_LEVEL_INDEX["CSO_PAKET"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

// LIST
$template->set_filenames(array('body' => 'laporan.rekap.setoran/index.tpl')); 

$sql=
	"SELECT *
	FROM tbl_user_setoran
	WHERE (DATE(WaktuSetoran) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND IdUser='$userdata[user_id]'";

if (!$result= $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$i=0;
$temp_tgl="";
$odd	= "even";
while($data_setoran = $db->sql_fetchrow($result)){
	$i++;
	
	if($temp_tgl!=substr($data_setoran[''],0,10)){
		$odd	= $odd=="even"?"odd":"even";
	}
	
	$act	=
		"<span class='b_print' onClick=\"Start('".append_sid("laporan.rekap.setoran.struk.php?id=".$data_setoran['IdSetoran'])."');\" title='cetak ulang setoran' >&nbsp;</span>&nbsp;|
		<span class='b_browse' onClick=\"Start('".append_sid("laporan.rekap.setoran.detail.php?id=".$data_setoran['IdSetoran'])."');\" title='detail' >&nbsp;</span>";
		
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'					=> $odd,
				'no'					=> $i,
				'waktu_setor'	=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_setoran['WaktuSetoran'])),
				'no_resi'			=> $data_setoran['IdSetoran'],
				'jum_pnp'			=> number_format($data_setoran['JumlahTiketUmum']+$data_setoran['JumlahTiketDiskon'],0,",","."),
				'omz_pnp'			=> "Rp.".number_format($data_setoran['OmzetPenumpangTunai']+$data_setoran['OmzetPenumpangDebit']+$data_setoran['OmzetPenumpangKredit'],0,",","."),
				'disc_pnp'		=> "Rp".number_format($data_setoran['TotalDiskon'],0,",","."),
				'jum_pkt'			=> number_format($data_setoran['TotalPaket'],0,",","."),
				'omz_pkt'			=> "Rp.".number_format($data_setoran['OmzetPaket'],0,",","."),
				'biaya'				=> "Rp.".number_format($data_setoran['TotalBiaya'],0,",","."),
				'total_setor'	=> "Rp.".number_format($data_setoran['OmzetPenumpangTunai']+$data_setoran['OmzetPenumpangDebit']+$data_setoran['OmzetPenumpangKredit']+$data_setoran['OmzetPaket']-$data_setoran['TotalBiaya'],0,",","."),
				'act'					=> $act
			)
		);
}

//QUERY PENUMPANG
$sql=
	"SELECT 
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPembayaran!=3,Total,0)),0) AS TotalOmzet,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE
		CetakTiket=1 
		AND FlagBatal!=1 
		AND PetugasCetakTiket=$userdata[user_id]
		AND (IdSetoran='' OR IdSetoran IS NULL)";

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}
	
$data_penumpang = $db->sql_fetchrow($result);

//QUERY PAKET
$sql=
	"SELECT 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(TotalBayar),0) AS TotalOmzet
	FROM tbl_paket
	WHERE 
		CetakTiket=1 
		AND FlagBatal!=1
		AND PetugasPenjual=$userdata[user_id]
		AND (IdSetoran='' OR IdSetoran IS NULL)";

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$data_paket = $db->sql_fetchrow($result);

//QUERY CARGO
$sql=
	"SELECT 
		IS_NULL(COUNT(Id),0) AS TotalPaket,
		IS_NULL(SUM(TotalBiaya),0) AS TotalOmzet
	FROM tbl_paket_cargo
	WHERE 
		(DicetakOleh IS NOT NULL OR DicetakOleh!='') 
		AND IsBatal=0
		AND DicetakOleh=$userdata[user_id]
		AND (IdSetoran='' OR IdSetoran IS NULL)";

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$data_cargo = $db->sql_fetchrow($result);

//QUERY BIAYA
$sql=
	"SELECT 
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		IdPetugas=$userdata[user_id]
		AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM' 
		AND (IdSetoran='' OR IdSetoran IS NULL)";

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$data_biaya = $db->sql_fetchrow($result);
			
if($data_penumpang["TotalPenumpang"]>0 || $data_paket["TotalPaket"]>0 || $data_cargo["TotalPaket"]>0 || $data_biaya["TotalBiaya"]>0){
	$i++;
	
	$act	=
		"<span class='b_ok' onClick=\"Start('".append_sid("laporan.rekap.setoran.struk.php")."');\" title='rekap setoran' >&nbsp;</span>&nbsp;|
		<span class='b_browse' onClick=\"Start('".append_sid("laporan.rekap.setoran.detail.php")."');\" title='detail' >&nbsp;</span>";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'					=> "blue",
				'no'					=> $i,
				'waktu_setor'	=> "BELUM SETOR",
				'no_resi'			=> "N/A",
				'jum_pnp'			=> number_format($data_penumpang['TotalPenumpang'],0,",","."),
				'disc_pnp'		=> "Rp.".number_format($data_penumpang['TotalDiscount'],0,",","."),
				'omz_pnp'			=> "Rp.".number_format($data_penumpang['TotalOmzet'],0,",","."),
				'jum_pkt'			=> number_format($data_paket['TotalPaket']+$data_cargo["TotalPaket"],0,",","."),
				'omz_pkt'			=> "Rp.".number_format($data_paket['TotalOmzet']+$data_cargo["TotalOmzet"],0,",","."),
				'biaya'				=> number_format($data_biaya['TotalBiaya'],0,",","."),
				'total_setor'	=> "Rp.".number_format($data_penumpang['TotalOmzet']+$data_paket['TotalOmzet']-$data_biaya['TotalBiaya'],0,",","."),
				'act'					=> $act
			)
		);
}
elseif($i<=0){
	$template->assign_block_vars('NO_DATA',array());
}


//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;												
$script_cetak_excel="Start('laporan_rekap_uang_user_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'URL'						=> append_sid('laporan.rekap.setoran.'.$phpEx).$parameter,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'SUM_PENUMPANG'	=>number_format($sum_penumpang,0,",","."),
	'SUM_OMZET_PENUMPANG'=>number_format($sum_omzet_penumpang,0,",","."),
	'SUM_DISCOUNT'	=>number_format($sum_discount,0,",","."),
	'SUM_PENDAPATAN_PENUMPANG'=>number_format($sum_pendapatan_penumpang,0,",","."),
	'SUM_PAKET'			=>number_format($sum_paket,0,",","."),
	'SUM_OMZET_PAKET'=>number_format($sum_omzet_paket,0,",","."),
	'SUM_ASURANSI'=>number_format($sum_asuransi,0,",","."),
	'SUM_OMZET_ASURANSI'=>number_format($sum_omzet_asuransi,0,",","."),
	'SUM_BIAYA'			=>number_format($sum_biaya,0,",","."),
	'SUM_PROFIT'		=>number_format($sum_profit,0,",","."),
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>