<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kode_cabang    = isset($HTTP_GET_VARS['kode_cabang'])? $HTTP_GET_VARS['kode_cabang'] : $HTTP_POST_VARS['kode_cabang'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);


if($kode_cabang == ""){
    $cabang = $userdata['KodeCabang'];
}else{
    $cabang = $kode_cabang;
}

$sql = "SELECT Nama FROM tbl_md_cabang WHERE KodeCabang = '$cabang';";
if(!$result = $db->sql_query($sql)){
    die_error("Gagal eksekusi query!",__LINE__,"Error Code",mysql_error());
}else{
    $dataCabang = $db->sql_fetchrow($result);
}

$kondisi =	$cari==""?"":
    " AND (KodeCabang LIKE '$cari%'
				OR NamaPetugas LIKE '$cari%' 
				OR Penerima LIKE '%$cari%'
				OR Keterangan LIKE '%$cari%' 
				OR Jumlah LIKE '%$cari%'
				OR JenisBiaya LIKE '%$cari%')";

$sql	= "SELECT *, f_cabang_get_name_by_kode(KodeCabang) AS CABANG FROM tbl_biaya_harian WHERE KodeCabang = '$cabang' AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi ORDER BY TglTransaksi DESC;";

if(!$result = $db->sql_query($sql)){
    die_error("Gagal eksekusi query!",__LINE__,"Error Code",mysql_error());
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Biaya Harian Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir)." Cabang ".$dataCabang[0]);

$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Petugas');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Penerima');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Jenis Biaya');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Jumlah');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

$idx=0;

while ($row = $db->sql_fetchrow($result)){
    $idx++;
    $idx_row=$idx+4;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglTransaksi'])));
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NamaPetugas']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Penerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['JenisBiaya']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Jumlah']);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Keterangan']);
}

$temp_idx=$idx_row;

$idx_row++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':E'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row,'=SUM(F4:F'.$temp_idx.')');

if ($idx>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Biaya Harian Per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).' Cabang '.$dataCabang[0].'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}