<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJadwal.php');


$Cabang	= new Cabang();
$Jadwal = new Jadwal();

//METHODS

// PARAMETER
$id_jurusan		= isset($HTTP_GET_VARS['idjurusan'])? $HTTP_GET_VARS['idjurusan'] : $HTTP_POST_VARS['idjurusan'];
$tanggal  		= isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl'];
$col			= isset($HTTP_GET_VARS['col'])? $HTTP_GET_VARS['col'] : $HTTP_POST_VARS['col'];


$template->set_filenames(array("body" => "bookingops/detail.tpl")); 

$tanggal			= ($tanggal!='')?$tanggal:dateD_M_Y();
$tanggal_mysql= FormatTglToMySQLDate($tanggal);
//======= INI PERCOBAAN ========//
	//MENGAMBIL ID JURUSAN DARI KODE JADWAL UTAMA
	$sql = "SELECT KodeJadwal, KodeJadwalUtama FROM tbl_md_jadwal WHERE IdJurusan = $id_jurusan";


//======= end INI PERCOBAAN ========//


//MENGAMBIL JUMLAH PENUMPANG BERANGKAT
$sql_sub 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)
		AND CetakTiket = 0
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";

/*$sql_sub2 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket = 0
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";*/

$sql_sub3 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)
		AND CetakTiket=1
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";

/*$sql_sub4 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket=1
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";*/
		
$sql	=
	"SELECT 
		KodeJadwal,
		IS_NULL(COUNT(IF(CetakTiket=0,1,NULL)),0) AS PenumpangBook,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS PenumpangConfirm,
		$sql_sub AS BookTransit1,
		/*$sql_sub2 AS BookTransit2,*/
		$sql_sub3 AS ConfirmTransit1
		/*$sql_sub4 AS ConfirmTransit2*/
	FROM tbl_reservasi tr1
	WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1
	AND  IdJurusan=$id_jurusan
	GROUP BY KodeJadwal;";


if(!$result = $db->sql_query($sql)){
	echo("Err:$sql".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_penumpang[$row["KodeJadwal"]]	= $row;
}

//KEBERANGKATAN DARI CABANG BERANGKAT
$sql=
	"SELECT
		IdPenjadwalan,tmj.KodeJadwal,tmj.KodeJadwalUtama,tmj.JamBerangkat,IdPenjadwalan,tpk.StatusAktif,
		tmj.JumlahKursi,LayoutKursi,NoSPJ,
		IS_NULL(tpk.StatusAktif,1) AS StatusPenjadwalan,FlagSubJadwal
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tanggal_mysql'
	LEFT JOIN tbl_spj spj ON spj.KodeJadwal = tmj.KodeJadwal AND spj.TglBerangkat = '$tanggal_mysql'
	WHERE tmj.IdJurusan=$id_jurusan AND (FlagAktif=1 OR tpk.StatusAktif=1) 
	ORDER BY tmj.JamBerangkat";
	
if(!$result = $db->sql_query($sql)){
	//die($sql);
	echo("Err:".__LINE__);exit;
}

$i=0;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$keterangan	= "<font style='color:green;'>BUKA</font>";
	$act = '<a href="#" onclick="setDialogSPJ(\''.$tanggal_mysql.'\',\''.$row['KodeJadwal'].'\',\'\')">Cetak Manifest</a>';
	if($row['StatusPenjadwalan']==0){
		$odd				= "red";
		$keterangan	= "<b>TUTUP</b>";
		$act 		= '';
	}

	if($row['NoSPJ'] != ''){
		$act = '<a href="#" onclick="setDialogSPJ(\''.$tanggal_mysql.'\',\''.$row['KodeJadwal'].'\',\''.$row['NoSPJ'].'\')">Cetak Ulang Manifest</a>';
	}

	$total_conf	= $data_penumpang[$row["KodeJadwal"]]['ConfirmTransit1'];
	$total_conf_transit = $data_penumpang[$row["KodeJadwal"]]['ConfirmTransit1']+$data_penumpang[$row["KodeJadwal"]]['ConfirmTransit2'];
	$total_book	= $data_penumpang[$row["KodeJadwal"]]['BookTransit1'];
	$total_book_transit = $data_penumpang[$row["KodeJadwal"]]['BookTransit1']+$data_penumpang[$row["KodeJadwal"]]['BookTransit2'];
	$sisa_kursi		= $row["IdPenjadwalan"]==""?$row["JumlahKursi"]-($total_book+$total_conf):$row["LayoutKursi"]-($total_book+$total_conf);
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'		=> ($sisa_kursi>2?$odd:"bookopshighlight"),
				'alert'		=> ($sisa_kursi>2?"":"18px"),
				'time'		=> $row['JamBerangkat'],
				'b'			=> $total_book,
				'b_transit'	=> $total_book_transit,
				'c'			=> $total_conf,
				'c_transit'	=> $total_conf_transit,
				'available'	=> $sisa_kursi,
				'keterangan'=> $keterangan,
				'act'=> $act
			)
		);
		
		$i++;
}

//MENGAMBIL NAMA CABANG TUJUAN
$sql = 
	"SELECT 
		f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabang,
		KodeCabangAsal,KodeCabangTujuan
	FROM tbl_md_jurusan WHERE IdJurusan=$id_jurusan";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);
$tujuan 	= $row[0];

$template->assign_vars(array(	'TUJUAN'=> $tujuan));

$template->pparse('body');
?>