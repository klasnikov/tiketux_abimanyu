<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Cargo{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Cargo(){
		$this->ID_FILE="C-CGO";
	}
	
	//BODY
	
	function ambilDataProvinsi($kondisi=""){
		
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_md_provinsi ".($kondisi==""?"":"WHERE $kondisi");
			
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $result;
		
	}//  END ambilDataProvinsi
	
	function ambilDataKota($kondisi=""){
		
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_md_kota ".($kondisi==""?"":"WHERE $kondisi");
			
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $result;
		
	}//  END ambilDataProvinsi
	
	function ambilHarga($kota_asal,$kota_tujuan){
		
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_md_harga
			WHERE KodeAsal='$kota_asal' AND KodeTujuan='$kota_tujuan';";
			
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row	= $db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilHarga
	
	function generateNoAWB(){
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		
		return "C".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
	}
	
	function getNoAWB(){
		global $db;
		
		do{
			$no_awb	= $this->generateNoAWB();
			
			$sql	= "SELECT COUNT(1) FROM tbl_paket_cargo WHERE NoAWB='$no_awb';";
			
			$result	= $db->sql_query($sql);
			
			$row	= $db->sql_fetchrow($result);
		}while($row[0]>0);
		
		return $no_awb;
		
	}
	
	function tambah(
		$NoAWB,
		$NamaPengirim, $TelpPengirim,$AlamatPengirim,
		$NamaPenerima, $TelpPenerima, $AlamatPenerima,
		$TglDiterima, $KodeAsal, $Asal,
		$KodeTujuan, $Tujuan, $JenisBarang,
		$Via, $Layanan, $Koli,
		$IsVolumetric,$DimensiPanjang,$DimensiLebar,$DimensiTinggi,
		$Berat, $LeadTime,$BiayaKirim,
		$BiayaTambahan, $BiayaPacking, $IsAsuransi,
		$HargaDiakui,$BiayaAsuransi,$TotalBiaya,
		$IsTunai,$DeskripsiBarang,$DicatatOleh,
		$NamaPencatat,$DicetakOleh,$NamaPencetak,
		$PoinPickedUp,$NamaPoinPickedUp){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql=
			"INSERT INTO tbl_paket_cargo(
				NoAWB,
				NamaPengirim, TelpPengirim,AlamatPengirim,
				NamaPenerima, TelpPenerima, AlamatPenerima,
				TglDiterima, KodeAsal, Asal,
				KodeTujuan, Tujuan, JenisBarang,
				Via, Layanan, Koli,
				IsVolumetric,DimensiPanjang,DimensiLebar,DimensiTinggi,
				Berat, LeadTime,BiayaKirim,
				BiayaTambahan, BiayaPacking, IsAsuransi,
				HargaDiakui,BiayaAsuransi,TotalBiaya,
				IsTunai,DeskripsiBarang,WaktuCatat,
				DicatatOleh,NamaPencatat,WaktuCetakAWB,
				DicetakOleh,NamaPencetak,PoinPickedUp,
				NamaPoinPickedUp)
			VALUES(
				'$NoAWB',
				'$NamaPengirim', '$TelpPengirim','$AlamatPengirim',
				'$NamaPenerima', '$TelpPenerima', '$AlamatPenerima',
				'$TglDiterima', '$KodeAsal', '$Asal',
				'$KodeTujuan', '$Tujuan', '$JenisBarang',
				'$Via', '$Layanan', '$Koli',
				'$IsVolumetric','$DimensiPanjang','$DimensiLebar','$DimensiTinggi',
				'$Berat', '$LeadTime','$BiayaKirim',
				'$BiayaTambahan', '$BiayaPacking', '$IsAsuransi',
				'$HargaDiakui','$BiayaAsuransi','$TotalBiaya',
				'$IsTunai','$DeskripsiBarang',NOW(),
				'$DicatatOleh','$NamaPencatat',NOW(),
				'$DicetakOleh','$NamaPencetak','$PoinPickedUp',
				'$NamaPoinPickedUp');";
		
		
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE $sql".__LINE__);
		}
		
		return true;
	}
	
	function ambilListCargo($TglDiterima,$PoinPickedUp){
		global $db;
				
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo
			WHERE TglDiterima='$TglDiterima' AND PoinPickedUp='$PoinPickedUp' AND IsBatal=0";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Error Query",__LINE__,$this->ID_FILE,"");
		}
		
		return $result;
	}
	
	function ambilDataManifestByTglPoin($TglDiterima,$PoinPickedUp){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
				
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_spj
			WHERE TglDiterima='$TglDiterima' AND PoinPickedUp='$PoinPickedUp'
			ORDER BY WaktuCetak DESC
			LIMIT 0,1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Error Query",__LINE__,$this->ID_FILE,"");
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilDataManifest
	
	function ambilDataManifestByNo($NoSPJ){
		
		//kamus
		global $db;
				
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_spj
			WHERE NoSPJ='$NoSPJ'
			ORDER BY WaktuCetak DESC
			LIMIT 0,1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Error Query",__LINE__,$this->ID_FILE,"");
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilDataManifestByNo
	
	function ambilDataManifestById($Id){
		
		//kamus
		global $db;
				
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo_spj
			WHERE Id='$Id'
			ORDER BY WaktuCetak DESC
			LIMIT 0,1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Error Query",__LINE__,$this->ID_FILE,"");
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilDataManifestByNo
	
	function ambilDataDetailById($id){
		
		/*
		Desc	:Mengembalikan data paket sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo
			WHERE Id='$id';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataDetailByAWB($awb){
		
		/*
		Desc	:Mengembalikan data paket sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_paket_cargo
			WHERE NoAWB='$awb';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDataDetailByAWB
	
	function batal($Id,$PetugasBatal,$NamaPetugasBatal){
		
		//kamus
		global $db;
	
		$sql = 
			"UPDATE tbl_paket_cargo
			SET IsBatal=1,PetugasBatal='$PetugasBatal',NamaPetugasBatal='$NamaPetugasBatal'
			WHERE Id='$Id';";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE" .__LINE__);
		}
		
		return true;
	}
	
	function mutasi(
		$Id,
		$TglDiterima,$PoinPickedUp,$NamaPoinPickedUp,
		$PetugasMutasi,$NamaPetugasMutasi){
	  
		//kamus
		global $db;
		
		$data_cargo	= $this->ambilDataDetailById($Id);
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql=
			"UPDATE tbl_paket_cargo SET
				TglDiterima='$TglDiterima',
				PoinPickedUp='$PoinPickedUp',
				NamaPoinPickedUp='$NamaPoinPickedUp',
				IsMutasi=1,
				TglDiterimaLama='$data_cargo[TglDiterima]',
				PoinPickedUpLama='$data_cargo[PoinPickedUp]',
				NamaPoinPickedUpLama='$data_cargo[NamaPoinPickedUp]',
				WaktuMutasi=NOW(),
				PetugasMutasi='$PetugasMutasi',
				NamaPetugasMutasi='$NamaPetugasMutasi',
				LogMutasi=CONCAT(IS_NULL(LogMutasi,''),'# $data_cargo[TglDiterima]|$data_cargo[PoinPickedUp]|$data_cargo[NamaPoinPickedUp]|$PetugasMutasi|$NamaPetugasMutasi|',NOW())
			WHERE Id='$Id';";
		
		
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function tambahSPJ(
		$NoSPJ,$TglDiterima,$PoinPickedUp,
		$NamaPoinPickedUp,$PetugasCetak,$NamaPetugasCetak,
		$BodyUnit,$Driver,$NamaDriver,
		$JumlahTransaksi,$JumlahKoli,$TotalOmzet,
		$TotalBerat,$PetugasPickedUp,$NamaPetugasPickedUp){
	  
		/*
		IS	: data spj belum ada dalam database
		FS	:Data lspj baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql	=
			"INSERT INTO tbl_paket_cargo_spj
				(NoSPJ,TglDiterima,PoinPickedUp,
				NamaPoinPickedUp,WaktuPickedUp,PetugasPemberi,
				NamaPetugasPemberi,WaktuCetak,PetugasCetak,
				NamaPetugasCetak,BodyUnit,Driver,
				NamaDriver,JumlahTransaksi,JumlahKoli,
				TotalOmzet,TotalBerat,PetugasPickedUp,
				NamaPetugasPickedUp)
			VALUES(
				'$NoSPJ','$TglDiterima','$PoinPickedUp',
				'$NamaPoinPickedUp',NOW(),'$PetugasCetak',
				'$NamaPetugasCetak',NOW(),'$PetugasCetak',
				'$NamaPetugasCetak','$BodyUnit','$Driver',
				'$NamaDriver','$JumlahTransaksi','$JumlahKoli',
				'$TotalOmzet','$TotalBerat','$PetugasPickedUp',
				'$NamaPetugasPickedUp');";
				
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		$sql	=
			"UPDATE tbl_paket_cargo SET
				NoSPJ='$NoSPJ',
				IsPickedUp=1,
				WaktuPickedUp=NOW(),
				PetugasPemberi='$PetugasCetak',
				NamaPetugasPemberi='$NamaPetugasCetak',
				PetugasPenerima='$PetugasPickedUp',
				NamaPetugasPenerima='$NamaPetugasPickedUp'
			WHERE TglDiterima='$TglDiterima' AND PoinPickedUp='$PoinPickedUp' AND IsBatal=0;";
				
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function cetakUlangSPJ(
		$Id,$PetugasCetak,$NamaPetugasCetak,
		$BodyUnit,$Driver,$NamaDriver,
		$JumlahTransaksi,$JumlahKoli,$TotalOmzet,
		$TotalBerat,$PetugasPickedUp,$NamaPetugasPickedUp){
	  
		/*
		IS	: data spj belum ada dalam database
		FS	:Data lspj baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql	=
			"UPDATE tbl_paket_cargo_spj SET
				WaktuPickedUp=NOW(),
				PetugasPemberi='$PetugasCetak',
				NamaPetugasPemberi='$NamaPetugasCetak',
				BodyUnit='$BodyUnit',
				Driver='$Driver',
				NamaDriver='$NamaDriver',
				JumlahTransaksi='$JumlahTransaksi',
				JumlahKoli='$JumlahKoli',
				TotalOmzet='$TotalOmzet',
				TotalBerat='$TotalBerat',
				PetugasPickedUp='$PetugasPickedUp',
				NamaPetugasPickedUp='$NamaPetugasPickedUp',
				LogCetak=CONCAT(IF(LogCetak IS NULL,'',LogCetak),'#',NOW(),'$PetugasCetak|$NamaPetugasCetak|$BodyUnit|$Driver|$NamaDriver|$JumlahTransaksi|$JumlahKoli|$TotalOmzet|$TotalBerat|$PetugasPickedUp|$NamaPetugasPickedUp')
			WHERE Id='$Id';";
				
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		$data_manifest	= $this->ambilDataManifestById($Id);
		
		$sql	=
			"UPDATE tbl_paket_cargo SET
				NoSPJ='$data_manifest[NoSPJ]',
				IsPickedUp=1,
				WaktuPickedUp=NOW(),
				PetugasPemberi='$PetugasCetak',
				NamaPetugasPemberi='$NamaPetugasCetak',
				PetugasPenerima='$PetugasPickedUp',
				NamaPetugasPenerima='$NamaPetugasPickedUp'
			WHERE TglDiterima='$data_manifest[TglDiterima]' AND PoinPickedUp='$data_manifest[PoinPickedUp]' AND IsBatal=0;";
				
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
}
?>