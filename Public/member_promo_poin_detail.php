<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberPromoTukarPoint.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$act    		= $HTTP_GET_VARS['act'];
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

$PromoPoin	=	new PromoPoin(); 	

switch($mode){
//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':
	
	$pesan="Penambahan";
	
	$template->set_filenames(array('body' => 'member_promo_poin_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  					=>$userdata['username'],
	   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo_poin.'.$phpEx).'">Promo Poin</a>',
			'PESAN'								=>$pesan
		));
break;

//TAMBAH MEMBER BARU ==========================================================================================================
case 'tambah_promo':
	
	//ambil isi dari parameter
	
	$kode_promo						=$HTTP_GET_VARS['kode_promo'];
	$nama_promo_poin			=$HTTP_GET_VARS['nama_promo_poin'];
	$poin									=$HTTP_GET_VARS['poin'];
	
	
	$return = $PromoPoin->tambah(
		$kode_promo, $nama_promo_poin, $poin);
	
	$return	= ($return)?"berhasil":"gagal";
	
	echo $return;
	
exit;

//UBAH MEMBER BARU ==========================================================================================================
case 'ubah_promo':
	
	//ambil isi dari inputan
	$id_promo_poin				=$HTTP_GET_VARS['id_promo_poin'];
	$nama_promo_poin			=$HTTP_GET_VARS['nama_promo_poin'];
	$poin									=$HTTP_GET_VARS['poin'];
	
	
	$return = $PromoPoin ->ubah($id_promo_poin,$nama_promo_poin, $poin);
	
	$return	= ($return)?"berhasil":"gagal";
	
	echo $return;
	
exit;

//AMBIL DATA MEMBER==========================================================================================================
case 'ambil_data_promo':
	
	$id_promo_poin    = $HTTP_GET_VARS['id_promo_poin'];
	
	$row	= $PromoPoin->ambilDataDetail($id_promo_poin);
	
	$id_promo_poin				=trim($row['id_promo_poin']);
	$kode_promo						=trim($row['kode_promo']);
	$nama_promo_poin			=trim($row['nama_penukaran_poin']);
	$poin									=trim($row['poin']);
	$dibuat_oleh					=trim($row['dibuat_oleh']);
	$waktu_dibuat					=trim($row['waktu_dibuat']);
	$diubah_oleh					=trim($row['diubah_oleh']);
	$waktu_diubah					=trim($row['waktu_diubah']);
	
	//memeriksa apakah data member dengan id_member ditemukan dalam database
	if($kode_promo==""){
		//jika tidak ditemukan, akan langsung didirect ke halaman member
		redirect(append_sid('member_promo_poin.'.$phpEx)."&pesan=$pesan",true); 
	}
	
	$pesan="Pengubahan";
	
	$template->set_filenames(array('body' => 'member_promo_poin_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  					=>$userdata['username'],
	   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo_poin.'.$phpEx).'">Promo Poin</a>',
			'ID_PROMO_POIN'				=>$id_promo_poin,
			'KODE_PROMO'					=>$kode_promo,
			'NAMA_PROMO_POIN'			=>$nama_promo_poin,
			'POIN'								=>$poin,
			'DIBUAT_OLEH'					=>$dibuat_oleh,					
			'WAKTU_DIBUAT'				=>$waktu_dibuat,
			'DIUBAH_OLEH'					=>$diubah_oleh,					
			'WAKTU_DIUBAH'				=>$waktu_diubah,
			'PESAN'								=>$pesan
  ));
	
break;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>